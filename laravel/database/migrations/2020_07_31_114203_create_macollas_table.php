<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMacollasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('macollas', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('camp_id')->unsigned()->index();
            $table->string('macolla_name')->nullable();
            $table->timestamps();
            $table->foreign('camp_id')
                	->references('id')
                	->on('camps')
                	->onDelete('cascade')
                	->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('macollas');
    }
}
