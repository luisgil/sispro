<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlanSurveysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('plan_surveys', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('well_id')->unsigned()->index();
            $table->date('ps_date')->nullable();
            $table->double('md')->nullable();
            $table->double('inclination')->nullable();
            $table->double('azim_grid')->nullable();
            $table->double('tvd')->nullable();
            $table->double('vsec')->nullable();
            $table->double('ns')->nullable();
            $table->double('ew')->nullable();
            $table->double('dls')->nullable();
            $table->double('la_g')->nullable();
            $table->double('la_m')->nullable();
            $table->double('la_s')->nullable();
            $table->double('lo_g')->nullable();
            $table->double('lo_m')->nullable();
            $table->double('lo_s')->nullable();
            $table->string('observations')->nullable();
            $table->string('identifier');
            $table->timestamps();
            $table->foreign('well_id')
                	->references('id')
                	->on('wells')
                	->onDelete('cascade')
                	->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('plan_surveys');
    }
}
