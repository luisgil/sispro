<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOriginalPressuresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('original_pressures', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('well_id')->unsigned()->index();
            $table->string('reservoir');
            $table->date('pressure_date')->nullable();
            $table->string('type_test')->nullable();
            $table->string('sensor_model')->nullable();
            $table->double('pressure_gradient')->nullable();
            $table->double('deepdatum_mc_md')->nullable();
            $table->double('deepdatum_mc_tvd')->nullable();
            $table->double('press_datum_macolla')->nullable();
            $table->double('temp_datum_macolla')->nullable();
            $table->double('temperature_gradient')->nullable();
            $table->string('identifier');
            $table->timestamps();
            $table->foreign('well_id')
                	->references('id')
                	->on('wells')
                	->onDelete('cascade')
                	->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('original_pressures');
    }
}
