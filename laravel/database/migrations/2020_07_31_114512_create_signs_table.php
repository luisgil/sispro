<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSignsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('signs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('well_id')->unsigned()->index();
            $table->date('sign_date')->nullable();
            $table->string('company');
            $table->time('sign_time')->nullable();
            $table->double('percent_mixture')->nullable();
            $table->double('percent_sediment')->nullable();
            $table->double('api_degree_diluted')->nullable();
            $table->double('ptb')->nullable();
            $table->string('identifier');
            $table->timestamps();
            $table->foreign('well_id')
                	->references('id')
                	->on('wells')
                	->onDelete('cascade')
                	->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('signs');
    }
}
