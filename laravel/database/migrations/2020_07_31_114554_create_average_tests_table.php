<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAverageTestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('average_tests', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('well_id')->unsigned()->index();
            $table->date('avg_start_test')->nullable();
            $table->date('avg_end_test')->nullable();
            $table->string('company');
            $table->integer('rpm')->nullable();
            $table->integer('duration')->nullable();
            $table->double('line_pressure')->nullable();
            $table->double('line_temperature')->nullable();
            $table->double('gas_rate_prod')->nullable();
            $table->double('water_rate_prod')->nullable();
            $table->double('fluid_total_rate')->nullable();
            $table->double('percent_mixture')->nullable();
            $table->double('api_degree_mixture')->nullable();
            $table->double('neat_rate_prod')->nullable();
            $table->double('percent_formation')->nullable();
            $table->double('gvf')->nullable();
            $table->double('inyected_diluent_rate')->nullable();
            $table->double('api_degree_diluent')->nullable();
            $table->string('test')->nullable();
            $table->integer('pip')->nullable();
            $table->string('observations');
            $table->string('identifier');
            $table->timestamps();
            $table->foreign('well_id')
                	->references('id')
                	->on('wells')
                	->onDelete('cascade')
                	->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('average_tests');
    }
}
