<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHistoricalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('historicals', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('well_id')->unsigned()->index();
            $table->string('reservoir');
            $table->datetime('begin_date')->nullable();
            $table->datetime('end_date')->nullable();
            $table->string('type_work_i');
            $table->string('type_work_ii');
            $table->string('rig');
            $table->string('sensor_brand');
            $table->string('sensor_model');
            $table->double('sensor_diameter')->nullable();
            $table->string('sensor_company');
            $table->double('deeptop_sensor_md')->nullable();
            $table->double('deepbottom_sensor_md')->nullable();
            $table->string('pump_model');
            $table->double('pump_diameter')->nullable();
            $table->string('pump_type');
            $table->double('pump_capacity')->nullable();
            $table->string('pump_company');
            $table->double('deeptop_pump_md')->nullable();
            $table->double('deepbottom_pump_md')->nullable();
            $table->string('inyected_diluent');
            $table->string('measure_from');
            $table->string('observations');
            $table->string('references');
            $table->string('identifier');
            $table->timestamps();
            $table->foreign('well_id')
                	->references('id')
                	->on('wells')
                	->onDelete('cascade')
                	->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('historicals');
    }
}
