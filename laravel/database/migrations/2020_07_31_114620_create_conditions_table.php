<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConditionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('conditions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('well_id')->unsigned()->index();
            $table->string('reservoir');
            $table->string('sand');
            $table->string('company');
            $table->date('pt_date')->nullable();
            $table->string('test');
            $table->double('deep_md')->nullable();
            $table->double('deep_tvd')->nullable();
            $table->double('temperature')->nullable();
            $table->integer('duration')->nullable();
            $table->double('pressbefore')->nullable();
            $table->double('pressafter')->nullable();
            $table->double('pressformation')->nullable();
            $table->double('mobility')->nullable();
            $table->string('observations')->nullable();
            $table->string('identifier');
            $table->timestamps();
            $table->foreign('well_id')
                	->references('id')
                	->on('wells')
                	->onDelete('cascade')
                	->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('conditions');
    }
}
