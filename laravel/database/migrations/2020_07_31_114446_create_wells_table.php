<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWellsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wells', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('reservoir_id')->unsigned()->index();
            $table->integer('macolla_id')->unsigned()->index();
            $table->integer('location_id')->unsigned()->index();
            $table->string('uwi')->nullable();
            $table->string('well_name');
            $table->string('type')->nullable();
            $table->enum('classification', ['PRODUCTOR', 'ESTRATIGRÁFICO', 'OBSERVADOR'])->nullable();
            $table->enum('condition', ['ACTIVO', 'ABANDONADO', 'NO COMPLETADO', 'EN PERFORACIÓN']);
            $table->string('drill')->nullable();
            $table->double('gl')->nullable();
            $table->double('rt')->nullable();
            $table->double('height');
            $table->double('ppn');
            $table->timestamps();
            $table->foreign('reservoir_id')
                	->references('id')
                	->on('reservoirs')
                	->onDelete('cascade')
                	->onUpdate('cascade');
            $table->foreign('macolla_id')
                	->references('id')
                	->on('macollas')
                	->onDelete('cascade')
                	->onUpdate('cascade');
            $table->foreign('location_id')
                	->references('id')
                	->on('locations')
                	->onDelete('cascade')
                	->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wells');
    }
}
