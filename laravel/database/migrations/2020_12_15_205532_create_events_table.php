<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('events', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('well_id')->unsigned()->index();
            $table->string('reservoir');
            $table->date('event_date')->nullable();
            $table->time('event_time')->nullable();
            $table->integer('diluent')->nullable();
            $table->integer('rpm')->nullable();
            $table->double('pump_efficiency')->nullable();
            $table->double('torque')->nullable();
            $table->double('frequency')->nullable();
            $table->double('current')->nullable();
            $table->double('power')->nullable();
            $table->double('mains_voltage')->nullable();
            $table->double('output_voltage')->nullable();
            $table->double('vfd_temperature')->nullable();
            $table->double('head_temperature')->nullable();
            $table->double('head_pressure')->nullable();
            $table->string('observations')->nullable();
            $table->string('identifier')->nullable();
            $table->timestamps();
            $table->foreign('well_id')
                    ->references('id')
                    ->on('wells')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('events');
    }
}
