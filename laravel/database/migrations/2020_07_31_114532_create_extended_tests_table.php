<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExtendedTestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('extended_tests', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('well_id')->unsigned()->index();
            $table->date('extended_date');
            $table->string('company');
            $table->string('test');
            $table->time('hour_start')->nullable();
            $table->time('hour_end')->nullable();
            $table->double('line_pressure')->nullable();
            $table->double('line_temperature')->nullable();
            $table->double('water_rate_prod')->nullable();
            $table->double('gas_rate_prod')->nullable();
            $table->double('total_barrels')->nullable();
            $table->double('neat_rate')->nullable();
            $table->double('gvf')->nullable();
            $table->double('inyected_diluent_rate')->nullable();
            $table->string('identifier');
            $table->timestamps();
            $table->foreign('well_id')
                ->references('id')
                ->on('wells')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('extended_tests');
    }
}
