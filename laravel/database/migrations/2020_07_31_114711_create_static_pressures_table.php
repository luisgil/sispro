<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStaticPressuresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('static_pressures', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('well_id')->unsigned()->index();
            $table->datetime('static_date')->nullable();
            $table->string('quality')->nullable();
            $table->double('pip')->nullable();
            $table->double('pdp')->nullable();
            $table->double('temperature')->nullable();
            $table->string('observations')->nullable();
            $table->string('identifier');
            $table->timestamps();
            $table->foreign('well_id')
                	->references('id')
                	->on('wells')
                	->onDelete('cascade')
                	->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('static_pressures');
    }
}
