<?php

use Illuminate\Database\Seeder;
use App\Block;

class BlockTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      if(Block::count() == 0) 
    	{
        factory(App\Block::class, 1)->create([
            'block_name' => 'Junín 6',
        ]);
      }
    }
}
