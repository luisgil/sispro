<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET foreign_key_checks = 0');
    		
        $this->call(UsersTableSeeder::class);
        $this->call(ProfilesTableSeeder::class);
        $this->call(BlockTableSeeder::class);
        $this->call(CampsTableSeeder::class);
        $this->call(MacollasTableSeeder::class);
        $this->call(LocationsTableSeeder::class);
        $this->call(ReservoirTableSeeder::class);
        $this->call(WellsTableSeeder::class);
    }
}
