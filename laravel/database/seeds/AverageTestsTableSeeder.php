<?php

use Illuminate\Database\Seeder;
use JeroenZwart\CsvSeeder\CsvSeeder;

class AverageTestsTableSeeder extends CsvSeeder
{
		public function __construct()
		{		
				$this->delimiter = ';';
				$this->file = '/database/seeds/csvs/average/average_tests-EB1.csv';
				$this->tablename = 'average_tests';
				$this->truncate = false;
		}
		
    public function run()
    {
    		parent::run();
    }
}
