<?php

use Illuminate\Database\Seeder;
use JeroenZwart\CsvSeeder\CsvSeeder;
use App\Macolla;
use App\Camp;

class MacollasTableSeeder extends CsvSeeder
{
		public function __construct()
		{		
				$this->delimiter = ';';
				$this->file = '/database/seeds/csvs/macollas.csv';
				$this->tablename = 'macollas';
		}
		
    public function run()
    {
    		if(Macolla::count() == 0)
    		{
        	parent::run();
      	}
    }
}
