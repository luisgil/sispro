<?php

use Illuminate\Database\Seeder;
use App\Reservoir;

class ReservoirTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if(Reservoir::count() == 0)  {
        	factory(Reservoir::class, 1)->create([
				'code' => '4DJ001362P',
            	'reservoir_name' => 'OFIIB SDZ 2',
            	'api_degree' => 8.5,
            	'hydrocarbon' => 'XP'
        	]);
        	
        	factory(Reservoir::class, 1)->create([
				'code' => '4EB000363P',
            	'reservoir_name' => 'OFIIA SDZ 2',
            	'api_degree' => 8.5,
            	'hydrocarbon' => 'XP'
        	]);
        	
        	factory(Reservoir::class, 1)->create([
				'code' => '4EB000306P',
            	'reservoir_name' => 'MER SDZ 2',
            	'api_degree' => 8.5,
            	'hydrocarbon' => 'XP'
        	]);
        	
        	factory(Reservoir::class, 1)->create([
				'code' => '000000000P',
            	'reservoir_name' => 'N/A',
            	'api_degree' => 0,
            	'hydrocarbon' => 'XP'
        	]);
      	}
    }
}
