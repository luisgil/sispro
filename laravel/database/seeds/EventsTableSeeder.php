<?php

use Illuminate\Database\Seeder;
use JeroenZwart\CsvSeeder\CsvSeeder;
use App\Event;

class EventsTableSeeder extends CsvSeeder
{
    public function __construct()
    {		
        $this->delimiter = ';';
        $this->file = '/database/seeds/csvs/events/events-GG1-02.csv';
        $this->tablename = 'events';
    }

    public function run()
    {
        if(Event::count() == 0) 
        {
            parent::run();
        }
    }
}
