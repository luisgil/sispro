<?php

use Illuminate\Database\Seeder;
use JeroenZwart\CsvSeeder\CsvSeeder;
use App\Production;

class ProductionTableSeeder extends CsvSeeder
{
    public function __construct()
		{		
				$this->delimiter = ';';
				$this->file = '/database/seeds/csvs/production/prod-DB2.csv';
				$this->tablename = 'productions';
				$this->truncate = false;
		}
		
    public function run()
    {
    		//if(Production::count() == 0) 
    		//{
        	parent::run();
      	//}
    }
}
