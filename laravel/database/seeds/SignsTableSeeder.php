<?php

use Illuminate\Database\Seeder;
use JeroenZwart\CsvSeeder\CsvSeeder;

class SignsTableSeeder extends CsvSeeder
{
    public function __construct()
		{		
				$this->delimiter = ';';
				$this->file = '/database/seeds/csvs/sign/signs-DB1-01.csv';
				$this->tablename = 'signs';
				$this->truncate = false;
		}
		
    public function run()
    {
        parent::run();
    }
}
