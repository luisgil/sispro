<?php

use Illuminate\Database\Seeder;
use JeroenZwart\CsvSeeder\CsvSeeder;
use App\{Macolla, Location};

class LocationsTableSeeder extends CsvSeeder
{
    public function __construct()
		{		
				$this->delimiter = ';';
				$this->file = '/database/seeds/csvs/locations.csv';
				$this->tablename = 'locations';
		}
		
    public function run()
    {
    		if(Location::count() == 0) 
    		{
        	parent::run();
      	}
    }
}
