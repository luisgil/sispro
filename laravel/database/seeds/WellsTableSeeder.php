<?php

use Illuminate\Database\Seeder;
use JeroenZwart\CsvSeeder\CsvSeeder;
use App\{Camp, Macolla, Well};

class WellsTableSeeder extends CsvSeeder
{
		public function __construct()
		{		
				$this->delimiter = ';';
				$this->file = '/database/seeds/csvs/wells.csv';
				$this->tablename = 'wells';
		}
		
    public function run()
    {
        if(Well::count() == 0) 
    		{
        	parent::run();
      	}
    }
}
