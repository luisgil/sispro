<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	if(User::count() == 0) 
    	{
        factory(User::class, 1)->create([
            'email' => 'luisgil@mail.com',
            'password'  => bcrypt('changeme')
        ]);
        
        factory(User::class, 1)->create([
            'email' => 'manolo.r@mail.com',
            'password'  => bcrypt('secret')
        ]);
      }
    }
}
