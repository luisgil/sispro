<?php

use Illuminate\Database\Seeder;
use App\Profile;

class ProfilesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	if(Profile::count() == 0) 
    	{
        factory(App\Profile::class, 1)->create([
        		'user_id' => '1',
            'firstname' => 'Luis',
            'lastname' => 'Gil'
        ]);
        
        factory(App\Profile::class, 1)->create([
        		'user_id' => '2',
            'firstname' => 'Manolo',
            'lastname' => 'Reina'
        ]);
      }
    }
}
