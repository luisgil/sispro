<?php

use Illuminate\Database\Seeder;
use JeroenZwart\CsvSeeder\CsvSeeder;
use App\Test;

class TestsTableSeeder extends CsvSeeder
{
    public function __construct()
		{		
				$this->delimiter = ';';
				$this->file = '/database/seeds/csvs/extended/extended_tests-EB1-01.csv';
				$this->tablename = 'extended_tests';
				$this->truncate = false;
		}
		
    public function run()
    {
        //if(Test::count() == 0) 
    		//{
    			parent::run();
      	//}
    }
}
