<?php

use Illuminate\Database\Seeder;
use App\Camp;

class CampsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      if(Camp::count() == 0) 
    	{
        factory(App\Camp::class, 1)->create([
        		'block_id' => '1',
            'camp_name' => 'Iguana Zuata'
        ]);
        
        factory(App\Camp::class, 1)->create([
        		'block_id' => '1',
            'camp_name' => 'Zuata Principal'
        ]);
      }
      
    }
}
