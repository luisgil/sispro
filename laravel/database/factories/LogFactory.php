<?php

use Faker\Generator as Faker;
use App\{User, Profile};

$factory->define(\App\Log::class, function (Faker $faker) {
    
    $actions = collect([
        'creó al usuario ',
        'eliminó al usuario ',
        'modificó la información del usuario ',
        'agregó la macolla ',
        'modificó la información la macolla ',
        'eliminó la macolla ',
        'agregó el yacimiento ',
        'modificó la información del yacimiento ',
        'eliminó el yacimiento ',
        'agregó el pozo ',
        'modificó la información del pozo ',
        'eliminó el pozo ',
        'accedió al sistema ',
        'salió del sistema '
    ]);

    $users = collect([1, 2]);

    return [
        'user_id' => $users->random(),
        'description' => $actions->random()
    ];
});
