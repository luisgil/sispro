<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Average extends Model
{
    protected $table = 'average_tests';
    protected $guarded = [];
    protected $fillable = ['well_id', 'avg_start_test', 'avg_end_test', 'company', 'rpm', 'duration', 'line_pressure', 'line_temperature', 'gas_rate_prod', 'water_rate_prod', 'fluid_total_rate', 'percent_mixture', 'api_degree_mixture', 'neat_rate_prod', 'percent_formation', 'gvf', 'inyected_diluent_rate', 'api_degree_diluent', 'test', 'observations', 'pip', 'identifier'];
    
    public function macolla()
    {
    	return $this->belongsTo(Macolla::class);
    }
}

