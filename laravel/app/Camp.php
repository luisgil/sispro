<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Camp extends Model
{
		protected $table = 'camps';
		
    protected $fillable = [
        'block_id', 'camp_name'
    ];
    
    public function macolla()
    {
    	return $this->hasMany(Macolla::class);
    }
    
    public function block()
    {
    	return $this->belongsTo(Block::class);
    }
}
