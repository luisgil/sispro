<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reservoir extends Model
{
    protected $table = 'reservoirs';
		
    protected $fillable = ['code', 'reservoir_name', 'api_degree', 'hydrocarbon'];
    
    public function well()
    {
    	return $this->hasOne(Well::class);
    }
}
