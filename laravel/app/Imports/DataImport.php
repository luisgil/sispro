<?php

namespace App\Imports;

use App\Imports\DataFirstSheetImport;
use Maatwebsite\Excel\Concerns\{Importable, WithMultipleSheets};

class DataImport implements WithMultipleSheets
{
    use Importable;
    
    public function sheets(): array
    {
        return [
        	0 => new DataFirstSheetImport()
        ];
    }
}
