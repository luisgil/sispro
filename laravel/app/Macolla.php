<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Macolla extends Model
{
    protected $table = 'macollas';
		
    protected $fillable = [
        'camp_id', 'macolla_name'
    ];
    
    public function well()
    {
    	return $this->hasMany(Well::class);
    }
    
    public function location()
    {
    	return $this->hasMany(Location::class);
    }
    
    public function camp()
    {
    	return $this->belongsTo(Camp::class);
    }
    
    public function average()
    {
    	return $this->hasMany(Average::class);
    }
    
    public function production()
    {
    	return $this->hasMany(Production::class);
    }
}
