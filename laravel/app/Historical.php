<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Historical extends Model
{
    protected $table = 'historicals';

    protected $fillable = ['well_id', 'reservoir', 'begin_date', 'end_date', 'type_work_i', 'type_work_ii', 'rig', 'sensor_brand', 'sensor_model', 'sensor_diameter', 'sensor_company', 'deeptop_sensor_md', 'deepbottom_sensor_md', 'pump_model', 'pump_diameter', 'pump_type', 'pump_capacity', 'pump_company', 'deeptop_pump_md', 'deepbottom_pump_md', 'inyected_diluent', 'measure_from', 'observations', 'references', 'identifier'];
    
    public function well()
    {
    	return $this->belongsTo(Well::class);
    }
}
