<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Condition extends Model
{
    protected $table = 'conditions';

    protected $fillable = ['well_id', 'reservoir', 'sand', 'company', 'pt_date', 'test', 'deep_md', 'deep_tvd', 'temperature', 'duration', 'pressbefore', 'pressafter', 'pressformation', 'mobility', 'observations', 'identifier'];
    
    public function well()
    {
    	return $this->hasOne(Well::class);
    }
}
