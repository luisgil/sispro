<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Well extends Model
{
    protected $table = 'wells';
		
    protected $fillable = [
        'reservoir_id', 'macolla_id', 'location_id', 'uwi', 'well_name', 'type', 'classification', 'condition', 'drill', 'gl', 'rt', 'height', 'ppn', 'pip'
    ];
    
    public function macolla()
    {
    	return $this->belongsTo(Macolla::class);
    }
    
    public function reservoir()
    {
    	return $this->belongsTo(Reservoir::class);
    }
    
    public function test()
    {
    	return $this->hasMany(Test::class);
    }
    
    public function sign()
    {
    	return $this->hasMany(Sign::class);
    }
    
    public function condition()
    {
    	return $this->hasMany(Condition::class);
    }
    
    public function historical()
    {
    	return $this->hasMany(Historical::class);
    }
    
    public function staticPressure()
    {
    	return $this->hasMany(StaticPressure::class);
    }
    
    public function dynamicPressure()
    {
    	return $this->hasMany(DynamicPressure::class);
    }
    
    public function originalPressure()
    {
    	return $this->hasMany(OriginalPressure::class);
    }
    
    public function realSurvey()
    {
    	return $this->hasMany(RealSurvey::class);
    }
    
    public function planSurvey()
    {
    	return $this->hasMany(PlanSurvey::class);
    }

    public function events()
    {
        return $this->hasMany(Event::class);
    }
}
