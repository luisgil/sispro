<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Test extends Model
{
    protected $table = 'extended_tests';
    
    protected $guarded = [];
    
    protected $fillable = ['well_id', 'extended_date', 'company', 'test', 'hour_start', 'hour_end', 'line_pressure', 'line_temperature', 'water_rate_prod', 'gas_rate_prod', 'total_barrels', 'neat_rate', 'gvf', 'inyected_diluent_rate', 'identifier'];
    
    public function well()
    {
    	return $this->belongsTo(Well::class);
    }
}
