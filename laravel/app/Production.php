<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Production extends Model
{
    protected $table = 'productions';
    
    protected $guarded = [];
    
    protected $fillable = ['macolla_id', 'fiscalized_date', 'production', 'identifier'];
    
    public function macolla()
    {
    	return $this->belongsTo(Macolla::class);
    }
}
