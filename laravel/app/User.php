<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    protected $guarded = [];
    
    CONST ROLES = [
        'ADMIN' => 'ADMIN',
        'OPERATOR' => 'OPERATOR',
        'VALIDATOR' => 'VALIDATOR',
        'EXAMINATOR' => 'EXAMINATOR',
    ];
    
    protected $table = 'users';
		
    protected $fillable = ['email', 'password', 'role'];
    
    protected $hidden = [ 'password' ];
    
    public function profile()
    {
    	return $this->hasOne('App\Profile');
    }
    
    public function getSpanishRole() {
        switch($this->role) {
            case self::ROLES['ADMIN']: return 'Administrador';
            case self::ROLES['OPERATOR']: return 'Operador';
            case self::ROLES['VALIDATOR']: return 'Validador';
            default: return 'Consultor';
        }
    }
}
