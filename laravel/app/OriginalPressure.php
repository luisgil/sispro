<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OriginalPressure extends Model
{
    protected $table = 'original_pressures';

    protected $guarded = [];
    
    protected $fillable = ['well_id', 'reservoir', 'pressure_date', 'type_test', 'sensor_model', 'pressure_gradient', 'deepdatum_mc_md', 'deepdatum_mc_tvd', 'press_datum_macolla', 'temp_datum_macolla', 'temperature_gradient', 'identifier'];
    
    public function well()
    {
    	return $this->belongsTo(Well::class);
    }
}
