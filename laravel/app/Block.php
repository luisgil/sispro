<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Block extends Model
{
    protected $table = 'block';

    protected $fillable = [
        'block_name'
    ];
    
    public function camp()
    {
    	return $this->hasMany('App\Camp');
    }
}
