<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sign extends Model
{
    protected $table = 'signs';
    protected $guarded = [];
    protected $fillable = ['well_id', 'sign_date', 'sign_time', 'company' , 'percent_mixture', 'percent_sediment', 'api_degree_diluted', 'ptb', 'identifier'];
    
    public function well()
    {
    	return $this->belongsTo(Well::class);
    }
}
