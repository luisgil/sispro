<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\{Validator, Route, Auth, Hash};
use Illuminate\Validation\Rule;
use Illuminate\Contracts\Validation\ImplicitRule;
use App\{Block, Camp, Macolla, Location, Test, Average, Sign, Reservoir, Condition, Historical, StaticPressure, OriginalPressure, DynamicPressure, RealSurvey, PlanSurvey, Production, Log, Well, Event};

class ProductionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    		$item = Block::first();
    		$macolla = Macolla::where('id', '<>', 6)->get();
    		$noMacolla = Macolla::where('id', '=', 6)->first();
    		$all = Macolla::all();
        $reservoir = Reservoir::where('reservoir_name', '<>', 'N/A')->get();
        
        /** prueba de mejoramiento */
        $collection = Macolla::where('id', '<>', 6)->get();

        if(Route::currentRouteName() == "extendedTests")
        {
        		return view('layouts.production.extendedTests', ['item' => $item, 'macolla' => $macolla, 'value' => $noMacolla, 'reservoirs' => $reservoir]);
      	}
        
        if(Route::currentRouteName() == "averageTests")
        {
        		return view('layouts.production.averageTests', ['macolla' => $macolla, 'value' => $noMacolla, 'reservoirs' => $reservoir]);
      	}
      	
      	if(Route::currentRouteName() == "signs")
        {
        		return view('layouts.production.signs', ['macolla' => $macolla, 'value' => $noMacolla, 'reservoirs' => $reservoir]);
      	}
      	
        if(Route::currentRouteName() == "pressureAndTemperature")
        {
        		$reservoir = Reservoir::all();
        		return view('layouts.subsoil.presstemp', ['macolla' => $macolla, 'value' => $noMacolla, 'reservoirs' => $reservoir]);
      	}
      	
      	if(Route::currentRouteName() == "historical")
        {
        		$reservoir = Reservoir::get();
        		return view('layouts.subsoil.historical', ['macolla' => $macolla, 'value' => $noMacolla, 'reservoirs' => $reservoir]);
      	}
      	
        if(Route::currentRouteName() == "staticPressure")
        {
        		return view('layouts.subsoil.static', ['macolla' => $macolla, 'value' => $noMacolla, 'reservoirs' => $reservoir]);
      	}
      	
      	if(Route::currentRouteName() == "dynamicPressure")
        {
        		return view('layouts.subsoil.dynamic', ['macolla' => $macolla, 'value' => $noMacolla, 'reservoirs' => $reservoir]);
      	}

      	if(Route::currentRouteName() == "originalPressure")
        {
        		return view('layouts.subsoil.original', ['macolla' => $macolla, 'value' => $noMacolla, 'all' => $all, 'reservoirs' => $reservoir]);
      	}
      	
      	if(Route::currentRouteName() == "realSurvey")
        {
        		$reservoir = Reservoir::get();
        		return view('layouts.subsoil.realsurvey', ['macolla' => $macolla, 'value' => $noMacolla, 'reservoirs' => $reservoir]);
      	}
      	
      	if(Route::currentRouteName() == "planSurvey")
        {
        		$reservoir = Reservoir::get();
        		return view('layouts.subsoil.plansurvey', ['item' => $item, 'macolla' => $macolla, 'value' => $noMacolla, 'reservoirs' => $reservoir]);
      	}
      	
      	if(Route::currentRouteName() == "controlledProduction")
        {
          return view('layouts.controlled.production', ['item' => $item, 'macolla' => $macolla, 'value' => $noMacolla]);
        }

        if(Route::currentRouteName() == "controlledByWell")
        {
            return view('layouts.controlled.well-production', ['macolla' => $macolla, 'value' => $noMacolla]);
        }

        if (Route::currentRouteName() == 'wellEvents')
        {
          return view('layouts.events.well-events', ['macolla' => $macolla, 'value' => $noMacolla, 'reservoirs' => $reservoir]);
        }

        if(Route::currentRouteName() == "managementGlobal")
        {
          $reservoir = Reservoir::get();
          return view('layouts.management.management-global', ['item' => $item, 'macolla' => $macolla, 'value' => $noMacolla, 'reservoirs' => $reservoir]);
        }

        if(Route::currentRouteName() == "managementExtended")
        {
          $reservoir = Reservoir::get();
          return view('layouts.management.management-extended', ['item' => $item, 'macolla' => $macolla, 'value' => $noMacolla, 'reservoirs' => $reservoir]);
        }

        if(Route::currentRouteName() == "managementAverage")
        {
        		return view('layouts.management.management-average', ['macolla' => $macolla, 'value' => $noMacolla, 'reservoirs' => $reservoir]);
        }
        
        if (Route::currentRouteName() == 'managementSign')
        {
          return view('layouts.management.management-sign', ['macolla' => $macolla, 'value' => $noMacolla, 'reservoirs' => $reservoir]);
        }

        if (Route::currentRouteName() == 'managementCondition')
        {
          $reservoir = Reservoir::all();
          return view('layouts.management.management-condition', ['macolla' => $macolla, 'value' => $noMacolla, 'reservoirs' => $reservoir]);
        }

        if (Route::currentRouteName() == 'managementHistorical')
        {
          $reservoir = Reservoir::all();
          return view('layouts.management.management-historical', ['macolla' => $macolla, 'value' => $noMacolla, 'reservoirs' => $reservoir]);
        }

        if(Route::currentRouteName() == "managementStatic")
        {
            return view('layouts.management.management-static', ['macolla' => $macolla, 'value' => $noMacolla, 'reservoirs' => $reservoir]);
        }

        if(Route::currentRouteName() == "managementDynamic")
        {
            return view('layouts.management.management-dynamic', ['macolla' => $macolla, 'value' => $noMacolla, 'reservoirs' => $reservoir]);
        }

        if(Route::currentRouteName() == "managementOriginal")
        {
            return view('layouts.management.management-original', ['macolla' => $macolla, 'value' => $noMacolla, 'reservoirs' => $reservoir]);
        }

        if(Route::currentRouteName() == "managementReal")
        {
            return view('layouts.management.management-real', ['macolla' => $macolla, 'value' => $noMacolla, 'reservoirs' => $reservoir]);
        }

        if(Route::currentRouteName() == "managementPlan")
        {
            return view('layouts.management.management-plan', ['macolla' => $macolla, 'value' => $noMacolla, 'reservoirs' => $reservoir]);
        }

        if (Route::currentRouteName() == 'managementWellEvents')
        {
          return view('layouts.management.management-well-events', ['macolla' => $macolla, 'value' => $noMacolla, 'reservoirs' => $reservoir]);
        }

        if (Route::currentRouteName() == 'managementFiscalized')
        {
          return view('layouts.management.management-fiscalized', ['macolla' => $macolla, 'value' => $noMacolla, 'reservoirs' => $reservoir]);
        }

        if (Route::currentRouteName() == 'navigation')
        {
          return view('layouts.subsoil.well-navigation', ['macolla' => $macolla, 'value' => $noMacolla]);
        }

        if (Route::currentRouteName() == 'pump')
        {
          return view('layouts.pump.pump', ['macolla' => $macolla, 'value' => $noMacolla]);
        }

        if (Route::currentRouteName() == 'optionAdminChooser')
        {
          return view('layouts.management.menu');
        }
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    public function saveSingleExtended(Request $request)
    {
        $request->line_temperature = $request->line_temperature < 60 ? -1 : $request->line_temperature;

        $validator = Validator::make($request->all(), [
          'well_id' => ['required'],
          'extended_date' => ['required', 'date'],
          'company' => ['required', 'regex:/^([ a-zA-Z0-9-,.]+$)+/'],
          'test' => ['required', 'regex:/^([ a-zA-Z0-9]+$)+/'],
          'hour_start' => ['required', 'date_format:H:i', 'before:hour_end'],
          'hour_end' => ['required', 'date_format:H:i', 'after:hour_start'],
          'line_pressure' => ['nullable', 'numeric', 'between:14,600'],
          'line_temperature' => ['nullable', 'numeric', 'between:60,125'],
          'water_rate_prod' => ['nullable', 'numeric', 'between:0,3000'],
          'gas_rate_prod' => ['nullable', 'numeric', 'between:0,3000'],
          'total_barrels' => ['nullable','numeric', 'between:0,3000'],
          'neat_rate' => ['nullable','numeric', 'between:0,3000'],
          'gvf' => ['nullable', 'numeric', 'between:0,100'],
          'inyected_diluent_rate' => ['nullable', 'numeric', 'between:0,1200']
        ]);

        if ($validator->fails()) {
          return response()->json([
            'fail' => true,
            'errors'=> $validator->errors(),
            'status' => 400,
          ], 400);
        }

        $test = new Test();
        $test->fill($request->all());
        
        \DB::transaction(function () use($test) {
          $test->save();
        });

        $reg = Location::select('location_name')->where('id', $request->well_id)->first();
        
        Log::create([
          'user_id' => Auth::user()->id,
          'description' => '- Agregó un registro de Prueba Extendida en la localización ' . $reg['location_name'],
        ]);
        
        return response()->json([
          'fail' => false,
          'message' => 'El registro ha sido almacenado exitosamente',
          'status' => 200,
        ], 200);
    }

    public function saveSingleAverage(Request $request)
    {
        $validator = Validator::make($request->all(), [
          'well_id' => ['required'],
          'avg_start_test' => ['required', 'date', 'before_or_equal:avg_end_test'],
          'avg_end_test' => ['required', 'date', 'after_or_equal:avg_start_test'],
          'company' => ['required', 'regex:/^([ a-zA-Z0-9-,.]+$)+/'],
          'rpm' => ['required', 'integer', 'between:0,1500'],
          'duration' => ['required', 'integer', 'between:0,120'],
          'line_pressure' => ['nullable', 'numeric', 'between:14,600'],
          'line_temperature' => ['nullable', 'numeric', 'between:60,125'],
          'water_rate_prod' => ['nullable', 'numeric', 'between:0,3000'],
          'gas_rate_prod' => ['nullable', 'numeric', 'between:0,3000'],
          'fluid_total_rate' => ['nullable', 'numeric', 'between:0,3000'],
          'percent_mixture' => ['nullable', 'numeric', 'between:0,100'],
          'api_degree_mixture' => ['nullable', 'numeric', 'between:0,70'],
          'neat_rate_prod' => ['nullable', 'numeric', 'between:0,3000'],
          'percent_formation' => ['nullable', 'numeric', 'between:0,100'],
          'gvf' => ['nullable', 'numeric', 'between:0,100'],
          'inyected_diluent_rate' => ['nullable', 'numeric', 'between:0,3000'],
          'api_degree_diluent' => ['nullable', 'numeric', 'between:0,70'],
          'pip' => ['nullable', 'numeric', 'between:0,3000'],
          'test' => ['required']
        ]);

        if ($validator->fails()) {
          return response()->json([
            'fail' => true,
            'observations' => $request->observations,
            'errors'=> $validator->errors(),
            'status' => 400,
          ], 400);
        }

        $avg = new Average();
        $avg->fill($request->all());
        
        \DB::transaction(function () use($avg) {
          $avg->save();
        });

        $reg = Location::select('location_name')->where('id', $request->well_id)->first();
        
        Log::create([
          'user_id' => Auth::user()->id,
          'description' => '- Agregó un registro de Prueba Promedio en la localización ' . $reg['location_name']
        ]);
        
        return response()->json([
          'fail' => false,
          'message' => 'El registro ha sido almacenado exitosamente',
          'status' => 200,
        ], 200);
    }

    public function saveSingleSign(Request $request)
    {
        $validator = Validator::make($request->all(), [
          'well_id' => ['required'],
          'sign_date' => ['required', 'date'],
          'sign_time' => ['required'],
          'company' => ['required', 'regex:/^([ a-zA-Z0-9-,.]+$)+/'],
          'percent_mixture' => ['nullable', 'numeric', 'between:0,100'],
          'percent_sediment' => ['nullable', 'numeric', 'between:0,100'],
          'api_degree_diluted' => ['nullable', 'numeric', 'between:7,70'],
          'ptb' => ['nullable', 'numeric', 'between:0,1500'],
        ]);

        if ($validator->fails()) {
          return response()->json([
            'fail' => true,
            'errors'=> $validator->errors(),
            'status' => 400,
          ], 400);
        }

        $sign = new Sign();
        $sign->fill($request->all());
        
        \DB::transaction(function () use($sign) {
          $sign->save();
        });
        
        $reg = Location::select('location_name')->where('id', $request->well_id)->first();

        Log::create([
          'user_id' => Auth::user()->id,
          'description' => '- Agregó un registro de Muestras en la localización ' . $reg['location_name']
        ]);
        
        return response()->json([
          'fail' => false,
          'message' => 'El registro ha sido almacenado exitosamente',
          'status' => 200,
        ], 200);
    }

    public function saveSingleFiscalized(Request $request)
    {
        $validator = Validator::make($request->all(), [
          'macolla' => ['required'],
          'fiscalized_date' => ['required', 'date'],
          'production' => ['required', 'numeric', 'between:0,3000'],
        ]);

        if ($validator->fails()) {
          return response()->json([
            'fail' => true,
            'errors'=> $validator->errors(),
            'status' => 400,
          ], 400);
        }

        $fiscal = new Production();
        $fiscal->fill($request->all());
        
        \DB::transaction(function () use($fiscal, $request) {
          $fiscal->macolla_id = $request->macolla;
          $fiscal->save();
        });
        
        $reg = Macolla::select('macolla_name')->where('id', $request->macolla)->first();

        Log::create([
          'user_id' => Auth::user()->id,
          'description' => '- Agregó un registro de Producción Fiscalizada en la macolla ' . $reg['macolla_name']
        ]);
        
        return response()->json([
          'fail' => false,
          'message' => 'El registro ha sido almacenado exitosamente',
          'status' => 200,
        ], 200);
    }

    public function saveSingleCondition(Request $request)
    {
        $validator = Validator::make($request->all(), [
          'well_id' => ['required'],
          'reservoir' => ['required'],
          'sand' => ['required'],
          'company' => ['required', 'regex:/^([ a-zA-Z0-9-,.]+$)+/'],
          'pt_date' => ['required', 'date'],
          'test' => ['required'],
          'deep_md' => ['nullable', 'numeric', 'between:0,5000'],
          'deep_tvd' => ['nullable', 'numeric', 'between:0,5000'],
          'temperature' => ['nullable', 'numeric', 'between:60,125'],
          'duration' => ['nullable', 'integer', 'between:0,300'],
          'pressbefore' => ['nullable', 'numeric', 'between:0,5000'],
          'pressafter' => ['nullable', 'numeric', 'between:0,5000'],
          'pressformation' => ['nullable', 'numeric', 'between:0,5000'],
          'mobility' => ['nullable', 'numeric', 'between:0,5000']
        ]);

        if ($validator->fails()) {
          return response()->json([
            'fail' => true,
            'errors'=> $validator->errors(),
            'status' => 400,
          ], 400);
        }

        $data = Reservoir::select('reservoir_name')->where('id', $request->reservoir)->first();

        $press = new Condition();
        $press->fill($request->all());
        
        \DB::transaction(function () use($press, $data) {
          $press->reservoir = $data['reservoir_name'];
          $press->save();
        });
        
        $reg = Location::select('location_name')->where('id', $request->well_id)->first();

        Log::create([
          'user_id' => Auth::user()->id,
          'description' => '- Agregó un registro de Presión y Temperatura en la localización ' . $reg['location_name']
        ]);
        
        return response()->json([
          'fail' => false,
          'message' => 'El registro ha sido almacenado exitosamente',
          'status' => 200,
        ], 200);
    }

    public function saveSingleHistorical(Request $request)
    {
        $validator = Validator::make($request->all(), [
          'well_id' => ['required'],
          'reservoir' => ['required'],
          'begin_date' => ['required', 'date', 'before_or_equal:end_date'],
          'end_date' => ['required', 'date', 'after_or_equal:begin_date'],
          'type_work_i' => ['required'],
          'type_work_ii' => ['required'],
          'rig' => ['required'],
          'sensor_brand' => ['nullable', 'string'],
          'sensor_model' => ['nullable', 'string'],
          'sensor_diameter' => ['nullable', 'numeric', 'between:0,200'],
          'deeptop_sensor_md' => ['nullable', 'numeric', 'between:0,5000'],
          'deepbottom_sensor_md' => ['nullable', 'numeric', 'between:0,5000'],
          'sensor_company' => ['nullable', 'string'],
          'pump_model' => ['required'],
          'pump_type' => ['required'],
          'pump_diameter' => ['nullable', 'numeric', 'between:0,200'],
          'pump_capacity' => ['nullable', 'numeric', 'between:0,500'],
          'pump_company' => ['required', 'string'],
          'deeptop_pump_md' => ['nullable', 'numeric', 'between:0,5000'],
          'deepbottom_pump_md' => ['nullable', 'numeric', 'between:0,5000'],
          'inyected_diluent' => ['required'],
          'observations' => ['required'],
          'measure_from' => ['required']
        ]);

        if ($validator->fails()) {
          return response()->json([
            'fail' => true,
            'errors'=> $validator->errors(),
            'status' => 400,
          ], 400);
        }

        $data = Reservoir::select('reservoir_name')->where('id', $request->reservoir)->first();

        $historical = new Historical();
        $historical->fill($request->all());
        
        \DB::transaction(function () use($historical, $data) {
          $historical->reservoir = $data['reservoir_name'];
          $historical->save();
        });
        
        $reg = Location::select('location_name')->where('id', $request->well_id)->first();

        Log::create([
          'user_id' => Auth::user()->id,
          'description' => '- Agregó un registro de Histórico de Completación en la localización ' . $reg['location_name']
        ]);
        
        return response()->json([
          'fail' => false,
          'message' => 'El registro ha sido almacenado exitosamente',
          'status' => 200,
        ], 200);
    }

    public function saveSingleStatic(Request $request)
    {
        $validator = Validator::make($request->all(), [
          'well_id' => ['required'],
          'static_date' => ['required', 'date'],
          'quality' => ['required'],
          'pip' => ['nullable', 'numeric', 'between:0,1500'],
          'pdp' => ['nullable', 'numeric', 'between:0,1500'],
          'temperature' => ['nullable', 'numeric', 'between:60,125']
        ]);

        if ($validator->fails()) {
          return response()->json([
            'fail' => true,
            'errors'=> $validator->errors(),
            'status' => 400,
          ], 400);
        }

        $static = new StaticPressure();
        $static->fill($request->all());
        
        \DB::transaction(function () use($static) {
          $static->save();
        });
        
        $reg = Location::select('location_name')->where('id', $request->well_id)->first();

        Log::create([
          'user_id' => Auth::user()->id,
          'description' => '- Agregó un registro de Presión Estática en la localización ' . $reg['location_name']
        ]);
        
        return response()->json([
          'fail' => false,
          'message' => 'El registro ha sido almacenado exitosamente',
          'status' => 200,
        ], 200);
    }

    public function saveSingleDynamic(Request $request)
    {
        $validator = Validator::make($request->all(), [
          'well_id' => ['required'],
          'dynamic_date' => ['required', 'date'],
          'quality' => ['required'],
          'pip' => ['nullable', 'numeric', 'between:0,1500'],
          'pdp' => ['nullable', 'numeric', 'between:0,1500'],
          'temperature' => ['nullable', 'numeric', 'between:60,125']
        ]);

        if ($validator->fails()) {
          return response()->json([
            'fail' => true,
            'errors'=> $validator->errors(),
            'status' => 400,
          ], 400);
        }

        $dynamic = new DynamicPressure();
        $dynamic->fill($request->all());
        
        \DB::transaction(function () use($dynamic) {
          $dynamic->save();
        });
        
        $reg = Location::select('location_name')->where('id', $request->well_id)->first();

        Log::create([
          'user_id' => Auth::user()->id,
          'description' => '- Agregó un registro de Presión Dinámica en la localización ' . $reg['location_name']
        ]);
        
        return response()->json([
          'fail' => false,
          'message' => 'El registro ha sido almacenado exitosamente',
          'status' => 200,
        ], 200);
    }

    public function saveSingleOriginal(Request $request)
    {
        $validator = Validator::make($request->all(), [
          'well_id' => ['required'],
          'reservoir' => ['required'],
          'pressure_date' => ['required', 'date'],
          'type_test' => ['required'],
          'sensor_model' => ['required'],
          'pressure_gradient' => ['nullable', 'numeric', 'between:14,600'],
          'temperature_gradient' => ['nullable', 'numeric', 'between:60,125'],
          'deepdatum_mc_md' => ['nullable', 'numeric', 'between:0,1500'],
          'deepdatum_mc_tvd' => ['nullable', 'numeric', 'between:0,1500'],
          'press_datum_macolla' => ['nullable', 'numeric', 'between:14,600'],
          'temp_datum_macolla' => ['nullable', 'numeric', 'between:60,125']
        ]);

        if ($validator->fails()) {
          return response()->json([
            'fail' => true,
            'errors'=> $validator->errors(),
            'status' => 400,
          ], 400);
        }

        $data = Reservoir::select('reservoir_name')->where('id', $request->reservoir)->first();

        $original = new OriginalPressure();
        $original->fill($request->all());
        
        \DB::transaction(function () use($original, $data) {
          $original->reservoir = $data['reservoir_name'];
          $original->save();
        });
        
        $reg = Location::select('location_name')->where('id', $request->well_id)->first();

        Log::create([
          'user_id' => Auth::user()->id,
          'description' => '- Agregó un registro de Presión Original PMVM en la localización ' . $reg['location_name']
        ]);
        
        return response()->json([
          'fail' => false,
          'message' => 'El registro ha sido almacenado exitosamente',
          'status' => 200,
        ], 200);
    }

    public function saveSingleReal(Request $request)
    {
        $validator = Validator::make($request->all(), [
          'well_id' => ['required'],
          'rs_date' => ['required', 'date'],
          'hole' => ['required'],
          'company' => ['required', 'regex:/^([ a-zA-Z0-9,.]+$)+/'],
          'md' => ['nullable', 'numeric', 'between:0,1500'],
          'inclination' => ['nullable', 'numeric', 'between:0,1500'],
          'azim_grid' => ['nullable', 'numeric', 'between:0,1500'],
          'tvd' => ['nullable', 'numeric', 'between:0,1500'],
          'vsec' => ['nullable', 'numeric', 'between:0,1500'],
          'dls' => ['nullable', 'numeric', 'max:1500'],
          'ns' => ['nullable', 'numeric', 'max:1500'],
          'ew' => ['nullable', 'numeric', 'max:1500'],
          'la_g' => ['nullable', 'numeric', 'max:200'],
          'la_m' => ['nullable', 'numeric', 'max:200'],
          'la_s' => ['nullable', 'numeric', 'max:200'],
          'lo_g' => ['nullable', 'numeric', 'max:200'],
          'lo_m' => ['nullable', 'numeric', 'max:200'],
          'lo_s' => ['nullable', 'numeric', 'max:200'],
          'observations' => ['required', 'string']
        ]);

        if ($validator->fails()) {
          return response()->json([
            'fail' => true,
            'errors'=> $validator->errors(),
            'status' => 400,
          ], 400);
        }

        $real = new RealSurvey();
        $real->fill($request->all());
        
        \DB::transaction(function () use($real) {
          $real->save();
        });
        
        $reg = Location::select('location_name')->where('id', $request->well_id)->first();

        Log::create([
          'user_id' => Auth::user()->id,
          'description' => '- Agregó un registro de Survey Real en la localización ' . $reg['location_name']
        ]);
        
        return response()->json([
          'fail' => false,
          'message' => 'El registro ha sido almacenado exitosamente',
          'status' => 200,
        ], 200);
    }

    public function saveSinglePlan(Request $request)
    {
      
        $validator = Validator::make($request->all(), [
          'well_id' => ['required'],
          'ps_date' => ['required', 'date'],
          'md' => ['nullable', 'numeric', 'between:0,1500'],
          'inclination' => ['nullable', 'numeric', 'between:0,1500'],
          'azim_grid' => ['nullable', 'numeric', 'between:0,1500'],
          'tvd' => ['nullable', 'numeric', 'between:0,1500'],
          'vsec' => ['nullable', 'numeric', 'between:0,1500'],
          'dls' => ['nullable', 'numeric', 'max:1500'],
          'ns' => ['nullable', 'numeric', 'max:1500'],
          'ew' => ['nullable', 'numeric', 'max:1500'],
          'la_g' => ['nullable', 'numeric', 'max:200'],
          'la_m' => ['nullable', 'numeric', 'max:200'],
          'la_s' => ['nullable', 'numeric', 'max:200'],
          'lo_g' => ['nullable', 'numeric', 'max:200'],
          'lo_m' => ['nullable', 'numeric', 'max:200'],
          'lo_s' => ['nullable', 'numeric', 'max:200'],
          'observations' => ['required', 'string']
        ]);

        if ($validator->fails()) {
          return response()->json([
            'fail' => true,
            'errors'=> $validator->errors(),
            'status' => 400,
          ], 400);
        }

        $plan = new PlanSurvey();
        $plan->fill($request->all());
        
        \DB::transaction(function () use($plan) {
          $plan->save();
        });
        
        $reg = Location::select('location_name')->where('id', $request->well_id)->first();

        Log::create([
          'user_id' => Auth::user()->id,
          'description' => '- Agregó un registro de Survey Plan en la localización ' . $reg['location_name']
        ]);
        
        return response()->json([
          'fail' => false,
          'message' => 'El registro ha sido almacenado exitosamente',
          'status' => 200,
        ], 200);
    }

    public function saveSingleEvent(Request $request)
    {
        $validator = Validator::make($request->all(), [
          'well_id' => ['required'],
          'event_date' => ['required', 'date'],
          'event_time' => ['required'],
          'diluent' => ['nullable', 'integer', 'between:0,500'],
          'rpm' => ['nullable', 'numeric', 'between:0,3000'],
          'pump_efficiency' => ['nullable', 'numeric', 'between:0,1500'],
          'torque' => ['nullable', 'numeric', 'between:0,1500'],
          'frequency' => ['nullable', 'numeric', 'between:0,1500'],
          'current' => ['nullable', 'numeric', 'between:0,1500'],
          'power' => ['nullable', 'numeric', 'between:0,1500'],
          'mains_voltage' => ['nullable', 'numeric', 'between:0,1500'],
          'output_voltage' => ['nullable', 'numeric', 'between:0,1500'],
          'vfd_temperature' => ['nullable', 'numeric', 'between:0,130'],
          'head_temperature' => ['nullable', 'numeric', 'between:0,130'],
          'head_pressure' => ['nullable', 'numeric', 'between:0,1500']
        ]);

        if ($validator->fails()) {
          return response()->json([
            'fail' => true,
            'errors'=> $validator->errors(),
            'status' => 400,
          ], 400);
        }

        $event = new Event();
        $event->fill($request->all());
        
        \DB::transaction(function () use($event) {
          $event->save();
        });
        
        $reg = Well::select('well_name')->where('id', $request->well_id)->first();

        Log::create([
          'user_id' => Auth::user()->id,
          'description' => '- Agregó un registro de Evento en el pozo ' . $reg['well_name']
        ]);
        
        return response()->json([
          'fail' => false,
          'message' => 'El registro ha sido almacenado exitosamente',
          'status' => 200,
        ], 200);
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
