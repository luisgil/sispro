<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\{Macolla, Camp, Block, Reservoir, Well, Location, Log};
use Illuminate\Support\Facades\{Hash, Validator, Auth};
use Illuminate\Validation\Rule;

class WellController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Auth::user()->role == "ADMIN" || Auth::user()->role == "OPERATOR") {
            $collection = Macolla::where('id', '<>', '6')->orderBy('id', 'asc')->get();
            $noMacolla = Macolla::where('id','=', '6')->first();
            $item = Block::first();
            return view('layouts.management.wells', ['collection' => $collection, 'item' => $item, 'value' => $noMacolla]);
        } else {
            return redirect()->route('home');
        }
    }
    
    public function setSelected(Request $request)
    {
        $macollaName = Macolla::select('macolla_name')->where('id', '=', $request->macolla_id)->first();
        
        $data = array('name' => $request->macolla_name, 'id' => $request->macolla_id);
        
        return response()->json([
            'fail' => false,
            'message' => 'Envío exitoso.',
            'data' => $data,
            'array' => Reservoir::all(),
            'status' => 200,
        ], 200);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function load(Request $request)
    {
        $collection = Macolla::where('id', '<>', '6')->orderBy('id', 'asc')->get();
        $without = Macolla::where('id', '=', '6')->first();
        return response()->json([
            'fail' => false,
            'data' => $collection,
            'array' => $without,
            'status' => 200,
        ], 200);
    }
    
    public function search(Request $request)
    {
        $collection = Well::join('reservoirs', 'wells.reservoir_id', '=', 'reservoirs.id')
        ->join('macollas', 'wells.macolla_id', '=', 'macollas.id')
        ->join('locations', 'wells.location_id', '=', 'locations.id')
        ->where('macollas.id', $request->macolla_id)
        ->orderBy('wells.id', 'asc')->get();
        
        $macollaName = Macolla::where('id', '=', $request->macolla_id)->first();
        
        return response()->json([
            'fail' => false,
            'data' => $collection,
            'macolla' => $macollaName,
            'status' => 200,
        ], 200);
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'macolla_name' => ['required'],
            'location_name' => ['required', 'regex:/^([A-Z0-9-]+$)+/', 'min:5', 'unique:locations'],
            'well_name' => ['required', 'regex:/^([a-zA-Z0-9]+$)+/', 'min:7', 'unique:wells'],
            'reservoir_name' => ['required'],
            'type' => ['required'],
            'classification' => ['required'],
            'condition' => ['required'],
            'drill' => ['required'],
            'gl' => ['required'],
            'rt' => ['required'],
            'height' => ['required'],
            'ppn' => ['required']
        ]);
        
        if ($validator->fails()) {
            return response()->json([
                'fail' => true,
                'errors'=> $validator->errors(),
                'status' => 400,
            ], 400);
        }
        
        $element = Macolla::select('id')->where('macolla_name', '=', $request->macolla_name)->get();
        foreach($element as $key) $macollaId = $key->id;
        
        Location::create([
            'macolla_id' => $macollaId,
            'location_name' => $request->location_name
        ]);
        
        $_element = Location::select('id')->where('location_name', '=', $request->location_name)->get();
        foreach($_element as $key)	$locationId = $key->id;
        
        $well = new Well();
        $well->fill($request->all());
        \DB::transaction(function () use($well, $request, $macollaId, $locationId) {
        		$well->reservoir_id = $request->reservoir_name;
        		$well->location_id = $locationId;
        		$well->macolla_id = $macollaId;
        		$well->uwi = $request->uwi;
        		$well->well_name = $request->well_name;
        		$well->type = $request->type;
        		$well->classification = $request->classification;
        		$well->condition = $request->condition;
            $well->save();
        });
        
        Log::create([
            'user_id' => Auth::user()->id,
            'description' => '- Creó el pozo '.$request->well_name.' ('.$request->location_name.')'
        ]);

        return response()->json([
            'fail' => false,
            'message' => 'Almacenamiento exitoso',
            'status' => 200,
        ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     
    public function update(Request $request, $id)
    {
        $well = Well::findOrFail($id);
        $id = $request->identity;
        
        $location = new Location();
    		
        $validator = Validator::make($request->all(), [
            'update_well_name' => ['required', 'regex:/^([a-zA-Z0-9 ]+$)+/', 'unique:wells,well_name,'.$id],
            'update_location_name' => ['required', 'string', 'unique:locations,location_name,'.$id],
            'update_reservoir_id' => ['required'],
            'update_type' => ['required'],
            'update_classification' => ['required'],
            'update_condition' => ['required'],
            'update_drill' => ['required'],
            'update_gl' => ['required'],
            'update_rt' => ['required'],
            'update_height' => ['required'],
            'update_ppn' => ['required']
        ]);
        
        if ($validator->fails()) {
            return response()->json([
                'fail' => true,
                'errors'=> $validator->errors(),
                'status' => 400,
            ], 400);
        }

        \DB::transaction(function () use($well, $location, $request) {
        		$well->well_name = $request->update_well_name;
        		$well->reservoir_id = $request->update_reservoir_id;
        		$well->uwi = $request->update_uwi;
        		$well->type = $request->update_type;
        		$well->classification = $request->update_classification;
        		$well->condition = $request->update_condition;
        		$well->drill = trim($request->update_drill) == null ? '' : trim($request->update_drill);
        		$well->gl = $request->update_gl;
        		$well->rt = $request->update_rt;
        		$well->height = $request->update_height;
        		$well->ppn = $request->update_ppn;
            $well->save();
        });

        /** update location name */
        Location::where('id', $id)->update(['location_name' => $request->update_location_name]);
        
        $collection = Well::join('reservoirs', 'wells.reservoir_id', '=', 'reservoirs.id')
        ->join('macollas', 'wells.macolla_id', '=', 'macollas.id')
        ->join('locations', 'wells.location_id', '=', 'locations.id')
        ->where('wells.id', $id)->first();

        Log::create([
            'user_id' => Auth::user()->id,
            'description' => '- Modificó la información del pozo '.$well->well_name
        ]);

        return response()->json([
            'fail' => false,
            'request' => $request->update_location_name,
            'data' => $collection,
            'status' => 200,
        ], 200); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $well = Well::findOrfail($id);
        
        if($well->delete()) {
            
            Log::create([
                'user_id' => Auth::user()->id,
                'description' => '- Eliminó el pozo '.$well->well_name
            ]);

            return response()->json([
                'fail' => false,
                'message' => 'Deleted successfully',
                'status' => 200,
            ], 200);
        }
    }
}
