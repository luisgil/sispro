<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\{User, Profile};

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Auth::check()) {
            return view('layouts.home.index');
        } else {
            return redirect()->route('login');
        }
    }

    public function createUser()
    {
        if(Auth::user()->role == "ADMIN") {
            return view('layouts.users.create');
        } else {
            return redirect()->route('home');
        }
    }

    public function managementUser()
    {
        if(Auth::user()->role == "ADMIN") {
            $collection = User::select('users.id as id', 'firstname', 'lastname', 'email', 'role', 'last_login', 'users.created_at as created_at')->join('profiles', 'profiles.user_id', 'users.id')->get();
            return view('layouts.users.management', ['users' => $collection]);
        } else {
            return redirect()->route('home');
        }
    }

    public function movement()
    {
        if(Auth::user()->role == "ADMIN") {
            return view('layouts.users.movements');
        } else {
            return redirect()->route('home');
        }
    }
}
