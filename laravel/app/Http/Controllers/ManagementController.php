<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\{Validator, Route, Auth, Hash};
use Illuminate\Validation\Rule;
use App\{Camp, Macolla, Location, Test, Average, Sign, Reservoir, Condition, Historical, StaticPressure, OriginalPressure, DynamicPressure, RealSurvey, PlanSurvey, Production, Log, Event, Well};

class ManagementController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    public function updateExtended(Request $request, $id)
    {
        $test = Test::findOrFail($id);

        $validator = Validator::make($request->all(), [
            'company' => ['required', 'string'],
            'test' => ['required', 'string'],
            'hour_start' => ['required', 'date_format:H:i', 'before:hour_end'],
            'hour_end' => ['required', 'date_format:H:i', 'after:hour_start'],
            'line_pressure' => ['nullable', 'numeric', 'between:14,600'],
            'line_temperature' => ['nullable', 'numeric', 'between:60,125'],
            'water_rate_prod' => ['nullable', 'numeric', 'between:0,3000'],
            'gas_rate_prod' => ['nullable', 'numeric', 'between:0,3000'],
            'total_barrels' => ['nullable', 'numeric', 'between:0,3000'],
            'neat_rate' => ['nullable', 'numeric', 'between:0,3000'],
            'gvf' => ['nullable', 'numeric', 'between:0,100'],
            'inyected_diluent_rate' => ['nullable', 'numeric', 'between:0,1200']
          ]);
  
          if ($validator->fails()) {
            return response()->json([
              'fail' => true,
              'errors'=> $validator->errors(),
              'status' => 400,
            ], 400);
          }

          Test::where('id', $id)->update([
            'company' => $request->company,
            'test' => $request->test,
            'hour_start' => $request->hour_start,
            'hour_end' => $request->hour_end,
            'line_pressure' => $request->line_pressure,
            'line_temperature' => $request->line_temperature,
            'water_rate_prod' => $request->water_rate_prod,
            'gas_rate_prod' => $request->gas_rate_prod,
            'total_barrels' => $request->total_barrels,
            'neat_rate' => $request->neat_rate,
            'gvf' => $request->gvf,
            'inyected_diluent_rate' => $request->inyected_diluent_rate
          ]);
          
          Log::create([
            'user_id' => Auth::user()->id,
            'description' => '- Modificó un registro de Prueba Extendida.'
          ]);

          $collection = Test::select('extended_tests.id as id', 'extended_date', 'company', 'test', 'hour_start', 'hour_end', 'well_name', 'macolla_name', 'location_name', 'reservoir_name', 'line_pressure', 'line_temperature', 'water_rate_prod', 'gas_rate_prod', 'total_barrels', 'neat_rate', 'gvf', 'inyected_diluent_rate')
          ->join('wells', 'wells.id', 'extended_tests.well_id')
          ->join('locations', 'locations.id', 'wells.location_id')
          ->join('reservoirs', 'reservoirs.id', 'wells.reservoir_id')
          ->join('macollas', 'macollas.id', 'wells.macolla_id')
          ->where('extended_tests.id', $id);

          $object = $collection->get();

          $info = $collection->first();

          $data = [];
          
          foreach($object as $key => $item) {

            $data[] = [
              0,
              date('d/m/Y', strtotime($item['extended_date'])),
              $this->replace_space($item['macolla_name']),
              $this->replace_space($item['location_name']),
              $this->replace_space($item['well_name']),
              $this->replace_space($item['reservoir_name']),
              $this->replace_space($item['company']),
              $this->replace_space($item['test']),
              date('H:i', strtotime($item['hour_start'])),
              date('H:i', strtotime($item['hour_end'])),
              $this->diff($item['hour_start'], $item['hour_end']),
              $this->check_data($item['line_pressure'], 10, 1, 'line_pressure'),
              $this->check_data($item['line_temperature'], 10, 1, 'line_temperature'),
              $this->check_data($item['water_rate_prod'], 10, 1, 'water_rate_prod'),
              $this->check_data($item['gas_rate_prod'], 10, 2, 'gas_rate_prod'),
              $this->check_data($item['total_barrels'], 10, 1, 'total_barrels'),
              $this->check_data($item['neat_rate'], 10, 1, 'neat_rate'),
              $this->check_data(($item['gvf']), 1, 3, 'gvf'),
              $this->check_data($item['inyected_diluent_rate'], 10, 1, 'inyected_diluent_rate'),
              '<div class="btn-group btn-group-sm"><button class="btn btn-info" id="btn-edit" data-toggle="modal" data-target="#modal-xl" title="Editar" value="'.$item['id'].'"><i class="fa fa-edit"></i></button><button class="btn btn-danger" id="btn-delete" value="'.$item['id'].'"><i class="fa fa-trash" title="Borrar"></i></button></div>'
            ];
          }

          return response()->json([
            'fail' => false,
            'data' => $data,
            'info' => $info['macolla_name'],
            'type' => $request->type,
            'message' => 'El registro ha sido actualizado exitosamente',
            'status' => 200,
          ], 200);
    }

    public function updateAverage(Request $request, $id)
    {
        $average = Average::findOrFail($id);

        $validator = Validator::make($request->all(), [
            'company' => ['required', 'string'],
            'rpm' => ['required', 'integer', 'between:0,2000'],
            'duration' => ['required', 'integer', 'between:0,120'],
            'line_pressure' => ['nullable', 'numeric', 'between:14,600'],
            'line_temperature' => ['nullable', 'numeric', 'between:60,125'],
            'gas_rate_prod' => ['nullable', 'numeric', 'between:0,3000'],
            'water_rate_prod' => ['nullable', 'numeric', 'between:0,3000'],
            'fluid_total_rate' => ['nullable', 'numeric', 'between:0,3000'],
            'neat_rate_prod' => ['nullable', 'numeric', 'between:0,3000'],
            'percent_mixture' => ['nullable', 'numeric', 'between:0,100'],
            'api_degree_mixture' => ['nullable', 'numeric', 'between:0,70'],
            'percent_formation' => ['nullable', 'numeric', 'between:0,100'],
            'inyected_diluent_rate' => ['nullable', 'numeric', 'between:0,3000'],
            'api_degree_diluent' => ['nullable', 'numeric', 'between:0,70'],
            'gvf' => ['nullable', 'numeric', 'between:0,100']
          ]);
  
          if ($validator->fails()) {
            return response()->json([
              'fail' => true,
              'errors'=> $validator->errors(),
              'status' => 400,
            ], 400);
          }

          Average::where('id', $id)->update([
            'company' => $request->company,
            'rpm' => $request->rpm,
            'duration' => $request->duration,
            'line_pressure' => $request->line_pressure,
            'line_temperature' => $request->line_temperature,
            'gas_rate_prod' => $request->gas_rate_prod,
            'water_rate_prod' => $request->water_rate_prod,
            'fluid_total_rate' => $request->fluid_total_rate,
            'neat_rate_prod' => $request->neat_rate_prod,
            'percent_mixture' => $request->percent_mixture,
            'api_degree_mixture' => $request->api_degree_mixture,
            'percent_formation' => $request->percent_formation,
            'test' => $request->test,
            'pip' => $request->pip,
            'inyected_diluent_rate' => $request->inyected_diluent_rate,
            'api_degree_diluent' => $request->api_degree_diluent,
            'gvf' => $request->gvf,
            'observations' => $request->observations
          ]);
          
          Log::create([
            'user_id' => Auth::user()->id,
            'description' => '- Modificó un registro de Prueba Promedio.'
          ]);

          $collection = Average::select('average_tests.id as id', 'well_name', 'avg_start_test', 'avg_end_test', 'company', 'duration', 'rpm', 'test', 'macolla_name', 'location_name', 'reservoir_name', 'line_pressure', 'line_temperature', 'gas_rate_prod', 'water_rate_prod', 'fluid_total_rate', 'neat_rate_prod', 'percent_mixture', 'api_degree_mixture', 'percent_formation', 'inyected_diluent_rate', 'api_degree_diluent', 'test', 'pip', 'gvf', 'observations')
          ->join('wells', 'wells.id', 'average_tests.well_id')
          ->join('locations', 'locations.id', 'wells.location_id')
          ->join('reservoirs', 'reservoirs.id', 'wells.reservoir_id')
          ->join('macollas', 'macollas.id', 'wells.macolla_id')
          ->where('average_tests.id', $id);

          $object = $collection->get();

          $info = $collection->first();

          $data = [];
          
          foreach($object as $key => $item) {

            $data[] = [
              0,
              $this->replace_space($item['location_name']),
              $this->replace_space($item['well_name']),
              $this->replace_space($item['reservoir_name']),
              date('d/m/Y', strtotime($item['avg_start_test'])),
              date('d/m/Y', strtotime($item['avg_end_test'])),
              $this->replace_space($item['company']),
              $this->check_data($item['duration'], 1000, 0, 'duration'),
              $this->replace_space($item['test']),
              $this->check_data($item['rpm'], 2000, 0, 'rpm'),
              $this->check_data($item['line_pressure'], 10, 1, 'line_pressure'),
              $this->check_data($item['line_temperature'], 10, 1, 'line_temperature'),
              $this->check_data($item['gas_rate_prod'], 10, 2, 'gas_rate_prod'),
              $this->check_data($item['water_rate_prod'], 10, 1, 'water_rate_prod'),
              $this->check_data($item['fluid_total_rate'], 10, 1, 'fluid_total_rate'),
              $this->check_data($item['percent_mixture'], 10, 1, 'percent_mixture'),
              $this->check_data($item['api_degree_mixture'], 70, 1, 'api_degree_mixture'),
              $this->check_data($item['neat_rate_prod'], 10, 1, 'neat_rate_prod'),
              $this->check_data($item['percent_formation'], 10, 1, 'percent_formation'),
              $this->check_data(($item['gvf']), 1, 3, 'gvf'),
              $this->check_data($item['inyected_diluent_rate'], 10, 1, 'inyected_diluent_rate'),
              $this->check_data($item['api_degree_diluent'], 70, 1, 'api_degree_diluent'),
              $this->check_data($item['pip'], 2000, 0, 'pip'),
              $this->format_text($item['observations']),
              '<div class="btn-group btn-group-sm"><button class="btn btn-info" id="btn-edit" data-toggle="modal" data-target="#modal-xl" title="Editar" value="'. $item['id'] .'"><i class="fa fa-edit"></i></button><button class="btn btn-danger" id="btn-delete" value="'. $item['id'] .'"><i class="fa fa-trash" title="Borrar"></i></button></div>'
            ];
          }

          return response()->json([
            'fail' => false,
            'data' => $data,
            'info' => $info['macolla_name'],
            'type' => (int) $request->type,
            'message' => 'El registro ha sido actualizado exitosamente',
            'status' => 200,
          ], 200);
    }

    public function updateSign(Request $request, $id)
    {
        $sign = Sign::findOrFail($id);

        $validator = Validator::make($request->all(), [
            'sign_time' => ['required'],
            'company' => ['required', 'string'],
            'percent_mixture' => ['nullable', 'numeric', 'between:0,100'],
            'percent_sediment' => ['nullable', 'numeric', 'between:0,100'],
            'api_degree_diluted' => ['nullable', 'numeric', 'between:0,70'],
            'ptb' => ['nullable', 'numeric', 'between:0,1500'],
          ]);
  
          if ($validator->fails()) {
            return response()->json([
              'fail' => true,
              'errors'=> $validator->errors(),
              'status' => 400,
            ], 400);
          }

          Sign::where('id', $id)->update([
            'sign_time' => $request->sign_time,
            'company' => $request->company,
            'percent_mixture' => $request->percent_mixture,
            'percent_sediment' => $request->percent_sediment,
            'api_degree_diluted' => $request->api_degree_diluted,
            'ptb' => $request->ptb,
          ]);
          
          Log::create([
            'user_id' => Auth::user()->id,
            'description' => '- Modificó un registro de Muestra.'
          ]);

          $collection = Sign::select('signs.id as id', 'sign_date', 'well_name', 'sign_time', 'macolla_name', 'location_name', 'reservoir_name', 'company', 'percent_mixture', 'percent_sediment', 'api_degree_diluted', 'ptb')
          ->join('wells', 'wells.id', 'signs.well_id')
          ->join('locations', 'locations.id', 'wells.location_id')
          ->join('reservoirs', 'reservoirs.id', 'wells.reservoir_id')
          ->join('macollas', 'macollas.id', 'wells.macolla_id')
          ->where('signs.id', $id);

          $object = $collection->get();

          $info = $collection->first();

          $data = [];
          
          foreach($object as $key => $item) {

            $data[] = [
              0,
              date('d/m/Y', strtotime($item['sign_date'])),
              $this->replace_space($item['location_name']),
              $this->replace_space($item['well_name']),
              $this->replace_space($item['reservoir_name']),
              $this->replace_space($item['company']),
              date('H:i', strtotime($item['sign_time'])),
              $this->check_data($item['percent_mixture'], 10, 1, 'percent_mixture'),
              $this->check_data($item['percent_sediment'], 10, 1, 'percent_sediment'),
              $this->check_data($item['api_degree_diluted'], 10, 1, 'api_degree_diluted'),
              $this->check_data($item['ptb'], 10, 1, 'ptb'),
              '<div class="btn-group btn-group-sm"><button class="btn btn-info" id="btn-edit" data-toggle="modal" data-target="#modal-xl" title="Editar" value="' . $item['id'] . '"><i class="fa fa-edit"></i></button><button class="btn btn-danger" id="btn-delete" value="' . $item['id'] . '"><i class="fa fa-trash" title="Borrar"></i></button></div>'
            ];
          }

          return response()->json([
            'fail' => false,
            'data' => $data,
            'info' => $info['macolla_name'],
            'type' => $request->type,
            'message' => 'El registro ha sido actualizado exitosamente',
            'status' => 200,
          ], 200);
    }

    public function updateCondition(Request $request, $id)
    {
        $condition = Condition::findOrFail($id);

        $validator = Validator::make($request->all(), [
          'reservoir' => ['required'],
          'sand' => ['required'],
          'company' => ['required', 'string'],
          'test' => ['required'],
          'deep_md' => ['nullable', 'numeric', 'between:0,5000'],
          'deep_tvd' => ['nullable', 'numeric', 'between:0,5000'],
          'temperature' => ['nullable', 'numeric', 'between:60,125'],
          'duration' => ['nullable', 'integer', 'between:0,300'],
          'pressbefore' => ['nullable', 'numeric', 'between:0,5000'],
          'pressafter' => ['nullable', 'numeric', 'between:0,5000'],
          'pressformation' => ['nullable', 'numeric', 'between:0,5000'],
          'mobility' => ['nullable', 'numeric', 'between:0,5000']
        ]);

        if ($validator->fails()) {
          return response()->json([
            'fail' => true,
            'errors'=> $validator->errors(),
            'status' => 400,
          ], 400);
        }

        Condition::where('id', $id)->update([
          'reservoir' => $this->getReservoirName($request->reservoir)['reservoir_name'],
          'sand' => $request->sand,
          'company' => $request->company,
          'test' => $request->test,
          'deep_md' => $request->deep_md,
          'deep_tvd' => $request->deep_tvd,
          'temperature' => $request->temperature,
          'duration' => $request->duration,
          'pressbefore' => $request->pressbefore,
          'pressafter' => $request->pressafter,
          'pressformation' => $request->pressformation,
          'mobility' => $request->mobility,
          'observations' => $request->observations
        ]);

        Log::create(['user_id' => Auth::user()->id,'description' => '- Modificó un registro de PyT Original.']);

        $collection = Condition::select('conditions.id as id', 'well_name', 'pt_date', 'company', 'macolla_name', 'location_name', 'reservoir', 'sand', 'test', 'deep_md', 'deep_tvd', 'temperature', 'duration', 'pressbefore', 'pressafter', 'pressformation', 'mobility', 'observations')
        ->join('wells', 'wells.id', 'conditions.well_id')
        ->join('locations', 'locations.id', 'wells.location_id')
        ->join('reservoirs', 'reservoirs.id', 'wells.reservoir_id')
        ->join('macollas', 'macollas.id', 'wells.macolla_id')
        ->where('conditions.id', $id);

        $object = $collection->get();

        $info = $collection->first();

        $data = [];

        foreach($object as $key => $item) {

          $data[] = [
            0,
            $this->replace_space($item['macolla_name']),
            $this->replace_space($item['well_name']),
            $this->replace_space($item['location_name']),
            $this->replace_space($item['reservoir']),
            $this->replace_space($item['company']),
            date('d/m/Y', strtotime($item['pt_date'])),
            $this->format_text($item['sand'], 'sand'),
            $this->format_text($item['test'], 'test'),
            $this->check_data($item['deep_md'], 0, 0, 'deep_md'),
            $this->check_data($item['deep_tvd'], 0, 0, 'deep_tvd'),
            $this->check_data($item['temperature'], 10, 2, 'temperature'),
            $this->check_data($item['duration'], 0, 0, 'duration'),
            $this->check_data($item['pressbefore'], 0, 2, 'pressbefore'),
            $this->check_data($item['pressafter'], 0, 2, 'pressafter'),
            $this->check_data($item['pressformation'], 0, 2, 'pressformation'),
            $this->check_data($item['mobility'], 0, 2, 'mobility'),
            $this->format_text($item['observations'], 'observations'),
            '<div class="btn-group btn-group-sm"><button class="btn btn-info" id="btn-edit" data-toggle="modal" data-target="#modal-xl" title="Editar" value="'. $item['id'] .'"><i class="fa fa-edit"></i></button><button class="btn btn-danger" id="btn-delete" value="'. $item['id'] .'"><i class="fa fa-trash" title="Borrar"></i></button></div>'
          ];
        }

        return response()->json([
          'fail' => false,
          'data' => $data,
          'info' => $info['macolla_name'],
          'type' => (int) $request->type,
          'message' => 'El registro ha sido actualizado exitosamente',
          'status' => 200,
        ], 200);
    }

    public function updateHistorical(Request $request, $id)
    {
        $historical = Historical::findOrFail($id);

        $validator = Validator::make($request->all(), [
          'reservoir' => ['required'],
          'type_work_i' => ['required'],
          'type_work_ii' => ['required'],
          'rig' => ['required'],
          'sensor_brand' => ['nullable', 'string'],
          'sensor_model' => ['nullable', 'string'],
          'sensor_diameter' => ['nullable', 'numeric', 'between:0,200'],
          'deeptop_sensor_md' => ['nullable', 'numeric', 'between:0,5000'],
          'deepbottom_sensor_md' => ['nullable', 'numeric', 'between:0,5000'],
          'sensor_company' => ['nullable', 'string'],
          'pump_model' => ['required'],
          'pump_type' => ['required'],
          'pump_diameter' => ['nullable', 'numeric', 'between:0,200'],
          'pump_capacity' => ['nullable', 'numeric', 'between:0,500'],
          'pump_company' => ['required', 'string'],
          'deeptop_pump_md' => ['nullable', 'numeric', 'between:0,5000'],
          'deepbottom_pump_md' => ['nullable', 'numeric', 'between:0,5000'],
          'inyected_diluent' => ['required'],
          'observations' => ['required'],
          'measure_from' => ['required']
        ]);

        if ($validator->fails()) {
          return response()->json([
            'fail' => true,
            'errors'=> $validator->errors(),
            'status' => 400,
          ], 400);
        }

        Historical::where('id', $id)->update([
          'reservoir' => $this->getReservoirName($request->reservoir)['reservoir_name'],
          'type_work_i' => $request->type_work_i,
          'type_work_ii' => $request->type_work_ii,
          'rig' => $request->rig,
          'sensor_brand' => $request->sensor_brand,
          'sensor_model' => $request->sensor_model,
          'sensor_diameter' => $request->sensor_diameter,
          'deeptop_sensor_md' => $request->deeptop_sensor_md,
          'deepbottom_sensor_md' => $request->deepbottom_sensor_md,
          'sensor_company' => $request->sensor_company,
          'pump_model' => $request->pump_model,
          'pump_type' => $request->pump_type,
          'pump_diameter' => $request->pump_diameter,
          'pump_capacity' => $request->pump_capacity,
          'pump_company' => $request->pump_company,
          'deeptop_pump_md' => $request->deeptop_sensor_md,
          'deepbottom_pump_md' => $request->deeptop_sensor_md,
          'inyected_diluent' => $request->inyected_diluent,
          'measure_from' => $request->measure_from,
          'observations' => $request->observations,
          'references' => $request->references
        ]);

        Log::create(['user_id' => Auth::user()->id,'description' => '- Modificó un registro de Hist. de completación.']);

        $collection = Historical::select("historicals.id as id", "type_work_i", "type_work_ii", "measure_from", "rig", "sensor_model", "sensor_diameter", "sensor_brand", "sensor_company", "deeptop_sensor_md", "deepbottom_sensor_md", "pump_model", "pump_diameter", "pump_company", "deeptop_pump_md", "deepbottom_pump_md", "pump_type", "pump_capacity", "inyected_diluent", "observations", "references", "well_name", "begin_date", "end_date", "macolla_name", "location_name", "reservoir")
        ->join('wells', 'wells.id', 'historicals.well_id')
        ->join('locations', 'locations.id', 'wells.location_id')
        ->join('reservoirs', 'reservoirs.id', 'wells.reservoir_id')
        ->join('macollas', 'macollas.id', 'wells.macolla_id')
        ->where('historicals.id', $id);

        $object = $collection->get();

        $info = $collection->first();

        $data = [];

        foreach($object as $key => $item) {

          $data[] = [
            0,
            $this->replace_space($item['location_name']),
            $this->replace_space($item['well_name']),
            $this->replace_space($item['reservoir']),
            date('d/m/Y H:i', strtotime($item['begin_date'])),
            date('d/m/Y H:i', strtotime($item['end_date'])),
            $this->format_text($item['type_work_i'], 'type_work_i'),
            $this->format_text($item['type_work_ii'], 'type_work_ii'),
            $this->format_text($item['rig'], 'rig'),
            $this->format_text($item['sensor_model'], 'sensor_model'),
            $this->format_text($item['sensor_brand'], 'sensor_brand'),
            $this->decimal_to_fraction($item['sensor_diameter']),
            $this->format_text($item['sensor_company'], 'sensor_company'),
            $this->check_data($item['deeptop_sensor_md'], 10, 2, 'deeptop_sensor_md'),
            $this->check_data($item['deepbottom_sensor_md'], 10, 2, 'deepbottom_sensor_md'),
            $this->format_text($item['pump_model'], 'pump_model'),
            $this->decimal_to_fraction($item['pump_diameter']),
            $this->format_text($item['pump_type'], 'pump_type'),
            $this->check_data($item['pump_capacity'], 0, 0, 'pump_capacity'),
            $this->format_text($item['pump_company'], 'pump_company'),
            $this->check_data($item['deeptop_pump_md'], 10, 2, 'deeptop_pump_md'),
            $this->check_data($item['deepbottom_pump_md'], 10, 2, 'deepbottom_pump_md'),
            $this->format_text($item['inyected_diluent'], 'inyected_diluent'),
            $this->format_text($item['measure_from'], 'measure_from'),
            $this->format_text($item['observations'], 'observations'),
            $this->format_text($item['references'], 'references'),
            '<div class="btn-group btn-group-sm"><button class="btn btn-info" id="btn-edit" data-toggle="modal" data-target="#modal-xl" title="Editar" value="'. $item['id'] .'"><i class="fa fa-edit"></i></button><button class="btn btn-danger" id="btn-delete" value="'. $item['id'] .'"><i class="fa fa-trash" title="Borrar"></i></button></div>'
          ];
        }

        return response()->json([
          'fail' => false,
          'data' => $data,
          'info' => $info['macolla_name'],
          'type' => $request->type,
          'message' => 'El registro ha sido actualizado exitosamente',
          'status' => 200,
        ], 200);
    }

    public function updateStatic(Request $request, $id)
    {
        $static = StaticPressure::findOrFail($id);

        $validator = Validator::make($request->all(), [
            'quality' => ['required'],
            'pip' => ['nullable', 'numeric', 'between:0,1500'],
            'pdp' => ['nullable', 'numeric', 'between:0,1500'],
            'temperature' => ['nullable', 'numeric', 'between:60,125']
        ]);

        if ($validator->fails()) {
          return response()->json([
            'fail' => true,
            'errors'=> $validator->errors(),
            'status' => 400,
          ], 400);
        }

        StaticPressure::where('id', $id)->update([
          'quality' => $request->quality,
          'pip' => $request->pip,
          'pdp' => $request->pdp,
          'temperature' => $request->temperature,
          'observations' => $request->observations
        ]);

        Log::create([
          'user_id' => Auth::user()->id,
          'description' => '- Modificó un registro de Presión Estática.'
        ]);

        $collection = StaticPressure::select('static_pressures.id as id', 'static_date', 'well_name', 'quality', 'macolla_name', 'location_name', 'reservoir_name', 'pip', 'pdp', 'temperature', 'observations')
        ->join('wells', 'wells.id', 'static_pressures.well_id')
        ->join('locations', 'locations.id', 'wells.location_id')
        ->join('reservoirs', 'reservoirs.id', 'wells.reservoir_id')
        ->join('macollas', 'macollas.id', 'wells.macolla_id')
        ->where('static_pressures.id', $id);

          $object = $collection->get();

          $info = $collection->first();

          $data = [];
          
          foreach($object as $key => $item) {

            $data[] = [
              0,
              $this->replace_space($item['location_name']),
              $this->replace_space($item['well_name']),
              $this->replace_space($item['reservoir_name']),
              date('d/m/Y H:i', strtotime($item['static_date'])),
              $this->format_text($item['quality'], 'quality'),
              $this->check_data($item['pip'], 0, 0, 'pip'),
              $this->check_data($item['pdp'], 0, 0, 'pdp'),
              $this->check_data($item['temperature'], 10, 2, 'temperature'),
              $this->format_text($item['observations'], 'observations'),
              '<div class="btn-group btn-group-sm"><button class="btn btn-info" id="btn-edit" data-toggle="modal" data-target="#modal-xl" title="Editar" value="' . $item['id'] . '"><i class="fa fa-edit"></i></button><button class="btn btn-danger" id="btn-delete" value="' . $item['id'] . '"><i class="fa fa-trash" title="Borrar"></i></button></div>'
            ];
          }

          return response()->json([
            'fail' => false,
            'data' => $data,
            'info' => $info['macolla_name'],
            'message' => 'El registro ha sido actualizado exitosamente',
            'status' => 200,
          ], 200);
    }

    public function updateDynamic(Request $request, $id)
    {
        $dynamic = DynamicPressure::findOrFail($id);

        $validator = Validator::make($request->all(), [
            'quality' => ['required'],
            'pip' => ['nullable', 'numeric', 'between:0,1500'],
            'pdp' => ['nullable', 'numeric', 'between:0,1500'],
            'temperature' => ['nullable', 'numeric', 'between:60,125']
        ]);

        if ($validator->fails()) {
          return response()->json([
            'fail' => true,
            'errors'=> $validator->errors(),
            'status' => 400,
          ], 400);
        }

        DynamicPressure::where('id', $id)->update([
          'quality' => $request->quality,
          'pip' => $request->pip,
          'pdp' => $request->pdp,
          'temperature' => $request->temperature,
          'observations' => $request->observations
        ]);

        Log::create([
          'user_id' => Auth::user()->id,
          'description' => '- Modificó un registro de Presión Dinámica.'
        ]);

        $collection = DynamicPressure::select('dynamic_pressures.id as id', 'dynamic_date', 'well_name', 'quality', 'macolla_name', 'location_name', 'reservoir_name', 'pip', 'pdp', 'temperature', 'observations')
        ->join('wells', 'wells.id', 'dynamic_pressures.well_id')
        ->join('locations', 'locations.id', 'wells.location_id')
        ->join('reservoirs', 'reservoirs.id', 'wells.reservoir_id')
        ->join('macollas', 'macollas.id', 'wells.macolla_id')
        ->where('dynamic_pressures.id', $id);

          $object = $collection->get();

          $info = $collection->first();

          $data = [];
          
          foreach($object as $key => $item) {

            $data[] = [
              0,
              $this->replace_space($item['location_name']),
              $this->replace_space($item['well_name']),
              $this->replace_space($item['reservoir_name']),
              date('d/m/Y H:i', strtotime($item['dynamic_date'])),
              $this->format_text($item['quality'], 'quality'),
              $this->check_data($item['pip'], 0, 0, 'pip'),
              $this->check_data($item['pdp'], 0, 0, 'pdp'),
              $this->check_data($item['temperature'], 10, 2, 'temperature'),
              $this->format_text($item['observations'], 'observations'),
              '<div class="btn-group btn-group-sm"><button class="btn btn-info" id="btn-edit" data-toggle="modal" data-target="#modal-xl" title="Editar" value="' . $item['id'] . '"><i class="fa fa-edit"></i></button><button class="btn btn-danger" id="btn-delete" value="' . $item['id'] . '"><i class="fa fa-trash" title="Borrar"></i></button></div>'
            ];
          }

          return response()->json([
            'fail' => false,
            'data' => $data,
            'info' => $info['macolla_name'],
            'message' => 'El registro ha sido actualizado exitosamente',
            'status' => 200,
          ], 200);
    }

    public function updateOriginal(Request $request, $id)
    {
        $original = OriginalPressure::findOrFail($id);

        $validator = Validator::make($request->all(), [
          'reservoir' => ['required'],
          'type_test' => ['required'],
          'sensor_model' => ['required'],
          'pressure_gradient' => ['nullable', 'numeric', 'between:0,1'],
          'temperature_gradient' => ['nullable', 'numeric', 'between:0,1'],
          'deepdatum_mc_md' => ['nullable', 'numeric', 'between:0,1500'],
          'deepdatum_mc_tvd' => ['nullable', 'numeric', 'between:0,1500'],
          'press_datum_macolla' => ['nullable', 'numeric', 'between:14,600'],
          'temp_datum_macolla' => ['nullable', 'numeric', 'between:60,125']
        ]);

        if ($validator->fails()) {
          return response()->json([
            'fail' => true,
            'errors'=> $validator->errors(),
            'status' => 400,
          ], 400);
        }

        OriginalPressure::where('id', $id)->update([
          'reservoir' => $request->reservoir,
          'type_test' => $request->type_test,
          'sensor_model' => $request->sensor_model,
          'pressure_gradient' => $request->pressure_gradient,
          'temperature_gradient' => $request->temperature_gradient,
          'deepdatum_mc_md' => $request->deepdatum_mc_md,
          'deepdatum_mc_tvd' => $request->deepdatum_mc_tvd,
          'press_datum_macolla' => $request->press_datum_macolla,
          'temp_datum_macolla' => $request->temp_datum_macolla
        ]);

        Log::create([
          'user_id' => Auth::user()->id,
          'description' => '- Modificó un registro de Presión Original PMVM.'
        ]);

        $collection = OriginalPressure::select('original_pressures.id as id', 'pressure_date', 'well_name', 'macolla_name', 'location_name', 'type_test', 'sensor_model', 'reservoir', 'pressure_gradient', 'deepdatum_mc_md', 'deepdatum_mc_tvd', 'press_datum_macolla', 'temp_datum_macolla', 'temperature_gradient')
        ->join('wells', 'wells.id', 'original_pressures.well_id')
        ->join('locations', 'locations.id', 'wells.location_id')
        ->join('macollas', 'macollas.id', 'wells.macolla_id')
        ->where('original_pressures.id', $id);

          $object = $collection->get();

          $info = $collection->first();

          $data = [];
          
          foreach($object as $key => $item) {

            $data[] = [
              0,
              $this->replace_space($item['macolla_name']),
              $this->replace_space($item['location_name']),
              $this->replace_space($item['well_name']),
              $this->replace_space($item['reservoir']),
              date('d/m/Y', strtotime($item['pressure_date'])),
              $this->format_text($item['type_test'], 'type_test'),
              $this->format_text($item['sensor_model'], 'sensor_model'),
              $this->check_data($item['pressure_gradient'], 1, 3, 'pressure_gradient'),
              $this->check_data($item['deepdatum_mc_md'], 0, 0, 'deepdatum_mc_md'),
              $this->check_data($item['deepdatum_mc_tvd'], 0, 0, 'deepdatum_mc_tvd'),
              $this->check_data($item['press_datum_macolla'], 0, 0, 'press_datum_macolla'),
              $this->check_data($item['temp_datum_macolla'], 0, 1, 'temp_datum_macolla'),
              $this->check_data($item['temperature_gradient'], 1, 3, 'temperature_gradient'),
              '<div class="btn-group btn-group-sm"><button class="btn btn-info" id="btn-edit" data-toggle="modal" data-target="#modal-xl" title="Editar" value="' . $item['id'] . '"><i class="fa fa-edit"></i></button><button class="btn btn-danger" id="btn-delete" value="' . $item['id'] . '"><i class="fa fa-trash" title="Borrar"></i></button></div>'
            ];
          }

          return response()->json([
            'fail' => false,
            'data' => $data,
            'info' => $info['macolla_name'],
            'message' => 'El registro ha sido actualizado exitosamente',
            'status' => 200,
          ], 200);
    }

    public function updateReal(Request $request, $id)
    {
        $real = RealSurvey::findOrFail($id);

        $validator = Validator::make($request->all(), [
          'hole' => ['required'],
          'company' => ['required', 'regex:/^([ a-zA-Z0-9,.]+$)+/'],
          'md' => ['nullable', 'numeric', 'between:0,1500'],
          'inclination' => ['nullable', 'numeric', 'between:0,1500'],
          'azim_grid' => ['nullable', 'numeric', 'between:0,1500'],
          'tvd' => ['nullable', 'numeric', 'between:0,1500'],
          'vsec' => ['nullable', 'numeric', 'between:0,1500'],
          'dls' => ['nullable', 'numeric', 'max:1500'],
          'ns' => ['nullable', 'numeric', 'max:1500'],
          'ew' => ['nullable', 'numeric', 'max:1500'],
          'la_g' => ['nullable', 'numeric', 'max:200'],
          'la_m' => ['nullable', 'numeric', 'max:200'],
          'la_s' => ['nullable', 'numeric', 'max:200'],
          'lo_g' => ['nullable', 'numeric', 'max:200'],
          'lo_m' => ['nullable', 'numeric', 'max:200'],
          'lo_s' => ['nullable', 'numeric', 'max:200'],
          'observations' => ['required', 'string']
        ]);

        if ($validator->fails()) {
          return response()->json([
            'fail' => true,
            'errors'=> $validator->errors(),
            'status' => 400,
          ], 400);
        }

        RealSurvey::where('id', $id)->update([
          'hole' => $request->hole,
          'company' => $request->company,
          'md' => $request->md,
          'inclination' => $request->inclination,
          'azim_grid' => $request->azim_grid,
          'tvd' => $request->tvd,
          'vsec' => $request->vsec,
          'dls' => $request->dls,
          'ns' => $request->ns,
          'ew' => $request->ew,
          'la_g' => $request->la_g,
          'la_m' => $request->la_m,
          'la_s' => $request->la_s,
          'lo_g' => $request->lo_g,
          'lo_m' => $request->lo_m,
          'lo_s' => $request->lo_s,
          'observations' => $request->observations
        ]);

        Log::create([
          'user_id' => Auth::user()->id,
          'description' => '- Modificó un registro de Survey Real.'
        ]);

        $collection = RealSurvey::selectRaw('`real_surveys`.`id` as `id`, `rs_date`, `well_name`, `macolla_name`, `location_name`, `reservoir_name`, `hole`, `company`, `md`, `inclination`, `azim_grid`, `tvd`, `vsec`, `ns`, `ew`, `dls`, IF((la_g = 0 AND la_m = 0 AND la_s = 0) OR (la_g = NULL OR la_m = NULL OR la_s = NULL), NULL, CONCAT(la_g, \' \', la_m, \' \', la_s)) AS `latitude`, IF((lo_g <= 0 AND lo_m <= 0 AND lo_s <= 0) OR (lo_g = NULL OR lo_m = NULL OR lo_s = NULL), NULL, CONCAT(lo_g, \' \', lo_m, \' \', lo_s)) as `longitude`, `observations`, `la_g`, `la_m`, `la_s`, `lo_g`, `lo_m`, `lo_s`')
        ->join('wells', 'wells.id', 'real_surveys.well_id')
        ->join('locations', 'locations.id', 'wells.location_id')
        ->join('macollas', 'macollas.id', 'wells.macolla_id')
        ->join('reservoirs', 'reservoirs.id', 'wells.reservoir_id')
        ->where('real_surveys.id', $id);

          $object = $collection->get();

          $info = $collection->first();

          $data = [];
          
          foreach($object as $key => $item) {

            $data[] = [
              0,
              $this->replace_space($item['macolla_name']),
              $this->replace_space($item['well_name']),
              $this->replace_space($item['location_name']),
              $this->replace_space($item['reservoir_name']),
              date('d/m/Y', strtotime($item['rs_date'])),
              $this->replace_space($item['company']),
              $this->format_text($item['hole'], 'hole'),
              $this->check_data($item['md'], 0, 2, 'md'),
              $this->check_data($item['inclination'], 0, 2, 'inclination'),
              $this->check_data($item['azim_grid'], 0, 2, 'azim_grid'),
              $this->check_data($item['tvd'], 0, 2, 'tvd'),
              $this->check_data($item['dls'], 100, 2, 'dls'),
              $this->check_data($item['vsec'], 0, 2, 'vsec'),
              $this->check_data($item['ns'], 0, 2,'ns'),
              $this->check_data($item['ew'], 0, 2, 'ew'),
              $this->format_text($item['latitude'], 'latitude'),
              $this->format_text($item['longitude'], 'longitude'),
              $this->format_text($item['observations'], 'observations'),
              '<div class="btn-group btn-group-sm"><button class="btn btn-info" id="btn-edit" data-toggle="modal" data-target="#modal-xl" title="Editar" value="'. $item['id'] .'"><i class="fa fa-edit"></i></button><button class="btn btn-danger" id="btn-delete" title="Borrar" value="'. $item['id'] .'"><i class="fa fa-trash"></i></button></div>',
              $this->setUndefined($item['la_g']),
              $this->setUndefined($item['la_m']),
              $this->setUndefined($item['la_s']),
              $this->setUndefined($item['lo_g']),
              $this->setUndefined($item['lo_m']),
              $this->setUndefined($item['lo_s'])
              ];
          }

          return response()->json([
            'fail' => false,
            'data' => $data,
            'info' => $info['macolla_name'],
            'message' => 'El registro ha sido actualizado exitosamente',
            'status' => 200,
          ], 200);
    }

    public function updatePlan(Request $request, $id)
    {
        $plan = PlanSurvey::findOrFail($id);

        $validator = Validator::make($request->all(), [
          'md' => ['nullable', 'numeric', 'between:0,1500'],
          'inclination' => ['nullable', 'numeric', 'between:0,1500'],
          'azim_grid' => ['nullable', 'numeric', 'between:0,1500'],
          'tvd' => ['nullable', 'numeric', 'between:0,1500'],
          'vsec' => ['nullable', 'numeric', 'between:0,1500'],
          'dls' => ['nullable', 'numeric', 'max:1500'],
          'ns' => ['nullable', 'numeric', 'max:1500'],
          'ew' => ['nullable', 'numeric', 'max:1500'],
          'la_g' => ['nullable', 'numeric', 'max:200'],
          'la_m' => ['nullable', 'numeric', 'max:200'],
          'la_s' => ['nullable', 'numeric', 'max:200'],
          'lo_g' => ['nullable', 'numeric', 'max:200'],
          'lo_m' => ['nullable', 'numeric', 'max:200'],
          'lo_s' => ['nullable', 'numeric', 'max:200'],
          'observations' => ['required', 'string']
        ]);

        if ($validator->fails()) {
          return response()->json([
            'fail' => true,
            'errors'=> $validator->errors(),
            'status' => 400,
          ], 400);
        }

        PlanSurvey::where('id', $id)->update([
          'md' => $request->md,
          'inclination' => $request->inclination,
          'azim_grid' => $request->azim_grid,
          'tvd' => $request->tvd,
          'vsec' => $request->vsec,
          'dls' => $request->dls,
          'ns' => $request->ns,
          'ew' => $request->ew,
          'la_g' => $request->la_g,
          'la_m' => $request->la_m,
          'la_s' => $request->la_s,
          'lo_g' => $request->lo_g,
          'lo_m' => $request->lo_m,
          'lo_s' => $request->lo_s,
          'observations' => $request->observations
        ]);

        Log::create([
          'user_id' => Auth::user()->id,
          'description' => '- Modificó un registro de Survey Plan.'
        ]);

        $collection = PlanSurvey::selectRaw('`plan_surveys`.`id` as `id`, `ps_date`, `well_name`, `macolla_name`, `location_name`, `reservoir_name`, `md`, `inclination`, `azim_grid`, `tvd`, `vsec`, `ns`, `ew`, `dls`, IF((la_g = 0 AND la_m = 0 AND la_s = 0) OR (la_g = NULL OR la_m = NULL OR la_s = NULL), NULL, CONCAT(la_g, \' \', la_m, \' \', la_s)) AS `latitude`, IF((lo_g <= 0 AND lo_m <= 0 AND lo_s <= 0) OR (lo_g = NULL OR lo_m = NULL OR lo_s = NULL), NULL, CONCAT(lo_g, \' \', lo_m, \' \', lo_s)) as `longitude`, `observations`, `la_g`, `la_m`, `la_s`, `lo_g`, `lo_m`, `lo_s`')
        ->join('wells', 'wells.id', 'plan_surveys.well_id')
        ->join('locations', 'locations.id', 'wells.location_id')
        ->join('macollas', 'macollas.id', 'wells.macolla_id')
        ->join('reservoirs', 'reservoirs.id', 'wells.reservoir_id')
        ->where('plan_surveys.id', $id);

          $object = $collection->get();

          $info = $collection->first();

          $data = [];
          
          foreach($object as $key => $item) {

            $data[] = [
              0,
              $this->replace_space($item['macolla_name']),
              $this->replace_space($item['well_name']),
              $this->replace_space($item['location_name']),
              $this->replace_space($item['reservoir_name']),
              date('d/m/Y', strtotime($item['rs_date'])),
              $this->check_data($item['md'], 0, 2, 'md'),
              $this->check_data($item['inclination'], 0, 2, 'inclination'),
              $this->check_data($item['azim_grid'], 0, 2, 'azim_grid'),
              $this->check_data($item['tvd'], 0, 2, 'tvd'),
              $this->check_data($item['dls'], 100, 2, 'dls'),
              $this->check_data($item['vsec'], 0, 2, 'vsec'),
              $this->check_data($item['ns'], 0, 2,'ns'),
              $this->check_data($item['ew'], 0, 2, 'ew'),
              $this->format_text($item['latitude'], 'latitude'),
              $this->format_text($item['longitude'], 'longitude'),
              $this->format_text($item['observations'], 'observations'),
              '<div class="btn-group btn-group-sm"><button class="btn btn-info" id="btn-edit" data-toggle="modal" data-target="#modal-xl" title="Editar" value="'. $item['id'] .'"><i class="fa fa-edit"></i></button><button class="btn btn-danger" id="btn-delete" title="Borrar" value="'. $item['id'] .'"><i class="fa fa-trash"></i></button></div>',
              $this->setUndefined($item['la_g']),
              $this->setUndefined($item['la_m']),
              $this->setUndefined($item['la_s']),
              $this->setUndefined($item['lo_g']),
              $this->setUndefined($item['lo_m']),
              $this->setUndefined($item['lo_s'])
              ];
          }

          return response()->json([
            'fail' => false,
            'data' => $data,
            'info' => $info['macolla_name'],
            'message' => 'El registro ha sido actualizado exitosamente',
            'status' => 200,
          ], 200);
    }

    public function updateFiscalized(Request $request, $id)
    {
        $production = Production::findOrFail($id);

        $validator = Validator::make($request->all(), [
            'production' => ['required', 'numeric', 'between:0,3000'],
          ]);
  
          if ($validator->fails()) {
            return response()->json([
              'fail' => true,
              'errors'=> $validator->errors(),
              'status' => 400,
            ], 400);
          }

          Production::where('id', $id)->update([
            'production' => $request->production
          ]);
          
          Log::create([
            'user_id' => Auth::user()->id,
            'description' => '- Modificó un registro de producción fiscalizada.'
          ]);

          $collection = Production::select('macolla_id', 'productions.id as id', 'fiscalized_date', 'production')->join('macollas', 'macollas.id', '=', 'productions.macolla_id')
          ->where('productions.id', ((int) $request->id))
          ->orderBy('fiscalized_date', 'ASC');

          $object = $collection->get();

          $info = $collection->first();

          $data = [];
          
          foreach($object as $key => $item) {

            $data[] = [
              0,
              date('d/m/Y', strtotime($item['fiscalized_date'])),
              $this->format_limit($item['production'], 10, 1),
              '-',
              '-',
              '-',
              '-',
              '-',
              '<div class="btn-group btn-group-sm"><button class="btn btn-info" id="btn-edit" data-toggle="modal" data-target="#modal-xl" title="Editar" value="'. $item['id'] .'"><i class="fa fa-edit"></i></button><button class="btn btn-danger" id="btn-delete" value="'. $item['id'] .'"><i class="fa fa-trash" title="Borrar"></i></button></div>'
            ];
          }

          return response()->json([
            'fail' => false,
            'data' => $data,
            'info' => $info['macolla_name'],
            'type' => $request->type,
            'message' => 'El registro ha sido actualizado exitosamente',
            'status' => 200,
          ], 200);
    }

    public function updateEvents(Request $request, $id)
    {
        $event = Event::findOrFail($id);

        $validator = Validator::make($request->all(), [
            'event_date' => ['required', 'date'],
            'event_time' => ['required', 'date_format:H:i'],
            'diluent' => ['nullable', 'numeric', 'between:0,500'],
            'rpm' => ['nullable', 'numeric', 'between:0,3000'],
            'pump_efficiency' => ['nullable', 'numeric', 'between:0,1500'],
            'torque' => ['nullable', 'numeric', 'between:0,1500'],
            'frequency' => ['nullable', 'numeric', 'between:0,1500'],
            'current' => ['nullable', 'numeric', 'between:0,1500'],
            'power' => ['nullable', 'numeric', 'between:0,1500'],
            'mains_voltage' => ['nullable', 'numeric', 'between:0,1500'],
            'output_voltage' => ['nullable', 'numeric', 'between:0,1500'],
            'vfd_temperature' => ['nullable', 'numeric', 'between:0,130'],
            'head_temperature' => ['nullable', 'numeric', 'between:0,130'],
            'head_pressure' => ['nullable', 'numeric', 'between:0,1500']
          ]);
  
          if ($validator->fails()) {
            return response()->json([
              'fail' => true,
              'errors'=> $validator->errors(),
              'status' => 400,
            ], 400);
          }

          Event::where('id', $id)->update([
            'event_date' => $request->event_date,
            'event_time' => $request->event_time,
            'diluent' => $request->diluent,
            'rpm' => $request->rpm,
            'pump_efficiency' => $request->pump_efficiency,
            'torque' => $request->torque,
            'frequency' => $request->frequency,
            'current' => $request->current,
            'power' => $request->power,
            'mains_voltage' => $request->mains_voltage,
            'output_voltage' => $request->output_voltage,
            'vfd_temperature' => $request->vfd_temperature,
            'head_temperature' => $request->head_temperature,
            'head_pressure' => $request->head_pressure,
            'observations' => $request->observations
          ]);
          
          Log::create([
            'user_id' => Auth::user()->id,
            'description' => '- Modificó un registro de eventos de pozo.'
          ]);

          $collection = Event::select('events.id as id', 'well_name', 'location_name', 'reservoir_name', 'event_date', 'event_time', 'diluent', 'rpm', 'pump_efficiency', 'torque', 'frequency', 'current', 'power', 'mains_voltage', 'output_voltage', 'vfd_temperature', 'head_temperature', 'head_pressure', 'observations')
          ->join('wells', 'wells.id', '=', 'events.well_id')
          ->join('locations', 'locations.id', 'wells.location_id')
          ->join('reservoirs', 'reservoirs.id', 'wells.reservoir_id')
          ->join('macollas', 'macollas.id', 'wells.macolla_id')
          ->where('events.id', $id);

          $object = $collection->get();

          $info = $collection->first();

          $data = [];
          
          foreach($object as $key => $item) {

            $data[] = [
              0,
              $this->replace_space($item['location_name']),
              $this->replace_space($item['well_name']),
              $this->replace_space($item['reservoir_name']),
              date('d/m/Y', strtotime($item['event_date'])),
              date('H:i', strtotime($item['event_time'])),
              $this->check_data($item['diluent'], 0, 0, 'diluent'),
              $this->check_data($item['rpm'], 0, 0, 'rpm'),
              $this->check_data($item['pump_efficiency'], 100, 2, 'pump_efficiency'),
              $this->check_data($item['torque'], 0, 2, 'torque'),
              $this->check_data($item['frequency'], 0, 2, 'frequency'),
              $this->check_data($item['current'], 0, 2, 'current'),
              $this->check_data($item['power'], 0, 2, 'power'),
              $this->check_data($item['mains_voltage'], 0, 2, 'mains_voltage'),
              $this->check_data($item['output_voltage'], 0, 2, 'output_voltage'),
              $this->check_data($item['vfd_temperature'], 0, 2, 'vfd_temperature'),
              $this->check_data($item['head_temperature'], 0, 2, 'head_temperature'),
              $this->check_data($item['head_pressure'], 0, 2, 'head_pressure'),
              $this->format_text($item['observations'], 'observations'),
              '<div class="btn-group btn-group-sm"><button class="btn btn-info" id="btn-edit" data-toggle="modal" data-target="#modal-xl" title="Editar" value="'. $item['id'] .'"><i class="fa fa-edit"></i></button><button class="btn btn-danger" id="btn-delete" value="'. $item['id'] .'"><i class="fa fa-trash" title="Borrar"></i></button></div>'
            ];
          }

          return response()->json([
            'fail' => false,
            'data' => $data,
            'info' => $info['macolla_name'],
            'type' => $request->type,
            'message' => 'El registro ha sido actualizado exitosamente',
            'status' => 200,
          ], 200);
    }

    public function deleteExtended($id)
    {
        $test = Test::findOrfail($id);
        
        if($test->delete()) {
            
            Log::create([
                'user_id' => Auth::user()->id,
                'description' => '- Eliminó un registro de Prueba Extendida.'
            ]);

            return response()->json([
                'fail' => false,
                'status' => 200,
            ], 200);
        }
    }

    public function deleteAverage($id)
    {
        $average = Average::findOrfail($id);
        
        if($average->delete()) {
            
            Log::create([
                'user_id' => Auth::user()->id,
                'description' => '- Eliminó un registro de Prueba Promedio.'
            ]);

            return response()->json([
                'fail' => false,
                'status' => 200,
            ], 200);
        }
    }

    public function deleteSign($id)
    {
        $sign = Sign::findOrfail($id);
        
        if($sign->delete()) {
            
            Log::create([
                'user_id' => Auth::user()->id,
                'description' => '- Eliminó un registro de Muestras.'
            ]);

            return response()->json([
                'fail' => false,
                'status' => 200,
            ], 200);
        }
    }

    public function deleteCondition($id)
    {
        $condition = Condition::findOrfail($id);
        
        if($condition->delete()) {
            
            Log::create(['user_id' => Auth::user()->id,'description' => '- Eliminó un registro de PyT Original.']);

            return response()->json([
                'fail' => false,
                'status' => 200,
            ], 200);
        }
    }

    public function deleteHistorical($id)
    {
        $historical = Historical::findOrfail($id);
        
        if($historical->delete()) {
            
            Log::create(['user_id' => Auth::user()->id,'description' => '- Eliminó un registro de Hist. de completación.']);

            return response()->json([
                'fail' => false,
                'status' => 200,
            ], 200);
        }
    }

    public function deleteStatic($id)
    {
        $static = StaticPressure::findOrfail($id);
        
        if($static->delete()) {
            
            Log::create([
                'user_id' => Auth::user()->id,
                'description' => '- Eliminó un registro de Presión Estática.'
            ]);

            return response()->json([
                'fail' => false,
                'status' => 200,
            ], 200);
        }
    }

    public function deleteDynamic($id)
    {
        $dynamic = DynamicPressure::findOrfail($id);
        
        if($dynamic->delete()) {
            
            Log::create([
                'user_id' => Auth::user()->id,
                'description' => '- Eliminó un registro de Presión Dinámica.'
            ]);

            return response()->json([
                'fail' => false,
                'status' => 200,
            ], 200);
        }
    }

    public function deleteOriginal($id)
    {
        $original = OriginalPressure::findOrfail($id);
        
        if($original->delete()) {
            
            Log::create([
                'user_id' => Auth::user()->id,
                'description' => '- Eliminó un registro de Presión Original PMVM.'
            ]);

            return response()->json([
                'fail' => false,
                'status' => 200,
            ], 200);
        }
    }

    public function deleteReal($id)
    {
        $real = RealSurvey::findOrfail($id);
        
        if($real->delete()) {
            
            Log::create([
                'user_id' => Auth::user()->id,
                'description' => '- Eliminó un registro de Survey Real.'
            ]);

            return response()->json([
                'fail' => false,
                'status' => 200,
            ], 200);
        }
    }

    public function deletePlan($id)
    {
        $plan = PlanSurvey::findOrfail($id);
        
        if($plan->delete()) {
            
            Log::create([
                'user_id' => Auth::user()->id,
                'description' => '- Eliminó un registro de Survey Plan.'
            ]);

            return response()->json([
                'fail' => false,
                'status' => 200,
            ], 200);
        }
    }

    public function deleteFiscalized($id)
    {
        $production = Production::findOrfail($id);
        
        if($production->delete()) {
            
            Log::create([
                'user_id' => Auth::user()->id,
                'description' => '- Eliminó un registro de producción fiscalizada.'
            ]);

            return response()->json([
                'fail' => false,
                'status' => 200,
            ], 200);
        }
    }

    public function deleteEvents($id)
    {
        $events = Event::findOrfail($id);
        
        if($events->delete()) {
            
            Log::create([
                'user_id' => Auth::user()->id,
                'description' => '- Eliminó un registro de Eventos.'
            ]);

            return response()->json([
                'fail' => false,
                'status' => 200,
            ], 200);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function format_limit($number, $limit, $decimal = 0)
    {
      if ($number == -1 || $number == -0.01) return '-';
      else return !is_null($number) ? ($number < $limit && $number != 0) ? number_format($number, $decimal, ',', '.') : number_format($number, 0, ',', '') : null;
    }

    public function diff($start, $end)
    {
      return date("H:i", strtotime("00:00") + strtotime($end) - strtotime($start));
    }

    public function getReservoirName($id)
    {
      return Reservoir::select('reservoir_name')->where('id', $id)->first();
    }

    public function replace_space($str)
    {
      return str_replace(' ', '&nbsp;', $str);
    }

    public function decimal_to_fraction($float)
    {
        if (is_numeric($float) && !is_null($float)) {
            $whole = floor($float);
            $decimal = $float-$whole;
            $leastCommonDenom = 48;
            $denominators = array(2, 3, 4, 8, 16, 24, 48);
            $roundDecimal = round($decimal * $leastCommonDenom) / $leastCommonDenom;

            if ($roundDecimal == 0) return $whole;
            else if ($roundDecimal == 1) return $whole + 1;

            foreach ($denominators as $d) {
              if ($roundDecimal * $d === floor($roundDecimal * $d)) {
                $denom = $d;
                break;
              }
            }

            return ($whole == 0 ? '' : $whole) . " " . ($roundDecimal * $denom) . "/". $denom;

        } else {
          return '-';
        }
    }

    public function check_data($value, $limit, $decimals, $key)
    {
        if ($key == "gvf") $value /= 100;
        else if ($key == "pump_capacity") $value *= 100;
        return isset($value) ? $this->format_limit($value, $limit, $decimals) : '-';
    }

    public function format_text($str)
    {
      return (is_null($str)) ? '-' : str_replace(' ', '&nbsp;', $str);
        
    }

    public function setUndefined($str)
    {
        return (is_null($str) ? 'undefined' : $str);
    }
}
