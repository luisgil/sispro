<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\{Macolla, Camp, Location, Block, Log};
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\{Hash, Validator, Auth};

class MacollaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Auth::user()->role == "ADMIN" || Auth::user()->role == "OPERATOR") {
            $collection = Macolla::where('macolla_name', '<>', 'SIN MACOLLA')->orderBy('id', 'asc')->get();
            $item = Block::first();
            return view('layouts.management.macollas', ['collection' => $collection, 'item' => $item]);
        } else {
            return redirect()->route('home');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }
    
    public function load(Request $request)
    {
        /** get all macollas */
        $collection = Macolla::orderBy('id', 'asc')->get();

        /** get macollas with names */
        $collection2 = Macolla::where('id', '<>', '6')->get();

        /** get macollas for list */
        $collection3 = Macolla::select('macolla_name', 'macollas.id as macolla_id', 'camp_name', 'camps.id as camp_id')
        ->join('camps', 'camps.id', 'macollas.camp_id')
        ->where('macollas.id', '<>', 6)->orderBy('macollas.id', 'asc')->get();

        return response()->json([
            'fail' => false,
            'data' => $collection,
            '_data' => $collection2,
            'list' => $collection3,
            'e' => Macolla::where('id', 6)->first(),
            'status' => 200,
        ], 200);
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {       		
        $validator = Validator::make($request->all(), [
            'macolla_name' => ['required', 'regex:/^([a-zA-Z0-9 ]+$)+/'],
            'camp_name' => ['required']
        ]);
        
        if ($validator->fails()) {
            return response()->json([
                'fail' => true,
                'errors'=> $validator->errors(),
                'status' => 400,
            ], 400);
        }   
        
        $macolla = new Macolla();
        $macolla->fill($request->all());
        \DB::transaction(function () use($macolla, $request) {
            $macolla->camp_id = $request->camp_name;
            $macolla->macolla_name = strtoupper($request->macolla_name);
            $macolla->save();
        });
        
        Log::create([
            'user_id' => Auth::user()->id,
            'description' => '- Creó la macolla '.$request->macolla_name
        ]);

        return response()->json([
            'fail' => false,
            'message' => 'Registro de macolla realizado exitosamente.',
            'data' => $macolla,
            'status' => 200,
        ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'macolla_name' => ['required', 'regex:/^([a-zA-Z0-9 ]+$)+/', 'min:3', 'unique:macollas']
        ]);

        if ($validator->fails()) {
            return response()->json([
                'fail' => true,
                'errors'=> $validator->errors(),
                'status' => 400,
            ], 400);
        }  
        
        $collection = Macolla::all();
    		
    		$camps = Camp::all();
    		return response()->json([
    			'fail' => false,
    			'data' => $collection,
    			'camps' => $camps,
    			'macolla' => strtoupper($request->macolla_name),
    			'status' => 200,
    		], 200);
    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $macolla = Macolla::findOrFail($id);
        
        $validator = Validator::make($request->all(), [
            'update_macolla_name' => ['required', 'regex:/^([a-zA-Z0-9 ]+$)+/', 'unique:macollas,macolla_name,'.$id],
        ]);
            
        if ($validator->fails()) {
            return response()->json([
                'fail' => true,
                'm' => $macolla,
                'errors'=> $validator->errors(),
                'status' => 400,
            ], 400);
        }

        \DB::transaction(function () use($macolla, $request) {
            $macolla->camp_id = $request->camp_name;
            $macolla->macolla_name = $request->update_macolla_name;
            $macolla->save();
        });

        $collection = Macolla::select('macolla_name', 'macollas.id as macolla_id', 'camp_name', 'camps.id as camp_id')
        ->join('camps', 'camps.id', 'macollas.camp_id')
        ->where('macollas.id', $id)->first();

        Log::create([
            'user_id' => Auth::user()->id,
            'description' => '- Modificó la información de la macolla '.$macolla->macolla_name
        ]);

        return response()->json([
            'fail' => false,
            'data' => $collection,
            'status' => 200,
        ], 200); 
    }

    public function setNewNames($str)
    {
        return isset(explode('-',$str)[1]) ? explode('-',$str)[1] : $str;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $macolla = Macolla::findOrfail($id);
        
        if($macolla->delete()) {
            
            Log::create([
                'user_id' => Auth::user()->id,
                'description' => '- Eliminó la macolla '.$macolla->macolla_name
            ]);

            return response()->json([
                'fail' => false,
                'status' => 200,
            ], 200);
        }
    }
}
