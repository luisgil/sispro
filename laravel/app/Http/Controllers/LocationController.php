<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\{Macolla, Camp, Location, Test};
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;

class LocationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }
    
    public function load(Request $request)
    {
        $collection = Location::join('macollas', 'locations.macolla_id', '=', 'macollas.id')
        ->join('wells', 'locations.id', '=', 'wells.location_id')
        ->join('reservoirs', 'wells.reservoir_id', '=', 'reservoirs.id')
        ->select('wells.well_name', 'wells.id as well_id', 'locations.location_name', 'locations.id as location_id', 'reservoirs.reservoir_name', 'reservoirs.id as oilfield_id')
        ->where('locations.macolla_id', '=', $request->macollaId)
        ->where('wells.classification', '<>', 'OBSERVADOR')
        ->orderBy('locations.location_name', 'asc')
        ->get();
        
        $withObservator = Location::join('macollas', 'locations.macolla_id', '=', 'macollas.id')
        ->join('wells', 'locations.id', '=', 'wells.location_id')
        ->join('reservoirs', 'wells.reservoir_id', '=', 'reservoirs.id')
        ->select('wells.well_name', 'wells.id as well_id', 'locations.location_name', 'locations.id as location_id', 'reservoirs.reservoir_name', 'reservoirs.id as oilfield_id')
        ->where(function ($query) {
            $query->where('wells.classification', '=', 'OBSERVADOR')
            ->orWhere('wells.classification', '=', 'ESTRATIGRÁFICO');
        })
        ->where('wells.condition', '<>', 'ABANDONADO')
        ->where('locations.macolla_id', '=', $request->macollaId)
        ->orderBy('locations.location_name', 'asc')
        ->get();
        
        $getAll = Location::join('macollas', 'locations.macolla_id', '=', 'macollas.id')
        ->join('wells', 'locations.id', '=', 'wells.location_id')
        ->join('reservoirs', 'wells.reservoir_id', '=', 'reservoirs.id')
        ->select('wells.well_name', 'wells.id as well_id', 'locations.location_name', 'locations.id as location_id', 'reservoirs.reservoir_name', 'reservoirs.id as oilfield_id')
        ->where('locations.macolla_id', '=', $request->macollaId)
        ->where('wells.condition', '<>', 'ABANDONADO')
        ->orderBy('locations.location_name', 'asc')
        ->get();
        									 																						
        return response()->json([
            'fail' => false,
            'data' => $collection,
            '_data' => $withObservator,
            'all' => $getAll,
            'status' => 200,
        ], 200);
    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       /*$location = Location::findOrFail($id);
    		$id = $request->identity;
    		
        $validator = Validator::make($request->all(), [
            'update_location_name' => ['required', 'regex:/^([A-Z0-9-]+$)+/', 'min:5', 'unique:locations,location_name,'.$id],
        ]);
        
        if ($validator->fails()) {
            return response()->json([
                'fail' => true,
                'location' => $location,
                'errors'=> $validator->errors(),
                'status' => 400,
            ], 400);
        }

        \DB::transaction(function () use($location, $request) {
        		$location->location_name = $request->update_location_name;
            $location->save();
            
        });
        
        return response()->json([
            'fail' => false,
            'status' => 200,
        ], 200);*/
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
