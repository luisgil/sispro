<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\{Log, User, Profile};

class LogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $collection = Log::select('firstname', 'lastname', 'email', 'description', 'role', 'logs.created_at as log_from')
        ->join('users', 'users.id', 'logs.user_id')
        ->join('profiles', 'profiles.user_id', 'users.id')
        ->where('logs.created_at', '>=', ($request->date_from.' 00:00:00'))
        ->where('logs.created_at', '<=', ($request->date_until.' 23:59:59'))
        ->orderBy('logs.created_at', 'DESC')
        ->get();

        $data = [];
        $i = 0;

		foreach ($collection as $key) {
			$data[] = [
				++$i,
                $this->replace_space(date('d/m/Y H:i', strtotime($key->log_from))),
                $this->getSpanishRole($key->role),
                $this->replace_space($key->email),
                $this->replace_space($key->lastname.', '.$key->firstname),
                $this->replace_space($key->description)
			];
        }
        
        return response()->json([
            'fail' => false,
            'data' => $data,
            'status' => 200
        ], 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function replace_space($str)
	{
		return str_replace(' ', '&nbsp;', $str);
    }
    
    public function getSpanishRole($role) {
        switch($role) {
            case 'ADMIN': return 'Administrador';
            case 'OPERATOR': return 'Operador';
            case 'VALIDATOR': return 'Validador';
            default: return 'Consultor';
        }
    }
}
