<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\{Macolla, Camp, Block, Reservoir, Log};
use Illuminate\Support\Facades\{Hash, Validator, Auth};
use Illuminate\Validation\Rule;

class ReservoirController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Auth::user()->role == "ADMIN" || Auth::user()->role == "OPERATOR") {
            $item = Block::first();
            $collection = Reservoir::where('reservoir_name', '<>', 'N/A')->orderBy('id', 'desc')->get();
            return view('layouts.management.reservoirs', ['item' => $item, 'collection' => $collection]);
        } else {
            return redirect()->route('home');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'reservoir_name' => ['required', 'regex:/^([a-zA-Z0-9 ]+$)+/', 'unique:reservoirs'],
            'api_degree' => ['required', 'numeric', 'between:3,70'],
            'code' => ['required', 'regex:/^([a-zA-Z0-9 ]+$)+/', 'min:10', 'unique:reservoirs'],
        ]);
        
        if ($validator->fails()) {
            return response()->json([
                'fail' => true,
                'message' => 'Petición inválida.',
                'errors'=> $validator->errors(),
                'status' => 400,
            ], 400);
        }
        
        $reservoir = new Reservoir();
        $reservoir->fill($request->all());
        \DB::transaction(function () use($reservoir, $request) {
            $reservoir->code = strtoupper($request->code);
            $reservoir->reservoir_name = strtoupper($request->reservoir_name);
            $reservoir->api_degree = $request->api_degree;
            $reservoir->hydrocarbon = $request->hydrocarbon;
            $reservoir->save();
        });
        
        Log::create([
            'user_id' => Auth::user()->id,
            'description' => '- Creó el yacimiento '.$request->reservoir_name
        ]);

        return response()->json([
            'fail' => false,
            'data' => $reservoir,
            'status' => 200,
        ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'reservoir_name' => ['required', 'regex:/^([a-zA-Z0-9 ]+$)+/', 'min:9', 'unique:reservoirs']
        ]);
        
        if ($validator->fails()) {
            return response()->json([
                'fail' => true,
                'message' => 'Petición inválida.',
                'errors'=> $validator->errors(),
                'status' => 400,
            ], 400);
        }
        
        return response()->json([
            'fail' => false,
            'message' => 'Request has been success',
            'reservoir' => strtoupper($request->reservoir_name),
            'status' => 200,
        ], 200);
    }
    
    public function refresh(Request $request)
    {
        $collection = Reservoir::where('reservoir_name', '<>', 'N/A')->orderBy('id', 'desc')->get();
        return response()->json([
            'fail' => false,
            'data' => $collection,
            'status' => 200,
        ], 200);
    }
    
    public function load(Request $request)
    {
        /** get all */
        $collection = Reservoir::orderBy('id', 'desc')->get();

        /** get all less N/A */
        $collection2 = Reservoir::where('id', '<>', 4)->orderBy('id', 'asc')->get();

        return response()->json([
            'fail' => false,
            'data' => $collection,
            'list' => $collection2,
            'status' => 200,
        ], 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {   
        /** Find this */
        $reservoir = Reservoir::findOrFail($id);
        $id = $request->identity;
        
        $validator = Validator::make($request->all(), [
            'update_reservoir_name' => ['required', 'regex:/^([a-zA-Z0-9 ]+$)+/', 'min:9', 'unique:reservoirs,reservoir_name,'.$id],
            'update_code' => ['required', 'regex:/^([a-zA-Z0-9 ]+$)+/', 'min:10', 'unique:reservoirs,code,'.$id],
            'update_api_degree' => ['required', 'numeric', 'between:3,70']
        ]);
        
        if ($validator->fails()) {
            return response()->json([
                'fail' => true,
                'message' => 'Invalid request',
                'errors'=> $validator->errors(),
                'status' => 400,
            ], 400);
        }
        
        \DB::transaction(function () use($reservoir, $request) {
            $reservoir->code = $request->update_code;
            $reservoir->reservoir_name = $request->update_reservoir_name;
            $reservoir->api_degree = $request->update_api_degree;
            $reservoir->hydrocarbon = $request->update_hydrocarbon;
            $reservoir->save();
        });
        
        $collection = Reservoir::where('id', $id)->first();

        Log::create([
            'user_id' => Auth::user()->id,
            'description' => '- Modificó la información del yacimiento '.$reservoir->reservoir_name
        ]);

        return response()->json([
            'fail' => false,
            'data' => $collection,
            'status' => 200,
        ], 200); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $reservoir = Reservoir::findOrfail($id);
        
        if($reservoir->delete()) {
            
            Log::create([
                'user_id' => Auth::user()->id,
                'description' => '- Eliminó el yacimiento '.$reservoir->reservoir_name
            ]);

            return response()->json([
                'fail' => false,
                'status' => 200,
            ], 200);
        }
    }
}
