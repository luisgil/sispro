<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\{Hash, Validator, Auth};
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\{User, Profile, Log};

class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $profile = Profile::findOrFail($id);
        
        $validator = Validator::make($request->all(), [
            'firstname' => ['required', 'regex:/^([a-zA-ZñÑáéíóúÁÉÍÓÚ ]+$)+/', 'max:27'],
            'lastname' => ['required', 'regex:/^([a-zA-ZñÑáéíóúÁÉÍÓÚ ]+$)+/', 'max:27'],
            'role' => ['required', Rule::in(User::ROLES)]
        ]);
        
        if ($validator->fails()) {
            return response()->json([
                'fail' => true,
                'message' => 'Invalid request',
                'errors'=> $validator->errors(),
                'status' => 400,
            ], 400);
        }

        User::where('id', $id)->update(['role' => $request->role]);

        $profile->fill($request->all());
        \DB::transaction(function () use($profile, $request) {
            $profile->user_id;
            $profile->firstname = ucwords($request->firstname);
            $profile->lastname = ucwords($request->lastname);
            $profile->save();
        });
        
        $user = User::findOrFail($id);
        Log::create([
            'user_id' => Auth::user()->id,
            'description' => '- Modificó la información del usuario asociado al correo '.$user->email
        ]);

        $collection = User::select('users.id as user_id', 'firstname', 'lastname', 'email', 'role', 'last_login', 'users.created_at as created_at')
        ->join('profiles', 'profiles.user_id', 'users.id')->where('users.id', $id)->first();

        return response()->json([
            'fail' => false,
            'message' => 'Usuario modificado exitosamente',
            'data' => $collection,
            'status' => 200,
        ], 200);  
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::findOrfail($id);
        if($user->delete()) {

            Log::create([
                'user_id' => Auth::user()->id,
                'description' => '- Eliminó al usuario asociado al correo '.$user->email
            ]);

            return response()->json([
                'fail' => false,
                'message' => 'Data has been removed successfully',
                'status' => 200,
            ], 200);
        }
    }
}
