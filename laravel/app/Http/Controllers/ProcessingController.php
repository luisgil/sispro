<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\{Validator, Route, Auth, Hash};
use Illuminate\Validation\Rule;
use App\{Camp, Macolla, Location, Test, Average, Sign, Reservoir, Condition, Historical, StaticPressure, OriginalPressure, DynamicPressure, RealSurvey, PlanSurvey, Production, Log, Event, Well};

class ProcessingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function searchExtended(Request $request)
    {
		$type = $request->type;
		
		/** array of selected items */
		$items = ['extended_tests.id as id', 'extended_date', 'company', 'test', 'hour_start', 'hour_end', 'well_name', 'macolla_name', 'location_name', 'reservoir_name', 'line_pressure', 'line_temperature', 'water_rate_prod', 'gas_rate_prod', 'total_barrels', 'neat_rate', 'gvf', 'inyected_diluent_rate'];
		
		/** get id of selected locations */
		$id_list = explode(',', $request->data);
		$sizeIs = sizeof($id_list);
		
		/** store ids inside integer array */
		$ids = $reservoir_ids = $ies = [];

		/** get reservoir ids */
		$reservoir_ids = !is_null($request->reservoir_id) ? explode(',', $request->reservoir_id) : null;

		for ($i = 0; $i < $sizeIs; $i++) $ids[$i] = (int) $id_list[$i];
		
		unset($id_list, $sizeIs); /** empty */

		$collection = Test::select($items)
		->join('wells', 'wells.id', 'extended_tests.well_id')
		->join('locations', 'locations.id', 'wells.location_id')
		->join('reservoirs', 'reservoirs.id', 'wells.reservoir_id')
		->join('macollas', 'macollas.id', 'wells.macolla_id')
		->where(function ($query) use($ids, $type, $reservoir_ids) {
			if ($type == 1) $query->whereIn('extended_tests.well_id', $ids);
			else $query->whereIn('wells.macolla_id', $ids)->whereIn('wells.reservoir_id', $reservoir_ids);
		})
		->where('extended_tests.extended_date', '>=', $request->date_from)
		->where('extended_tests.extended_date', '<=', $request->date_to)
		->orderByRaw('extended_date, hour_start, macolla_name, location_name, test, identifier ASC')
		->get();
		
		$macolla_data = Macolla::where('id', $request->macolla_id)->first();
		
		// data processing 
		$data = $filtered = $results = [];
		$i = 0;
		
		foreach ($collection as $key => $item) {
			$data[] = [
				++$i,
				date('d/m/Y', strtotime($item['extended_date'])),
				$this->replace_space($item['location_name']),
				$this->replace_space($item['well_name']),
				$this->replace_space($item['reservoir_name']),
				$this->replace_space($item['company']),
				$this->replace_space($item['test']),
				date('H:i', strtotime($item['hour_start'])),
				date('H:i', strtotime($item['hour_end'])),
				$this->diff($item['hour_start'], $item['hour_end']),
				$this->check_data($item['line_pressure'], 10, 1, 'line_pressure', $items),
				$this->check_data($item['line_temperature'], 10, 1, 'line_temperature', $items),
				$this->check_data($item['water_rate_prod'], 10, 1, 'water_rate_prod', $items),
				$this->check_data($item['gas_rate_prod'], 10, 2, 'gas_rate_prod', $items),
				$this->check_data($item['total_barrels'], 10, 1, 'total_barrels', $items),
				$this->check_data($item['neat_rate'], 10, 1, 'neat_rate', $items),
				$this->check_data(($item['gvf']), 1, 3, 'gvf', $items),
				$this->check_data($item['inyected_diluent_rate'], 10, 1, 'inyected_diluent_rate', $items),
                '<div class="btn-group btn-group-sm"><button class="btn btn-info" id="btn-edit" data-toggle="modal" data-target="#modal-xl" title="Editar" value="'. $item['id'] .'"><i class="fa fa-edit"></i></button><button class="btn btn-danger" id="btn-delete" value="'. $item['id'] .'"><i class="fa fa-trash" title="Borrar"></i></button></div>'
			];
		}
		
		$row_size = sizeof($data);
		
		for ($i = 0; $i < $row_size; $i++) {
			array_walk($data[$i], 
			function($v) use(&$filtered) {
				if (!is_null($v)) $filtered[] = $v;
			});
			
			$results[$i] = $filtered;
			unset($filtered); /** remove */
		}
		
		unset($filtered, $i, $row_size); /** empty */

		$total = null;

		if ($type == 2) {
			
			foreach ($collection as $key)  $ies[] = (array) $key->macolla_name;
			
			$result = array_unique($ies, SORT_REGULAR);
			
			$total = [];
			
			for ($i = 0, $j = 0; $i < sizeof($ies); $i++) {
				if(isset($result[$i])) {
					$total[$j] = $result[$i];
					$j++;
				}
			}
		}

		unset($result, $j); /** empty */

		return response()->json([
			'fail' => false,
			'info' => $macolla_data['macolla_name'],
			'data' => $collection,
			'total' => $total,
			'type' => (int) $type,
			'results' => $results,
			'status' => 200
		], 200);
    }

    public function searchAverage(Request $request)
    {
		$type = $request->type;
		
		/** array of selected items */
		$items = ['average_tests.id as id', 'well_name', 'avg_start_test', 'avg_end_test', 'company', 'duration', 'test', 'macolla_name', 'location_name', 'reservoir_name', 'rpm', 'line_pressure', 'line_temperature', 'gas_rate_prod','water_rate_prod', 'fluid_total_rate', 'percent_mixture', 'api_degree_mixture', 'neat_rate_prod', 'percent_formation', 'gvf', 'inyected_diluent_rate', 'api_degree_diluent', 'pip', 'observations'];
		
		/** get id of selected locations */
		$id_list = explode(',', $request->data);
		$sizeIs = sizeof($id_list);
		
		/** store ids inside integer array */
		$ids = $reservoir_ids = $ies = [];

		/** get reservoir ids */
		$reservoir_ids = !is_null($request->reservoir_id) ? explode(',', $request->reservoir_id) : null;

		for ($i = 0; $i < $sizeIs; $i++) $ids[$i] = (int) $id_list[$i];
		
		unset($id_list, $sizeIs); /** empty */

		$collection = Average::select($items)
		->join('wells', 'wells.id', 'average_tests.well_id')
		->join('locations', 'locations.id', 'wells.location_id')
		->join('reservoirs', 'reservoirs.id', 'wells.reservoir_id')
		->join('macollas', 'macollas.id', 'wells.macolla_id')
		->where(function ($query) use($ids, $type, $reservoir_ids) {
			if ($type == 1) $query->whereIn('average_tests.well_id', $ids);
			else $query->whereIn('wells.macolla_id', $ids)->whereIn('wells.reservoir_id', $reservoir_ids);
		})
		->where('avg_end_test', '>=', $request->date_from)
		->where('avg_end_test', '<=', $request->date_to)
		->orderByRaw('macolla_name, location_name, avg_start_test, avg_end_test ASC')
		->get();
		
		$macolla_data = Macolla::where('id', $request->macolla_id)->first();
		
		// data processing 
		$data = $filtered = $results = [];
		$i = 0;
		
		foreach ($collection as $key => $item) {
			$data[] = [
			  ++$i,
			  $this->replace_space($item['location_name']),
              $this->replace_space($item['well_name']),
              $this->replace_space($item['reservoir_name']),
              date('d/m/Y', strtotime($item['avg_start_test'])),
              date('d/m/Y', strtotime($item['avg_end_test'])),
              $this->replace_space($item['company']),
              $this->check_data($item['duration'], 1000, 0, 'duration', $items),
              $this->replace_space($item['test']),
              $this->check_data($item['rpm'], 2000, 0, 'rpm', $items),
              $this->check_data($item['line_pressure'], 10, 1, 'line_pressure', $items),
              $this->check_data($item['line_temperature'], 10, 1, 'line_temperature', $items),
              $this->check_data($item['gas_rate_prod'], 10, 2, 'gas_rate_prod', $items),
              $this->check_data($item['water_rate_prod'], 10, 1, 'water_rate_prod', $items),
              $this->check_data($item['fluid_total_rate'], 10, 1, 'fluid_total_rate', $items),
              $this->check_data($item['percent_mixture'], 10, 1, 'percent_mixture', $items),
              $this->check_data($item['api_degree_mixture'], 70, 1, 'api_degree_mixture', $items),
              $this->check_data($item['neat_rate_prod'], 10, 1, 'neat_rate_prod', $items),
              $this->check_data($item['percent_formation'], 10, 1, 'percent_formation', $items),
              $this->check_data(($item['gvf']), 1, 3, 'gvf', $items),
              $this->check_data($item['inyected_diluent_rate'], 10, 1, 'inyected_diluent_rate', $items),
              $this->check_data($item['api_degree_diluent'], 70, 1, 'api_degree_diluent', $items),
              $this->check_data($item['pip'], 2000, 0, 'pip', $items),
              $this->format_text($item['observations'], 'observations', $items),
              '<div class="btn-group btn-group-sm"><button class="btn btn-info" id="btn-edit" data-toggle="modal" data-target="#modal-xl" title="Editar" value="'. $item['id'] .'"><i class="fa fa-edit"></i></button><button class="btn btn-danger" id="btn-delete" value="'. $item['id'] .'"><i class="fa fa-trash" title="Borrar"></i></button></div>'
			];
		}
		
		$row_size = sizeof($data);
		
		for ($i = 0; $i < $row_size; $i++) {
			array_walk($data[$i], 
			function($v) use(&$filtered) {
				if (!is_null($v)) $filtered[] = $v;
			});
			
			$results[$i] = $filtered;
			unset($filtered); /** remove */
		}
		
		unset($filtered, $i, $row_size); /** empty */

		$total = null;

		if ($type == 2) {
			
			foreach ($collection as $key)  $ies[] = (array) $key->macolla_name;
			
			$result = array_unique($ies, SORT_REGULAR);
			
			$total = [];
			
			for ($i = 0, $j = 0; $i < sizeof($ies); $i++) {
				if(isset($result[$i])) {
					$total[$j] = $result[$i];
					$j++;
				}
			}
		}

		unset($result, $j); /** empty */

		return response()->json([
			'fail' => false,
			'info' => $macolla_data['macolla_name'],
			'data' => $collection,
			'total' => $total,
			'type' => (int) $type,
			'results' => $results,
			'status' => 200
		], 200);
    }

    public function searchSign(Request $request)
    {
		$type = $request->type;
		$items = ['signs.id as id', 'sign_date', 'well_name', 'sign_time', 'macolla_name', 'location_name', 'reservoir_name', 'company', 'percent_mixture', 'percent_sediment', 'api_degree_diluted', 'ptb'];
		
		/** get id of selected locations */
		$id_list = explode(',', $request->data);
		$sizeIs = sizeof($id_list);
		
		/** store ids inside integer array */
		$ids = $reservoir_ids = $ies = [];

		/** get reservoir ids */
		$reservoir_ids = !is_null($request->reservoir_id) ? explode(',', $request->reservoir_id) : null;

		for ($i = 0; $i < $sizeIs; $i++) $ids[$i] = (int) $id_list[$i];
		
		unset($id_list, $sizeIs); /** empty */

		$collection = Sign::select($items)
		->join('wells', 'wells.id', 'signs.well_id')
		->join('locations', 'locations.id', 'wells.location_id')
		->join('reservoirs', 'reservoirs.id', 'wells.reservoir_id')
		->join('macollas', 'macollas.id', 'wells.macolla_id')
		->where(function ($query) use($ids, $type, $reservoir_ids) {
			if ($type == 1) $query->whereIn('signs.well_id', $ids);
			//else $query->whereIn('wells.macolla_id', $ids)->whereIn('wells.reservoir_id', $reservoir_ids);
		})
		->where('sign_date', '>=', $request->date_from)
		->where('sign_date', '<=', $request->date_to)
		->get();
		
		$macolla_data = Macolla::where('id', $request->macolla_id)->first();
		
		// data processing 
		$data = $filtered = $results = [];
		$i = 0;
		
		foreach ($collection as $key=>$item) {
			$data[] = [
				++$i,
				date('d/m/Y', strtotime($item['sign_date'])),
				$this->replace_space($item['location_name']),
				$this->replace_space($item['well_name']),
				$this->replace_space($item['reservoir_name']),
				$this->replace_space($item['company']),
				date('H:i', strtotime($item['sign_time'])),
				$this->check_data($item['percent_mixture'], 10, 1, 'percent_mixture', $items),
				$this->check_data($item['percent_sediment'], 10, 1, 'percent_sediment', $items),
				$this->check_data($item['api_degree_diluted'], 10, 1, 'api_degree_diluted', $items),
				$this->check_data($item['ptb'], 10, 1, 'ptb', $items),
				'<div class="btn-group btn-group-sm"><button class="btn btn-info" id="btn-edit" data-toggle="modal" data-target="#modal-xl" title="Editar" value="' . $item['id'] . '"><i class="fa fa-edit"></i></button><button class="btn btn-danger" id="btn-delete" value="' . $item['id'] . '"><i class="fa fa-trash" title="Borrar"></i></button></div>'
			];
		}
		
		$row_size = sizeof($data);
		
		for ($i = 0; $i < $row_size; $i++) {
			array_walk($data[$i], 
			function($v) use(&$filtered) {
				if (!is_null($v)) $filtered[] = $v;
			});
			
			$results[$i] = $filtered;
			unset($filtered); /** remove */
		}
		
		unset($filtered, $i, $row_size); /** empty */

		$total = null;

		if ($type == 2) {
			
			foreach ($collection as $key)  $ies[] = (array) $key->macolla_name;
			
			$result = array_unique($ies, SORT_REGULAR);
			
			$total = [];
			
			for ($i = 0, $j = 0; $i < sizeof($ies); $i++) {
				if(isset($result[$i])) {
					$total[$j] = $result[$i];
					$j++;
				}
			}
		}

		unset($result, $j); /** empty */

		return response()->json([
			'fail' => false,
			'info' => $macolla_data['macolla_name'],
			'data' => $collection,
			'total' => $total,
			'd1' => $request->date_from,
			'd2' => $request->date_to,
			'type' => (int) $type,
			'results' => $results,
			'status' => 200
		], 200);
    }

    public function searchCondition(Request $request)
    {
		$type = $request->type;
		$selected = $request->items;
		
		/** array of selected items */
		$items = explode(',', $selected);
		
		/** get id of selected locations */
		$id_list = explode(',', $request->data);
		$sizeIs = sizeof($id_list);
		
		/** store ids inside integer array */
		$ids = $reservoir_ids = $reservoir_names = $ies = [];

		/** get reservoir ids */
		$reservoir_ids = !is_null($request->reservoir_id) ? explode(',', $request->reservoir_id) : null;

		for ($i = 0; $i < $sizeIs; $i++) $ids[$i] = (int) $id_list[$i];

		if (!is_null($reservoir_ids)) for ($i = 0; $i < count($reservoir_ids); $i++) $reservoir_names[$i] = $this->getNamesReservoirs($reservoir_ids[$i]);
		
		unset($id_list, $sizeIs); /** empty */

		$collection = Condition::select($items)
		->join('wells', 'wells.id', 'conditions.well_id')
		->join('locations', 'locations.id', 'wells.location_id')
		->join('reservoirs', 'reservoirs.id', 'wells.reservoir_id')
		->join('macollas', 'macollas.id', 'wells.macolla_id')
		->where(function ($query) use($ids, $type, $reservoir_names) {
			if ($type == 1) $query->whereIn('conditions.well_id', $ids);
			else $query->whereIn('wells.macolla_id', $ids)->whereIn('reservoir', $reservoir_names);
		})
		->orderByRaw('macolla_name, location_name, identifier ASC')
		->get();
		
		$macolla_data = Macolla::where('id', $request->macolla_id)->first();

		// data processing 
		$data = $filtered = $results = [];
		$i = 0;

		foreach ($collection as $key => $item) {
			$data[] = [
				++$i,
				$this->replace_space($item['macolla_name']),
				$this->replace_space($item['well_name']),
				$this->replace_space($item['location_name']),
				$this->replace_space($item['reservoir']),
				$this->replace_space($item['company']),
				date('d/m/Y', strtotime($item['pt_date'])),
				$this->format_text($item['sand'], 'sand', $items),
				$this->format_text($item['test'], 'test', $items),
				$this->check_data($item['deep_md'], 0, 0, 'deep_md', $items),
				$this->check_data($item['deep_tvd'], 0, 0, 'deep_tvd', $items),
				$this->check_data($item['temperature'], 10, 2, 'temperature', $items),
				$this->check_data($item['duration'], 0, 0, 'duration', $items),
				$this->check_data($item['pressbefore'], 0, 2, 'pressbefore', $items),
				$this->check_data($item['pressafter'], 0, 2, 'pressafter', $items),
				$this->check_data($item['pressformation'], 0, 2, 'pressformation', $items),
				$this->check_data($item['mobility'], 0, 2, 'mobility', $items),
				$this->format_text($item['observations'], 'observations', $items),
				'<div class="btn-group btn-group-sm"><button class="btn btn-info" id="btn-edit" data-toggle="modal" data-target="#modal-xl" title="Editar" value="'. $item['id'] .'"><i class="fa fa-edit"></i></button><button class="btn btn-danger" id="btn-delete" value="'. $item['id'] .'"><i class="fa fa-trash" title="Borrar"></i></button></div>'
			];
		}
		
		$row_size = sizeof($data);
		
		for ($i = 0; $i < $row_size; $i++) {
			array_walk($data[$i], 
			function($v) use(&$filtered) {
				if (!is_null($v)) $filtered[] = $v;
			});
			
			$results[$i] = $filtered;
			unset($filtered); /** remove */
		}
		
		unset($filtered, $i, $row_size); /** empty */

		$total = null;

		if ($type == 2) {
			
			foreach ($collection as $key)  $ies[] = (array) $key->macolla_name;
			
			$result = array_unique($ies, SORT_REGULAR);
			
			$total = [];
			
			for ($i = 0, $j = 0; $i < sizeof($ies); $i++) {
				if(isset($result[$i])) {
					$total[$j] = $result[$i];
					$j++;
				}
			}
		}

		unset($result, $j); /** empty */

		return response()->json([
			'fail' => false,
			'info' => $macolla_data['macolla_name'],
			'data' => $collection,
			'total' => $total,
			'type' => (int) $type,
			'results' => $results,
			'status' => 200
		], 200);
    }

    public function searchHistorical(Request $request)
    {
    	$type = $request->type;
		$selected = $request->items;
		
		/** array of selected items */
		$items = explode(',', $selected);
		
		/** get id of selected locations */
		$id_list = explode(',', $request->data);
		$sizeIs = sizeof($id_list);
		
		/** store ids inside integer array */
		$ids = $reservoir_ids = $ies = [];

		/** get reservoir ids */
		$reservoir_ids = !is_null($request->reservoir_id) ? explode(',', $request->reservoir_id) : null;

		for ($i = 0; $i < $sizeIs; $i++) $ids[$i] = (int) $id_list[$i];
		
		unset($id_list, $sizeIs); /** empty */

		$collection = Historical::select("historicals.id as id", "type_work_i", "type_work_ii", "measure_from", "rig", "sensor_model", "sensor_diameter", "sensor_brand", "sensor_company", "deeptop_sensor_md", "deepbottom_sensor_md", "pump_model", "pump_diameter", "pump_company", "deeptop_pump_md", "deepbottom_pump_md", "pump_type", "pump_capacity", "inyected_diluent", "observations", "references", "well_name", "begin_date", "end_date", "macolla_name", "location_name", "reservoir")
		->join('wells', 'wells.id', 'historicals.well_id')
		->join('locations', 'locations.id', 'wells.location_id')
		->join('reservoirs', 'reservoirs.id', 'wells.reservoir_id')
		->join('macollas', 'macollas.id', 'wells.macolla_id')
		->where(function ($query) use($ids, $type, $reservoir_ids) {
			if ($type == 1) $query->whereIn('historicals.well_id', $ids);
			//else $query->whereIn('wells.macolla_id', $ids)->whereIn('wells.reservoir_id', $reservoir_ids);
		})
		->where('end_date', '>=', ($request->date_from.' 00:00:00'))
		->where('end_date', '<=', ($request->date_to.' 23:59:59'))
		->get();
		
		$macolla_data = Macolla::where('id', $request->macolla_id)->first();
		 
		// data processing 
		$data = $filtered = $results = [];
		$i = 0;

		foreach ($collection as $key => $item) {
			$data[] = [
				++$i,
				($type == 2 ? $this->replace_space($item['macolla_name']) : null),
				$this->replace_space($item['location_name']),
				$this->replace_space($item['well_name']),
				$this->replace_space($item['reservoir']),
				date('d/m/Y H:i', strtotime($item['begin_date'])),
				date('d/m/Y H:i', strtotime($item['end_date'])),
				$this->format_text($item['type_work_i'], 'type_work_i', $items),
				$this->format_text($item['type_work_ii'], 'type_work_ii', $items),
				$this->format_text($item['rig'], 'rig', $items),
				$this->format_text($item['sensor_model'], 'sensor_model', $items),
				$this->format_text($item['sensor_brand'], 'sensor_brand', $items),
				$this->decimal_to_fraction($item['sensor_diameter'], 'sensor_diameter', $items),
				$this->format_text($item['sensor_company'], 'sensor_company', $items),
				$this->check_data($item['deeptop_sensor_md'], 10, 2, 'deeptop_sensor_md', $items),
				$this->check_data($item['deepbottom_sensor_md'], 10, 2, 'deepbottom_sensor_md', $items),
				$this->format_text($item['pump_model'], 'pump_model', $items),
				$this->decimal_to_fraction($item['pump_diameter'], 'pump_diameter', $items),
				$this->format_text($item['pump_type'], 'pump_type', $items),
				$this->check_data($item['pump_capacity'], 0, 0, 'pump_capacity', $items),
				$this->format_text($item['pump_company'], 'pump_company', $items),
				$this->check_data($item['deeptop_pump_md'], 10, 2, 'deeptop_pump_md', $items),
				$this->check_data($item['deepbottom_pump_md'], 10, 2, 'deepbottom_pump_md', $items),
				$this->format_text($item['inyected_diluent'], 'inyected_diluent', $items),
				$this->format_text($item['measure_from'], 'measure_from', $items),
				$this->format_text($item['observations'], 'observations', $items),
				$this->format_text($item['references'], 'references', $items),
				'<div class="btn-group btn-group-sm"><button class="btn btn-info" id="btn-edit" data-toggle="modal" data-target="#modal-xl" title="Editar" value="'. $item['id'] .'"><i class="fa fa-edit"></i></button><button class="btn btn-danger" id="btn-delete" value="'. $item['id'] .'"><i class="fa fa-trash" title="Borrar"></i></button></div>'
			];
		}
		
		$row_size = sizeof($data);
		
		for ($i = 0; $i < $row_size; $i++) {
			array_walk($data[$i], 
			function($v) use(&$filtered) {
				if (!is_null($v)) $filtered[] = $v;
			});
			
			$results[$i] = $filtered;
			unset($filtered); /** remove */
		}
		
		unset($filtered, $i, $row_size); /** empty */

		$total = null;

		if ($type == 2) {
			
			foreach ($collection as $key)  $ies[] = (array) $key->macolla_name;
			
			$result = array_unique($ies, SORT_REGULAR);
			
			$total = [];
			
			for ($i = 0, $j = 0; $i < sizeof($ies); $i++) {
				if(isset($result[$i])) {
					$total[$j] = $result[$i];
					$j++;
				}
			}
		}

		unset($result, $j); /** empty */

		return response()->json([
			'fail' => false,
			'info' => $macolla_data['macolla_name'],
			'data' => $collection,
			'total' => $total,
			'type' => (int) $type,
			'results' => $results,
			'status' => 200
		], 200);
    }

    public function searchStatic(Request $request)
    {
    	$type = $request->type;
		$selected = $request->items;
		
		/** array of selected items */
		$items =  ["static_pressures.id as id", "quality", "pip", "pdp", "temperature", "observations", "well_name", "static_date", "macolla_name", "location_name", "reservoir_name"];
		
		/** get id of selected locations */
		$id_list = explode(',', $request->data);
		$sizeIs = sizeof($id_list);
		
		/** store ids inside integer array */
		$ids = $reservoir_ids = $ies = [];

		/** get reservoir ids */
		$reservoir_ids = !is_null($request->reservoir_id) ? explode(',', $request->reservoir_id) : null;

		for ($i = 0; $i < $sizeIs; $i++) $ids[$i] = (int) $id_list[$i];
		
		unset($id_list, $sizeIs); /** empty */

		$collection = StaticPressure::select($items)
		->join('wells', 'wells.id', 'static_pressures.well_id')
		->join('locations', 'locations.id', 'wells.location_id')
		->join('reservoirs', 'reservoirs.id', 'wells.reservoir_id')
		->join('macollas', 'macollas.id', 'wells.macolla_id')
		->where(function ($query) use($ids, $type, $reservoir_ids) {
			if ($type == 1) $query->whereIn('static_pressures.well_id', $ids);
			else $query->whereIn('wells.macolla_id', $ids)->whereIn('wells.reservoir_id', $reservoir_ids);
		})
		->where('static_date', '>=', ($request->date_from . ' 00:00:00'))
		->where('static_date', '<=', ($request->date_to . ' 23:59:59'))
		->orderByRaw('macolla_name, location_name ASC')
		->get();
		
		$macolla_data = Macolla::where('id', $request->macolla_id)->first();

		// data processing 
		$data = $filtered = $results = [];
		$i = 0;

		foreach ($collection as $key => $item) {
			$data[] = [
				++$i,
				$this->replace_space($item['location_name']),
				$this->replace_space($item['well_name']),
				$this->replace_space($item['reservoir_name']),
				date('d/m/Y H:i', strtotime($item['static_date'])),
				$this->format_text($item['quality'], 'quality', $items),
				$this->check_data($item['pip'], 0, 0, 'pip', $items),
				$this->check_data($item['pdp'], 0, 0, 'pdp', $items),
				$this->check_data($item['temperature'], 10, 2, 'temperature', $items),
				$this->format_text($item['observations'], 'observations', $items),
				'<div class="btn-group btn-group-sm"><button class="btn btn-info" id="btn-edit" data-toggle="modal" data-target="#modal-xl" title="Editar" value="'. $item['id'] .'"><i class="fa fa-edit"></i></button><button class="btn btn-danger" id="btn-delete" title="Borrar" value="'. $item['id'] .'"><i class="fa fa-trash"></i></button></div>'
			];
		}
		
		$row_size = sizeof($data);
		
		for ($i = 0; $i < $row_size; $i++) {
			array_walk($data[$i], 
			function($v) use(&$filtered) {
				if (!is_null($v)) $filtered[] = $v;
			});
			
			$results[$i] = $filtered;
			unset($filtered); /** remove */
		}
		
		unset($filtered, $i, $row_size); /** empty */

		return response()->json([
			'fail' => false,
			'info' => $macolla_data['macolla_name'],
			'data' => $collection,
			'type' => (int) $type,
			'results' => $results,
			'status' => 200
		], 200);
    }

    public function searchDynamic(Request $request)
    {
    	$type = $request->type;
		$selected = $request->items;
		
		/** array of selected items */
		$items =  ["dynamic_pressures.id as id", "quality", "pip", "pdp", "temperature", "observations", "well_name", "dynamic_date", "macolla_name", "location_name", "reservoir_name"];
		
		/** get id of selected locations */
		$id_list = explode(',', $request->data);
		$sizeIs = sizeof($id_list);
		
		/** store ids inside integer array */
		$ids = $reservoir_ids = $ies = [];

		/** get reservoir ids */
		$reservoir_ids = !is_null($request->reservoir_id) ? explode(',', $request->reservoir_id) : null;

		for ($i = 0; $i < $sizeIs; $i++) $ids[$i] = (int) $id_list[$i];
		
		unset($id_list, $sizeIs); /** empty */

		$collection = DynamicPressure::select($items)
		->join('wells', 'wells.id', 'dynamic_pressures.well_id')
		->join('locations', 'locations.id', 'wells.location_id')
		->join('reservoirs', 'reservoirs.id', 'wells.reservoir_id')
		->join('macollas', 'macollas.id', 'wells.macolla_id')
		->where(function ($query) use($ids, $type, $reservoir_ids) {
			if ($type == 1) $query->whereIn('dynamic_pressures.well_id', $ids);
			else $query->whereIn('wells.macolla_id', $ids)->whereIn('wells.reservoir_id', $reservoir_ids);
		})
		->where('dynamic_date', '>=', ($request->date_from . ' 00:00:00'))
		->where('dynamic_date', '<=', ($request->date_to . ' 23:59:59'))
		->orderByRaw('macolla_name, location_name ASC')
		->get();
		
		$macolla_data = Macolla::where('id', $request->macolla_id)->first();

		// data processing 
		$data = $filtered = $results = [];
		$i = 0;

		foreach ($collection as $key => $item) {
			$data[] = [
				++$i,
				$this->replace_space($item['location_name']),
				$this->replace_space($item['well_name']),
				$this->replace_space($item['reservoir_name']),
				date('d/m/Y H:i', strtotime($item['dynamic_date'])),
				$this->format_text($item['quality'], 'quality', $items),
				$this->check_data($item['pip'], 0, 0, 'pip', $items),
				$this->check_data($item['pdp'], 0, 0, 'pdp', $items),
				$this->check_data($item['temperature'], 10, 2, 'temperature', $items),
				$this->format_text($item['observations'], 'observations', $items),
				'<div class="btn-group btn-group-sm"><button class="btn btn-info" id="btn-edit" data-toggle="modal" data-target="#modal-xl" title="Editar" value="'. $item['id'] .'"><i class="fa fa-edit"></i></button><button class="btn btn-danger" id="btn-delete" title="Borrar" value="'. $item['id'] .'"><i class="fa fa-trash"></i></button></div>'
			];
		}
		
		$row_size = sizeof($data);
		
		for ($i = 0; $i < $row_size; $i++) {
			array_walk($data[$i], 
			function($v) use(&$filtered) {
				if (!is_null($v)) $filtered[] = $v;
			});
			
			$results[$i] = $filtered;
			unset($filtered); /** remove */
		}
		
		unset($filtered, $i, $row_size); /** empty */

		return response()->json([
			'fail' => false,
			'info' => $macolla_data['macolla_name'],
			'data' => $collection,
			'type' => (int) $type,
			'results' => $results,
			'status' => 200
		], 200);
    }

    public function searchOriginal(Request $request)
    {
    	$type = $request->type;
		$selected = $request->items;
		
		/** array of selected items */
		$items =  ["original_pressures.id as id", "type_test", "sensor_model", "pressure_gradient", "deepdatum_mc_md", "deepdatum_mc_tvd", "press_datum_macolla", "temp_datum_macolla", "temperature_gradient", "well_name", "pressure_date", "macolla_name", "location_name", "reservoir"];
		
		/** get id of selected locations */
		$id_list = explode(',', $request->data);
		$sizeIs = sizeof($id_list);
		
		/** store ids inside integer array */
		$ids = $reservoir_ids = $reservoir_names = $ies = [];

		/** get reservoir ids */
		$reservoir_ids = !is_null($request->reservoir_id) ? explode(',', $request->reservoir_id) : null;

		for ($i = 0; $i < $sizeIs; $i++) $ids[$i] = (int) $id_list[$i];
		
		if (!is_null($reservoir_ids)) for ($i = 0; $i < count($reservoir_ids); $i++) $reservoir_names[$i] = $this->getNamesReservoirs($reservoir_ids[$i]);
		
		unset($id_list, $sizeIs); /** empty */

		$collection = OriginalPressure::select($items)
		->join('wells', 'wells.id', 'original_pressures.well_id')
		->join('locations', 'locations.id', 'wells.location_id')
		->join('reservoirs', 'reservoirs.id', 'wells.reservoir_id')
		->join('macollas', 'macollas.id', 'wells.macolla_id')
		->where(function ($query) use($ids, $type, $reservoir_names) {
			if ($type == 1) $query->whereIn('original_pressures.well_id', $ids);
			else $query->whereIn('wells.macolla_id', $ids)->whereIn('reservoir', $reservoir_names);
		})->get();

		$macolla_data = Macolla::where('id', $request->macolla_id)->first();

		// data processing 
		$data = $filtered = $results = [];
		$i = 0;

		foreach ($collection as $key => $item) {
			$data[] = [
				++$i,
				$this->replace_space($item['macolla_name']),
				$this->replace_space($item['location_name']),
				$this->replace_space($item['well_name']),
				$this->replace_space($item['reservoir']),
				date('d/m/Y', strtotime($item['pressure_date'])),
				$this->format_text($item['type_test'], 'type_test', $items),
				$this->format_text($item['sensor_model'], 'sensor_model', $items),
				$this->check_data($item['pressure_gradient'], 1, 3, 'pressure_gradient', $items),
				$this->check_data($item['deepdatum_mc_md'], 0, 0, 'deepdatum_mc_md', $items),
				$this->check_data($item['deepdatum_mc_tvd'], 0, 0, 'deepdatum_mc_tvd', $items),
				$this->check_data($item['press_datum_macolla'], 0, 0, 'press_datum_macolla', $items),
				$this->check_data($item['temp_datum_macolla'], 0, 1, 'temp_datum_macolla', $items),
				$this->check_data($item['temperature_gradient'], 1, 3, 'temperature_gradient', $items),
				'<div class="btn-group btn-group-sm"><button class="btn btn-info" id="btn-edit" data-toggle="modal" data-target="#modal-xl" title="Editar" value="'. $item['id'] .'"><i class="fa fa-edit"></i></button><button class="btn btn-danger" id="btn-delete" title="Borrar" value="'. $item['id'] .'"><i class="fa fa-trash"></i></button></div>'
			];
		}
		
		$row_size = sizeof($data);
		
		for ($i = 0; $i < $row_size; $i++) {
			array_walk($data[$i], 
			function($v) use(&$filtered) {
				if (!is_null($v)) $filtered[] = $v;
			});
			
			$results[$i] = $filtered;
			unset($filtered); /** remove */
		}
		
		unset($filtered, $i, $row_size); /** empty */

		$total = null;

		if ($type == 2) {
			
			foreach ($collection as $key)  $ies[] = (array) $key->macolla_name;
			
			$result = array_unique($ies, SORT_REGULAR);
			
			$total = [];
			
			for ($i = 0, $j = 0; $i < sizeof($ies); $i++) {
				if(isset($result[$i])) {
					$total[$j] = $result[$i];
					$j++;
				}
			}
		}

		unset($result, $j); /** empty */

		return response()->json([
			'fail' => false,
			'info' => $macolla_data['macolla_name'],
			'data' => $collection,
			'type' => (int) $type,
			'total' => $total,
			'results' => $results,
			'status' => 200
		], 200);
    }

    public function searchReal(Request $request)
    {
    	$type = $request->type;
		
		/** array of selected items */
		$items =  ["real_surveys.id as id", "macolla_name", "location_name", "well_name", "reservoir_name", "hole", "company", "md", "inclination", "azim_grid", "tvd", "vsec", "ns", "ew", "dls", "longitude", "latitude", "observations"];

		$size_items = sizeof($items);

		$sql = "";

		for ($i=0; $i < $size_items; $i++) { 
			if ($items[$i] == 'latitude') {
				$sql .= "IF((la_g = 0 AND la_m = 0 AND la_s = 0) OR (la_g = NULL OR la_m = NULL OR la_s = NULL), NULL, CONCAT(la_g, ' ', la_m, ' ', la_s)) AS latitude";
			} else if($items[$i] == 'longitude') {
				$sql .= "IF((lo_g <= 0 AND lo_m <= 0 AND lo_s <= 0) OR (lo_g = NULL OR lo_m = NULL OR lo_s = NULL), NULL, CONCAT(lo_g, ' ', lo_m, ' ', lo_s)) as longitude";
			} else {
				$sql .= $items[$i];
			}

			if ($i < $size_items-1) {
				$sql .= ", ";
			}
		}
		
		/* exception */
		$sql .= ", la_g, la_m, la_s, lo_g, lo_m, lo_s";

		$items_formatted = $sql; //explode(', ', $sql);

		$id_list = explode(',', $request->data);
		$sizeIs = sizeof($id_list);		
		
		$ids = $reservoir_ids = $ies = [];

		$reservoir_ids = !is_null($request->reservoir_id) ? explode(',', $request->reservoir_id) : null;

		for ($i = 0; $i < $sizeIs; $i++) $ids[$i] = (int) $id_list[$i];
		
		unset($id_list, $sizeIs);

		$collection = RealSurvey::selectRaw($sql)
		->join('wells', 'wells.id', 'real_surveys.well_id')
		->join('locations', 'locations.id', 'wells.location_id')
		->join('macollas', 'macollas.id', 'wells.macolla_id')
		->join('reservoirs', 'reservoirs.id', 'wells.reservoir_id')
		->where(function ($query) use($ids, $type) {
			if ($type == 1) $query->whereIn('real_surveys.well_id', $ids);
			else $query->whereIn('wells.macolla_id', $ids);
		})->get();

		$macolla_data = Macolla::where('id', $request->macolla_id)->first();

		// data processing 
		$data = $filtered = $results = [];
		$i = 0;

		foreach ($collection as $key => $item) {
			$data[] = [
				++$i,
				$this->replace_space($item['macolla_name']),
				$this->replace_space($item['well_name']),
				$this->replace_space($item['location_name']),
				$this->replace_space($item['reservoir_name']),
				date('d/m/Y', strtotime($item['rs_date'])),
				$this->replace_space($item['company']),
				$this->format_text($item['hole'], 'hole', $items),
				$this->check_data($item['gl'], 0, 0, 'wells.gl AS gl', $items),
				$this->check_data($item['rt'], 0, 0, 'wells.rt AS rt', $items),
				$this->check_data($item['md'], 0, 2, 'md', $items),
				$this->check_data($item['inclination'], 0, 2, 'inclination', $items),
				$this->check_data($item['azim_grid'], 0, 2, 'azim_grid', $items),
				$this->check_data($item['tvd'], 0, 2, 'tvd', $items),
				$this->check_data($item['dls'], 100, 2, 'dls', $items),
				$this->check_data($item['vsec'], 0, 2, 'vsec', $items),
				$this->check_data($item['ns'], 0, 2,'ns', $items),
				$this->check_data($item['ew'], 0, 2, 'ew', $items),
				$this->format_text($item['latitude'], 'latitude', $items),
				$this->format_text($item['longitude'], 'longitude', $items),
				$this->format_text($item['observations'], 'observations', $items),
				'<div class="btn-group btn-group-sm"><button class="btn btn-info" id="btn-edit" data-toggle="modal" data-target="#modal-xl" title="Editar" value="'. $item['id'] .'"><i class="fa fa-edit"></i></button><button class="btn btn-danger" id="btn-delete" title="Borrar" value="'. $item['id'] .'"><i class="fa fa-trash"></i></button></div>',
				$this->setUndefined($item['la_g']),
				$this->setUndefined($item['la_m']),
				$this->setUndefined($item['la_s']),
				$this->setUndefined($item['lo_g']),
				$this->setUndefined($item['lo_m']),
				$this->setUndefined($item['lo_s'])
			];
		}
		
		$row_size = sizeof($data);
		
		for ($i = 0; $i < 20; $i++) {
			array_walk($data[$i], 
			function($v) use(&$filtered) {
				if (!is_null($v)) $filtered[] = $v;
			});
			
			$results[$i] = $filtered;
			unset($filtered);
		}
		
		unset($filtered, $i, $row_size);

		$total = null;

		if ($type == 2) {
			foreach ($collection as $key)  $ies[] = (array) $key->macolla_name;
			$result = array_unique($ies, SORT_REGULAR);
			$total = [];
			for ($i = 0, $j = 0; $i < sizeof($ies); $i++) {
				if(isset($result[$i])) {
					$total[$j] = $result[$i];
					$j++;
				}
			}
		}

		unset($result, $j);

		return response()->json([
			'fail' => false,
			'info' => $macolla_data['macolla_name'],
			'data' => $collection,
			'total' => $total,
			'type' => (int) $type,
			'results' => $results,
			'status' => 200
		], 200);
    }

    public function searchPlan(Request $request)
    {
    	$type = $request->type;
		
		/** array of selected items */
		$items =  ["plan_surveys.id as id", "ps_date", "macolla_name", "location_name", "well_name", "reservoir_name", "md", "inclination", "azim_grid", "tvd", "vsec", "ns", "ew", "dls", "longitude", "latitude", "observations"];

		$size_items = sizeof($items);

		$sql = "";

		for ($i=0; $i < $size_items; $i++) { 
			if ($items[$i] == 'latitude') {
				$sql .= "IF((la_g = 0 AND la_m = 0 AND la_s = 0) OR (la_g = NULL OR la_m = NULL OR la_s = NULL), NULL, CONCAT(la_g, ' ', la_m, ' ', la_s)) AS latitude";
			} else if($items[$i] == 'longitude') {
				$sql .= "IF((lo_g <= 0 AND lo_m <= 0 AND lo_s <= 0) OR (lo_g = NULL OR lo_m = NULL OR lo_s = NULL), NULL, CONCAT(lo_g, ' ', lo_m, ' ', lo_s)) as longitude";
			} else {
				$sql .= $items[$i];
			}

			if ($i < $size_items-1) {
				$sql .= ", ";
			}
		}
		
		/* exception */
		$sql .= ", la_g, la_m, la_s, lo_g, lo_m, lo_s";

		$items_formatted = $sql; //explode(', ', $sql);

		$id_list = explode(',', $request->data);
		$sizeIs = sizeof($id_list);		
		
		$ids = $reservoir_ids = $ies = [];

		$reservoir_ids = !is_null($request->reservoir_id) ? explode(',', $request->reservoir_id) : null;

		for ($i = 0; $i < $sizeIs; $i++) $ids[$i] = (int) $id_list[$i];
		
		unset($id_list, $sizeIs);

		$collection = PlanSurvey::selectRaw($sql)
		->join('wells', 'wells.id', 'plan_surveys.well_id')
		->join('locations', 'locations.id', 'wells.location_id')
		->join('macollas', 'macollas.id', 'wells.macolla_id')
		->join('reservoirs', 'reservoirs.id', 'wells.reservoir_id')
		->where(function ($query) use($ids, $type) {
			if ($type == 1) $query->whereIn('plan_surveys.well_id', $ids);
			else $query->whereIn('wells.macolla_id', $ids);
		})->get();

		$macolla_data = Macolla::where('id', $request->macolla_id)->first();

		// data processing 
		$data = $filtered = $results = [];
		$i = 0;

		foreach ($collection as $key => $item) {
			$data[] = [
				++$i,
				$this->replace_space($item['macolla_name']),
				$this->replace_space($item['well_name']),
				$this->replace_space($item['location_name']),
				$this->replace_space($item['reservoir_name']),
				date('d/m/Y', strtotime($item['ps_date'])),
				$this->check_data($item['md'], 0, 2, 'md', $items),
				$this->check_data($item['inclination'], 0, 2, 'inclination', $items),
				$this->check_data($item['azim_grid'], 0, 2, 'azim_grid', $items),
				$this->check_data($item['tvd'], 0, 2, 'tvd', $items),
				$this->check_data($item['dls'], 100, 2, 'dls', $items),
				$this->check_data($item['vsec'], 0, 2, 'vsec', $items),
				$this->check_data($item['ns'], 0, 2,'ns', $items),
				$this->check_data($item['ew'], 0, 2, 'ew', $items),
				$this->format_text($item['latitude'], 'latitude', $items),
				$this->format_text($item['longitude'], 'longitude', $items),
				$this->format_text($item['observations'], 'observations', $items),
				'<div class="btn-group btn-group-sm"><button class="btn btn-info" id="btn-edit" data-toggle="modal" data-target="#modal-xl" title="Editar" value="'. $item['id'] .'"><i class="fa fa-edit"></i></button><button class="btn btn-danger" id="btn-delete" title="Borrar" value="'. $item['id'] .'"><i class="fa fa-trash"></i></button></div>',
				$this->setUndefined($item['la_g']),
				$this->setUndefined($item['la_m']),
				$this->setUndefined($item['la_s']),
				$this->setUndefined($item['lo_g']),
				$this->setUndefined($item['lo_m']),
				$this->setUndefined($item['lo_s'])
			];
		}
		
		$row_size = sizeof($data);
		
		for ($i = 0; $i < 20; $i++) {
			array_walk($data[$i], 
			function($v) use(&$filtered) {
				if (!is_null($v)) $filtered[] = $v;
			});
			
			$results[$i] = $filtered;
			unset($filtered);
		}
		
		unset($filtered, $i, $row_size);

		$total = null;

		if ($type == 2) {
			foreach ($collection as $key)  $ies[] = (array) $key->macolla_name;
			$result = array_unique($ies, SORT_REGULAR);
			$total = [];
			for ($i = 0, $j = 0; $i < sizeof($ies); $i++) {
				if(isset($result[$i])) {
					$total[$j] = $result[$i];
					$j++;
				}
			}
		}

		unset($result, $j);

		return response()->json([
			'fail' => false,
			'info' => $macolla_data['macolla_name'],
			'data' => $collection,
			'total' => $total,
			'type' => (int) $type,
			'results' => $results,
			'status' => 200
		], 200);
    }

    public function searchFiscalized(Request $request)
    {
    	$collection = Production::select('macolla_id', 'productions.id as id', 'fiscalized_date', 'production')->join('macollas', 'macollas.id', '=', 'productions.macolla_id')
		->where('productions.macolla_id', ((int) $request->id))
		->whereBetween('fiscalized_date', [$request->date_from, $request->date_to])
		->orderBy('fiscalized_date', 'ASC')
		->get();
		
		$name = Macolla::select('macolla_name')->where('id', ((int) $request->id))->first();

		$data = $dates = $filtered = $results = [];
		
		$i = 0;
		
		foreach ($collection as $key => $item) {
			$data[] = [
				++$i,
				date('d/m/Y', strtotime($item['fiscalized_date'])),
				$this->format_limit($item['production'], 10, 1),
				'-',
				'-',
				'-',
				'-',
				'-',
				'<div class="btn-group btn-group-sm"><button class="btn btn-info" id="btn-edit" data-toggle="modal" data-target="#modal-xl" title="Editar" value="'. $item['id'] .'"><i class="fa fa-edit"></i></button><button class="btn btn-danger" id="btn-delete" title="Borrar" value="'. $item['id'] .'"><i class="fa fa-trash"></i></button></div>',
			];
		}
		
		$row_size = sizeof($data);
		
		for ($i = 0; $i < $row_size; $i++) {
			array_walk($data[$i], 
			function($v) use(&$filtered) {
				if (!is_null($v)) $filtered[] = $v;
			});
			
			$results[$i] = $filtered;
			unset($filtered); /** remove */
		}
		
		unset($filtered, $i, $row_size); /** empty */
		
		$from = strtotime($request->date_from);
		$until = strtotime($request->date_to);

		for ($i = $from, $j = 0; $i < $until; $i+=86400, $j++) $dates[$j] = date('Y-m-d', $i);
		
		unset($i, $j, $from, $until); /** empty */

		return response()->json([
			'name' => $name['macolla_name'],
			'dates' => $dates,
			'data' => $collection,
			'results' => $results,
			'fail' => false,
            'status' => 200
        ], 200);
    }

    public function searchEvents(Request $request)
    {
		$type = $request->type;
		
		/** array of selected items */
		$items = ['events.id as id', 'well_name', 'location_name', 'reservoir_name', 'event_date', 'event_time', 'diluent', 'rpm', 'pump_efficiency', 'torque', 'frequency', 'current', 'power', 'mains_voltage', 'output_voltage', 'vfd_temperature', 'head_temperature', 'head_pressure', 'observations'];
		
		/** get id of selected locations */
		$id_list = explode(',', $request->data);
		$sizeIs = sizeof($id_list);
		
		/** store ids inside integer array */
		$ids = $reservoir_ids = $ies = [];

		/** get reservoir ids */
		$reservoir_ids = !is_null($request->reservoir_id) ? explode(',', $request->reservoir_id) : null;

		for ($i = 0; $i < $sizeIs; $i++) $ids[$i] = (int) $id_list[$i];
		
		unset($id_list, $sizeIs); /** empty */

		$collection = Event::select($items)
		->join('wells', 'wells.id', 'events.well_id')
		->join('locations', 'locations.id', 'wells.location_id')
		->join('reservoirs', 'reservoirs.id', 'wells.reservoir_id')
		->join('macollas', 'macollas.id', 'wells.macolla_id')
		->where(function ($query) use($ids, $type, $reservoir_ids) {
			if ($type == 1) $query->whereIn('events.well_id', $ids);
			else $query->whereIn('wells.macolla_id', $ids)->whereIn('wells.reservoir_id', $reservoir_ids);
		})
		->where('events.event_date', '>=', $request->date_from)
		->where('events.event_date', '<=', $request->date_to)
		->orderByRaw('event_date, event_time, macolla_name, location_name, identifier ASC')
		->get();
		
		$macolla_data = Macolla::where('id', $request->macolla_id)->first();
		
		// data processing 
		$data = $filtered = $results = [];
		$i = 0;
		
		foreach ($collection as $key => $item) {
			$data[] = [
				++$i,
				$this->replace_space($item['location_name']),
				$this->replace_space($item['well_name']),
				$this->replace_space($item['reservoir_name']),
				date('d/m/Y', strtotime($item['event_date'])),
				date('H:i', strtotime($item['event_time'])),
				$this->check_data($item['diluent'], 0, 0, 'diluent', $items),
				$this->check_data($item['rpm'], 0, 0, 'rpm', $items),
				$this->check_data($item['pump_efficiency'], 100, 2, 'pump_efficiency', $items),
				$this->check_data($item['torque'], 0, 2, 'torque', $items),
				$this->check_data($item['frequency'], 0, 2, 'frequency', $items),
				$this->check_data($item['current'], 0, 2, 'current', $items),
				$this->check_data($item['power'], 0, 2, 'power', $items),
				$this->check_data($item['mains_voltage'], 0, 2, 'mains_voltage', $items),
				$this->check_data($item['output_voltage'], 0, 2, 'output_voltage', $items),
				$this->check_data($item['vfd_temperature'], 0, 2, 'vfd_temperature', $items),
				$this->check_data($item['head_temperature'], 0, 2, 'head_temperature', $items),
				$this->check_data($item['head_pressure'], 0, 2, 'head_pressure', $items),
				$this->format_text($item['observations'], 'observations', $items),
                '<div class="btn-group btn-group-sm"><button class="btn btn-info" id="btn-edit" data-toggle="modal" data-target="#modal-xl" title="Editar" value="'. $item['id'] .'"><i class="fa fa-edit"></i></button><button class="btn btn-danger" id="btn-delete" value="'. $item['id'] .'"><i class="fa fa-trash" title="Borrar"></i></button></div>'
			];
		}
		
		$row_size = sizeof($data);
		
		for ($i = 0; $i < $row_size; $i++) {
			array_walk($data[$i], 
			function($v) use(&$filtered) {
				if (!is_null($v)) $filtered[] = $v;
			});
			
			$results[$i] = $filtered;
			unset($filtered); /** remove */
		}
		
		unset($filtered, $i, $row_size); /** empty */

		$total = null;

		if ($type == 2) {
			
			foreach ($collection as $key)  $ies[] = (array) $key->macolla_name;
			
			$result = array_unique($ies, SORT_REGULAR);
			
			$total = [];
			
			for ($i = 0, $j = 0; $i < sizeof($ies); $i++) {
				if(isset($result[$i])) {
					$total[$j] = $result[$i];
					$j++;
				}
			}
		}

		unset($result, $j); /** empty */

		return response()->json([
			'fail' => false,
			'info' => $macolla_data['macolla_name'],
			'data' => $collection,
			'total' => $total,
			'type' => (int) $type,
			'results' => $results,
			'status' => 200
		], 200);
    }

    public function format_limit($number, $limit, $decimal = 0)
    {
    	return !is_null($number) ? ($number < $limit && $number != 0) ? number_format($number, $decimal, ',', '.') : number_format($number, 0, ',', '') : null;
	}
	
	public function diff($start, $end)
	{
		return date("H:i", strtotime("00:00") + strtotime($end) - strtotime($start));
	}

	public function replace_space($str)
	{
		return str_replace(' ', '&nbsp;', $str);
	}

	public function getNamesReservoirs($id)
    {
        $query = Reservoir::select('reservoir_name')->where('id', $id)->first();
        return $query->reservoir_name;
    }

	public function decimal_to_fraction($float, $key, $array)
	{
		if (in_array($key, $array)) {
			if (is_numeric($float) && !is_null($float)) {
				$whole = floor($float);
				$decimal = $float-$whole;
				$leastCommonDenom = 48;
				$denominators = array(2, 3, 4, 8, 16, 24, 48);
				$roundDecimal = round($decimal * $leastCommonDenom) / $leastCommonDenom;
				
				if ($roundDecimal == 0) return $whole;
				else if ($roundDecimal == 1) return $whole + 1;

				foreach ($denominators as $d) {
					if ($roundDecimal * $d === floor($roundDecimal * $d)) {
						$denom = $d;
						break;
					}
				}
				return ($whole == 0 ? '' : $whole) . " " . ($roundDecimal * $denom) . "/". $denom;
			} else {
				return '-';
			}
		}
	}

	public function check_data($value, $limit, $decimals, $key, $array)
	{
		if (in_array($key, $array)) {
			if ($key == "gvf") {
				if (!is_null($value)) $value /= 100;
			} else if ($key == "pump_capacity") {
				if (!is_null($value)) $value *= 100;
			}
			return isset($value) ? $this->format_limit($value, $limit, $decimals) : '-';
		} else {
			return null;
		}
	}

	public function format_text($str, $key, $array)
	{
		if (in_array($key, $array)) {
			if (is_null($str)) return '-';
			else return str_replace(' ', '&nbsp;', $str);
		} else {
			return null;
		}
	}

	public function setUndefined($str)
	{
		return (is_null($str) ? 'undefined' : $str);
	}
}
