<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\{Macolla, Location, Camp, Block, Test, Average, Sign, Condition, Historical, StaticPressure, OriginalPressure, RealSurvey, DynamicPressure, PlanSurvey, Production, Log, Well, Event};
use Illuminate\Support\Facades\{Hash, Validator, Auth};
use Illuminate\Validation\Rule;
use App\Imports\{DataImport, DataFirstSheetImport};
use Maatwebsite\Excel\Facades\Excel;
use PhpOffice\PhpSpreadsheet\Shared\Date;

class UploadController extends Controller
{   
	public function indexProduction()
    {
		if(Auth::user()->role == "ADMIN" || Auth::user()->role == "OPERATOR") {
			return view('layouts.upload.production');
		} else {
			return redirect()->route('home');
		}
    }
    
    public function indexCondition()
    {
		if(Auth::user()->role == "ADMIN" || Auth::user()->role == "OPERATOR") {
			return view('layouts.upload.condition');
		} else {
			return redirect()->route('home');
		}
    }
    
    public function indexControlledProduction()
    {
		if(Auth::user()->role == "ADMIN" || Auth::user()->role == "OPERATOR") {
			return view('layouts.upload.fiscalized');
		} else {
			return redirect()->route('home');
		}
    }

    public function indexEvents()
    {
		if(Auth::user()->role == "ADMIN" || Auth::user()->role == "OPERATOR") {
			return view('layouts.upload.events');
		} else {
			return redirect()->route('home');
		}
    }
    
    public function uploadExtended(Request $request)
    {
		$file = $request->file('extendedTests');
		$extension = $file->getClientOriginalExtension();
		$filename = $file->getClientOriginalName();
		$path = public_path($filename);
		
		\Storage::disk('local')->put($filename, \File::get($file));
		
		$data['extension'] = $extension;
		
		$import = new DataImport();
		$import->sheets(0);
		$collection = $import->toCollection($filename);
		
		$sheet = $location = [];
		
		foreach($collection as $key => $value) {
			foreach($value as $index) {
				if ((!empty($index['extended_date']) && !empty($index['hour_start']) && !empty($index['hour_end'])) && (is_numeric($index['extended_date'])) && ((is_numeric($index['hour_start']) && is_numeric($index['hour_end'])) || ($this->convertHour($index['hour_start']) && $this->convertHour($index['hour_end'])) || (is_numeric($index['hour_start'])) && $this->convertHour($index['hour_end']) || ($this->convertHour($index['hour_start'])) && (is_numeric($index['hour_end'])) && !empty($index['identifier']))) {
					$sheet[] = [
						'extended_date' => Date::excelToDateTimeObject($index['extended_date']),
						'company' => $this->format_text($index['company']),
						'test' => $this->format_text($index['test']),
						'hour_start' => is_numeric($index['hour_start']) ? Date::excelToDateTimeObject($index['hour_start']) : ['date' => '2012-01-01 '.$this->convertHour($index['hour_start']).':00'],
						'hour_end' => is_numeric($index['hour_end']) ? Date::excelToDateTimeObject($index['hour_end']) : ['date' => '2012-01-01 '.$this->convertHour($index['hour_end']).':00'],
						'line_pressure' => is_numeric($index['line_pressure']) ? (double) $index['line_pressure'] : $this->setValue($index['line_pressure']),
						'line_temperature' => is_numeric($index['line_temperature']) ? (double) $index['line_temperature'] : $this->setValue($index['line_temperature']),
						'gas_rate_prod' => is_numeric($index['gas_rate_prod']) ? (double) $index['gas_rate_prod'] : $this->setValue($index['gas_rate_prod']),
						'water_rate_prod' => is_numeric($index['water_rate_prod']) ? (double) $index['water_rate_prod'] : $this->setValue($index['water_rate_prod']),
						'total_barrels' => is_numeric($index['total_barrels']) ? (double) $index['total_barrels'] : $this->setValue($index['total_barrels']),
						'oil_rate_prod' => is_numeric($index['oil_rate_prod']) ? (double) $index['oil_rate_prod'] : $this->setValue($index['oil_rate_prod']),
						'gvf' => is_numeric($index['gvf']) ? (double) $index['gvf'] : $this->setValue($index['gvf']),
						'inyected_diluent_rate' => is_numeric($index['inyected_diluent_rate']) ? (double) $index['inyected_diluent_rate'] : $this->setValue($index['inyected_diluent_rate']),
						'identifier' => $index['identifier']
					];
				}
			}
		}

		foreach($collection as $key => $value) {
			foreach($value as $index) {
				$location[] = [
					'location' => $index['location']
				];
			}
		}
		
		$nameLocation = '';

		for ($i = 0; $i < 1; $i++) {
			if (isset($location)) {
				$nameLocation = strtoupper(trim($location[$i]['location']));
			}
		}
		
		/* Clear data */
		$rows = sizeof($sheet);
		for ($i = 0; $i < $rows; $i++) {
			if (empty($sheet[$i]['test'])) {
				unset($sheet[$i]);
			}
		}
		
		sort($sheet);
		
		$data['size'] = sizeof($sheet);
		$data['sheet'] = $sheet;
		$data['location'] = $nameLocation;

		/* Check if does location */
		$verifyIfLocationExists = Location::where('location_name', $nameLocation)->get()->toArray();
		$my = ($verifyIfLocationExists) ? true : false;

		$macollaGotFromLocation = Location::select('macolla_name', 'location_name')
		->join('macollas', 'macollas.id', 'locations.macolla_id')
		->where('location_name', $nameLocation)
		->first();

		$data['macolla'] = $macollaGotFromLocation['macolla_name'];

		$data['my'] = $my;
		
		return $data;
    }
    
    public function uploadAverage(Request $request)
    {
		$file = $request->file('averageTests');
		$extension = $file->getClientOriginalExtension();
		$filename = $file->getClientOriginalName();
		$path = public_path($filename);
		
		\Storage::disk('local')->put($filename, \File::get($file));
		
		$data['extension'] = $extension;
		
		$import = new DataImport();
		$import->sheets(0);
		$collection = $import->toCollection($filename);
		
		$sheet = $macolla = [];
		
		foreach($collection as $key => $value) {
			foreach($value as $index) {
				$macolla[] = [
					'macolla' => $index['macolla']
				];
			}
		}
		
		$nameMacolla = '';

		for ($i = 0; $i < 1; $i++) {
			if (isset($macolla)) {
				$nameMacolla = strtoupper(trim($macolla[$i]['macolla']));
			}
		}

		$counter = 0;

		foreach($collection as $key => $value) {
			foreach($value as $index) {
				if (!empty($index['avg_start_test']) && !empty($index['avg_end_test']) && (is_numeric($index['avg_start_test']) && is_numeric($index['avg_end_test'])) && !empty($index['identifier'])) {
					$sheet[] = [
						'counter' => ++$counter,
						'location' => ($this->getValidLocationsFromMacolla($index['location'], $nameMacolla)) ? $this->getValidLocationsFromMacolla($index['location'], $nameMacolla) : null,
						'avg_start_test' => Date::excelToDateTimeObject($index['avg_start_test']),
						'avg_end_test' => Date::excelToDateTimeObject($index['avg_end_test']),
						'company' => $this->format_text($index['company']),
						'rpm' => is_numeric($index['rpm']) ? (int) $index['rpm'] : 0,
						'duration' => is_numeric($index['duration']) ? (double) $index['duration'] : $this->setValue($index['duration']),
						'line_pressure' => is_numeric($index['line_pressure']) ? (double) $index['line_pressure'] : $this->setValue($index['line_pressure']),
						'line_temperature' => is_numeric($index['line_temperature']) ? (double) $index['line_temperature'] : $this->setValue($index['line_temperature']),
						'gas_rate_prod' => is_numeric($index['gas_rate_prod']) ? (double) $index['gas_rate_prod'] : $this->setValue($index['gas_rate_prod']),
						'water_rate_prod' => is_numeric($index['water_rate_prod']) ? (double) $index['water_rate_prod'] : $this->setValue($index['water_rate_prod']),
						'fluid_total_rate' => is_numeric($index['fluid_total_rate']) ? (double) $index['fluid_total_rate'] : $this->setValue($index['fluid_total_rate']),
						'percent_mixture' => is_numeric($index['percent_mixture']) ? (double) $index['percent_mixture'] : $this->setValue($index['percent_mixture']),
						'api_degree_mixture' => is_numeric($index['api_degree_mixture']) ? (double) $index['api_degree_mixture'] : $this->setValue($index['api_degree_mixture']),
						'neat_rate_prod' => is_numeric($index['neat_rate_prod']) ? (double) $index['neat_rate_prod'] : $this->setValue($index['neat_rate_prod']),
						'percent_formation' => is_numeric($index['percent_formation']) ? (double) $index['percent_formation'] : $this->setValue($index['percent_formation']),
						'gvf' => is_numeric($index['gvf']) ? (double) $index['gvf'] : $this->setValue($index['gvf']),
						'inyected_diluent_rate' => is_numeric($index['inyected_diluent_rate']) ? (double) $index['inyected_diluent_rate'] : $this->setValue($index['inyected_diluent_rate']),
						'api_degree_diluent' => is_numeric($index['api_degree_diluent']) ? (double) $index['api_degree_diluent'] : $this->setValue($index['api_degree_diluent']),
						'test' => $this->format_text($index['test']),
						'pip' => is_numeric($index['pip']) ? (int) $index['pip'] : $this->setValue($index['pip']),
						'observations' => $this->format_text($index['observations']),
						'identifier' => $index['identifier']
					];
				}
			}
		}

		/* Clear data */
		$rows = sizeof($sheet);
		for ($i = 0; $i < $rows; $i++) {
			if (empty($sheet[$i]['location'])) {
				unset($sheet[$i]);
			}
		}

		sort($sheet);
		
		$data['size'] = sizeof($sheet);
		$data['sheet'] = $sheet;

		/* Check if does location */
		$verifyIfMacollaExists = Macolla::where('macolla_name', $nameMacolla)->first();
		$my = ($verifyIfMacollaExists) ? true : false;

		$data['macolla'] = $nameMacolla;
		$data['my'] = $my;
		
		return $data;
    }
    
    public function uploadSign(Request $request)
    {
		$file = $request->file('signs');
		$extension = $file->getClientOriginalExtension();
		$filename = $file->getClientOriginalName();
		$path = public_path($filename);
		
		\Storage::disk('local')->put($filename, \File::get($file));

		$data['extension'] = $extension;
		
		$import = new DataImport();
		$import->sheets(0);
		$collection = $import->toCollection($filename);
		
		$sheet = $location = [];
		
		$counter = 0;
		foreach($collection as $key => $value){
			foreach($value as $index) {
				if (!empty($index['sign_date']) && !empty($index['sign_time']) && (is_numeric($index['sign_date']) && is_numeric($index['sign_time'])) && !empty($index['identifier'])) {
					$sheet[] = [
						'counter' => ++$counter,
						'sign_date' => Date::excelToDateTimeObject($index['sign_date']),
						'company' => $this->format_text($index['company']),
						'sign_time' => Date::excelToDateTimeObject($index['sign_time']),
						'percent_mixture' => is_numeric($index['percent_mixture']) ? (double) $index['percent_mixture'] : $this->setValue($index['percent_mixture']),
						'percent_sediment' => is_numeric($index['percent_sediment']) ? (double) $index['percent_sediment'] : $this->setValue($index['percent_sediment']),
						'api_degree_diluted' => is_numeric($index['api']) ? (double) $index['api'] : $this->setValue($index['api']),
						'ptb' => is_numeric($index['ptb']) ? (double) $index['ptb'] : $this->setValue($index['ptb']),
						'identifier' => $index['identifier']
					];
				}
			}
		}
		
		foreach($collection as $key => $value) {
			foreach($value as $index) {
				$location[] = [
					'location' => $index['location']
				];
			}
		}

		$nameLocation = '';

		for ($i = 0; $i < 1; $i++) {
			if (isset($location)) {
				$nameLocation = strtoupper(trim($location[$i]['location']));
			}
		}
		
		sort($sheet);
		
		$data['size'] = sizeof($sheet);
		$data['sheet'] = $sheet;
		$data['location'] = $nameLocation;

		/* Check if does location */
		$verifyIfLocationExists = Location::where('location_name', $nameLocation)->get()->toArray();
		$my = ($verifyIfLocationExists) ? true : false;

		$macollaGotFromLocation = Location::select('macolla_name', 'location_name')
		->join('macollas', 'macollas.id', 'locations.macolla_id')
		->where('location_name', $nameLocation)
		->first();

		$data['macolla'] = $macollaGotFromLocation['macolla_name'];

		$data['my'] = $my;
		
		return $data;
    }
    
    public function uploadCondition(Request $request)
    {
		$file = $request->file('condition');
		$extension = $file->getClientOriginalExtension();
		$filename = $file->getClientOriginalName();
		$path = public_path($filename);
		
		\Storage::disk('local')->put($filename, \File::get($file));
		
		$data['extension'] = $extension;
		
		$import = new DataImport();
		$import->sheets(0);
		$collection = $import->toCollection($filename);
		
		$sheet = $location = [];
		
		foreach($collection as $key => $value){
			foreach($value as $index) {
				if (!empty($index['date']) && is_numeric($index['date']) && !empty($index['identifier'])) {
					$sheet[] = [
						'company' => $this->format_text($index['company']),
						'reservoir' => $this->setValueWithout($index['reservoir']),
						'sand' => $this->setValueWithout($index['sand']),
						'pt_date' => Date::excelToDateTimeObject($index['date']),
						'test' => $this->format_text($index['test']),
						'deep_md' => is_numeric($index['deep_md']) ? (double) $index['deep_md'] : $this->setValue($index['deep_md']),
						'deep_tvd' => is_numeric($index['deep_tvd']) ? (double) $index['deep_tvd'] : $this->setValue($index['deep_tvd']),
						'temperature' => is_numeric($index['temperature']) ? (double) $index['temperature'] : $this->setValue($index['temperature']),
						'duration' => is_numeric($index['duration']) ? (integer) $index['duration'] : $this->setValue($index['duration']),
						'pressbefore' => is_numeric($index['pressbefore']) ? (double) $index['pressbefore'] : $this->setValue($index['pressbefore']),
						'pressafter' => is_numeric($index['pressafter']) ? (double) $index['pressafter'] : $this->setValue($index['pressafter']),
						'pressformation' => is_numeric($index['pressformation']) ? (double) $index['pressformation'] : $this->setValue($index['pressformation']),
						'mobility' => is_numeric($index['mobility']) ? (double) $index['mobility'] : $this->setValue($index['mobility']),
						'observations' => $this->format_text($index['observations']),
						'identifier' => $index['identifier']
					];
				}
			}
		}
		
		foreach($collection as $key => $value) {
			foreach($value as $index) {
				$location[] = ['location' => $index['location']];
			}
		}

		$nameLocation = '';

		for ($i = 0; $i < 1; $i++) {
			if (isset($location)) {
				$nameLocation = strtoupper(trim($location[$i]['location']));
			}
		}
		
		$data['size'] = sizeof($sheet);
		$data['sheet'] = $sheet;
		$data['location'] = $nameLocation;

		/* Check if does location */
		$verifyIfLocationExists = Location::where('location_name', $nameLocation)->get()->toArray();
		$my = ($verifyIfLocationExists) ? true : false;

		$macollaGotFromLocation = Location::select('macolla_name', 'location_name')
		->join('macollas', 'macollas.id', 'locations.macolla_id')
		->where('location_name', $nameLocation)
		->first();

		$data['macolla'] = $macollaGotFromLocation['macolla_name'];

		$data['my'] = $my;
		
		return $data;
    }
    
    public function uploadHistorical(Request $request)
    {
		$file = $request->file('historical');
		$extension = $file->getClientOriginalExtension();
		$filename = $file->getClientOriginalName();
		$path = public_path($filename);
		
		\Storage::disk('local')->put($filename, \File::get($file));
		
		$data['extension'] = $extension;
		
		$import = new DataImport();
		$import->sheets(0);
		$collection = $import->toCollection($filename);
		
		$sheet = $macolla = [];

		foreach($collection as $key => $value) {
			foreach($value as $index) {
				$macolla[] = ['macolla' => $index['macolla']];
			}
		}
		
		$nameMacolla = '';

		for ($i = 0; $i < 1; $i++) {
			if (isset($macolla)) {
				$nameMacolla = strtoupper(trim($macolla[$i]['macolla']));
			}
		}
		
		$aux_date = "2012-01-01 12:00:00"; /* default time */
		$begin_valid = $end_valid = "";
		
		foreach($collection as $key => $value){
			foreach($value as $index) {
				if (empty($index['begin_date']) && !empty($index['end_date'])) {
					$begin_valid = Date::excelToDateTimeObject($index['end_date']);
					$end_valid = Date::excelToDateTimeObject($index['end_date']);
				} else if (empty($index['end_date'])) {
					$end_valid = ["date" => $aux_date];
					$begin_valid = ["date" => $aux_date];
				} else if (!empty($index['begin_date']) && !empty($index["end_date"])) {
					$begin_valid = Date::excelToDateTimeObject($index['begin_date']);;
					$end_valid = Date::excelToDateTimeObject($index['end_date']);;
				} if (empty($index['begin_date']) && empty($index['end_date'])) {
					$end_valid = ["date" => $aux_date];
					$begin_valid = ["date" => $aux_date];
				}
				
				$sheet[] = [
					'location' => ($this->getValidLocationsFromMacolla($index['location'], $nameMacolla)) ? $this->getValidLocationsFromMacolla($index['location'], $nameMacolla) : null,
					'reservoir' => $this->format_text($index['reservoir']),
					'begin_date' => $begin_valid, 
					'end_date' => $end_valid,
					'type_work_i' => $this->format_text($index['type_work_i']),
					'type_work_ii' => $this->format_text($index['type_work_ii']),
					'rig' => $this->format_text($index['rig']),
					'sensor_model' => $this->format_text($index['sensor_model']),
					'sensor_diameter' => is_numeric($index['sensor_diameter']) ? (double) $index['sensor_diameter'] : $this->setValue($index['sensor_diameter']),
					'sensor_company' => $this->format_text($index['sensor_company']),
					'sensor_brand' => $this->format_text($index['sensor_brand']),
					'deeptop_sensor_md' => is_numeric($index['deeptop_sensor_md']) ? (double) $index['deeptop_sensor_md'] : $this->setValue($index['deeptop_sensor_md']),
					'deepbottom_sensor_md' => is_numeric($index['deepbottom_sensor_md']) ? (double) $index['deepbottom_sensor_md'] : $this->setValue($index['deepbottom_sensor_md']),
					'pump_model' => $this->format_text($index['pump_model']),
					'pump_type' => $this->format_text($index['pump_type']),
					'pump_capacity' => is_numeric($index['pump_capacity']) ? (double) $index['pump_capacity'] : $this->setValue($index['pump_capacity']),
					'pump_diameter' => is_numeric($index['pump_diameter']) ? (double) $index['pump_diameter'] : $this->setValue($index['pump_diameter']),
					'pump_company' => $this->format_text($index['pump_company']),
					'deeptop_pump_md' => is_numeric($index['deeptop_pump_md']) ? (double) $index['deeptop_pump_md'] : $this->setValue($index['deeptop_pump_md']),
					'deepbottom_pump_md' => is_numeric($index['deepbottom_pump_md']) ? (double) $index['deepbottom_pump_md'] : $this->setValue($index['deepbottom_pump_md']),
					'inyected_diluent' => $this->format_text($index['inyected_diluent']),
					'measure_from' => $this->format_text($index['measure_from']),
					'observations' => trim($this->format_text($index['observations'])),
					'references' => trim($this->format_text($index['references'])),
					'identifier' => $index['identifier']
				];
			}
		}

		foreach($collection as $key => $value) {
			foreach($value as $index) {
				$location[] = [
					'location' => $index['location']
				];
			}
		}
		
		/* Clear data */
		$rows = sizeof($sheet);
		for ($i = 0; $i < $rows; $i++) {
			if (empty($sheet[$i]['location'])) {
				unset($sheet[$i]);
			}
		}
		
		sort($sheet);
		
		$data['size'] = sizeof($sheet);
		$data['sheet'] = $sheet;

		/* Check if does location */
		$verifyIfMacollaExists = Macolla::where('macolla_name', $nameMacolla)->first();
		$my = ($verifyIfMacollaExists) ? true : false;

		$data['macolla'] = $nameMacolla;
		$data['my'] = $my;
		
		return $data;
    }
    
    public function uploadStaticPressure(Request $request)
    {
		$file = $request->file('staticPressure');
		$extension = $file->getClientOriginalExtension();
		$filename = $file->getClientOriginalName();
		$path = public_path($filename);
		
		\Storage::disk('local')->put($filename, \File::get($file));
		
		$data['extension'] = $extension;
		
		$import = new DataImport();
		$import->sheets(0);
		$collection = $import->toCollection($filename);
		
		$sheet = $macolla = [];

		foreach($collection as $key => $value) {
			foreach($value as $index) {
				$macolla[] = [
					'macolla' => $index['macolla']
				];
			}
		}
		
		$nameMacolla = '';

		for ($i = 0; $i < 1; $i++) {
			if (isset($macolla)) {
				$nameMacolla = strtoupper(trim($macolla[$i]['macolla']));
			}
		}
		
		foreach($collection as $key => $value) {
			foreach($value as $index) {
				if (!empty($index['static_date']) && is_numeric($index['static_date']) && !empty($index['identifier'])) {
					$sheet[] = [
						'location' => ($this->getValidLocationsFromMacolla($index['location'], $nameMacolla)) ? $this->getValidLocationsFromMacolla($index['location'], $nameMacolla) : null,
						'static_date' => Date::excelToDateTimeObject($index['static_date']),
						'quality' => $this->format_text($index['quality']),
						'pip' => is_numeric($index['pip']) ? (double) $index['pip'] : $this->setValue($index['pip']),
						'pdp' => is_numeric($index['pdp']) ? (double) $index['pdp'] : $this->setValue($index['pdp']),
						'temperature' => is_numeric($index['temperature']) ? (double) $index['temperature'] : $this->setValue($index['temperature']),
						'observations' => $this->format_text($index['observations']),
						'identifier' => $index['identifier']
					];
				}
			}
		}
		
		/* Clear data */
		$rows = sizeof($sheet);
		for ($i = 0; $i < $rows; $i++) {
			if (empty($sheet[$i]['location'])) {
				unset($sheet[$i]);
			}
		}
		
		sort($sheet);
		
		$data['size'] = sizeof($sheet);
		$data['sheet'] = $sheet;

		/* Check if does location */
		$verifyIfMacollaExists = Macolla::where('macolla_name', $nameMacolla)->first();
		$my = ($verifyIfMacollaExists) ? true : false;

		$data['macolla'] = $nameMacolla;
		$data['my'] = $my;
		
		return $data;
    }
    
    public function uploadDynamicPressure(Request $request)
    {
		$file = $request->file('dynamicPressure');
		$extension = $file->getClientOriginalExtension();
		$filename = $file->getClientOriginalName();
		$path = public_path($filename);
		
		\Storage::disk('local')->put($filename, \File::get($file));

		$data['extension'] = $extension;
		
		$import = new DataImport();
		$import->sheets(0);
		$collection = $import->toCollection($filename);
		
		$sheet = $macolla = [];

		foreach($collection as $key => $value) {
			foreach($value as $index) {
				$macolla[] = [
					'macolla' => $index['macolla']
				];
			}
		}
		
		$nameMacolla = '';

		for ($i = 0; $i < 1; $i++) {
			if (isset($macolla)) {
				$nameMacolla = strtoupper(trim($macolla[$i]['macolla']));
			}
		}
		
		foreach($collection as $key => $value) {
			foreach($value as $index) {
				if (!empty($index['dynamic_date']) && is_numeric($index['dynamic_date']) && !empty($index['identifier'])) {
					$sheet[] = [
						'location' => ($this->getValidLocationsFromMacolla($index['location'], $nameMacolla)) ? $this->getValidLocationsFromMacolla($index['location'], $nameMacolla) : null,
						'dynamic_date' => Date::excelToDateTimeObject($index['dynamic_date']),
						'quality' => $this->format_text($index['quality']),
						'pip' => is_numeric($index['pip']) ? (double) $index['pip'] : $this->setValue($index['pip']),
						'pdp' => is_numeric($index['pdp']) ? (double) $index['pdp'] : $this->setValue($index['pdp']),
						'temperature' => is_numeric($index['temperature']) ? (double) $index['temperature'] : $this->setValue($index['temperature']),
						'observations' => $this->format_text($index['observations']),
						'identifier' => $index['identifier']
					];
				}
			}
		}
		
		/* Clear data */
		$rows = sizeof($sheet);
		for ($i = 0; $i < $rows; $i++) {
			if (empty($sheet[$i]['location'])) {
				unset($sheet[$i]);
			}
		}
		
		sort($sheet);
		
		$data['size'] = sizeof($sheet);
		$data['sheet'] = $sheet;

		/* Check if does location */
		$verifyIfMacollaExists = Macolla::where('macolla_name', $nameMacolla)->first();
		$my = ($verifyIfMacollaExists) ? true : false;

		$data['macolla'] = $nameMacolla;
		$data['my'] = $my;
		
		return $data;
    }
    
    public function uploadOriginalPressure(Request $request)
    {
		$file = $request->file('originalPressure');
		$extension = $file->getClientOriginalExtension();
		$filename = $file->getClientOriginalName();
		$path = public_path($filename);
		
		\Storage::disk('local')->put($filename, \File::get($file));
		
		$data['extension'] = $extension;
		
		$import = new DataImport();
		$import->sheets(0);
		$collection = $import->toCollection($filename);
		
		$sheet = [];
		
		foreach($collection as $key => $value) {
			foreach($value as $index) {
				if (!empty($index['pressure_date']) && is_numeric($index['pressure_date']) && !empty($index['identifier'])) {
					$sheet[] = [
						'location' => trim($this->getNameLocation($index['location'])),
						'reservoir' => trim($this->format_text($index['reservoir'])),
						'pressure_date' => Date::excelToDateTimeObject($index['pressure_date']),
						'type_test' => $this->format_text($index['type_test']),
						'pressure_gradient' => is_numeric($index['pressure_gradient']) ? (double) $index['pressure_gradient'] : $this->setValue($index['pressure_gradient']),
						'sensor_model' => $this->format_text($index['sensor']),
						'deepdatum_mc_md' => is_numeric($index['deepdatum_mc_md']) ? (double) $index['deepdatum_mc_md'] : $this->setValue($index['deepdatum_mc_md']),
						'deepdatum_mc_tvd' => is_numeric($index['deepdatum_mc_tvd']) ? (double) $index['deepdatum_mc_tvd'] : $this->setValue($index['deepdatum_mc_tvd']),
						'press_datum_macolla' => is_numeric($index['press_datum_macolla']) ? (double) $index['press_datum_macolla'] : $this->setValue($index['press_datum_macolla']),
						'temp_datum_macolla' => is_numeric($index['temp_datum_macolla']) ? (double) $index['temp_datum_macolla'] : $this->setValue($index['temp_datum_macolla']),
						'temperature_gradient' => is_numeric($index['temperature_gradient']) ? (double) $index['temperature_gradient'] : $this->setValue($index['temperature_gradient']),
						'identifier' => $index['identifier']
					];
				}
			}
		}
		
		/* Clear data */
		$rows = sizeof($sheet);
		for ($i = 0; $i < $rows; $i++) {
			if (empty($sheet[$i]['location'])) {
				unset($sheet[$i]);
			}
		}
		
		sort($sheet);
		
		$data['size'] = sizeof($sheet);
		$data['sheet'] = $sheet;
		$data['my'] = $sheet ? true : false;
		
		return $data;
    }
    
    public function uploadRealSurvey(Request $request)
    {
		$file = $request->file('realSurvey');
		$extension = $file->getClientOriginalExtension();
		$filename = $file->getClientOriginalName();
		$path = public_path($filename);
		
		\Storage::disk('local')->put($filename, \File::get($file));
		
		$data['extension'] = $extension;
		
		$import = new DataImport();
		$import->sheets(0);
		$collection = $import->toCollection($filename);
		
		$sheet = [];
		
		$latitude = $longitude = '';

		foreach($collection as $key => $value){
			foreach($value as $index) {
				if (!empty($index['real_date']) && !empty($index['identifier']) && is_numeric($index['real_date']) ) {
					$sheet[] = [
						'location' => trim($this->getNameLocation($index['location'])),
						'rs_date' => Date::excelToDateTimeObject($index['real_date']),
						'hole' => $this->format_text($index['hole']),
						'company' => $this->format_text($index['company']),
						'md' => is_numeric($index['md']) ? (double) $index['md'] : $this->setValue($index['md']),
						'inclination' => is_numeric($index['inclination']) ? (double) $index['inclination'] : $this->setValue($index['inclination']),
						'azim_grid' => is_numeric($index['azim_grid']) ? (double) $index['azim_grid'] : $this->setValue($index['azim_grid']),
						'tvd' => is_numeric($index['tvd']) ? (double) $index['tvd'] : $this->setValue($index['tvd']),
						'vsec' => is_numeric($index['vsec']) ? (double) $index['vsec'] : $this->setValue($index['vsec']),
						'ns' => is_numeric($index['ns']) ? (double) $index['ns'] : $this->setValue($index['ns']),
						'ew' => is_numeric($index['ew']) ? (double) $index['ew'] : $this->setValue($index['ew']),
						'dls' => is_numeric($index['dls']) ? (double) $index['dls'] : $this->setValue($index['dls']),
						'la_g' => is_numeric($index['la_g']) ? (double) $index['la_g'] : $this->setValue($index['la_g']), 
						'la_m' => is_numeric($index['la_m']) ? (double) $index['la_m'] : $this->setValue($index['la_m']),
						'la_s' => is_numeric($index['la_s']) ? (double) $index['la_s'] : $this->setValue($index['la_s']),
						'lo_g' => is_numeric($index['lo_g']) ? (double) $index['lo_g'] : $this->setValue($index['lo_g']), 
						'lo_m' => is_numeric($index['lo_m']) ? (double) $index['lo_m'] : $this->setValue($index['lo_m']),
						'lo_s' => is_numeric($index['lo_s']) ? (double) $index['lo_s'] : $this->setValue($index['lo_s']),
						'observations' => $this->format_text($index['observations']),
						'identifier' => $index['identifier']
					];
				}
			}
		}
		
		/* Clear data */
		$rows = sizeof($sheet);
		for ($i = 0; $i < $rows; $i++) {
			if (empty($sheet[$i]['location'])) {
				unset($sheet[$i]);
			}
		}
		
		sort($sheet);
		
		$data['size'] = sizeof($sheet);
		$data['sheet'] = $sheet;
		$data['my'] = $sheet ? true : false;
		
		return $data;
    }
    
    public function uploadPlanSurvey(Request $request)
    {
		$file = $request->file('planSurvey');
		$extension = $file->getClientOriginalExtension();
		$filename = $file->getClientOriginalName();
		$path = public_path($filename);
		
		\Storage::disk('local')->put($filename, \File::get($file));
		
		$data['extension'] = $extension;
		
		$import = new DataImport();
		$import->sheets(0);
		$collection = $import->toCollection($filename);
		
		$sheet = [];
		
		foreach($collection as $key => $value){
			foreach($value as $index) {
				if (!empty($index['plan_date']) && !empty($index['identifier']) && is_numeric($index['plan_date']) ) {
					$sheet[] = [
						'location' => trim($this->getNameLocation($index['location'])),
						'ps_date' => Date::excelToDateTimeObject($index['plan_date']),
						'md' => is_numeric($index['md']) ? (double) $index['md'] : $this->setValue($index['md']),
						'inclination' => is_numeric($index['inclination']) ? (double) $index['inclination'] : $this->setValue($index['inclination']),
						'azim_grid' => is_numeric($index['azim_grid']) ? (double) $index['azim_grid'] : $this->setValue($index['azim_grid']),
						'tvd' => is_numeric($index['tvd']) ? (double) $index['tvd'] : $this->setValue($index['tvd']),
						'vsec' => is_numeric($index['vsec']) ? (double) $index['vsec'] : $this->setValue($index['vsec']),
						'ns' => is_numeric($index['ns']) ? (double) $index['ns'] : $this->setValue($index['ns']),
						'ew' => is_numeric($index['ew']) ? (double) $index['ew'] : $this->setValue($index['ew']),
						'dls' => is_numeric($index['dls']) ? $index['dls'] : $this->setValue($index['dls']),
						'la_g' => is_numeric($index['la_g']) ? (double) $index['la_g'] : $this->setValue($index['la_g']), 
						'la_m' => is_numeric($index['la_m']) ? (double) $index['la_m'] : $this->setValue($index['la_m']),
						'la_s' => is_numeric($index['la_s']) ? (double) $index['la_s'] : $this->setValue($index['la_s']),
						'lo_g' => is_numeric($index['lo_g']) ? (double) $index['lo_g'] : $this->setValue($index['lo_g']), 
						'lo_m' => is_numeric($index['lo_m']) ? (double) $index['lo_m'] : $this->setValue($index['lo_m']),
						'lo_s' => is_numeric($index['lo_s']) ? (double) $index['lo_s'] : $this->setValue($index['lo_s']),
						'observations' => $this->format_text($index['observations']),
						'identifier' => $index['identifier']
					];
				}
			}
		}
		
		/* Clear data */
		$rows = sizeof($sheet);
		for ($i = 0; $i < $rows; $i++) {
			if (empty($sheet[$i]['location'])) {
				unset($sheet[$i]);
			}
		}
		
		sort($sheet);
		
		$data['size'] = sizeof($sheet);
		$data['sheet'] = $sheet;
		$data['my'] = $sheet ? true : false;
		
		return $data;
    }
    
    public function uploadFiscalized(Request $request)
    {
		$file = $request->file('fiscalized');
		$extension = $file->getClientOriginalExtension();
		$filename = $file->getClientOriginalName();
		$path = public_path($filename);
		
		\Storage::disk('local')->put($filename, \File::get($file));
		
		$data['extension'] = $extension;
		
		$import = new DataImport();
		$import->sheets(0);
		$collection = $import->toCollection($filename);
		
		$sheet = $macolla = [];
		
		foreach($collection as $key => $value) {
			foreach($value as $index) {
				$macolla[] = [
					'macolla' => $index['macolla']
				];
			}
		}
		
		$nameMacolla = '';

		for ($i = 0; $i < 1; $i++) {
			if (isset($macolla)) {
				$nameMacolla = strtoupper(trim($macolla[$i]['macolla']));
			}
		}
		
		foreach($collection as $key => $value) {
			foreach($value as $index) {
				if (!empty($index['fiscalized_date']) && (is_numeric($index['fiscalized_date']))  && !empty($index['identifier'])) {
					$sheet[] = [
						'fiscalized_date' => Date::excelToDateTimeObject($index['fiscalized_date']),
						'production' => $index['production'] == null ? 0 : (double) $index['production'],
						'identifier' => $index['identifier']
					];
				}
			}
		}
		
		$data['size'] = sizeof($sheet);
		$data['sheet'] = $sheet;
		
		/* Check if does location */
		$verifyIfMacollaExists = Macolla::where('macolla_name', $nameMacolla)->first();
		$my = ($verifyIfMacollaExists) ? true : false;

		$data['macolla'] = $nameMacolla;
		$data['my'] = $my;
		
		return $data;
	}
	
	public function uploadEvents(Request $request)
    {
		$file = $request->file('events');
		$extension = $file->getClientOriginalExtension();
		$filename = $file->getClientOriginalName();
		$path = public_path($filename);
		
		\Storage::disk('local')->put($filename, \File::get($file));
		
		$data['extension'] = $extension;
		
		$import = new DataImport();
		$import->sheets(0);
		$collection = $import->toCollection($filename);
		

		foreach($collection as $key => $value) {
			foreach($value as $index) {
				$well[] = ['well' => $index['well']];
				$reservoir[] = ['reservoir' => $index['reservoir']];
			}
		}
		
		$nameWell = $nameReservoir = '';

		for ($i = 0; $i < 1; $i++) {
			if (isset($well)) $nameWell = strtoupper(trim($well[$i]['well']));
			if (isset($reservoir)) $nameReservoir = strtoupper(trim($reservoir[$i]['reservoir']));
		}

		$sheet = $well = $reservoir = [];

		foreach($collection as $key => $value) {
			foreach($value as $index) {
				if ( (!empty($index['date'])) && (!empty($index['time'])) && is_numeric($index['date']) && is_numeric($index['time']) && (!empty($index['identifier'])) ) {

					$sheet[] = [
						'reservoir' => $nameReservoir,
						'event_date' => Date::excelToDateTimeObject($index['date']),
						'event_time' => is_numeric($index['time']) ? Date::excelToDateTimeObject($index['time']) : ['event_time' => $this->convertHour($index['time'].':00')],
						'diluent' => is_numeric($index['diluent']) ? (double) $index['diluent'] : $this->setValue($index['diluent']),
						'rpm' => is_numeric($index['rpm']) ? (double) $index['rpm'] : $this->setValue($index['rpm']),
						'pump_efficiency' => is_numeric($index['pump_efficiency']) ? (double) $index['pump_efficiency'] : $this->setValue($index['pump_efficiency']),
						'torque' => is_numeric($index['torque']) ? (double) $index['torque'] : $this->setValue($index['torque']),
						'frequency' => is_numeric($index['frequency']) ? (double) $index['frequency'] : $this->setValue($index['frequency']),
						'current' => is_numeric($index['current']) ? (double) $index['current'] : $this->setValue($index['current']),
						'head_pressure' => is_numeric($index['head_pressure']) ? (double) $index['head_pressure'] : $this->setValue($index['head_pressure']),
						'power' => is_numeric($index['power']) ? (double) $index['power'] : $this->setValue($index['power']),
						'mains_voltage' => is_numeric($index['mains_voltage']) ? (double) $index['mains_voltage'] : $this->setValue($index['mains_voltage']),
						'output_voltage' => is_numeric($index['output_voltage']) ? (double) $index['output_voltage'] : $this->setValue($index['output_voltage']),
						'vfd_temperature' => is_numeric($index['vfd_temperature']) ? (double) $index['vfd_temperature'] : $this->setValue($index['vfd_temperature']),
						'head_temperature' => is_numeric($index['head_temperature']) ? (double) $index['head_temperature'] : $this->setValue($index['head_temperature']),
						'head_pressure' => is_numeric($index['head_pressure']) ? (double) $index['head_pressure'] : $this->setValue($index['head_pressure']),
						'observations' => $this->format_text($index['observations']),
						'identifier' => $index['identifier']
					];
				}
			}
		}
		
		sort($sheet);
		
		$data['size'] = sizeof($sheet);
		$data['sheet'] = $sheet;
		$data['well'] = $nameWell;
		$data['reservoir'] = $nameReservoir;

		/* Check if does location */
		$verifyIfWellExists = Well::where('well_name', $nameWell)->get()->toArray();
		$my = ($verifyIfWellExists) ? true : false;

		$macollaGotFromWellName = Well::select('macolla_name', 'well_name')
		->join('macollas', 'macollas.id', 'wells.macolla_id')
		->where('well_name', $nameWell)
		->first();

		$data['macolla'] = $macollaGotFromWellName['macolla_name'];

		$data['my'] = $my;
		
		return $data;
    }
    
    // Finally save file to database
    /* Save extended */
    public function saveExtended(Request $request)
    {
		$collection = json_decode($request->elements);
		$getIdByName = Location::select('id')->where('location_name', $request->location_name)->first();
		
		$id = $getIdByName['id'];
		
		$exist = Location::findOrFail($id);

		foreach($collection as $key) {
			$key->well_id = $id;
		}

		$data = array();
		
		foreach($collection as $key) {
			$data[] = (array) $key;
		}
		
		/* get collection from extended tests */
		$query = Test::join('wells', 'extended_tests.well_id', '=', 'wells.id')
		->join('locations', 'wells.location_id', '=', 'locations.id')
		->select('extended_tests.well_id', 'extended_date', 'identifier', 'company', 'test', 'hour_start', 'hour_end', 'line_pressure', 'line_temperature', 'gas_rate_prod', 'water_rate_prod', 'total_barrels', 'neat_rate', 'gvf', 'inyected_diluent_rate')
		->where('locations.id', $id)
		->get();

		$db = $query->toArray();
		
		/* get data */
		$get = $this->dataToSave($data, $db);

		Test::insert($get['store']);

		$this->setLogActivity($get['store'], 'Pruebas Extendidas de '.$request->location_name);

		return response()->json([
			'msgStore' => 'Se han agregado '.sizeof($get['store']).' registro(s) nuevo(s)',
			'message' => sizeof($get['repeated']) > 0 ? sizeof($get['repeated']) . ' identificadores repetidos' : null,
			'store' => $get['store'],
			'repeated' => $get['repeated'],
			'status' => 200
		], 200);
	}
    
    /* Save average */
    public function saveAverage(Request $request)
    {
		$collection = json_decode($request->elements);
		$collectNames = json_decode($request->names);
		
		$ids = $names = array();
		
		// Get names locations
		foreach($collectNames as $key) {
			$names[] = (array) $key->location;
		}
		
		// Get macolla name
		$macollaName = $request->macolla_name;
		
		// Convert name locations to locations ID
		for ($i = 0; $i < count($names); $i++) {
			$ids[$i] = $this->getIdByName(implode('', $names[$i]));
		}
		
		$count = 0;
		foreach($collection as $key) {
			$key->well_id = $ids[$count]['id'];
			$count++;
		}
		
		$data = array();
		foreach($collection as $key) {
			$data[] = (array) $key;
		}
		
		//Get collection from average tests
		$query = Average::join('wells', 'average_tests.well_id', '=', 'wells.id')
		->join('locations', 'wells.location_id', '=', 'locations.id')
		->join('macollas', 'wells.macolla_id', '=', 'macollas.id')
		->select('average_tests.well_id', 'avg_start_test', 'avg_end_test', 'company', 'rpm', 'duration', 'line_pressure', 'line_temperature', 'gas_rate_prod', 'water_rate_prod', 'fluid_total_rate', 'percent_mixture', 'api_degree_mixture', 'neat_rate_prod', 'percent_formation', 'gvf', 'inyected_diluent_rate', 'api_degree_diluent', 'test', 'pip', 'observations', 'identifier')
		->where('macollas.macolla_name', '=', ''.$macollaName.'')
		->get();
		   
		$db = $query->toArray();

		/* get data */
		$get = $this->dataToSave($data, $db);

		Average::insert($get['store']);
		
		$this->setLogActivity($get['store'], 'Pruebas Promedio de '.$macollaName);
		
		return response()->json([
			'msgStore' => 'Se han agregado '.sizeof($get['store']).' registro(s) nuevo(s)',
			'message' => sizeof($get['repeated']) > 0 ? sizeof($get['repeated']) . ' identificadores repetidos' : null,
			'store' => $get['store'],
			'repeated' => $get['repeated'],
			'status' => 200
		], 200);
    }
    
    /* Save sign */
    public function saveSign(Request $request)
    {
		$collection = json_decode($request->elements);
		$getIdByLocationName = $this->getIdByName($request->location_name);
		
		$id = $getIdByLocationName['id'];
		
		foreach($collection as $key) {
			$key->well_id = $id;
		}
		
		$exist = Location::findOrFail($id);

		$data = array();

		foreach($collection as $key) {
			$data[] = (array) $key;
		}
		
		//Get collection from average tests
		$query = Sign::join('wells', 'signs.well_id', '=', 'wells.id')
		->join('locations', 'wells.location_id', '=', 'locations.id')
		->select('signs.well_id', 'sign_date', 'sign_time', 'company', 'percent_mixture', 'percent_sediment', 'api_degree_diluted', 'ptb', 'identifier')
		->where('locations.id', $id)
		->get();

		$db = $query->toArray();

		/* get data */
		$get = $this->dataToSave($data, $db);
		
		Sign::insert($get['store']);

		$this->setLogActivity($get['store'], 'Muestras de '.$request->location_name);
		
		return response()->json([
			'msgStore' => 'Se han agregado '.sizeof($get['store']).' registro(s) nuevo(s)',
			'message' => sizeof($get['repeated']) > 0 ? sizeof($get['repeated']) . ' identificadores repetidos' : null,
			'store' => $get['store'],
			'repeated' => $get['repeated'],
			'status' => 200
		], 200);
    }
    
    /* Save condition */
    public function saveCondition(Request $request)
    {
		$collection = json_decode($request->elements);
		$getIdByLocationName = $this->getIdByName($request->location_name);
		
		$id = $getIdByLocationName['id'];
		
		foreach($collection as $key) {
			$key->well_id = $id;
		}

		$exist = Location::findOrFail($id);

		$data = array();
		
		foreach($collection as $key) {
			$data[] = (array) $key;
		}
		
		//Get collection from average tests
		$query = Condition::join('wells', 'conditions.well_id', '=', 'wells.id')
		->join('locations', 'wells.location_id', '=', 'locations.id')
		->select('conditions.well_id', 'company', 'sand', 'reservoir', 'pt_date', 'test',	'deep_md', 'deep_tvd', 'temperature',	'duration',	'pressbefore',	'pressafter',	'pressformation', 'mobility', 'observations', 'identifier')
		->where('locations.id', $id)
		->get();

		$db = $query->toArray();
		
		/* get data */
		$get = $this->dataToSave($data, $db);

		Condition::insert($get['store']);

		$this->setLogActivity($get['store'], 'PyT Original de '.$request->location_name);

		return response()->json([
			'msgStore' => 'Se han agregado '.sizeof($get['store']).' registro(s) nuevo(s)',
			'message' => sizeof($get['repeated']) > 0 ? sizeof($get['repeated']) . ' identificadores repetidos' : null,
			'store' => $get['store'],
			'repeated' => $get['repeated'],
			'status' => 200
		], 200);
    }
    
    /* Save historical */
    public function saveHistorical(Request $request)
    {
		$collection = json_decode($request->elements);
		$collectNames = json_decode($request->names);
		
		$ids = $names = $_ids = array();
		
		// Get names locations
		foreach($collectNames as $key) {
			$names[] = (array) $key->location;
		}
		
		// Convert name locations to locations ID
		for ($i = 0; $i < count($names); $i++) {
			$ids[$i] = $this->getIdByName(implode('', $names[$i])); 
		}
		
		$count = 0;
		
		foreach($collection as $key) {
			$key->well_id = $ids[$count]['id'];
			$_ids[$count] = $ids[$count]['id'];
			$count++;
		}
		
		$data = array();
		
		foreach($collection as $key) {
			$data[] = (array) $key;
		}
		
		//Get collection from average tests
		$query = Historical::join('wells', 'historicals.well_id', '=', 'wells.id')
		->join('locations', 'wells.location_id', '=', 'locations.id')
		->select('historicals.well_id', 'reservoir', 'begin_date', 'end_date', 'type_work_i', 'type_work_ii', 'rig',	'sensor_model',	'sensor_diameter', 'sensor_brand', 'sensor_company', 'deeptop_sensor_md', 'deepbottom_sensor_md',	'pump_model', 'pump_diameter', 'pump_type',	'pump_capacity','pump_company',	'deeptop_pump_md', 'deepbottom_pump_md', 'inyected_diluent',	'measure_from',	'observations',	'references', 'identifier')
		->get();

		$db = $query->toArray();

		/* get data */
		$get = $this->dataToSave($data, $db);

		Historical::insert($get['store']);

		$this->setLogActivity($get['store'], 'Histórico de completación de '.$request->macolla_name);

		return response()->json([
			'msgStore' => 'Se han agregado '.sizeof($get['store']).' registro(s) nuevo(s)',
			'message' => sizeof($get['repeated']) > 0 ? sizeof($get['repeated']) . ' identificadores repetidos' : null,
			'store' => $get['store'],
			'repeated' => $get['repeated'],
			'status' => 200
		], 200);
	}
    
    /* Save static pressure */
    public function saveStaticPressure(Request $request)
    {
    	$collection = json_decode($request->elements);
    	$collectNames = json_decode($request->names);
    		
    	$ids = $names = $_ids = array();
    		
    	// Get names locations
		foreach($collectNames as $key) {
    		$names[] = (array) $key->location;
    	}
    		
    	// Convert name locations to locations ID
    	for ($i = 0; $i < count($names); $i++) {
    		$ids[$i] = $this->getIdByName(implode('', $names[$i]));
		}
    		
    	$count = 0;
    	foreach($collection as $key) {
			$key->well_id = $ids[$count]['id'];
    		$_ids[$count] = $ids[$count]['id'];
    		$count++;
    	}
    						
    	$data = array();
    	foreach($collection as $key) {
    		$data[] = (array) $key;
    	}	
    				
		$query = StaticPressure::join('wells', 'static_pressures.well_id', 'wells.id')
		->join('locations', 'wells.location_id', '=', 'locations.id')
		->select('static_pressures.well_id', 'static_date', 'quality', 'pip', 'pdp', 'temperature', 'observations',	'identifier')
		->whereIn('static_pressures.well_id', $_ids)
		->get();
    			
		$db = $query->toArray();

		/* get data */
		$get = $this->dataToSave($data, $db);

		StaticPressure::insert($get['store']);

		$this->setLogActivity($get['store'], 'Presión Estática de '.$request->macolla_name);

		return response()->json([
			'msgStore' => 'Se han agregado '.sizeof($get['store']).' registro(s) nuevo(s)',
			'message' => sizeof($get['repeated']) > 0 ? sizeof($get['repeated']) . ' identificadores repetidos' : null,
			'store' => $get['store'],
			'repeated' => $get['repeated'],
			'status' => 200
		], 200);
	}
    
    /* Save dynamic pressure */
    public function saveDynamicPressure(Request $request)
    {
    	$collection = json_decode($request->elements);
    	$collectNames = json_decode($request->names);
    		
    	$ids = $names = $_ids = array();
    		
    	// Get names locations
    	foreach($collectNames as $key) {
    		$names[] = (array) $key->location;
    	}
    		
    	// Convert name locations to locations ID
    	for ($i = 0; $i < count($names); $i++) {
			$ids[$i] = $this->getIdByName(implode('', $names[$i]));
    	}
    		
    	$count = 0;
    	foreach($collection as $key) {
    		$key->well_id = $ids[$count]['id'];
    		$_ids[$count] = $ids[$count]['id'];
			$count++;
		}
    						
    	$data = array();
    	foreach($collection as $key) {
    		$data[] = (array) $key;
    	}	
    				
    	//Get collection from average tests
		$query = DynamicPressure::join('wells', 'dynamic_pressures.well_id', 'wells.id')
		->join('locations', 'wells.location_id', '=', 'locations.id')
		->select('dynamic_pressures.well_id', 'dynamic_date', 'quality', 'pip', 'pdp', 'temperature', 'observations',	'identifier')
		->whereIn('dynamic_pressures.well_id',$_ids)
		->get();

		$db = $query->toArray();
    			
       	/* get data */
		$get = $this->dataToSave($data, $db);

		DynamicPressure::insert($get['store']);

		$this->setLogActivity($get['store'], 'Presión Dinámica de '.$request->macolla_name);

		return response()->json([
			'msgStore' => 'Se han agregado '.sizeof($get['store']).' registro(s) nuevo(s)',
			'message' => sizeof($get['repeated']) > 0 ? sizeof($get['repeated']) . ' identificadores repetidos' : null,
			'store' => $get['store'],
			'repeated' => $get['repeated'],
			'status' => 200
		], 200);
    }
    
    /* Save original pressure */
    public function saveOriginalPressure(Request $request)
    {
		$collection = json_decode($request->elements);
		$collectNames = json_decode($request->names);
		$ids = $names = $_ids = array();
		
		// Get names locations
		foreach($collectNames as $key) {
			$names[] = (array) $key->location;
		}
		
		// Convert name locations to locations ID
		for ($i = 0; $i < count($names); $i++) {
			$ids[$i] = $this->getIdByName(implode('', $names[$i]));
		}
		
		$count = 0;
		
		foreach($collection as $key) {
			$key->well_id = $ids[$count]['id'];
			$_ids[$count] = $ids[$count]['id'];
			$count++;
		}
		
		$data = array();
		foreach($collection as $key) {
			$data[] = (array) $key;
		}
		
		//Get collection from average tests
		$query = OriginalPressure::join('wells', 'original_pressures.well_id', 'wells.id')
		->join('locations', 'wells.location_id', 'locations.id')
		->select('original_pressures.well_id', 'reservoir', 'pressure_date', 'type_test',	'sensor_model',	'pressure_gradient',	'deepdatum_mc_md',	'deepdatum_mc_tvd',	'press_datum_macolla', 'temp_datum_macolla', 'temperature_gradient', 'identifier')
		->get();

		$db = $query->toArray();
								
		/* get data */
		$get = $this->dataToSave($data, $db);

		OriginalPressure::insert($get['store']);

		$this->setLogActivity($get['store'], 'Presión Original PMVM');

		return response()->json([
			'msgStore' => 'Se han agregado '.sizeof($get['store']).' registro(s) nuevo(s)',
			'message' => sizeof($get['repeated']) > 0 ? sizeof($get['repeated']) . ' identificadores repetidos' : null,
			'store' => $get['store'],
			'repeated' => $get['repeated'],
			'status' => 200
		], 200);
	}
    
    /* Save real survey */
    public function saveRealSurvey(Request $request)
    {
		$collection = json_decode($request->elements);
		$collectNames = json_decode($request->names);
		
		$ids = $names = $_ids = array();
		
		// Get names locations
		foreach($collectNames as $key) {
			$names[] = (array) $key->location;
		}
		
		// Convert name locations to locations ID
		for ($i = 0; $i < count($names); $i++) {
			$ids[$i] = $this->getIdByName(implode('', $names[$i]));
		}
		
		$count = 0;
		foreach($collection as $key) {
			$key->well_id = $ids[$count]['id'];
			$_ids[$count] = $ids[$count]['id'];
			$count++;
		}
		
		$data = array();
		foreach($collection as $key) {
			$data[] = (array) $key;
		}
		
		//Get collection from average tests
		$query = RealSurvey::join('wells', 'real_surveys.well_id', 'wells.id')
		->join('locations', 'wells.location_id', 'locations.id')
		->select('real_surveys.well_id', 'rs_date',	'hole',	'company', 'md', 'inclination', 'azim_grid', 'tvd',	'vsec',	'ns', 'ew', 'dls', 'la_g', 'la_m', 'la_s', 'lo_g', 'lo_m', 'lo_s', 'observations', 'identifier')
		->get();

		$db = $query->toArray();

		/* get data */
		$get = $this->dataToSave($data, $db);

		RealSurvey::insert($get['store']);

		$this->setLogActivity($get['store'], 'Real Survey');

		return response()->json([
			'msgStore' => 'Se han agregado '.sizeof($get['store']).' registro(s) nuevo(s)',
			'message' => sizeof($get['repeated']) > 0 ? sizeof($get['repeated']) . ' identificadores repetidos' : null,
			'store' => $get['store'],
			'repeated' => $get['repeated'],
			'status' => 200
		], 200);
    }
    
    /* Save plan survey */
    public function savePlanSurvey(Request $request)
    {
		$collection = json_decode($request->elements);
		$collectNames = json_decode($request->names);
		
		$ids = $names = $_ids = array();
		
		// Get names locations
		foreach($collectNames as $key) {
			$names[] = (array) $key->location;
		}
		
		// Convert name locations to locations ID
		for ($i = 0; $i < count($names); $i++) {
			$ids[$i] = $this->getIdByName(implode('', $names[$i]));
		}
		
		$count = 0;
		foreach($collection as $key) {
			$key->well_id = $ids[$count]['id'];
			$_ids[$count] = $ids[$count]['id'];
			$count++;
		}
		
		$data = array();
		foreach($collection as $key) {
			$data[] = (array) $key;
		}
		
		$query = PlanSurvey::join('wells', 'plan_surveys.well_id', 'wells.id')
		->join('locations', 'wells.location_id', 'locations.id')
		->select('plan_surveys.well_id', 'ps_date', 'md', 'inclination', 'azim_grid', 'tvd',	'vsec',	'ns', 'ew', 'dls', 'la_g', 'la_m', 'la_s', 'lo_g', 'lo_m', 'lo_s', 'observations', 'identifier')
		->whereIn('locations.id', $_ids)
		->get();

		$db = $query->toArray();
    			
       	/* get data */
		$get = $this->dataToSave($data, $db);

		PlanSurvey::insert($get['store']);

		$this->setLogActivity($get['store'], 'Plan Survey');

		return response()->json([
			'msgStore' => 'Se han agregado '.sizeof($get['store']).' registro(s) nuevo(s)',
			'message' => sizeof($get['repeated']) > 0 ? sizeof($get['repeated']) . ' identificadores repetidos' : null,
			'store' => $get['store'],
			'repeated' => $get['repeated'],
			'status' => 200
		], 200);
    }
    
    /* Save plan survey */
    public function saveFiscalized(Request $request)
    {
		$collection = json_decode($request->elements);
		$macollaName = $request->macolla_name;

		$idMacolla = $this->getIdMacolla($macollaName);
		
		foreach($collection as $key) {
			$key->macolla_id = $idMacolla;
		}
		
		$data = array();
		foreach($collection as $key) {
			$data[] = (array) $key;
		}
		
		//Get collection from average tests
		$query = Production::join('macollas', 'productions.macolla_id', '=', 'macollas.id')
		->select('productions.macolla_id', 'fiscalized_date', 'production', 'identifier')
		->where('productions.macolla_id', $idMacolla)
		->get();

		$db = $query->toArray();
		
		/* get data */
		$get = $this->dataToSave($data, $db);
		
		Production::insert($get['store']);

		Log::create([
			'user_id' => Auth::user()->id,
			'description' => '- Cargó datos de Producción Fiscalizada de '.$macollaName
		]);
		
		return response()->json([
			'msgStore' => 'Se han agregado '.sizeof($get['store']).' registro(s) nuevo(s)',
			'message' => sizeof($get['repeated']) > 0 ? sizeof($get['repeated']) . ' identificadores repetidos' : null,
			'store' => $get['store'],
			'repeated' => $get['repeated'],
			'status' => 200
		], 200);
	}

	public function saveEvents(Request $request)
    {
		$collection = json_decode($request->elements);
		$getIdByName = Well::select('id')->where('well_name', $request->well_name)->first();
		
		$id = $getIdByName['id'];
		
		$exist = Well::findOrFail($id);

		foreach($collection as $key) {
			$key->well_id = $id;
		}

		$data = array();
		
		foreach($collection as $key) {
			$data[] = (array) $key;
		}
		
		/* get collection from extended tests */
		$query = Event::join('wells', 'events.well_id', 'wells.id')
		->select('events.well_id', 'event_date', 'event_time', 'diluent', 'rpm', 'pump_efficiency', 'torque', 'frequency', 'current', 'power', 'mains_voltage', 'output_voltage', 'vfd_temperature', 'head_temperature', 'head_pressure', 'observations', 'identifier')
		->where('wells.id', $id)
		->get();

		$db = $query->toArray();
		
		/* get data */
		$get = $this->dataToSave($data, $db);

		Event::insert($get['store']);

		$this->setLogActivity($get['store'], 'Eventos de '.$request->well_name);

		return response()->json([
			'msgStore' => 'Se han agregado '.sizeof($get['store']).' registro(s) nuevo(s)',
			'message' => sizeof($get['repeated']) > 0 ? sizeof($get['repeated']) . ' identificadores repetidos' : null,
			'store' => $get['store'],
			'repeated' => $get['repeated'],
			'status' => 200
		], 200);
	}
    
    /**
     * Display the specified resource.
     * Functions
     */
     
    public function setArrayValues($arr)
    {
		$pos = 0;
		$result = [];
		for ($i = 0; $i < sizeof($arr); $i++) {
			$result[$i] = $arr[$i]['identifier'];
		}
		return $result;
    }
    
    public function setValidNumber($number)
    {
		$value = is_numeric($number) ? (double) $number : 0;
		return $value;
    }
     
    public function setValue($str)
    {
    	return empty($str) || !is_numeric($str) ? null : $str;
    }
    
    public function format_text($str)
    {
    	return empty($str) || is_null($str) ? '-' : strtoupper($str);
    }

    public function setValueWithout($str)
    {
		$value = empty($str) ? '-' : $str;
		return $value;
    }
    
    public function validating($date)
    {
		$value = explode('/', $date);
		
		$first = (int) $value[1];
		$second = (int) $value[0];
		$third = (int) $value[2];
		
		if (count($value) == 3 && checkdate($first, $second, $third)) {
			return true;
		} else {
			return false;
		}
    }
     
    public function convertDate($str)
    {
		$now = new \DateTime();
		$date = $now->createFromFormat('d/m/Y', $str);
		return $date->format('Y-m-d');
    }
    
    public function convertFullDate($str)
    {
		$now = new \DateTime();
		$date = $now->createFromFormat('d/m/Y H:i:s', $str);
		return $date->format('Y-m-d H:i:s');
    }
    
    public function convertHour($str)
    {
		$time = explode(':', $str);
		if (sizeof($time) == 2) {
			if ($time[0] <= 23 && $time[1] <= 59) {
				return $time[0].':'.$time[1];
			}
		} else return false;
    }
    
    // Debe terminarse...
    /*public function convertValidDate($str)
    {
		$date = explode('/', $str);
		if (sizeof($time) == 2) {
			if ($date[0] <= 23 && $time[1] <= 59) {
				return $time[0].':'.$time[1];
			}
		} else return false;
    }*/
    
    public function getIdByName($str)
    {
		$id = Location::select('id')->where('location_name', '=', ''.$str.'')->first();
		return $id;
    }
	
	public function getValidLocationsFromMacolla($sLocation, $sMacolla)
	{
		$query = Location::select('location_name')
		->join('macollas', 'macollas.id', 'locations.macolla_id')
		->where('location_name', trim($sLocation))
		->where('macolla_name', trim($sMacolla))
		->first();
		
		return $query['location_name'];
	
	}

    public function setNumber($number)
    {
		return is_numeric($number) ? $number : 0;
    }
    
    public function wipeArray($arr)
    {
		for ($i = 0; $i < sizeof($arr); $i++) {
			if (empty($arr[$i]['well_id'])) {
				unset($arr[$i]);
			}
		}
		
		return $arr;
    }
    
    public function wipeArrayMajor($arr)
    {
		for ($i = 0; $i < sizeof($arr); $i++) {
			if (empty($arr[$i]['macolla_id'])) {
				unset($arr[$i]);
			}
		}
		
		return $arr;
    }
    
    public function getNameLocation($str) 
    {
		$query = Location::select('location_name')->where('location_name', '=', ''.$str.'')->first();
		$result = $query['location_name'];
		return $result;
    }
    
    public function getIdMacolla($str) 
    {
		$query = Macolla::select('id')->where('macolla_name', '=', ''.$str.'')->first();
		$result = $query['id'];
		return $result;
    }
    
    public function create()
    {
        //
    }
    
    public function setId($number)
    {
    		$text = '';
    		if ($number < 10) {
    		 	$text = '0'.$number;
    		} else {
    			$text = ''.$number;
    		}
    		return $text;
	}

	public function setLogActivity($x, $title) {
		$actions = '';
		$section = $title;
		
		if (sizeof($x) > 0) $actions .= '- Cargó '.sizeof($x).' registros nuevos en '.$section.'<br>';
		
		if ((sizeof($x) > 0)) {
			Log::create([
				'user_id' => Auth::user()->id,
				'description' => $actions
			]);
		}
	}

	public function dataToSave($arr, $stack)
	{
		$x = $store = $count = [];

		for ($i = 0; $i < sizeof($arr); $i++) {
			if (!in_array($arr[$i]['identifier'], array_column($stack, 'identifier'))) {
				$x[] = $arr[$i];
			}
		}
		
		$filter = collect($x)->groupBy('identifier')->all();
		
		foreach ($filter as $key) {
			if (sizeof($key) == 1) {
				foreach ($key as $value) {
					$store[] = $value;
				}
			} else {
				foreach ($key as $value) {
					$count[] = $value;
				}
			}
		}

		$repeated = collect($count)->unique('identifier')->all();
		sort($repeated);

		return ['store' => $store, 'repeated' => $repeated];
	}
    
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}