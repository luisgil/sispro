<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\{Hash, Validator, Auth};
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\{User, Profile, Log};

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'firstname' => ['required', 'regex:/^([a-zA-ZñÑáéíóúÁÉÍÓÚ ]+$)+/', 'max:27'],
            'lastname' => ['required', 'regex:/^([a-zA-ZñÑáéíóúÁÉÍÓÚ ]+$)+/', 'max:27'],
            'email' => ['required', 'email', 'min:3', 'unique:users'],
            'password' => ['required', 'min:6', 'confirmed'],
            'password_confirmation' => ['required', 'min:6'],
            'role' => ['required', Rule::in(User::ROLES)],
        ]);
        
        if ($validator->fails()) {
            return response()->json([
                'fail' => true,
                'message' => 'Peticion invalida',
                'errors'=> $validator->errors(),
                'status' => 400,
            ], 400);
        }

        $user = new User();
        $user->fill($request->all());
        $user->password = Hash::make($request->password);

        $profile = new Profile();
        $profile->fill($request->all());
        \DB::transaction(function() use($user, $profile){
            $user->save();
            $profile->user_id = $user->id;
            $profile->save();
        });
        $user->profile;
        
        Log::create([
            'user_id' => Auth::user()->id,
            'description' => '- Creó el usuario '.$profile->firstname.' '.$profile->lastname.' ('.$user->email.')'
        ]);

        return response()->json([
            'fail' => false,
            'message' => 'User has been registered successfully',
            'data' => $user,
            'status' => 200,
        ], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    public function load(Request $request)
    {
        $collection = User::select('users.id as user_id', 'firstname', 'lastname', 'email', 'role', 'last_login', 'users.created_at as created_at')
        ->join('profiles', 'profiles.user_id', 'users.id')->where('users.id', '<>', Auth::user()->id)->get();

        return response()->json([
            'fail' => false,
            'message' => Auth::user()->id,
            'data' => $collection,
            'status' => 200,
        ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
