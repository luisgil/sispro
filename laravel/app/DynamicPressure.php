<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DynamicPressure extends Model
{
    protected $table = 'dynamic_pressures';

		protected $guarded = [];
    
    public function well()
    {
    	return $this->belongsTo(Well::class);
    }
}
