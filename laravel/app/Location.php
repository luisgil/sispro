<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Location extends Model
{
    protected $fillable = [
        'macolla_id', 'location_name'
    ];

    public function macolla()
    {
    	return $this->belongsTo(Macolla::class);
    }
}
