<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RealSurvey extends Model
{
    protected $table = 'real_surveys';
    
    protected $guarded = [];
    
    protected $fillable = ['well_id', 'rs_date', 'hole', 'company', 'md', 'inclination', 'azim_grid', 'tvd', 'vsec', 'ns', 'ew', 'dls', 'la_g', 'la_m', 'la_s', 'lo_g', 'lo_m', 'lo_s', 'observations', 'identifier'];
		
		public function well()
    {
    	return $this->belongsTo(Well::class);
    }
}
