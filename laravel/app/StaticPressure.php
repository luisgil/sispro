<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StaticPressure extends Model
{
    protected $table = 'static_pressures';
    
    protected $guarded = [];

    protected $fillable = ['well_id', 'static_date', 'quality', 'pip', 'pdp', 'temperature', 'observations', 'identifier'];
    
    public function well()
    {
    	return $this->belongsTo(Well::class);
    }
}
