<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    protected $table = 'events';
    
    protected $guarded = [];
    
    protected $fillable = ["well_id", "reservoir", "event_date", "event_time", "diluent", "rpm", "pump_efficiency", 'torque', 'frequency', 'current', 'power', 'mains_voltage', 'output_voltage', 'vfd_temperature', 'head_temperature', 'head_pressure', "observations", "identifier"];
    
    public function well()
    {
    	return $this->belongsTo(Well::class);
    }
}
