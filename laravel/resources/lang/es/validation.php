<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | such as the size rules. Feel free to tweak each of these messages.
    |
    */
    'accepted'             => ':attribute debe ser aceptado.',
    'active_url'           => ':attribute no es una URL válida.',
    'after'                => 'La :attribute debe ser una fecha posterior a :date.',
    'alpha'                => ':attribute solo debe contener letras.',
    'alpha_dash'           => ':attribute solo debe contener letras, números y guiones.',
    'alpha_num'            => ':attribute solo debe contener letras y números.',
    'array'                => ':attribute debe ser un conjunto.',
    'before'               => 'La :attribute debe ser una fecha anterior a :date.',
    'between'              => [
        'numeric' => 'El :attribute tiene que estar entre :min y :max.',
        'file'    => ':attribute debe pesar entre :min - :max kilobytes.',
        'string'  => ':attribute tiene que tener entre :min - :max caracteres.',
        'array'   => ':attribute tiene que tener entre :min - :max ítems.',
    ],
    'boolean'              => 'El campo :attribute debe tener un valor verdadero o falso.',
    'confirmed'            => 'La confirmación de :attribute no coincide.',
    'date'                 => ':attribute no es una fecha válida.',
    'date_format'          => 'El :attribute no corresponde al formato :format.',
    'different'            => ':attribute y :other deben ser diferentes.',
    'digits'               => ':attribute debe tener :digits dígitos.',
    'digits_between'       => ':attribute debe tener entre :min y :max dígitos.',
    'distinct'             => 'El campo :attribute contiene un valor duplicado.',
    'email'                => 'El :attribute no es un correo válido',
    'exists'               => ':attribute es inválido.',
    'filled'               => 'El campo :attribute es obligatorio.',
    'image'                => ':attribute debe ser una imagen.',
    'in'                   => ':attribute es inválido.',
    'in_array'             => 'El campo :attribute no existe en :other.',
    'integer'              => ':attribute debe ser un número entero.',
    'ip'                   => ':attribute debe ser una dirección IP válida.',
    'json'                 => 'El campo :attribute debe tener una cadena JSON válida.',
    'max'                  => [
        'numeric' => ':attribute no debe ser mayor a :max.',
        'file'    => ':attribute no debe ser mayor que :max kilobytes.',
        'string'  => ':attribute no debe ser mayor que :max caracteres.',
        'array'   => ':attribute no debe tener más de :max elementos.',
    ],
    'mimes'                => ':attribute debe ser un archivo con formato: :values.',
    'min'                  => [
        'numeric' => 'El tamaño de :attribute debe ser de al menos :min.',
        'file'    => 'El tamaño de :attribute debe ser de al menos :min kilobytes.',
        'string'  => 'El :attribute debe contener al menos :min caracteres.',
        'array'   => ':attribute debe tener al menos :min elementos.',
    ],
    'not_in'               => ':attribute es inválido.',
    'numeric'              => ':attribute debe ser numérico.',
    'present'              => 'El campo :attribute debe estar presente.',
    'regex'                => 'El formato de :attribute es inválido.',
    'required'             => 'El campo :attribute es obligatorio.',
    'required_if'          => 'El campo :attribute es obligatorio cuando :other es :value.',
    'required_unless'      => 'El campo :attribute es obligatorio a menos que :other esté en :values.',
    'required_with'        => 'El campo :attribute es obligatorio cuando :values está presente.',
    'required_with_all'    => 'El campo :attribute es obligatorio cuando :values está presente.',
    'required_without'     => 'El campo :attribute es obligatorio cuando :values no está presente.',
    'required_without_all' => 'El campo :attribute es obligatorio cuando ninguno de :values estén presentes.',
    'same'                 => ':attribute y :other deben coincidir.',
    'size'                 => [
        'numeric' => 'El tamaño de :attribute debe ser :size.',
        'file'    => 'El tamaño de :attribute debe ser :size kilobytes.',
        'string'  => ':attribute debe contener :size caracteres.',
        'array'   => ':attribute debe contener :size elementos.',
    ],
    'string'               => 'El campo :attribute debe ser una cadena de caracteres.',
    'timezone'             => 'El :attribute debe ser una zona válida.',
    'unique'               => 'El :attribute ya ha sido registrado.',
    'url'                  => 'El formato :attribute es inválido.',
    'captcha'              => 'El código captcha ingresado no es correcto',
    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */
    'custom'               => [
            'rs_date' => [
                'required' => 'Asegúrese de ingresar una fecha válida.'
            ],
            'ps_date' => [
                'required' => 'Asegúrese de ingresar una fecha válida.'
            ],
            'pressure_date' => [
                'required' => 'Asegúrese de ingresar una fecha válida.'
            ],
            'dynamic_date' => [
                'required' => 'Asegúrese de ingresar una fecha válida.'
            ],
            'static_date' => [
                'required' => 'Asegúrese de ingresar una fecha válida.'
            ],
            'pt_date' => [
                'required' => 'Asegúrese de ingresar una fecha válida.'
            ],
            'fiscalized_date' => [
                'required' => 'Asegúrese de ingresar una fecha válida.'
            ],
            'sign_date' => [
                'required' => 'Asegúrese de ingresar una fecha válida.'
            ],
            'avg_start_test' => [
                'required' => 'Asegúrese de ingresar una fecha válida.'
            ],
            'avg_end_test' => [
                'required' => 'Asegúrese de ingresar una fecha válida.'
            ],
            'extended_date' => [
                'required' => 'Asegúrese de ingresar una fecha válida.'
            ],
            'hour_start' => [
                'before' => 'La :attribute debe ser inferior a la hora final.'
            ],
            'hour_end' => [
                'after' => 'La :attribute debe ser posterior a la hora inicial.'
            ],
    		'inyected_diluent_rate' => [
    			'between' => 'La :attribute debe estar entre 0 y 3000'
    		],
    		'total_barrels' => [
    			'between' => 'Los :attribute deben estar entre 0 y 3000'
    		],
    		'water_rate_prod' => [
    			'between' => 'La :attribute debe estar entre 0 y 3000'
    		],
    		'gas_rate_prod' => [
    			'between' => 'La :attribute debe estar entre 0 y 3000'
    		],
    		'line_pressure' => [
    			'numeric' => 'La :attribute debe ser numérica',
    			'between' => 'La :attribute debe estar entre :min y :max'
    		],
    		'line_temperature' => [
    			'numeric' => 'La :attribute debe ser numérica',
    			'between' => 'La :attribute debe estar entre :min y :max'
    		],
    		'neat_rate' => [
    			'required' => 'La :attribute es requerida',
    			'numeric' => 'La :attribute debe ser numérica',
    			'between' => 'La :attribute debe estar entre 0 y 3000'
            ],
            'well_id' => [
                'required' => 'Este campo es obligatorio.'
            ],
        'location_name' => [
            'min' => 'Debe completar correctamente la localización',
            'unique' => 'Esta :attribute ya ha sido registrada.',
        ],
        'update_location_name' => [
            'min' => 'Debe completar correctamente la localización',
            'unique' => 'Esta :attribute ya ha sido registrada.',
        ],
        'macolla_name' => [
            'unique' => 'Esta :attribute ya ha sido registrada.',
        ],
        'update_macolla_name' => [
            'unique' => 'Esta :attribute ya ha sido registrada.',
        ],
        'camp_name' => [
            'required' => 'Este campo es requerido.',
        ],
        'code' => [
            'unique' => 'Este :attribute ya ha sido registrado.',
            'regex' => 'Solo se permiten letras y números.'
        ],
        'reservoir_name' => [
            'regex' => 'Solo se permiten letras y números.'
        ],
        'api_degree' => [
            'between' => 'El ºAPI debe estar entre :min y 70'
        ],
        'date' => [
        	'date_format' => 'La fecha introducida es inválida ya que el formato es d/m/Y',
        	'after' => 'La fecha debe ser posterior a :date'
        ],
        'sample_type' => [
        	'required' => 'El :attribute es obligatorio'
        ],
        'hours' => [
        	'between' => 'Las horas deben estar entre 0,1 y :max'
        ],
        'pressure' => [
        	'required' => 'La presión es obligatoria',
        	'between' => 'La :attribute debe estar entre :min y :max'
        ],
        'temperature' => [
        	'required' => 'La temperatura es obligatoria',
        	'between' => 'La :attribute debe estar entre :min y :max'
        ],
        'diluted_oil_rate' => [
        	'required' => 'La :attribute es obligatoria',
        	'between' => 'La :attribute debe estar entre :min y :max'
        ],
        'diluent_rate' => [
        	'required' => 'La :attribute es obligatoria',
        	'between' => 'La :attribute debe estar entre :min y :max'
        ],
        'classification' => [
        	'required' => 'La :attribute es requerida'
        ],
        'condition' => [
        	'required' => 'La :attribute es requerida'
        ],
        'pump_efficiency' => [
            'between' => 'La eficiencia de la bomba debe estar entre 0 y :max'
        ],
        'rpm' => [
            'between' => 'El RPM debe estar entre 0 y :max'
        ],
        'diluent' => [
            'between' => 'El diluente debe estar entre 0 y :max'
        ],
        'torque' => [
            'between' => 'El torque debe estar entre 0 y :max'
        ],
        'frequency' => [
            'between' => 'La frecuencia debe estar entre 0 y :max'
        ],
        'current' => [
            'between' => 'La corriente debe estar entre 0 y :max'
        ],
        'power' => [
            'between' => 'La potencia debe estar entre 0 y :max'
        ],
        'mains_voltage' => [
            'between' => 'La tensión red debe estar entre 0 y :max'
        ],
        'output_voltage' => [
            'between' => 'La tensión salida debe estar entre 0 y :max'
        ],
        'vfd_temperature' => [
            'between' => 'La temperatura VFD debe estar entre 0 y :max'
        ],
        'head_temperature' => [
            'between' => 'La temperatura cabezal debe estar entre 0 y :max'
        ],
        'head_pressure' => [
            'between' => 'La presión cabezal debe estar entre 0 y :max'
        ],
        'avg_start_test' => [
            'before_or_equal' => 'La :attribute debe ser anterior o igual a la fecha final.'
        ],
        'avg_end_test' => [
            'after_or_equal' => 'La :attribute debe ser posterior o igual a la fecha inicial.'
        ],
        'sensor_diameter' => [
            'between' => 'El :attribute debe ser hasta :max como máximo'
        ],
        'deeptop_sensor_md' => [
            'between' => 'La :attribute debe ser entre 0 y :max'
        ],
        'deepbottom_sensor_md' => [
            'between' => 'La :attribute debe ser entre 0 y :max'
        ],
        'pump_diameter' => [
            'between' => 'El :attribute debe ser hasta :max como máximo'
        ],
        'deeptop_pump_md' => [
            'between' => 'La :attribute debe ser entre 0 y :max'
        ],
        'deepbottom_pump_md' => [
            'between' => 'La :attribute debe ser entre 0 y :max'
        ],
        'pump_capacity' => [
            'between' => 'La :attribute debe ser entre 0 y :max'
        ],
        'duration' => [
            'between' => 'La :attribute debe estar entre 0 y :max'
        ],
        'mobility' => [
            'between' => 'La :attribute debe estar entre 0 y :max'
        ],
        'deep_md' => [
            'between' => 'La :attribute debe estar entre 0 y :max'
        ],
        'deep_tvd' => [
            'between' => 'La :attribute debe estar entre 0 y :max'
        ],
        'pressbefore' => [
            'between' => 'La :attribute debe estar entre 0 y :max'
        ],
        'pressafter' => [
            'between' => 'La :attribute debe estar entre 0 y :max'
        ],
        'pressformation' => [
            'between' => 'La :attribute debe estar entre 0 y :max'
        ],
        'fluid_total_rate' => [
            'between' => 'La :attribute debe estar entre 0 y :max'
        ],
        'neat_rate_prod' => [
            'between' => 'La :attribute debe estar entre 0 y :max'
        ],
        'percent_mixture' => [
            'between' => 'El :attribute debe estar entre 0 y :max'
        ],
        'percent_formation' => [
            'between' => 'El :attribute debe estar entre 0 y :max'
        ],
        'pip' => [
            'between' => 'El :attribute debe estar entre 0 y :max'
        ],
        'gvf' => [
            'between' => 'La :attribute debe estar entre 0 y :max'
        ],
        'percent_sediment' => [
            'between' => 'El :attribute debe estar entre 0 y :max'
        ],
        'ptb' => [
            'between' => 'El :attribute debe estar entre 0 y :max'
        ],
        'pip' => [
            'between' => 'El :attribute debe estar entre 0 y :max'
        ],
        'pdp' => [
            'between' => 'El :attribute debe estar entre 0 y :max'
        ],
        'begin_date' => [
            'before_or_equal' => 'La :attribute debe ser anterior o igual a la fecha final.'
        ],
        'end_date' => [
            'after_or_equal' => 'La :attribute debe ser posterior o igual a la fecha inicial.'
        ],
        'deepdatum_mc_md' => [
            'between' => 'El :attribute debe estar entre 0 y :max'
        ],
        'deepdatum_mc_tvd' => [
            'between' => 'El :attribute debe estar entre 0 y :max'
        ],
        'api_degree_mixture' => [
            'between' => 'El :attribute debe estar entre 7 y :max'
        ],
        'api_degree_diluent' => [
            'between' => 'El :attribute debe estar entre 7 y :max'
        ],
        'api_degree_diluted' => [
            'between' => 'El :attribute debe estar entre 7 y :max'
        ],
        'temperature' => [
            'between' => 'La :attribute debe estar entre 60 y :max'
        ]
    ],
    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap attribute place-holders
    | with something more reader friendly such as E-Mail Address instead
    | of "email". This simply helps us make messages a little cleaner.
    |
    */
    'attributes'           => [
            'api_degree_diluted'                       => '°API diluido',
            'percent_sediment'                       => 'porcentaje de sedimento',
            'diluent'                       => 'diluente',
            'event_date'                    => 'fecha de evento',
            'event_time'                    => 'hora de evento',
            'temperature'                   => 'temperatura',
            'hole'                   => 'hoyo',
            'type_test'                   => 'tipo de prueba',
            'pressure_gradient'                   => 'gradiente de presión',
            'temperature_gradient'                   => 'gradiente de temperatura',
            'reservoir'                     => 'yacimiento',
            'sand'                     => 'arena',
            'mobility'                     => 'movilidad',
            'deep_md'                     => 'profundidad MD',
            'deep_tvd'                     => 'profundidad TVD',
            'deepdatum_mc_md'                     => 'profundidad DATUM MD',
            'deepdatum_mc_tvd'                     => 'profundidad DATUM TVD',
            'press_datum_macolla'                     => 'presión DATUM',
            'temp_datum_macolla'                     => 'temperatura DATUM',
            'pressbefore'                     => 'presión antes',
            'pressafter'                     => 'presión después',
            'pressformation'                     => 'presión de formación',
            'production'                     => 'producción',
            'height'                            => 'altura',
            'vsec'                            => 'sección vertical',
            'inclination'                            => 'inclinación',
            'azim_grid'                            => 'AZIM GRID',
            'dls'                            => 'DLS',
            'ns'                            => 'norte/sur',
            'ew'                            => 'este/oeste',
            'latitude'                            => 'latitud',
            'longitude'                            => 'longitud',
            'emr'										=> 'EMR',
            'md'										=> 'MD',
            'tvd'										=> 'TVD',
            'ppn'										=> 'PPN',
            'pdp'										=> 'PDP',
    		'pip'										=> 'PIP',
    		'drill'									=> 'taladro',
    		'gl'										=> 'GL',
            'rt'										=> 'RT',
            'quality'										=> 'calidad',
    		'avg_start_test'						=> 'fecha de inicio',
            'avg_end_test'							=> 'fecha de finalización',
            'begin_date'						=> 'fecha inicial',
            'end_date'							=> 'fecha final',
            'type_work_i'							=> 'tipo de trabajo I',
            'type_work_ii'							=> 'tipo de trabajo II',
            'rig'							=> 'equipo',
            'sensor_brand'							=> 'marca de sensor',
            'sensor_model'							=> 'modelo de sensor',
            'sensor_diameter'							=> 'diámetro de sensor',
            'sensor_company'							=> 'compañía del sensor',
            'deeptop_sensor_md'							=> 'prof. superior MD',
            'deepbottom_sensor_md'							=> 'prof. inferior MD',
            'pump_model'							=> 'modelo de la bomba',
            'pump_type'							=> 'tipo de bomba',
            'pump_diameter'							=> 'diámetro de la bomba',
            'pump_capacity'							=> 'capacidad de la bomba',
            'pump_company'							=> 'compañía de la bomba',
            'deeptop_pump_md'							=> 'prof. superior MD',
            'deepbottom_pump_md'							=> 'prof. inferior MD',
            'inyected_diluent'							=> 'inyección de diluente',
            'measure_from'							=> '"medidas desde"',
            'sign_time'                             => 'hora de muestreo',
    		'total_fluid'						=> 'fluido total',
    		'duration'							=> 'duración',
    		'company'								=> 'compañía',
    		'total_barrels'					=> 'barriles totales',
    		'hour_start'						=> 'hora inicial',
    		'hour_end'							=> 'hora final',
    		'test'									=> 'prueba',
    		'condition'							=> 'condición',
    		'classification' 				=> 'clasificación',
    		'ptb'										=> 'PTB',
    		'gvf'										=> 'fracción volumétrica de gas',
    		'percent_mixture'				=> 'porcentaje de la mezcla',
    		'api_degree_mixture'		=> 'ºAPI de la mezcla',
    		'percent_formation'			=> 'porcentaje de la formación',
    		'api_degree_formation'	=> 'ºAPI de la formación',
    		'diluted_oil_api_degree'=> 'ºAPI de crudo diluido',
    		'api_degree_diluent'		=> 'ºAPI diluente',
    		'pip'										=> 'PIP',
    		'inyected_diluent_rate' => 'tasa de diluente inyectado', 
    		'diluted_oil_rate'			=> 'tasa de crudo diluido',
            'neat_rate'							=> 'tasa neta',
            'fluid_total_rate'							=> 'tasa de fluido',
    		'neat_rate_prod'				=> 'tasa neta',
    		'water_rate_prod'				=> 'tasa de agua',
    		'gas_rate_prod'					=> 'tasa de gas',
    		'line_temperature'			=> 'temperatura',
    		'line_pressure'					=> 'presión',
    		'rpm'										=> 'RPM',
    		'hours'									=> 'horas',
    		'sample_type'						=> 'tipo de muestra',
    		'enterprise' 						=> 'empresa',
    		'code' 									=> 'código',
    		'type'									=> 'tipo',
    		'uwi'										=> 'UWI',
    		'update_macolla_name'		=> 'macolla (*)',
    		'update_camp_name'			=> 'campo (*)',
            'update_reservoir_name'	=> 'yacimiento (*)',
    		'update_code'						=> 'código (*)',
    		'update_api_degree'			=> 'ºAPI (*)',
    		'update_location_name'  => 'localización (*)',
            'update_well_name'  		=> 'pozo (*)',
            'update_gl'  		=> 'GL (*)',
            'update_rt'  		=> 'RT (*)',
            'update_emr'  		=> 'EMR (*)',
            'update_ppn'  		=> 'PPN (*)',
            'update_drill'  		=> 'taladro (*)',
            'update_height'  		=> 'altura (*)',
    		'macolla_name'					=> 'macolla',
    		'reservoir_name'					=> 'yacimiento',
    		'camp_name'							=> 'campo',
    		'location_name'					=> 'localización',
    		'well_name'							=> 'pozo',
    		'api_degree'						=> 'ºAPI',
    		'firstname'             => 'nombre',
    		'lastname'              => 'apellido',
        'username'              => 'usuario',
        'email'                 => 'correo electrónico',
        'username'							=> 'usuario',
        'password'              => 'contraseña',
        'password_confirmation' => 'confirmación de la contraseña',
        'city'                  => 'ciudad',
        'country'               => 'país',
        'address'               => 'dirección',
        'phone'                 => 'teléfono',
        'mobile'                => 'celular',
        'age'                   => 'edad',
        'sex'                   => 'sexo',
        'gender'                => 'género',
        'year'                  => 'año',
        'month'                 => 'mes',
        'day'                   => 'día',
        'hour'                  => 'hora',
        'minute'                => 'minuto',
        'second'                => 'segundo',
        'title'                 => 'título',
        'body'                  => 'contenido',
        'description'           => 'descripción',
        'wage'									=> 'salario',
        'percent'								=> 'porcentaje',
        'month'									=> 'mes',
        'driver'								=> 'taxista',
        'role'									=> 'rol',
        'excerpt'               => 'extracto',
        'date'                  => 'fecha',
        'search'								=> 'búsqueda',
        'time'                  => 'hora',
        'dateFrom'							=> 'fecha inicio',
        'dateTo'								=> 'fecha final',
        'subject'               => 'asunto',
        'message'               => 'mensaje',
        'required'              => 'Requerido',
    ],

];
