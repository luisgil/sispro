@extends('auth.app')

@section('content')
<title>SISPRO | Iniciar Sesión</title>

@section('content')
<p class="login-box-msg">Iniciar Sesión</p>

      <form method="post" action="{{ route('login') }}">
      	@csrf
        <div class="input-group mb-3">
          <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" placeholder="Correo Electrónico" name="email" value="{{ old('email') }}" autofocus autocomplete="off"/>         
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-envelope"></span>
            </div>
          </div>
        </div>
        
        <div class="input-group mb-3">
          <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" placeholder="Contraseña" name="password" required autocomplete="current-password">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-lock"></span>
            </div>
          </div>
        </div>
        
        @if ($errors->has('email'))
        <small class="text-danger">{{ $errors->first('email') }}</small>
        <hr/>
        @endif
        @if ($errors->has('password'))
        <small class="text-danger">{{ $errors->first('password') }}</small>
        <hr/>
        @endif
        
        <div class="row">
          <div class="col-8">
            <div class="icheck-primary">
              <input type="checkbox" id="remember" {{ old('remember') ? 'checked' : '' }}>
              <label for="remember">
                Recordarme
              </label>
            </div>
          </div>
          <div class="col-4">
            <button type="submit" class="btn btn-warning btn-block btn-flat">Entrar</button>
          </div>
        </div>
      </form>
@endsection