<!DOCTYPE html>
<html lang="es">

<head>
		<!-- METATAGS IMPORTANTS-->
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- METATAGS PROGRESSIVE WEB APP ULTRA NECESARIES-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="theme-color" content="#F7D1E"> {{-- link manifest --}}
    <!-- CSRF Token SUPER IMPORTANT TO AUTENTICATION OF AJAX -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Add to home screen for Safari on iOS -->
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-title" content="Weather PWA"> {{-- add to windows phone --}}
    <meta name="msapplication-TileImage" content="{{ asset('dist/img/logo.png') }}">
    <link rel="icon" type="image/png" sizes="128x128" href="{{ asset('dist/img/logo.png') }}">
    @yield('title')
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    @section('css')
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{asset('plugins/fontawesome-free/css/all.min.css')}}">
  	<!-- Theme style -->
  	<link rel="stylesheet" href="{{asset('dist/css/adminlte.min.css')}}">
  	
  	<style>
  		.login-page{background-color: #dc3545;}
  		#logo-text{color:white;}
  	</style>
  	
  	@show
</head>

<body class="hold-transition login-page">
	<div class="login-page">
		<div class="login-box">
			<div class="login-logo">
				<img src="{{ asset('dist/img/logo.png') }}" width=45 height=45><br/>
				<a href="javascript:void(0);" id="logo-text"><b>SIS</b>PRO</a>
			</div>
			
			<div class="card">
				<div class="card-body login-card-body">
					@yield('content')
				</div>
			</div>
		</div>
		
		@section('script')
		<!-- jQuery -->
		<script src="{{asset('plugins/jquery/jquery.min.js')}}"></script>
		<!-- Bootstrap 4 -->
		<script src="{{asset('plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
		@show
	</div>
</body>
</html>
