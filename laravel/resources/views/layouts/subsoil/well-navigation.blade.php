@extends('layouts.app')

@section('title')
<title>SISPRO | Navegación por Pozo</title>
@endsection


@section('chart')
<!-- chart -->
<script src="{{ asset('plugins/amcharts/amcharts.js') }}"></script>
<script src="{{ asset('plugins/amcharts/xy.js') }}"></script>
<script src="{{ asset('plugins/amcharts/lang/es.js') }}"></script>
<script src="{{ asset('plugins/amcharts/plugins/export/export.js') }}"></script>
<script src="{{ asset('plugins/amcharts/plugins/export/lang/es.js') }}"></script>
<link rel="stylesheet" href="{{ asset('plugins/amcharts/plugins/export/export.css') }}">
@endsection

@section('extra-css')
<!-- Bootstrap4 Duallistbox -->
<link rel="stylesheet" href="{{ asset('plugins/bootstrap-duallistbox/bootstrap-duallistbox.min.css') }}">
<!-- Custom -->
<link rel="stylesheet" href="{{ asset('dist/css/multiselect.css') }}">
<link rel="stylesheet" href="{{ asset('dist/css/loading.css') }}">
@endsection

@section('content-header')
<div class="content-header">
	<div class="container-fluid">
		<div class="row mb-2">
			<div class="col-sm-6">
				<h3 class="m-0 text-dark">Navegación por Pozo</h3>
			</div>
			<div class="col-sm-6">
				<ol class="breadcrumb float-sm-right">
					<li class="breadcrumb-item"><a href="{{ route('home') }}">Inicio</a></li>
					<li class="breadcrumb-item active">Navegación por Pozo</li>
				</ol>
			</div>
		</div>
	</div>
</div>
@endsection

@section('content')
<div class="card card-secondary">
	<div class="card-body">
		<label>Filtrar por:</label>
		<div class="row">
			<div class="form-group col-md-6">
				<small class="text-muted">Macolla</small>
				<div class="form-group">
					<select id="macolla" name="macolla" class="form-control" onkeydown="event.preventDefault();" onchange="m.loadLocationsToSelect(this.value)">
						<optgroup label="Seleccionar macolla">
							<option value="">--</option>
							@php $all = '' @endphp
							@foreach($macolla as $key)
							@php $all .= $key->id.',' @endphp
							<option value="{{ $key->id }}|{{ $key->macolla_name }}">{{ $key->macolla_name }}</option>
							@endforeach
							<option value="{{ $value->id }}|{{ $value->macolla_name }}">S/M</option>
						</optgroup>
					</select>
				</div>
			</div>
			<div class="form-group col-md-6">
				<small class="text-muted">Localización</small>
				<div class="form-group">
					<select id="well" name="well" class="form-control" onkeydown="event.preventDefault();">
						<optgroup label="Seleccionar pozo">
							<option value="">--</option>
						</optgroup>
					</select>
				</div>
			</div>
		</div>
		
		<center id="loading"></center>

	</div>
	<div class="card-footer text-center">
		<button class="btn btn-info" onclick="nbw.searchByFilter()" id="btn-consult">Consultar</button>
	</div>
</div>

<br>

<div id="showChart" style="display:none">
	<h4 style="text-align: center; margin-bottom: 30px;" id="title-charts"></h4>
	<div id="chart" style="height:500px;background-color:white;"></div>
	<div id="chart-2" style="height:500px;background-color:white;"></div>
	<div id="chart-3" style="height:500px;background-color:white;"></div>
	<br>
</div>

<br>

@section('extra-script')
<!--moment-->
<script src="{{ asset('plugins/moment/moment.min.js') }}"></script>
<!-- export data -->
<script src="{{ asset('plugins/jquery-datatable/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('plugins/jquery-datatable/select.js') }}"></script>
<script src="{{ asset('plugins/jquery-datatable/dataTables.scroller.min.js') }}"></script>
<link rel="stylesheet" href="{{ asset('plugins/jquery-datatable/jquery.dataTables.min.css') }}" />
<!-- Bootstrap4 Duallistbox -->
<script src="{{ asset('plugins/bootstrap-duallistbox/jquery.bootstrap-duallistbox.min.js') }}"></script>
<!-- custom scripts -->
<script src="{{ asset('js/myScripts.js') }}"></script>
<script src="{{ asset('js/Method.js') }}"></script>
<script src="{{ asset('js/NavigationByWell.js') }}"></script>
@endsection

@endsection