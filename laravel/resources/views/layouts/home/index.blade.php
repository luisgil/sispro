@extends('layouts.app')

@section('title')
<title>Inicio</title>
@endsection

@section('content-header')
<div class="content-header">
	<div class="container-fluid">
		<div class="row mb-2">
			<div class="col-sm-6">
				<h1 class="m-0 text-dark">¡Bienvenido!</h1>
			</div>
			<div class="col-sm-6">
				<ol class="breadcrumb float-sm-right">
					<li class="breadcrumb-item"><a href="{{ route('home') }}">Inicio</a></li>
				</ol>
			</div>
		</div>
	</div>
</div>
@endsection

@section('content')
<div class="card">
	<div class="card-header">
		<h3 class="card-title">Hola {{ ucwords(Auth::user()->profile->firstname).' '.ucwords(Auth::user()->profile->lastname) }}</h3>
	</div>
	<div class="card-body">
		@if (isset(Auth::user()->last_login))
		<h6>Última conexión el {{ date('d/m/Y h:i:s', strtotime(Auth::user()->last_login)) }}</h6>
		@endif
	</div>
</div>
@endsection