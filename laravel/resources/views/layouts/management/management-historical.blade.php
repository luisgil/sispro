@extends('layouts.app')

@section('title')
<title>SISPRO | Gestión de Hist. completaciones</title>
@endsection

@section('extra-css')
<!-- Bootstrap4 Duallistbox -->
<link rel="stylesheet" href="{{ asset('plugins/bootstrap-duallistbox/bootstrap-duallistbox.min.css') }}">
<!-- Lightpick -->
<link rel="stylesheet" href="{{ asset('plugins/lightpick/lightpick.css') }}">
<!-- Custom -->
<link rel="stylesheet" href="{{ asset('dist/css/multiselect.css') }}">
<link rel="stylesheet" href="{{ asset('dist/css/loading.css') }}">
@endsection

@section('content-header')
<div class="content-header">
	<div class="container-fluid">
		<div class="row mb-2">
			<div class="col-sm-6">
				<h3 class="m-0 text-info">Gestión de Hist. completaciones</h3>
			</div>
			<div class="col-sm-6">
				<ol class="breadcrumb float-sm-right">
					<li class="breadcrumb-item"><a href="{{ route('home') }}">Inicio</a></li>
					<li class="breadcrumb-item active">Gestionar datos</li>
				</ol>
			</div>
		</div>
	</div>
</div>
@endsection

@section('content')
<div class="callout callout-danger">
	<h6> Aviso</h6>
	<p class="text-muted">Este módulo permite realizar una búsqueda de la información almacenada en la base de datos del sistema para modificar o remover datos. Por favor, asegurarse de tener los permisos necesarios para efectuar cambios en los registros.</p>
</div>
<div class="card card-secondary">
	<div class="card-body">
		<label>Filtrar por:</label>
		<div class="row">
			<div class="form-group col-md-6">
				<small class="text-muted">Elija una opción</small>
				<div class="form-group">
					<select id="macolla" name="macolla" class="form-control" onkeydown="event.preventDefault();" onchange="m.getOptionSelected(this.value)">
						<optgroup label="Seleccionar macolla">
							<option value="">--</option>
							@php $all = '' @endphp
							@foreach($macolla as $key)
							@php $all .= $key->id.',' @endphp
							<option value="{{ $key->id }}|{{ $key->macolla_name }}">{{ $key->macolla_name }}</option>
							@endforeach
							<option value="{{ $value->id }}|{{ $value->macolla_name }}">S/M</option>
						</optgroup>
					</select>
				</div>
			</div>
			<div class="form-group col-md-6 multiselect" id="reservoirs" style="display:none">
				<small class="text-muted">Yacimientos</small>
				<div class="selectBox" onclick="showCheckboxes()">
					<select id="reservoirs" class="form-control">
						<option value="">Seleccionar opción</option>
					</select>
					<div class="overSelect"></div>
				</div>
				<div id="checkboxes">
					@foreach($reservoirs as $key)
					<label for="elementWhatFor{{ $key->id }}">
						<input type="checkbox" name="option[]" id="elementWhatFor{{ $key->id }}" value="{{ $key->id }}" checked/> {{ $key->reservoir_name }}
					</label>
					@endforeach
				</div>
			</div>
		</div>

		<div id="dual-list" style="display:none"></div>
		<div id="dual-list-2" style="display:none"></div>

		<div class="row">
			<div class="form-group col-md-6">
				<label>Fecha</label>
				<select class="form-control" onchange="m.setFilterDateImproved(this.value)" id="filter-date">
					<option value="1">Predeterminada</option>
					<option value="2">Intervalo</option>
				</select>
			</div>
			<div class="form-group col-md-6" id="filter-predetermined">
				<label class="text-muted">Predeterminada</label>
				<select class="form-control" name="date-pick" id="date-pick">
					<option value="{{ date('d/m/Y')}}">Hoy</option>
					<option value="{{ date('d/m/Y', strtotime('-1 days')) }}">Ayer</option>
					<option value="{{ date('d/m/Y', strtotime('-7 days')) }} - {{ date('d/m/Y') }}">Últimos 7 días</option>
					<option value="{{ date('d/m/Y', strtotime('-15 days')) }} - {{ date('d/m/Y') }}">Últimos 15 días</option>
					<option value="{{ date('01/m/Y') }} - {{ date('d/m/Y') }}">Este mes</option>
					<option value="{{ date('01/m/Y', strtotime('-1 month')) }} - {{ date('t/m/Y', strtotime('-1 month')) }}">Mes pasado</option>
					<option value="{{ date('01/m/Y', strtotime('-6 month')) }} - {{ date('d/m/Y') }}">Últimos 6 meses</option>
					<option value="{{ date('01/m/Y', strtotime('-12 month')) }} - {{ date('d/m/Y') }}">Último año</option>
				</select>
			</div>
			<div class="form-group col-md-6" id="filter-customized" style="display:none"></div>
		</div>

		<div class="row" style="display:none">	
			<div class="form-group col-md-3">
				<div class="form-check">
					<label class="form-check-label">
						<input class="form-check-input" type="checkbox" id="item" name="item[]" value="type_work_i" checked>Tipo de trabajo
					</label>
				</div>
				<div class="form-check">
					<label class="form-check-label">
						<input class="form-check-input" type="checkbox" id="item" name="item[]" value="type_work_ii" checked>Tipo de trabajo 2
					</label>
				</div>
				<div class="form-check">
					<label class="form-check-label">
      					<input class="form-check-input" type="checkbox" id="item" name="item[]" value="measure_from" checked>Medidas desde
      				</label>
      			</div>
				<div class="form-check">
					<label class="form-check-label">
      					<input class="form-check-input" type="checkbox" id="item" name="item[]" value="rig" checked>Equipo
      				</label>
      			</div>
				  <div class="form-check">
					<label class="form-check-label">
						<input class="form-check-input" type="checkbox" id="item" name="item[]" value="sensor_model" checked>Modelo Sensor
					</label>
				</div>
			</div>
			
			<div class="form-group col-md-3">
				<div class="form-check">
					<label class="form-check-label">
						<input class="form-check-input" type="checkbox" id="item" name="item[]" value="sensor_diameter" checked>Diámetro Sensor
					</label>
				</div>
				<div class="form-check">
					<label class="form-check-label">
						<input class="form-check-input" type="checkbox" id="item" name="item[]" value="sensor_brand" checked>Marca Sensor
					</label>
				</div>
				<div class="form-check">
					<label class="form-check-label">
						<input class="form-check-input" type="checkbox" id="item" name="item[]" value="sensor_company" checked>Compañía Sensor
					</label>
				</div>
				<div class="form-check">
      				<label class="form-check-label">
      					<input class="form-check-input" type="checkbox" id="item" name="item[]" value="deeptop_sensor_md" checked>Prof. Sup. Sensor (MD)
      				</label>
      			</div>
				<div class="form-check">
      				<label class="form-check-label">
      					<input class="form-check-input" type="checkbox" id="item" name="item[]" value="deepbottom_sensor_md" checked>Prof. Inf. Sensor (MD)
      				</label>
      			</div>
			</div>
			
			<div class="form-group col-md-3">
				<div class="form-check">
					<label class="form-check-label">
      					<input class="form-check-input" type="checkbox" id="item" name="item[]" value="pump_model" checked>Modelo Bomba
      				</label>
      			</div>
				<div class="form-check">
					<label class="form-check-label">
						<input class="form-check-input" type="checkbox" id="item" name="item[]" value="pump_diameter" checked>Diámetro Bomba
					</label>
				</div>
      			<div class="form-check">
      				<label class="form-check-label">
      					<input class="form-check-input" type="checkbox" id="item" name="item[]" value="pump_company" checked>Compañía Bomba
      				</label>
      			</div>
      			<div class="form-check">
      				<label class="form-check-label">
      					<input class="form-check-input" type="checkbox" id="item" name="item[]" value="deeptop_pump_md" checked>Prof. Sup. Bomba (MD)
      				</label>
      			</div>
				<div class="form-check">
      				<label class="form-check-label">
      					<input class="form-check-input" type="checkbox" id="item" name="item[]" value="deepbottom_pump_md" checked>Prof. Inf. Bomba (MD)
      				</label>
      			</div>
      		</div>
			
			<div class="form-group col-md-3">
				<div class="form-check">
      				<label class="form-check-label">
      					<input class="form-check-input" type="checkbox" id="item" name="item[]" value="pump_type" checked>Método bombeo
      				</label>
      			</div>
      			<div class="form-check">
      				<label class="form-check-label">
      					<input class="form-check-input" type="checkbox" id="item" name="item[]" value="pump_capacity" checked>Capacidad Bomba
      				</label>
      			</div>
      			<div class="form-check">
      				<label class="form-check-label">
      					<input class="form-check-input" type="checkbox" id="item" name="item[]" value="inyected_diluent" checked>Inyección diluente
      				</label>
      			</div>
      			<div class="form-check">
      				<label class="form-check-label">
      					<input class="form-check-input" type="checkbox" id="item" name="item[]" value="observations" checked>Observaciones
      				</label>
      			</div>
				  <div class="form-check">
      				<label class="form-check-label">
      					<input class="form-check-input" type="checkbox" id="item" name="item[]" value="references" checked>Referencias
      				</label>
      			</div>
      		</div>
		</div>
		<center id="loading"></center>
	</div>
	
	<div class="card-footer text-center">
		<button class="btn btn-info" onclick="return validate(hst)" id="btn-consult">Consultar</button>
	</div>
</div>

<div class="modal fade" id="modal-xl" style="display:none" aria-hidden="true" data-keyboard="false" data-backdrop="static"></div>

<!-- table data -->
<div id="table-content"></div>
<br>

@section('extra-script')
<script src="{{ asset('plugins/moment/moment.min.js') }}"></script>
<script src="{{ asset('plugins/jquery-datatable/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('plugins/jquery-datatable/select.js') }}"></script>
<script src="{{ asset('plugins/jquery-datatable/dataTables.scroller.min.js') }}"></script>
<link rel="stylesheet" href="{{ asset('plugins/jquery-datatable/jquery.dataTables.min.css') }}" />
<script src="{{ asset('plugins/bootstrap-duallistbox/jquery.bootstrap-duallistbox.min.js') }}"></script>
<script src="{{ asset('plugins/lightpick/lightpick.js') }}"></script>
<script src="{{ asset('plugins/jquery-numeric/jquery.numeric.js') }}"></script>
<script src="{{ asset('js/myScripts.js') }}"></script>
<script src="{{ asset('js/Method.js') }}"></script>
<script src="{{ asset('js/Management/ManagementHistorical.js') }}"></script>
@endsection

@endsection