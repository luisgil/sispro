@extends('layouts.app')

@section('title')
<title>SISPRO | Macollas</title>
@endsection

@section('content-header')
<div class="content-header">
	<div class="container-fluid">
		<div class="row mb-2">
			<div class="col-sm-6">
				<h3 class="m-0 text-dark">Macollas</h3>
			</div>
			<div class="col-sm-6">
				<ol class="breadcrumb float-sm-right">
					<li class="breadcrumb-item"><a href="{{ route('home') }}">Inicio</a></li>
					<li class="breadcrumb-item active">Macollas</li>
				</ol>
			</div>
		</div>
	</div>
</div>
@endsection

@section('content')
<div class="callout callout-danger">
	<h6> Aviso</h6>
	<p class="text-muted">Este módulo permite agregar, modificar o remover <b>macollas</b>. Por favor, asegurarse de tener los permisos necesarios para efectuar cambios en los registros.</p>
</div>
<div class="card card-secondary" id="main-options" style="display:block">
	<div class="card-body">
		<div class="col-md-6">
			<h6>¿Qué desea hacer?</h6>
			<div class="input-group mb-3">
				<select class="form-control" id="options" name="options">
					<option value>-</option>
					<option value=1>Agregar </option>
					<option value=2>Gestionar</option>
				</select>
				<span class="input-group-append">
					<button class="btn btn-info btn-block" id="btn-search" onclick="myMacolla.getSelectedOption()">OK</button>
				</span>
			</div>
		</div>
	</div>
</div>

@section('extra-script')
<script src="{{ asset('plugins/jquery-datatable/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('plugins/jquery-datatable/select.js') }}"></script>
<script src="{{ asset('plugins/jquery-datatable/dataTables.scroller.min.js') }}"></script>
<link rel="stylesheet" href="{{ asset('plugins/jquery-datatable/jquery.dataTables.min.css') }}" />
<script src="{{ asset('js/Information/Macolla.js') }}"></script>
@endsection

@endsection