@extends('layouts.app')

@section('title')
<title>SISPRO | Gestionar datos</title>
@endsection

@section('extra-css')
<link rel="stylesheet" href="{{ asset('plugins/hint/hint.min.css') }}">
@endsection

@section('content-header')
<div class="content-header">
	<div class="container-fluid">
		<div class="row mb-2">
			<div class="col-sm-6">
				<h3 class="m-0 text-dark">Gestionar datos</h3>
			</div>
			<div class="col-sm-6">
				<ol class="breadcrumb float-sm-right">
					<li class="breadcrumb-item"><a href="{{ route('home') }}">Inicio</a></li>
					<li class="breadcrumb-item active">Gestionar datos</li>
				</ol>
			</div>
		</div>
	</div>
</div>
@endsection

@section('content')
<div class="card card-secondary" id="main-options" style="display:block">
	<div class="card-body" id="body-option">
		<div class="row">
			<div class="col-md-5">
				<select class="form-control" id="select-option" name="select-option">
                    <option value>-Seleccione una opción-</option>
                    <option value=1> Pruebas Extendidas</option>
                    <option value=2> Pruebas Promedio</option>
                    <option value=3> Muestras</option>
					<option value=4> PyT Original</option>
                    <option value=5> Histórico completaciones</option>
                    <option value=6> Presión Estática</option>
					<option value=7> Presión Dinámica</option>
                    <option value=8> Presión Original PMVM</option>
                    <option value=9> Survey Real </option>
					<option value=10> Survey Plan</option>
					<option value=11> Producción Fiscalizada </option>
					<option value=12> Eventos de pozo</option>
				</select>
			</div>
			<div class="col-md-5">
				<span class="hint--right hint--info text-primary" data-hint="Elija uno de los módulos que se muestra en la lista para efectuar cambios o eliminar registros"><i class="fa fa-question-circle"></i></span>
			</div>
		</div>
	</div>

    <div class="card-footer" style="text-align: center">
		<button class="btn btn-info" onclick="mnt.optionChooser()">Continuar</button>
	</div>
</div>

@section('extra-script')
<script async src="{{ asset('js/Management/Management.js') }}"></script>
@endsection

@endsection