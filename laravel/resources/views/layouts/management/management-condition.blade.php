@extends('layouts.app')

@section('title')
<title>SISPRO | Gestión de PyT Original</title>
@endsection

@section('extra-css')
<!-- Bootstrap4 Duallistbox -->
<link rel="stylesheet" href="{{ asset('plugins/bootstrap-duallistbox/bootstrap-duallistbox.min.css') }}">
<!-- Lightpick -->
<link rel="stylesheet" href="{{ asset('plugins/lightpick/lightpick.css') }}">
<!-- Custom -->
<link rel="stylesheet" href="{{ asset('dist/css/multiselect.css') }}">
<link rel="stylesheet" href="{{ asset('dist/css/loading.css') }}">
@endsection

@section('content-header')
<div class="content-header">
	<div class="container-fluid">
		<div class="row mb-2">
			<div class="col-sm-6">
				<h3 class="m-0 text-info">Gestión de PyT Original</h3>
			</div>
			<div class="col-sm-6">
				<ol class="breadcrumb float-sm-right">
					<li class="breadcrumb-item"><a href="{{ route('home') }}">Inicio</a></li>
					<li class="breadcrumb-item active">Gestionar datos</li>
				</ol>
			</div>
		</div>
	</div>
</div>
@endsection

@section('content')
<div class="callout callout-danger">
	<h6> Aviso</h6>
	<p class="text-muted">Este módulo permite realizar una búsqueda de la información almacenada en la base de datos del sistema para modificar o remover datos. Por favor, asegurarse de tener los permisos necesarios para efectuar cambios en los registros.</p>
</div>
<div class="card card-secondary">
	<div class="card-body">
		<label>Filtrar por:</label>
		<div class="row">
			<div class="form-group col-md-6">
				<small class="text-muted">Elija una opción</small>
				<div class="form-group">
					<select id="macolla" name="macolla" class="form-control" onkeydown="event.preventDefault();" onchange="m.getOptionSelected(this.value)">
						<optgroup label="Seleccionar macolla">
							<option value="">--</option>
							<option value="all">BLOQUE JUNÍN 6</option>
						</optgroup>
					</select>
				</div>
			</div>
			<div class="form-group col-md-6 multiselect" id="reservoirs" style="display:none">
				<small class="text-muted">Yacimientos</small>
				<div class="selectBox" onclick="showCheckboxes()">
					<select id="reservoir" class="form-control">
						<option value="">Seleccionar opción</option>
					</select>
					<div class="overSelect"></div>
				</div>
				<div id="checkboxes">
					@foreach($reservoirs as $key)
					<label for="elementWhatFor{{ $key->id }}">
						<input type="checkbox" name="option[]" id="elementWhatFor{{ $key->id }}" value="{{ $key->id }}" checked/> {{ $key->reservoir_name }}
					</label>
					@endforeach
				</div>
			</div>
		</div>

		<div id="dual-list" style="display:none"></div>
		<div id="dual-list-2" style="display:none"></div>

		<div class="row" style="display:none">	
			<div class="form-group col-md-3">
				<div class="form-check">
					<label class="form-check-label">
						<input class="form-check-input" type="checkbox" id="item" name="item[]" value="test" checked>Test
					</label>
				</div>
				<div class="form-check">
					<label class="form-check-label">
						<input class="form-check-input" type="checkbox" id="item" name="item[]" value="deep_md" checked>Profundidad MD
					</label>
				</div>
				<div class="form-check">
					<label class="form-check-label">
						<input class="form-check-input" type="checkbox" id="item" name="item[]" value="deep_tvd" checked>Profundidad TVD
					</label>
				</div>
			</div>
			
			<div class="form-group col-md-3">
				<div class="form-check">
					<label class="form-check-label">
      					<input class="form-check-input" type="checkbox" id="item" name="item[]" value="pressbefore" checked>Presión Hid. Antes
      				</label>
      			</div>
      			<div class="form-check">
      				<label class="form-check-label">
      					<input class="form-check-input" type="checkbox" id="item" name="item[]" value="pressafter" checked>Presión Hid. Después
      				</label>
      			</div>
				<div class="form-check">
					<label class="form-check-label">
      					<input class="form-check-input" type="checkbox" id="item" name="item[]" value="pressformation" checked>Presión de Formación
      				</label>
      			</div>
      		</div>
			
			<div class="form-group col-md-3">
				<div class="form-check">
      				<label class="form-check-label">
      					<input class="form-check-input" type="checkbox" id="item" name="item[]" value="temperature" checked>Temperatura
      				</label>
      			</div>
      			<div class="form-check">
      				<label class="form-check-label">
      					<input class="form-check-input" type="checkbox" id="item" name="item[]" value="duration" checked>Tiempo
      				</label>
      			</div>
				  <div class="form-check">
      				<label class="form-check-label">
      					<input class="form-check-input" type="checkbox" id="item" name="item[]" value="sand" checked>Arena
      				</label>
      			</div>
      		</div>
			
			<div class="form-group col-md-3">
				<div class="form-check">
      				<label class="form-check-label">
      					<input class="form-check-input" type="checkbox" id="item" name="item[]" value="mobility" checked>Movilidad
      				</label>
      			</div>
      			<div class="form-check">
      				<label class="form-check-label">
      					<input class="form-check-input" type="checkbox" id="item" name="item[]" value="observations" checked>Observaciones
      				</label>
      			</div>
      		</div>
		</div>

		<center id="loading"></center>

	</div>

	<div class="card-footer text-center">
		<button class="btn btn-info" onclick="return validate(cnd)" id="btn-consult">Consultar</button>
	</div>
</div>

<div class="modal fade" id="modal-xl" style="display:none" aria-hidden="true" data-keyboard="false" data-backdrop="static"></div>

<!-- table data -->
<div id="table-content"></div>
<br>

@section('extra-script')
<!--moment-->
<script src="{{ asset('plugins/moment/moment.min.js') }}"></script>
<!-- export data -->
<script src="{{ asset('plugins/jquery-datatable/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('plugins/jquery-datatable/select.js') }}"></script>
<script src="{{ asset('plugins/jquery-datatable/dataTables.scroller.min.js') }}"></script>
<link rel="stylesheet" href="{{ asset('plugins/jquery-datatable/jquery.dataTables.min.css') }}" />
<!-- Bootstrap4 Duallistbox -->
<script src="{{ asset('plugins/bootstrap-duallistbox/jquery.bootstrap-duallistbox.min.js') }}"></script>
<!-- Lightpick -->
<script src="{{ asset('plugins/lightpick/lightpick.js') }}"></script>
<!-- custom scripts -->
<script src="{{ asset('plugins/jquery-numeric/jquery.numeric.js') }}"></script>
<script src="{{ asset('js/myScripts.js') }}"></script>
<script src="{{ asset('js/Method.js') }}"></script>
<script src="{{ asset('js/Management/ManagementCondition.js') }}"></script>
@endsection

@endsection