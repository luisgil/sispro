@extends('layouts.app')

@section('title')
<title>SISPRO | Administrar información</title>
@endsection

@section('extra-css')
<link rel="stylesheet" href="{{ asset('plugins/hint/hint.min.css') }}">
@endsection

@section('content-header')
<div class="content-header">
	<div class="container-fluid">
		<div class="row mb-2">
			<div class="col-sm-6">
				<h3 class="m-0">Administrar información</h3>
			</div>
			<div class="col-sm-6">
				<ol class="breadcrumb float-sm-right">
					<li class="breadcrumb-item"><a href="{{ route('home') }}">Inicio</a></li>
					<li class="breadcrumb-item active">Administrar información</li>
				</ol>
			</div>
		</div>
	</div>
</div>
@endsection

@section('content')
<div class="card card-secondary" id="main-options" style="display:block">
	<div class="card-body">
		<div class="row">
			<div class="col-md-5">
				<select class="form-control" id="options" name="options">
					<option value>-Seleccione una opción-</option>
					<option value=1>Macollas</option>
					<option value=2>Yacimientos</option>
					<option value=3>Pozos</option>
				</select>
			</div>
			<div class="col-md-5">
				<span class="hint--right hint--info text-primary" data-hint="Elija uno de los módulos que se muestra en la lista para efectuar cambios o eliminar registros"><i class="fa fa-question-circle"></i></span>
			</div>
		</div>
	</div>
	<div class="card-footer text-center">
		<button class="btn btn-info" id="btn-search" onclick="optionChooser()">Continuar</button>
	</div>
</div>

<script>
	function optionChooser() {
		var select = parseInt(document.querySelector(`#options`).value);
		switch(select) {
			case 1:
			location.href = `${globalConfig.appUrl}/macollaManagement`;
			break;
			case 2:
			location.href = `${globalConfig.appUrl}/reservoirManagement`;
			break;
			case 3:
			location.href = `${globalConfig.appUrl}/wellManagement`;
			break;
			default:
			alertify.alert('Aviso', 'Debe seleccionar una opción para poder continuar.', function(){}).set({'movable': false, 'moveBounded': false});
		}
	}
</script>

@endsection