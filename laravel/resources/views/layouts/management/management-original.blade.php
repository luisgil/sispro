@extends('layouts.app')

@section('title')
<title>SISPRO | Gestión Pres. Original PMVM</title>
@endsection

@section('extra-css')
<!-- Bootstrap4 Duallistbox -->
<link rel="stylesheet" href="{{ asset('plugins/bootstrap-duallistbox/bootstrap-duallistbox.min.css') }}">
<!-- Custom -->
<link rel="stylesheet" href="{{ asset('dist/css/multiselect.css') }}">
<link rel="stylesheet" href="{{ asset('dist/css/loading.css') }}">
@endsection

@section('content-header')
<div class="content-header">
	<div class="container-fluid">
		<div class="row mb-2">
			<div class="col-sm-6">
				<h3 class="m-0 text-info">Gestión Pres. Original PMVM</h3>
			</div>
			<div class="col-sm-6">
				<ol class="breadcrumb float-sm-right">
					<li class="breadcrumb-item"><a href="{{ route('home') }}">Inicio</a></li>
					<li class="breadcrumb-item active">Gestionar datos</li>
				</ol>
			</div>
		</div>
	</div>
</div>
@endsection

@section('content')
<div class="callout callout-danger">
	<h6> Aviso</h6>
	<p class="text-muted">Este módulo permite realizar una búsqueda de la información almacenada en la base de datos del sistema para modificar o remover datos. Por favor, asegurarse de tener los permisos necesarios para efectuar cambios en los registros.</p>
</div>
<div class="card card-secondary">
	<div class="card-body">
		<label>Filtrar por:</label>
		<div class="row">
			<div class="form-group col-md-6">
				<small class="text-muted">Elija una opción</small>
				<div class="form-group">
					<select id="macolla" name="macolla" class="form-control" onkeydown="event.preventDefault();" onchange="m.getOptionSelected(this.value)">
						<optgroup label="Seleccionar una opción">
							<option value="">--</option>
							@php $all = '' @endphp
							<option value="{{ $value->id }}|{{ $value->macolla_name }}">SIN MACOLLAS</option>
							<option value="all">BLOQUE JUNÍN 6</option>
						</optgroup>
					</select>
				</div>
			</div>
			<div class="form-group col-md-6 multiselect" id="reservoirs" style="display:none">
				<small class="text-muted">Yacimientos</small>
				<div class="selectBox" onclick="showCheckboxes()">
					<select id="reservoir" class="form-control">
						<option value="">Seleccionar opción</option>
					</select>
					<div class="overSelect"></div>
				</div>
				<div id="checkboxes">
					@foreach($reservoirs as $key)
					<label for="elementWhatFor{{ $key->id }}">
						<input type="checkbox" name="option[]" id="elementWhatFor{{ $key->id }}" value="{{ $key->id }}" checked/> {{ $key->reservoir_name }}
					</label>
					@endforeach
				</div>
			</div>
		</div>
		
		<div id="dual-list" style="display:none"></div>
		<div id="dual-list-2" style="display:none"></div>

		<div class="row" style="display: none;">	
			<div class="form-group col-md-3">
				<div class="form-check">
					<label class="form-check-label">
						<input class="form-check-input" type="checkbox" id="item" name="item[]" value="type_test" checked>Tipo de prueba
					</label>
				</div>
				<div class="form-check">
					<label class="form-check-label">
						<input class="form-check-input" type="checkbox" id="item" name="item[]" value="sensor_model" checked>Modelo Sensor
					</label>
				</div>
				<div class="form-check">
					<label class="form-check-label">
      					<input class="form-check-input" type="checkbox" id="item" name="item[]" value="pressure_gradient" checked>Gradiente de Presión
      				</label>
      			</div>
      		</div>
			<div class="form-group col-md-4">
				<div class="form-check">
					<label class="form-check-label">
      					<input class="form-check-input" type="checkbox" id="item" name="item[]" value="deepdatum_mc_md" checked>Prof. DATUM Macolla MD
      				</label>
      			</div>
      			<div class="form-check">
					<label class="form-check-label">
						<input class="form-check-input" type="checkbox" id="item" name="item[]" value="deepdatum_mc_tvd" checked>Prof. DATUM Macolla TVD
					</label>
				</div>
				<div class="form-check">
					<label class="form-check-label">
      					<input class="form-check-input" type="checkbox" id="item" name="item[]" value="press_datum_macolla" checked>Presión Original DATUM Macolla
      				</label>
      			</div>
      		</div>
			
			<div class="form-group col-md-5">
      			<div class="form-check">
      				<label class="form-check-label">
      					<input class="form-check-input" type="checkbox" id="item" name="item[]" value="temp_datum_macolla" checked>Temp. Original DATUM Macolla
      				</label>
      			</div>
				<div class="form-check">
					<label class="form-check-label">
      					<input class="form-check-input" type="checkbox" id="item" name="item[]" value="temperature_gradient" checked>Gradiente de Temperatura
      				</label>
      			</div>
      		</div>
		</div>
		<div class="row" style="display: none;">
			<div class="form-group col-md-12">
				<div class="form-check">
      				<label class="form-check-label text-muted">
      					<input class="form-check-input" type="checkbox" value="" onclick="selectCheckbox(this)" checked>Marcar/Desmarcar todos
      				</label>
      			</div>
      		</div>
		</div>
		<center id="loading"></center>
	</div>
	<div id="message"></div>
	<div class="card-footer text-center">
		<button class="btn btn-info" onclick="return validate(org)" id="btn-consult">Consultar</button>
	</div>
</div>

<div class="modal fade" id="modal-xl" style="display:none" aria-hidden="true" data-keyboard="false" data-backdrop="static"></div>

<!-- table data -->
<div id="table-content"></div>
<br>

@section('extra-script')
<!--moment-->
<script src="{{ asset('plugins/moment/moment.min.js') }}"></script>
<!-- export data -->
<script src="{{ asset('plugins/jquery-datatable/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('plugins/jquery-datatable/select.js') }}"></script>
<script src="{{ asset('plugins/jquery-datatable/dataTables.scroller.min.js') }}"></script>
<link rel="stylesheet" href="{{ asset('plugins/jquery-datatable/jquery.dataTables.min.css') }}" />
<script src="{{ asset('plugins/jquery-datatable/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('plugins/jquery-datatable/buttons.bootstrap4.min.js') }}"></script>
<script src="{{ asset('plugins/jquery-datatable/buttons.html5.min.js') }}"></script>
<script src="{{ asset('plugins/jquery-datatable/jszip.min.js') }}"></script>
<!-- Bootstrap4 Duallistbox -->
<script src="{{ asset('plugins/bootstrap-duallistbox/jquery.bootstrap-duallistbox.min.js') }}"></script>
<!-- custom scripts -->
<script src="{{ asset('plugins/jquery-numeric/jquery.numeric.js') }}"></script>
<script src="{{ asset('js/myScripts.js') }}"></script>
<script src="{{ asset('js/Method.js') }}"></script>
<script src="{{ asset('js/Management/ManagementOriginal.js') }}"></script>
@endsection

@endsection