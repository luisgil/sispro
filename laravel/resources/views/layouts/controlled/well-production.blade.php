@extends('layouts.app')

@section('title')
<title>SISPRO | Prod. por Pozo</title>
@endsection

@section('extra-css')
<!-- Bootstrap4 Duallistbox -->
<link rel="stylesheet" href="{{ asset('plugins/bootstrap-duallistbox/bootstrap-duallistbox.min.css') }}">
<!-- Custom -->
<link rel="stylesheet" href="{{ asset('dist/css/multiselect.css') }}">
<link rel="stylesheet" href="{{ asset('dist/css/loading.css') }}">
@endsection

@section('content-header')
<div class="content-header">
	<div class="container-fluid">
		<div class="row mb-2">
			<div class="col-sm-6">
				<h3 class="m-0 text-dark">Producción por Pozo</h3>
			</div>
			<div class="col-sm-6">
				<ol class="breadcrumb float-sm-right">
					<li class="breadcrumb-item"><a href="{{ route('home') }}">Inicio</a></li>
					<li class="breadcrumb-item active">Producción por Pozo</li>
				</ol>
			</div>
		</div>
	</div>
</div>
@endsection

@section('content')
<div class="card card-secondary">
	<div class="card-body">
		<label>Filtrar por:</label>
		<div class="row">
			<div class="form-group col-md-6">
				<small class="text-muted">Macolla</small>
				<div class="form-group">
					<select id="macolla" name="macolla" class="form-control" onkeydown="event.preventDefault();" onchange="m.loadLocationsToSelect(this.value)">
						<optgroup label="Seleccionar macolla">
							<option value="">--</option>
							@php $all = '' @endphp
							@foreach($macolla as $key)
							@php $all .= $key->id.',' @endphp
							<option value="{{ $key->id }}|{{ $key->macolla_name }}">{{ $key->macolla_name }}</option>
							@endforeach
							<option value="{{ $value->id }}|{{ $value->macolla_name }}">S/M</option>
						</optgroup>
					</select>
				</div>
			</div>
			<div class="form-group col-md-6">
				<small class="text-muted">Localización</small>
				<div class="form-group">
					<select id="well" name="well" class="form-control" onkeydown="event.preventDefault();">
						<optgroup label="Seleccionar pozo">
							<option value="">--</option>
						</optgroup>
					</select>
				</div>
			</div>
		</div>
		
		<div class="row">
			<div class="form-group col-md-6">
				<label>Fecha</label>
				<select class="form-control" onchange="m.setFilterDateImproved(this.value)" id="filter-date">
					<option value="1">Predeterminada</option>
					<option value="2">Intervalo</option>
				</select>
			</div>
			<div class="form-group col-md-6" id="filter-predetermined"></div>
			<div class="form-group col-md-6" id="filter-customized" style="display:none"></div>
		</div>
		
		<center id="loading"></center>

	</div>
	<div class="card-footer text-center">
		<button class="btn btn-info" onclick="pbw.searchByFilter()" id="btn-consult">Consultar</button>
	</div>
</div>
<!-- table data -->
<div id="table-content"></div>
<br>

@section('extra-script')
<!--moment-->
<script src="{{ asset('plugins/moment/moment.min.js') }}"></script>
<!-- export data -->
<script src="{{ asset('plugins/jquery-datatable/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('plugins/jquery-datatable/select.js') }}"></script>
<script src="{{ asset('plugins/jquery-datatable/dataTables.scroller.min.js') }}"></script>
<link rel="stylesheet" href="{{ asset('plugins/jquery-datatable/jquery.dataTables.min.css') }}" />
<script src="{{ asset('plugins/jquery-datatable/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('plugins/jquery-datatable/buttons.bootstrap4.min.js') }}"></script>
<script src="{{ asset('plugins/jquery-datatable/buttons.html5.min.js') }}"></script>
<script src="{{ asset('plugins/jquery-datatable/jszip.min.js') }}"></script>
<!-- Bootstrap4 Duallistbox -->
<script src="{{ asset('plugins/bootstrap-duallistbox/jquery.bootstrap-duallistbox.min.js') }}"></script>
<!-- custom scripts -->
<script src="{{ asset('js/myScripts.js') }}"></script>
<script src="{{ asset('js/Method.js') }}"></script>
<script src="{{ asset('js/ProductionByWell.js') }}"></script>
@endsection

@endsection