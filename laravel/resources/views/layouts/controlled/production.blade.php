@extends('layouts.app')

@section('title')
<title>SISPRO | Producción por Macolla</title>
@endsection

@section('script-compressed')
<!-- custom scripts -->
<script async src="{{ asset('js/myScripts.js') }}"></script>
<script async src="{{ asset('js/Method.js') }}"></script>
<script async src="{{ asset('js/FiscalizedProduction.js') }}"></script>
@endsection

@section('extra-css')
<!-- Bootstrap4 Duallistbox -->
<link rel="stylesheet" href="{{ asset('plugins/bootstrap-duallistbox/bootstrap-duallistbox.min.css') }}">
<!-- Custom -->
<link rel="stylesheet" href="{{ asset('dist/css/multiselect.css') }}">
<link rel="stylesheet" href="{{ asset('dist/css/loading.css') }}">
@endsection

@section('content-header')
<div class="content-header">
	<div class="container-fluid">
		<div class="row mb-2">
			<div class="col-sm-6">
				<h3 class="m-0 text-dark">Producción por Macolla</h3>
			</div>
			<div class="col-sm-6">
				<ol class="breadcrumb float-sm-right">
					<li class="breadcrumb-item"><a href="{{ route('home') }}">Inicio</a></li>
					<li class="breadcrumb-item active">Producción por Macolla</li>
				</ol>
			</div>
		</div>
	</div>
</div>
@endsection

@section('content')
<div class="card card-secondary">
	<div class="card-body">
		<label>Filtrar por:</label>
		<div class="row">
			<div class="form-group col-md-6">
				<small class="text-muted">Elija una opción</small>
				<div class="form-group">
					<select id="macolla" name="macolla" class="form-control" onkeydown="event.preventDefault();">
						<optgroup label="Seleccionar una opción">
							<option value="">--</option>
							@php $all = '' @endphp
							@foreach($macolla as $key)
							@php $all .= $key->id.',' @endphp
							<option value="{{ $key->id }}">{{ $key->macolla_name }}</option>
							@endforeach
							<option value="{{ $value->id }}">S/M</option>
						</optgroup>
					</select>
				</div>
			</div>
			<div class="form-group col-md-6 multiselect" id="oilfields" style="display:none">
			</div>
		</div>

		<div id="dual-list" style="display:none"></div>
		<div id="dual-list-2" style="display:none"></div>

		<div class="row">
			<div class="form-group col-md-6">
				<label>Fecha</label>
				<select class="form-control" onchange="m.setFilterDateImproved(this.value)" id="filter-date">
					<option value="1">Predeterminada</option>
					<option value="2">Intervalo</option>
				</select>
			</div>
			<div class="form-group col-md-6" id="filter-predetermined"></div>
			<div class="form-group col-md-6" id="filter-customized" style="display:none"></div>
			<input type="checkbox" name="item[]" style="display:none" checked>
		</div>
		
		<center id="loading"></center>
		
	</div>
	
	<div id="message"></div>
	
	<div class="card-footer text-center">
		<button class="btn btn-info" onclick="fpr.validateSearch()" value="true" id="btn-consult">Consultar</button>
	</div>
	
</div>

<!-- table data -->
<div id="table-content"></div>

<br>

@section('extra-script')
<!--moment-->
<script src="{{ asset('plugins/moment/moment.min.js') }}"></script>
<!-- masked-input -->
<script src="{{ asset('plugins/jquery-maskedinput/jquery.maskedinput.min.js') }}"></script>
<!-- export data -->
<script src="{{ asset('plugins/jquery-datatable/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('plugins/jquery-datatable/select.js') }}"></script>
<script src="{{ asset('plugins/jquery-datatable/dataTables.scroller.min.js') }}"></script>
<link rel="stylesheet" href="{{ asset('plugins/jquery-datatable/jquery.dataTables.min.css') }}" />
<script src="{{ asset('plugins/jquery-datatable/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('plugins/jquery-datatable/buttons.bootstrap4.min.js') }}"></script>
<script src="{{ asset('plugins/jquery-datatable/buttons.html5.min.js') }}"></script>
<script src="{{ asset('plugins/jquery-datatable/jszip.min.js') }}"></script>
<!-- Bootstrap4 Duallistbox -->
<script src="{{ asset('plugins/bootstrap-duallistbox/jquery.bootstrap-duallistbox.min.js') }}"></script>
@endsection

@endsection