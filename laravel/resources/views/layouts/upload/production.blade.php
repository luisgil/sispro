@extends('layouts.app')

@section('title')
<title>SISPRO | Cargar datos</title>
@endsection


@section('extra-css')
<link rel="stylesheet" href="{{ asset('dist/css/loading.css') }}">
<script async src="{{ asset('js/Menu.js') }}"></script>
<script async src="{{ asset('js/Method.js') }}"></script>
<script async src="{{ asset('js/Add.js') }}"></script>
<script async src="{{ asset('js/Upload/UploadProduction.js') }}"></script>
@endsection

@section('content-header')
<div class="content-header">
	<div class="container-fluid">
		<div class="row mb-2">
			<div class="col-sm-6"></div>
		</div>
	</div>
</div>
@endsection

@section('content')
<div class="card card-default" id="main-card">
	<div class="card-header">
		<h3 class="card-title" id="card-title"><b>Cargar datos</b> / Pruebas de Producción</h3>
	</div>
	<div class="card-body">
		<div class="row" id="main-options"></div>
		<div class="col-md-6" id="show-form"></div>
		<div class="row" id="macolla-chooser"></div>
		<div id="excel-output"></div>
		<center id="loading"></center>
		<center id="loading-msg"></center>
		<div id="add-self"></div>
	</div>	
	<div id="option-buttons"></div>
</div>
<br/>

@section('extra-script')
<script src="{{ asset('plugins/jquery-numeric/jquery.numeric.js') }}"></script>
@endsection

@endsection