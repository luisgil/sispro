@extends('layouts.app')
@section('title')
    <title>SISPRO | Usuarios</title>
@endsection

@section('extra-css')
<!-- Lightpick -->
<link rel="stylesheet" href="{{ asset('plugins/lightpick/lightpick.css') }}">
<!-- Custom -->
<link rel="stylesheet" href="{{ asset('dist/css/multiselect.css') }}">
<link rel="stylesheet" href="{{ asset('dist/css/loading.css') }}">
@endsection

@section('content-header')
<div class="content-header">
	<div class="container-fluid">
		<div class="row mb-2">
			<div class="col-sm-6">
				<h3 class="m-0 text-dark">Movimientos</h3>
			</div>
			<div class="col-sm-6">
				<ol class="breadcrumb float-sm-right">
					<li class="breadcrumb-item"><a href="{{ route('home') }}">Inicio</a></li>
					<li class="breadcrumb-item active">Movimentos</li>
				</ol>
			</div>
		</div>
	</div>
</div>
@endsection

@section('content')
<div class="card card-secondary" id="main-options" style="display:block">
	<div class="card-body">
		<div class="row">
			<div class="form-group col-md-6">
				<label>Fecha</label>
				<select class="form-control" onchange="m.setFilterDateImproved(this.value)" id="filter-date">
					<option value="1">Predeterminada</option>
					<option value="2">Intervalo</option>
				</select>
			</div>
			<div class="form-group col-md-6" id="filter-predetermined">
				<label class="text-muted">Predeterminada</label>
				<select class="form-control" name="date-pick" id="date-pick">
					<option value="{{ date('d/m/Y')}}">Hoy</option>
					<option value="{{ date('d/m/Y', strtotime('-1 days')) }}">Ayer</option>
					<option value="{{ date('d/m/Y', strtotime('-7 days')) }} - |{{ date('d/m/Y') }}">Últimos 7 días</option>
					<option value="{{ date('d/m/Y', strtotime('-15 days')) }} - {{ date('d/m/Y') }}">Últimos 15 días</option>
					<option value="{{ date('01/m/Y') }} - {{ date('d/m/Y') }}">Este mes</option>
					<option value="{{ date('01/m/Y', strtotime('-1 month')) }} - {{ date('t/m/Y', strtotime('-1 month')) }}">Mes pasado</option>
					<option value="{{ date('01/m/Y', strtotime('-6 month')) }} - {{ date('d/m/Y') }}">Últimos 6 meses</option>
					<option value="{{ date('01/m/Y', strtotime('-12 month')) }} - {{ date('d/m/Y') }}">Último año</option>
				</select>
			</div>
			<div class="form-group col-md-6" id="filter-customized" style="display:none">
				<label class="text-muted">Intervalo</label>
				<input type="text" id="daterangepick" class="form-control" value="{{ date('d/m/Y') }} - {{ date('d/m/Y') }}" readonly>	
			</div>
		</div>
	</div>

	<center id="loading"></center>
	<br>

	<div class="card-footer text-center">
		<button class="btn btn-info" id="btn-consult" onclick="mvt.showResults()">Consultar</button>
	</div>
</div>
		
<div id="table-content"></div>
<br>

@section('extra-script')
<script src="{{ asset('plugins/moment/moment.min.js') }}"></script>
<!-- export data -->
<script src="{{ asset('plugins/jquery-datatable/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('plugins/jquery-datatable/select.js') }}"></script>
<script src="{{ asset('plugins/jquery-datatable/dataTables.scroller.min.js') }}"></script>
<link rel="stylesheet" href="{{ asset('plugins/jquery-datatable/jquery.dataTables.min.css') }}" />
<script src="{{ asset('plugins/jquery-datatable/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('plugins/jquery-datatable/buttons.bootstrap4.min.js') }}"></script>
<script src="{{ asset('plugins/jquery-datatable/buttons.html5.min.js') }}"></script>
<script src="{{ asset('plugins/jquery-datatable/jszip.min.js') }}"></script>
<!-- Bootstrap4 Duallistbox -->
<script src="{{ asset('plugins/bootstrap-duallistbox/jquery.bootstrap-duallistbox.min.js') }}"></script>
<script async src="{{ asset('js/myScripts.js') }}"></script>
<script async src="{{ asset('js/Method.js') }}"></script>
<script async src="{{ asset('js/Movement.js') }}"></script>
@endsection

@endsection