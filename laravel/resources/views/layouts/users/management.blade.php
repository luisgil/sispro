@extends('layouts.app')
@section('title')
    <title>SISPRO | Usuarios</title>
@endsection

@section('content-header')
<div class="content-header">
	<div class="container-fluid">
		<div class="row mb-2">
			<div class="col-sm-6">
				<h3 class="m-0 text-dark">Gestionar Usuarios</h3>
			</div>
			<div class="col-sm-6">
				<ol class="breadcrumb float-sm-right">
					<li class="breadcrumb-item"><a href="{{ route('home') }}">Inicio</a></li>
					<li class="breadcrumb-item active">Gestionar Usuarios</li>
				</ol>
			</div>
		</div>
	</div>
</div>
@endsection

@section('content')
<div class="card card-default" id="main-options">
    
    <div id="header">
		<h6 style="text-align:center;position: relative;top: 23px;">Cargando, por favor espere...</h6>
	</div>
    <div class="card-body" id="body"></div>
	
</div>
<div class="modal fade" id="modal-xl" style="display: none;" aria-hidden="true" data-keyboard="false" data-backdrop="static"></div>
<br>

@section('extra-script')
<!--moment-->
<script src="{{ asset('plugins/moment/moment.min.js') }}"></script>
<!-- export data -->
<!--<script src="{{ asset('plugins/jquery-datatable/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('plugins/jquery-datatable/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('plugins/jquery-datatable/buttons.flash.min.js') }}"></script>
<script src="{{ asset('plugins/jquery-datatable/jszip.min.js') }}"></script>
<script src="{{ asset('plugins/jquery-datatable/buttons.html5.min.js') }}"></script>
<script src="{{ asset('plugins/jquery-datatable/buttons.print.min.js') }}"></script>		
<script src="{{ asset('plugins/jquery-datatable/buttons.bootstrap4.min.js') }}"></script>-->
<script src="{{ asset('plugins/jquery-datatable/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('plugins/jquery-datatable/select.js') }}"></script>
<script src="{{ asset('plugins/jquery-datatable/dataTables.scroller.min.js') }}"></script>
<link rel="stylesheet" href="{{ asset('plugins/jquery-datatable/jquery.dataTables.min.css') }}" />
<script async src="{{ asset('js/User.js') }}"></script>
@endsection

@endsection