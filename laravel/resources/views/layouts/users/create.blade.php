@extends('layouts.app')
@section('title')
    <title>SISPRO | Usuarios</title>
@endsection

@section('content-header')
<div class="content-header">
	<div class="container-fluid">
		<div class="row mb-2">
			<div class="col-sm-6">
				<h3 class="m-0 text-dark">Agregar Usuarios</h3>
			</div>
			<div class="col-sm-6">
				<ol class="breadcrumb float-sm-right">
					<li class="breadcrumb-item"><a href="{{ route('home') }}">Inicio</a></li>
					<li class="breadcrumb-item active">Agregar usuario</li>
				</ol>
			</div>
		</div>
	</div>
</div>
@endsection

@section('content')
<div class="card card-secondary" id="main-options" style="display:block">
	<div class="card-body">
		<div class="row">
            <div class="col-md-6 pt-1">
                <label>Nombre</label>
                <input id="firstname" type="firstname" class="form-control onlychars" name="firstname" minlength="3" maxlength="27" placeholder="Nombre" autofocus autocomplete="off"/>
                <small id="wrapper-firstname" class="text-danger"></small>
            </div>

            <div class="col-md-6 pt-1">
                <label>Apellido</label>
                <input id="lastname" type="lastname" class="form-control onlychars" name="lastname" minlength="3" maxlength="27" placeholder="Apellido" autofocus autocomplete="off"/>
                <small id="wrapper-lastname" class="text-danger"></small>
            </div>

            <div class="col-md-6 pt-3">
                <label>Email</label>
                <input id="email" type="email" class="form-control" name="email" placeholder="Correo Electrónico" autofocus autocomplete="off"/>
                <small id="wrapper-email" class="text-danger"></small>
            </div>

            <div class="col-md-6 pt-3">
                <label>Rol</label>
                <select id="role" name="role" class="form-control">
                    <option value="">--</option>
                    <option value="ADMIN">Administrador</option>
                    <option value="OPERATOR">Operador</option>
                    <option value="EXAMINATOR">Consultor</option>
                </select>
                <small id="wrapper-role" class="text-danger"></small>
            </div>

            <div class="col-md-6 pt-3">
                <label>Contraseña</label>
                <input id="password" type="password" class="form-control" name="password" minlength="6" maxlength="20" placeholder="Contraseña"/>
                <small id="wrapper-password" class="text-danger"></small>
            </div>
            
            <div class="col-md-6 pt-3">
                <label>Repetir contraseña</label>
                <input type="password" class="form-control" id="password_confirmation" name="password_confirmation" minlength="6" maxlength="20" placeholder="Repetir Contraseña" required/>
                <small id="wrapper-password_confirmation" class="text-danger"></small>
            </div>
        </div>
    </div>
    
    <div class="card-footer text-center">
		<button class="btn btn-info" id="btn-add">Agregar</button>
	</div>
</div>
<br>

@section('extra-script')
<script async src="{{ asset('js/User.js') }}"></script>
@endsection

@endsection