<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    
    @yield('title')
    @yield('chart')
    @yield('script-compressed')
    @yield('extra-css')
    
    @section('css')
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="{{ asset('plugins/fontawesome-free/css/all.min.css') }}">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="{{ asset('dist/css/adminlte.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/overlayScrollbars/css/OverlayScrollbars.min.css') }}">
    <link rel="icon" type="image/png" sizes="128x128" href="{{ asset('dist/img/logo.png') }}">
    <link href="{{ asset('plugins/alertifyjs/alertify.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('plugins/alertifyjs/default.min.css') }}" rel="stylesheet" type="text/css">
    @show
  </head>
  
  <body class="hold-transition sidebar-mini layout-fixed">
    <div class="wrapper">
      <nav class="main-header navbar navbar-expand navbar-dark navbar-danger">
        <ul class="navbar-nav">
          <li class="nav-item">
            <a class="nav-link" data-widget="pushmenu" id="pushmenu" href="#"><i class="fas fa-bars"></i></a>
          </li>
          <li class="nav-item d-none d-sm-inline-block">
            <a href="{{ route('home') }}" class="nav-link">Inicio</a>
          </li>
          <li class="nav-item d-none d-sm-inline-block">
            <strong class="nav-link" >{{ Auth::user()->getSpanishRole() }}</strong>
          </li>
        </ul>
      </nav>
      
      <aside class="main-sidebar elevation-4 sidebar-light-danger">
        <a href="{{ route('home') }}" class="brand-link">
          <img src="{{ asset('dist/img/logo.png') }}" class="brand-image img-circle elevation-3" style="opacity: .8">
          <span class="brand-text font-weight-light">SISPRO</span>
          <span class="right badge badge-primary">1.7.0</span>
        </a>
        
        <div class="sidebar">
          <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="image">
              <img src="dist/img/user.png" class="img-circle elevation-2" alt="User Image">
            </div>
            <div class="info">
              <a href="javascript:void(0);" class="d-block">{{ Auth::user()->email }}</a>
            </div>
          </div>
          
          <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
              <li class="nav-item has-treeview">
                <a href="" class="nav-link">
                  <i class="nav-icon fas fa-oil-can"></i>
                  <p>
                    Pruebas Producción
                    <i class="right fas fa-angle-left"></i>
                  </p>
                </a>
                <ul class="nav nav-treeview">
                  <li class="nav-item">
                    <a href="{{ route('extendedTests') }}" class="nav-link">
                      <i class="far fa-circle nav-icon"></i>
                      <p>Pruebas Extendidas</p>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a href="{{ route('averageTests') }}" class="nav-link">
                      <i class="far fa-circle nav-icon"></i>
                      <p>Pruebas Promedio</p>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a href="{{ route('signs') }}" class="nav-link">
                      <i class="far fa-circle nav-icon"></i>
                      <p>Muestras</p>
                    </a>
                  </li>
                </ul>
              </li>

              <li class="nav-item has-treeview">
                <a href="javascript:void(0);" class="nav-link">
                  <i class="nav-icon fas fa-th"></i>
                  <p>
                    Prod. Fiscalizada
                    <i class="fas fa-angle-left right"></i>
                  </p>
                </a>
                <ul class="nav nav-treeview">
                  <li class="nav-item">
                    <a href="{{ route('controlledProduction') }}" class="nav-link">
                      <i class="far fa-circle nav-icon"></i>
                      <p>Por Macolla</p>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a href="{{ route('controlledByWell') }}" class="nav-link">
                      <i class="far fa-circle nav-icon"></i>
                      <p>Por Pozo</i></p>
                    </a>
                  </li>
                </ul>
              </li>
              
              <li class="nav-item has-treeview">
                <a href="javascript:void(0);" class="nav-link">
                  <i class="nav-icon fas fa-temperature-high"></i>
                  <p>
                    Cond. de Subsuelo
                    <i class="fas fa-angle-left right"></i>
                  </p>
                </a>
                <ul class="nav nav-treeview">
                  <li class="nav-item">
                    <a href="{{ route('pressureAndTemperature') }}" class="nav-link">
                      <i class="far fa-circle nav-icon"></i>
                      <p>PyT Original</p>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a href="{{ route('historical') }}" class="nav-link">
                      <i class="far fa-circle nav-icon"></i>
                      <p>Hist. de completación</p>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a href="{{ route('staticPressure') }}" class="nav-link">
                      <i class="far fa-circle nav-icon"></i>
                      <p>Hist. Presión Estática</p>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a href="{{ route('dynamicPressure') }}" class="nav-link">
                      <i class="far fa-circle nav-icon"></i>
                      <p>Hist. Presión Dinámica</p>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a href="{{ route('originalPressure') }}" class="nav-link">
                      <i class="far fa-circle nav-icon"></i>
                      <p>Presión Original PMVM</p>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a href="{{ route('realSurvey') }}" class="nav-link">
                      <i class="far fa-circle nav-icon"></i>
                      <p>Survey Real</p>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a href="{{ route('planSurvey') }}" class="nav-link">
                      <i class="far fa-circle nav-icon"></i>
                      <p>Survey Plan</p>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a href="{{ route('navigation') }}" class="nav-link">
                      <i class="far fa-circle nav-icon"></i>
                      <p>Navegación por Pozo</p>
                    </a>
                  </li>
                </ul>
              </li>
              
              <li class="nav-item">
                <a href="{{ route('wellEvents') }}" class="nav-link">
                  <i class="nav-icon fas fa-cubes"></i>
                  <p>
                    Eventos de pozo
                  </p>
                </a>
              </li>

              <li class="nav-item">
                <a href="{{ route('pump') }}" class="nav-link">
                  <i class="nav-icon fa fa-cog"></i>
                  <p>
                    Bomba
                  </p>
                </a>
              </li>

              @if (Auth::user()->role == "ADMIN" || Auth::user()->role == "OPERATOR")
              <li class="nav-item has-treeview">
                <a href="javascript:void(0);" class="nav-link">
                  <i class="nav-icon fas fa-upload"></i>
                  <p>
                    Cargar datos
                    <i class="fas fa-angle-left right"></i>
                  </p>
                </a>
                <ul class="nav nav-treeview">
                  <li class="nav-item">
                    <a href="{{ route('uploadProduction') }}" class="nav-link">
                      <i class="far fa-circle nav-icon"></i>
                      <p>Pruebas de Producción</p>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a href="{{ route('uploadSubsoilCondition') }}" class="nav-link">
                      <i class="far fa-circle nav-icon"></i>
                      <p>Cond. de Subsuelo</p>
                    </a>
                  </li><li class="nav-item">
                    <a href="{{ route('uploadControlledProduction') }}" class="nav-link">
                      <i class="far fa-circle nav-icon"></i>
                      <p>Producción Fiscalizada</p>
                    </a>
                  </li>
                  </li><li class="nav-item">
                    <a href="{{ route('uploadEvents') }}" class="nav-link">
                      <i class="far fa-circle nav-icon"></i>
                      <p>Eventos de pozo</p>
                    </a>
                  </li>
                </ul>
              </li>
              
              <li class="nav-item">
                <a href="{{ route('managementGlobal') }}" class="nav-link" title="Opción para modificar y/o eliminar datos registrados">
                  <i class="nav-icon fas fa-edit"></i>
                  <p>
                    Gestionar datos
                  </p>
                </a>
              </li>
              @endif
              @if (Auth::user()->role == "ADMIN")
              <li class="nav-item">
                <a href="{{ route('optionAdminChooser') }}" class="nav-link">
                  <i class="nav-icon fas fa-tools"></i>
                  <p>
                    Adm. Información
                  </p>
                </a>
              </li>
              <li class="nav-item has-treeview">
                <a href="" class="nav-link">
                  <i class="nav-icon fas fa-user"></i>
                  <p>
                    Usuarios
                    <i class="right fas fa-angle-left"></i>
                  </p>
                </a>
                <ul class="nav nav-treeview">
                  <li class="nav-item">
                    <a href="{{ route('create') }}" class="nav-link">
                      <i class="far fa-circle nav-icon"></i>
                      <p>Agregar nuevo</p>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a href="{{ route('management') }}" class="nav-link">
                      <i class="far fa-circle nav-icon"></i>
                      <p>Gestión de usuarios</p>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a href="{{ route('movements') }}" class="nav-link">
                      <i class="far fa-circle nav-icon"></i>
                      <p>Movimientos</p>
                    </a>
                  </li>
                </ul>
              </li>
              @endif
              
              <li class="nav-item">
                <a href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout').submit();" class="nav-link">
                  <i class="nav-icon fas fa-key"></i>
                  <p>
                    Cerrar sesión
                  </p>
                </a>
              </li>
              
              <!-- Log out-->
              <form id="logout" action="{{ route('logout') }}" method="POST" style="display: none;">
                @csrf
              </form>

            </ul>
          </nav>
        </div>
      </aside>
      
      <div class="content-wrapper">
        @yield('content-header')
        <div class="content">
          <div class="content-fluid">
            @yield('content')
          </div>
        </div>
      </div>
      
      <footer class="main-footer" id="imhere" tabindex="-1">
        <small>SISPRO <b>versión 1.7.0</b></small>
      </footer>

    </div>
    
    @section('script')
    <script src="{{ asset('plugins/jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('plugins/jquery-ui/jquery-ui.min.js') }}"></script>
    <script>
      $.widget.bridge('uibutton', $.ui.button)
    </script>
    <script src="{{ asset('plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js') }}"></script>
    <script async src="{{ asset('plugins/alertifyjs/alertify.min.js') }}"></script>
    <script async src="{{ asset('plugins/moment/moment.min.js') }}"></script>
    <script async src="{{ asset('plugins/cryptojs/components/core-min.js') }}"></script>
    <script async src="{{ asset('plugins/cryptojs/rollups/sha1.js') }}"></script>
    <script async src="{{ asset('plugins/cryptojs/rollups/aes.js') }}"></script>
    <script src="{{ asset('dist/js/adminlte.js') }}"></script>

    @yield('extra-script')
    @component('components.globalScripts') @endcomponent
    @show
  </body>
</html>
