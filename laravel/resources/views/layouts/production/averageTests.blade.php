@extends('layouts.app')

@section('title')
<title>SISPRO | Pruebas Promedio</title>
@endsection

@section('extra-css')
<!-- Bootstrap4 Duallistbox -->
<link rel="stylesheet" href="{{ asset('plugins/bootstrap-duallistbox/bootstrap-duallistbox.min.css') }}">
<!-- Custom -->
<link rel="stylesheet" href="{{ asset('dist/css/multiselect.css') }}">
<link rel="stylesheet" href="{{ asset('dist/css/loading.css') }}">
@endsection

@section('content-header')
<div class="content-header">
	<div class="container-fluid">
		<div class="row mb-2">
			<div class="col-sm-6">
				<h3 class="m-0 text-dark">Pruebas Promedio</h3>
			</div>
			<div class="col-sm-6">
				<ol class="breadcrumb float-sm-right">
					<li class="breadcrumb-item"><a href="{{ route('home') }}">Inicio</a></li>
					<li class="breadcrumb-item active">Pruebas Promedio</li>
				</ol>
			</div>
		</div>
	</div> 
</div>
@endsection

@section('content')
<div class="card card-secondary">
	<div class="card-body">
		<label>Filtrar por:</label>
		<div class="row">
			<div class="form-group col-md-6">
				<small class="text-muted">Elija una opción</small>
				<div class="form-group">
					<select id="macolla" name="macolla" class="form-control" onkeydown="event.preventDefault();" onchange="m.getOptionSelected(this.value)">
						<optgroup label="Seleccionar macolla">
							<option value="">--</option>
							@php $all = '' @endphp
							@foreach($macolla as $key)
							@php $all .= $key->id.',' @endphp
							<option value="{{ $key->id }}|{{ $key->macolla_name }}">{{ $key->macolla_name }}</option>
							@endforeach
							<option value="{{ $value->id }}|{{ $value->macolla_name }}">S/M</option>
							<option value="all">BLOQUE JUNÍN 6</option>
						</optgroup>
					</select>
				</div>
			</div>
			<div class="form-group col-md-6 multiselect" id="reservoirs" style="display:none">
				<small class="text-muted">Yacimientos</small>
				<div class="selectBox" onclick="showCheckboxes()">
					<select id="reservoir" class="form-control">
						<option value="">Seleccionar opción</option>
					</select>
					<div class="overSelect"></div>
				</div>
				<div id="checkboxes">
					@foreach($reservoirs as $key)
					<label for="elementWhatFor{{ $key->id }}">
						<input type="checkbox" name="option[]" id="elementWhatFor{{ $key->id }}" value="{{ $key->id }}" checked/> {{ $key->reservoir_name }}
					</label>
					@endforeach
				</div>
			</div>
		</div>

		<div id="dual-list" style="display:none"></div>
		<div id="dual-list-2" style="display:none"></div>

		<div class="row">
			<div class="form-group col-md-6">
				<label>Fecha</label>
				<select class="form-control" onchange="m.setFilterDateImproved(this.value)" id="filter-date">
					<option value="1">Predeterminada</option>
					<option value="2">Intervalo</option>
				</select>
			</div>
			<div class="form-group col-md-6" id="filter-predetermined"></div>
			<div class="form-group col-md-6" id="filter-customized" style="display:none"></div>
		</div>

		<hr/>

		<label>Seleccione ítem:</label>
		<div class="row">
			<div class="form-group col-md-3">
				<div class="form-check">
					<label class="form-check-label">
						<input class="form-check-input" type="checkbox" id="item" name="item[]" value="line_pressure" checked>Presión de línea
					</label>
				</div>
				<div class="form-check">
					<label class="form-check-label">
						<input class="form-check-input" type="checkbox" id="item" name="item[]" value="line_temperature" checked>Temperatura de línea
					</label>
				</div>
				<div class="form-check">
					<label class="form-check-label">
						<input class="form-check-input" type="checkbox" id="item" name="item[]" value="gas_rate_prod" checked>Tasa de gas
					</label>
				</div>
				<div class="form-check">
      				<label class="form-check-label">
      					<input class="form-check-input" type="checkbox" id="item" name="item[]" value="water_rate_prod" checked>Tasa de agua
      				</label>
      			</div>
      			
			</div>
			
			<div class="form-group col-md-3">
				<div class="form-check">
      				<label class="form-check-label">
      					<input class="form-check-input" type="checkbox" id="item" name="item[]" value="neat_rate_prod" checked>Tasa neta
      				</label>
      			</div>
				<div class="form-check">
      				<label class="form-check-label">
      					<input class="form-check-input" type="checkbox" id="item" name="item[]" value="fluid_total_rate" checked>Tasa de fluido total
      				</label>
      			</div>
				<div class="form-check">
					<label class="form-check-label">
      					<input class="form-check-input" type="checkbox" id="item" name="item[]" value="inyected_diluent_rate" checked>Tasa diluente inyectado
      				</label>
      			</div>
				<div class="form-check">
      				<label class="form-check-label">
      					<input class="form-check-input" type="checkbox" id="item" name="item[]" value="api_degree_diluent" checked>ºAPI Diluente
      				</label>
      			</div>
      			
			</div>
			
			<div class="form-group col-md-3">
				<div class="form-check">
      				<label class="form-check-label">
      					<input class="form-check-input" type="checkbox" id="item" name="item[]" value="percent_formation" checked>%&nbsp;AyS Formación
      				</label>
      			</div>
				<div class="form-check">
      				<label class="form-check-label">
      					<input class="form-check-input" type="checkbox" id="item" name="item[]" value="percent_mixture" checked>%&nbsp;AyS Mezcla
      				</label>
      			</div>
				<div class="form-check">
      				<label class="form-check-label">
      					<input class="form-check-input" type="checkbox" id="item" name="item[]" value="api_degree_mixture" checked>ºAPI Mezcla
      				</label>
      			</div>
      			<div class="form-check">
      				<label class="form-check-label">
      					<input class="form-check-input" type="checkbox" id="item" name="item[]" value="rpm" checked>RPM
      				</label>
      			</div>
			</div>

			<div class="form-group col-md-3">
				<div class="form-check">
      				<label class="form-check-label">
      					<input class="form-check-input" type="checkbox" id="item" name="item[]" value="gvf" checked>Fracción volumen gas
      				</label>
      			</div>
				<div class="form-check">
      				<label class="form-check-label">
      					<input class="form-check-input" type="checkbox" id="item" name="item[]" value="pip" checked>PIP
      				</label>
      			</div>
      			<div class="form-check">
      				<label class="form-check-label">
      					<input class="form-check-input" type="checkbox" id="item" name="item[]" value="observations" checked>Observaciones
      				</label>
      			</div>
			</div>
		</div>
		
		<div class="row">
			<div class="form-group col-md-12">
				<div class="form-check">
					<label class="form-check-label text-muted">
      					<input class="form-check-input" type="checkbox" value="" onclick="selectCheckbox(this)" checked>Marcar/Desmarcar todos
					</label>
				</div>
			</div>
		</div>
		<!-- note -->
		<center id="loading"></center>
	</div>
	
	<div class="card-footer text-center">
		<button class="btn btn-info" onclick="return validate(avg)" id="btn-consult">Consultar</button>
	</div>
	
</div>

<!-- table data -->
<div id="table-content"></div>

<br>

@section('extra-script')
<!--moment-->
<script src="{{ asset('plugins/moment/moment.min.js') }}"></script>
<!-- export data -->
<script src="{{ asset('plugins/jquery-datatable/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('plugins/jquery-datatable/select.js') }}"></script>
<script src="{{ asset('plugins/jquery-datatable/dataTables.scroller.min.js') }}"></script>
<link rel="stylesheet" href="{{ asset('plugins/jquery-datatable/jquery.dataTables.min.css') }}" />
<script src="{{ asset('plugins/jquery-datatable/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('plugins/jquery-datatable/buttons.bootstrap4.min.js') }}"></script>
<script src="{{ asset('plugins/jquery-datatable/buttons.html5.min.js') }}"></script>
<script src="{{ asset('plugins/jquery-datatable/jszip.min.js') }}"></script>
<!-- Bootstrap4 Duallistbox -->
<script src="{{ asset('plugins/bootstrap-duallistbox/jquery.bootstrap-duallistbox.min.js') }}"></script>
<!-- custom scripts -->
<script src="{{ asset('js/myScripts.js') }}"></script>
<script src="{{ asset('js/Method.js') }}"></script>
<script src="{{ asset('js/AverageTest.js') }}"></script>
@endsection

@endsection