<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Exports\ProductionsExport;
use Maatwebsite\Excel\Facades\Excel;

Route::middleware(['auth'])->group(function () {
		
		//Home
		Route::get('/', 'HomeController@index')->name('home');
		
		//Predetermined information
		
		/*Macollas*/
		Route::get('/macollaManagement', 'MacollaController@index')->name('macollaManagement');
		Route::post('/macollaManagement/preAddMacolla', 'MacollaController@show')->name('preAddMacolla');
		Route::post('/macollaManagement/addMacolla', 'MacollaController@store')->name('addMacolla');
		Route::post('/macollaManagement/refreshMacollas', 'MacollaController@refresh')->name('refreshMacollas');
		Route::post('/macollaManagement/updateMacolla/{id}', 'MacollaController@update')->name('updateMacolla');
		Route::post('/macollaManagement/deleteMacolla/{id}', 'MacollaController@destroy')->name('deleteMacolla');
		
		/*Yacimientos*/
		Route::get('/reservoirManagement', 'ReservoirController@index')->name('reservoirManagement');
		Route::post('/reservoirManagement/preAddReservoir', 'ReservoirController@show')->name('preAddReservoir');
		Route::post('/reservoirManagement/addReservoir', 'ReservoirController@store')->name('addReservoir');
		Route::post('/reservoirManagement/refreshReservoirs', 'ReservoirController@refresh')->name('refreshReservoirs');
		Route::post('/reservoirManagement/updateReservoir/{id}', 'ReservoirController@update')->name('updateReservoir');
		Route::post('/reservoirManagement/deleteReservoir/{id}', 'ReservoirController@destroy')->name('deleteReservoir');
		Route::post('/reservoirManagement/loadReservoirs', 'ReservoirController@load')->name('loadReservoirs');
		
		/*Pozos*/
		Route::get('/wellManagement', 'WellController@index')->name('wellManagement');
		Route::post('/wellManagement/setSelected', 'WellController@setSelected')->name('setSelected');
		Route::post('/wellManagement/addWell', 'WellController@store')->name('addWell');
		Route::post('/wellManagement/loadMacollas', 'WellController@load')->name('loadMacollas');
		Route::post('/wellManagement/searchWells', 'WellController@search')->name('searchWells');
		Route::post('/wellManagement/updateWell/{id}', 'WellController@update')->name('updateWell');
		Route::post('/wellManagement/deleteWell/{id}', 'WellController@destroy')->name('deleteWell');

		/*Localización*/
		Route::post('/locationManagement/updateLocation/{id}', 'LocationController@update')->name('updateLocation');
		
		// Consults
		Route::get('/extendedTests', 'ProductionController@index')->name('extendedTests');
		Route::get('/averageTests', 'ProductionController@index')->name('averageTests');
		Route::get('/signs', 'ProductionController@index')->name('signs');
		Route::get('/historical', 'ProductionController@index')->name('historical');
		Route::get('/pressureAndTemperature', 'ProductionController@index')->name('pressureAndTemperature');
		Route::get('/originalPressure', 'ProductionController@index')->name('originalPressure');
		Route::get('/staticPressure','ProductionController@index')->name('staticPressure');
		Route::get('/dynamicPressure','ProductionController@index')->name('dynamicPressure');
		Route::get('/realSurvey', 'ProductionController@index')->name('realSurvey');
		Route::get('/planSurvey', 'ProductionController@index')->name('planSurvey');
		Route::get('/controlledProduction', 'ProductionController@index')->name('controlledProduction');
		
		//Cargar macollas y localizaciones
		Route::post('/loadMacollas', 'MacollaController@load');
		Route::post('/loadLocations', 'LocationController@load');
		
		//Recuperar información
		/* Production */
		Route::post('/extendedTests/searchByFilterExtended', 'FilterController@searchByFilterExtended')->name('searchByFilterExtended');
		Route::post('/averageTests/searchByFilterAverage', 'FilterController@searchByFilterAverage')->name('searchByFilterAverage');
		Route::post('/signs/searchByFilterSign', 'FilterController@searchByFilterSign')->name('searchByFilterSign');
		
		/* Subsoil conditions */
		Route::post('/pressureAndTemperature/searchByFilterPressAndTemp', 'FilterController@searchByFilterPressAndTemp')->name('searchByFilterPressAndTemp');
		Route::post('/historical/searchByFilterHistorical', 'FilterController@searchByFilterHistorical')->name('searchByFilterHistorical');
		Route::post('/staticPressure/searchByFilterStatic', 'FilterController@searchByFilterStatic')->name('searchByFilterStatic');
		Route::post('/dynamicPressure/searchByFilterDynamic', 'FilterController@searchByFilterDynamic')->name('searchByFilterDynamic');
		Route::post('/originalPressure/searchByFilterOriginal', 'FilterController@searchByFilterOriginal')->name('searchByFilterOriginal');
		Route::post('/realSurvey/searchByFilterSurvey', 'FilterController@searchByFilterSurvey')->name('searchByFilterSurvey');
		Route::post('/planSurvey/searchByFilterPlanSurvey', 'FilterController@searchByFilterPlanSurvey')->name('searchByFilterPlanSurvey');
		
		/* Controlled production */
		Route::post('/controlledProduction/searchByFilterProduction', 'FilterController@searchByFilterProduction')->name('searchByFilterProduction');

		/* Events */
		Route::post('/wellEvents/searchByFilterEvents', 'FilterController@searchByFilterEvents')->name('searchByFilterEvents');
		
		//Vista para cargar datos
		Route::get('/uploadProduction', 'UploadController@indexProduction')->name('uploadProduction');
		Route::get('/uploadSubsoilCondition', 'UploadController@indexCondition')->name('uploadSubsoilCondition');
		Route::get('/uploadControlledProduction', 'UploadController@indexControlledProduction')->name('uploadControlledProduction');
		Route::get('/uploadEvents', 'UploadController@indexEvents')->name('uploadEvents');
		
		//Importar el archivo Excel
		/* Production */
		Route::post('/extendedImport', 'UploadController@uploadExtended');
		Route::post('/averageImport', 'UploadController@uploadAverage');
		Route::post('/signImport', 'UploadController@uploadSign');
		
		/* Pressure and Temperature */
		Route::post('/conditionImport', 'UploadController@uploadCondition');
		Route::post('/historicalImport', 'UploadController@uploadHistorical');
		Route::post('/staticPressureImport', 'UploadController@uploadStaticPressure');
		Route::post('/dynamicPressureImport', 'UploadController@uploadDynamicPressure');
		Route::post('/originalPressureImport', 'UploadController@uploadOriginalPressure');
		Route::post('/realSurveyImport', 'UploadController@uploadRealSurvey');
		Route::post('/planSurveyImport', 'UploadController@uploadPlanSurvey');
		
		/* Controlled Production */
		Route::post('/fiscalizedImport', 'UploadController@uploadFiscalized');
		
		/* Events */
		Route::post('/eventsImport', 'UploadController@uploadEvents');

		//Guardar la información del archivo Excel
		/* Production */
		Route::post('/saveExtended', 'UploadController@saveExtended');
		Route::post('/saveAverage', 'UploadController@saveAverage');
		Route::post('/saveSign', 'UploadController@saveSign');
		
		/* Pressure and Temperature */
		Route::post('/saveCondition', 'UploadController@saveCondition');
		Route::post('/saveHistorical', 'UploadController@saveHistorical');
		Route::post('/saveStaticPressure', 'UploadController@saveStaticPressure');
		Route::post('/saveDynamicPressure', 'UploadController@saveDynamicPressure');
		Route::post('/saveOriginalPressure', 'UploadController@saveOriginalPressure');
		Route::post('/saveRealSurvey', 'UploadController@saveRealSurvey');
		Route::post('/savePlanSurvey', 'UploadController@savePlanSurvey');
		
		/* Fiscalized */
		Route::post('/saveFiscalized', 'UploadController@saveFiscalized');

		/* Events */
		Route::post('/saveEvents', 'UploadController@saveEvents');

		/** events */
		Route::get('/wellEvents', 'ProductionController@index')->name('wellEvents');

		/* Users */
		Route::get('/users', 'HomeController@createUser')->name('create');
		Route::post('/users/create', 'UserController@create');
		Route::get('/management', 'HomeController@managementUser')->name('management');
		Route::post('/edit/{id}', 'ProfileController@update');
		Route::post('/delete/{id}', 'ProfileController@destroy');
		Route::post('/loadUsers', 'UserController@load');

		/* Logs */
		Route::get('/movements', 'HomeController@movement')->name('movements');
		Route::post('/movements/consult', 'LogController@show');

		/** adding manually */
		Route::post('/saveSingleExtended', 'ProductionController@saveSingleExtended');
		Route::post('/saveSingleAverage', 'ProductionController@saveSingleAverage');
		Route::post('/saveSingleSign', 'ProductionController@saveSingleSign');
		Route::post('/saveSingleCondition', 'ProductionController@saveSingleCondition');
		Route::post('/saveSingleHistorical', 'ProductionController@saveSingleHistorical');
		Route::post('/saveSingleStatic', 'ProductionController@saveSingleStatic');
		Route::post('/saveSingleDynamic', 'ProductionController@saveSingleDynamic');
		Route::post('/saveSingleOriginal', 'ProductionController@saveSingleOriginal');
		Route::post('/saveSingleReal', 'ProductionController@saveSingleReal');
		Route::post('/saveSinglePlan', 'ProductionController@saveSinglePlan');
		Route::post('/saveSingleFiscalized', 'ProductionController@saveSingleFiscalized');
		Route::post('/saveSingleEvent', 'ProductionController@saveSingleEvent');

		/** Gestión de información */
		Route::get('/managementGlobal', 'ProductionController@index')->name('managementGlobal');
		Route::get('/managementExtended', 'ProductionController@index')->name('managementExtended');
		Route::get('/managementAverage', 'ProductionController@index')->name('managementAverage');
		Route::get('/managementSign', 'ProductionController@index')->name('managementSign');
		Route::get('/managementCondition', 'ProductionController@index')->name('managementCondition');
		Route::get('/managementStatic', 'ProductionController@index')->name('managementStatic');
		Route::get('/managementDynamic', 'ProductionController@index')->name('managementDynamic');
		Route::get('/managementHistorical', 'ProductionController@index')->name('managementHistorical');
		Route::get('/managementOriginal', 'ProductionController@index')->name('managementOriginal');
		Route::get('/managementReal', 'ProductionController@index')->name('managementReal');
		Route::get('/managementPlan', 'ProductionController@index')->name('managementPlan');
		Route::get('/managementWellEvents', 'ProductionController@index')->name('managementWellEvents');
		Route::get('/managementFiscalized', 'ProductionController@index')->name('managementFiscalized');

		/** modules **/
		Route::post('/searchExtended', 'ProcessingController@searchExtended');
		Route::post('/updateExtended/{id}', 'ManagementController@updateExtended');
		Route::post('/deleteExtended/{id}', 'ManagementController@deleteExtended');

		Route::post('/searchAverage', 'ProcessingController@searchAverage');
		Route::post('/updateAverage/{id}', 'ManagementController@updateAverage');
		Route::post('/deleteAverage/{id}', 'ManagementController@deleteAverage');

		Route::post('/searchSign', 'ProcessingController@searchSign');
		Route::post('/updateSign/{id}', 'ManagementController@updateSign');
		Route::post('/deleteSign/{id}', 'ManagementController@deleteSign');

		Route::post('/searchCondition', 'ProcessingController@searchCondition');
		Route::post('/updateCondition/{id}', 'ManagementController@updateCondition');
		Route::post('/deleteCondition/{id}', 'ManagementController@deleteCondition');

		Route::post('/searchHistorical', 'ProcessingController@searchHistorical');
		Route::post('/updateHistorical/{id}', 'ManagementController@updateHistorical');
		Route::post('/deleteHistorical/{id}', 'ManagementController@deleteHistorical');

		Route::post('/searchStatic', 'ProcessingController@searchStatic');
		Route::post('/updateStatic/{id}', 'ManagementController@updateStatic');
		Route::post('/deleteStatic/{id}', 'ManagementController@deleteStatic');

		Route::post('/searchDynamic', 'ProcessingController@searchDynamic');
		Route::post('/updateDynamic/{id}', 'ManagementController@updateDynamic');
		Route::post('/deleteDynamic/{id}', 'ManagementController@deleteDynamic');

		Route::post('/searchOriginal', 'ProcessingController@searchOriginal');
		Route::post('/updateOriginal/{id}', 'ManagementController@updateOriginal');
		Route::post('/deleteOriginal/{id}', 'ManagementController@deleteOriginal');

		Route::post('/searchReal', 'ProcessingController@searchReal');
		Route::post('/updateReal/{id}', 'ManagementController@updateReal');
		Route::post('/deleteReal/{id}', 'ManagementController@deleteReal');

		Route::post('/searchPlan', 'ProcessingController@searchPlan');
		Route::post('/updatePlan/{id}', 'ManagementController@updatePlan');
		Route::post('/deletePlan/{id}', 'ManagementController@deletePlan');

		Route::post('/searchEvents', 'ProcessingController@searchEvents');
		Route::post('/updateEvents/{id}', 'ManagementController@updateEvents');
		Route::post('/deleteEvents/{id}', 'ManagementController@deleteEvents');

		Route::post('/searchFiscalized', 'ProcessingController@searchFiscalized');
		Route::post('/updateFiscalized/{id}', 'ManagementController@updateFiscalized');
		Route::post('/deleteFiscalized/{id}', 'ManagementController@deleteFiscalized');

		Route::get('/optionAdminChooser', 'ProductionController@index')->name('optionAdminChooser');

		Route::post('/loadHoles/{id}', 'FilterController@set_holes');

		# nuevo modulo
		Route::get('/pump', 'ProductionController@index')->name('pump');
		Route::post('/getRunData', 'FilterController@getRunData');

		/* prueba produccion diaria por pozo */
		Route::get('/controlledByWell', 'ProductionController@index')->name('controlledByWell');
		Route::post('/controlledByWell/searchByFilterControlledByWell', 'FilterController@searchByFilterControlledByWell');

		/* navegacion por pozo */
		Route::get('/navigation', 'ProductionController@index')->name('navigation');
		Route::post('/navigation/searchByFilterNavigation', 'FilterController@searchByFilterNavigation');
});
Auth::routes();