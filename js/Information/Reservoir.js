class Reservoir {
	constructor() {
		this.adding = this.adding.bind(this);
		this.setTypeApi = this.setTypeApi.bind(this);
		this.modify = this.modify.bind(this);
		this.updating = this.updating.bind(this);
		this.reloadView = this.reloadView.bind(this);
		this.showInputErrors = this.showInputErrors.bind(this);
		this.reservoirName = ['reservoir_name'];
		this.inputNames = ['reservoir_name','api_degree','code'];
		this.inputEditName = ['update_reservoir_name','update_code','update_api_degree',];
		this.genToPut = ``;
	}
	
	reloadView() {
		document.querySelectorAll('.text-danger').forEach(function(a){
			a.remove()
		});
	}

	showInputErrors(errors, inputNames) {
		for(const errorName in errors) {
			inputNames.map(inputName => {
				if (errorName === inputName) {
					document.getElementById(`${inputName}`).className = "form-control is-invalid";
					document.querySelector(`#wrapper-${inputName}`).innerHTML += `<small class="text-danger">${errors[errorName]}</small>`;
					document.querySelector(`#${inputName}`).focus();
				}
			});
		}
	}
	
	showInputCorrects(errors, inputNames) {
		for(const errorName in errors) {
			inputNames.map(inputName => {
				if (errorName !== inputName) {
					document.getElementById(`${inputName}`).className = "form-control is-valid";      		
				}
			});
		}
	}
	
	getSelectedOption() {

		var option = $(`#options`).val();
		
		if (option == 1 || option == 2) {
			if (option == 1) {
				this.addAndRemoveClass('main-options', 'card-secondary', 'card-default');
				$(`#main-options`).html(this.setHtmlSources(1));
			} else {
				this.addAndRemoveClass('main-options', 'card-secondary', 'card-default');
				$(`#main-options`).html(this.setHtmlSources(2));
				
				const ajax = new Ajax(`${globalConfig.appUrl}/reservoirManagement/loadReservoirs`, {
					headers: {
						accept: 'application/json',
					},
					method: 'POST'
				});
				
				ajax.result().then(response => {
					
					this.genToPut = this.randomString(240);
					var size = Object.keys(response.list).length, datatable = ``, dataset = [], data = [], mk = CryptoJS.SHA1(this.genToPut).toString(), encrypted;
					
					for (var i = 0; i < size; i++) {
						
						dataset.push({
							reservoir_id: response.list[i].id,
							reservoir_name: response.list[i].reservoir_name,
							code: response.list[i].code,
							api: response.list[i].api_degree,
							hydrocarbon: response.list[i].hydrocarbon,
							index: (i+1)
						});

						encrypted = CryptoJS.AES.encrypt(JSON.stringify(dataset), mk);

						data.push([
							(i+1),
							response.list[i].code,
							response.list[i].reservoir_name,
							this.toLocale(response.list[i].api_degree),
							response.list[i].hydrocarbon,
							`<div class="btn-group btn-group-sm">
								<button class="btn btn-info" id="btn-edit" data-toggle="modal" data-target="#modal-xl" value='${encrypted}' title="Editar"><i class="fa fa-edit"></i></button>
								<button class="btn btn-danger" id="btn-delete" value="${response.list[i].id}"><i class="fa fa-trash" title="Borrar"></i></button>
							</div>`
						]);
						
						dataset = [];
					}
				
					var table = $("#table-list").DataTable({
						dom: 'frptip',
						processing: true,
						draw: data.draw,
						data: data,
						deferRender: true,
						recordsTotal: size,
						recordsFiltered: size,
						searching: true,
						info: true,
						ordering: false,
						scrollY: 400,
						scrollX: true,
						scrollColapse: true,
						paging: false,
						language: {
							info: "Mostrando _START_ a _END_ de _TOTAL_ resultados"
						}
					});

					$('.dataTables_filter').hide();
					$('#table-search').keyup(function(){table.search($(this).val()).draw();});

					table.on('click', '#btn-delete', function() {
						let $tr = $(this).closest('tr'), $button = $(this), id = $(this).val();
						alertify.confirm("<i class='fa fa-exclamation-triangle text-danger'></i> Advertencia", "La eliminación de este yacimiento causará la pérdida de la información asociada al registro en otros módulos del sistema. ¿Está seguro que desea continuar?", function() {},
						function(e) {
							if (e) {
								$button.attr('disabled', true);
								const ajax = new Ajax(`${globalConfig.appUrl}/reservoirManagement/deleteReservoir/${id}`, {
									headers: { accept: 'application/json' },
									method: 'POST'
								});
								ajax.result().then(response => {table.row($tr).remove().draw(false);alertify.warning("El yacimiento ha sido eliminado");
								}).catch(error => { console.log(error); });
							}
						}).set({'movable': false, 'moveBounded': false, 'modal': false, 'labels': {ok: 'No', cancel: 'Sí'}});
					});

					table.on('click', '#btn-edit', function() {
						var $tr = $(this).closest('tr'), $value = $(this).val();
						myReservoir.modify($value, table, $tr);
					});

				}).catch(error => { console.log(error) });
			}
			$(`#alert`).css('display','none');
		}
		return false;
	}
	
	adding(event) {

		this.reloadView();
		
		var aux = $('#api_degree').val(), api = this.setSeparator(aux);
		
		const ajax = new Ajax(`${globalConfig.appUrl}/reservoirManagement/addReservoir`, {
			headers: {
				accept: 'application/json',
			},
			method: 'POST',
			body: {
				reservoir_name: $('#reservoir_name').val().toUpperCase(),
				code: $('#code').val().toUpperCase(),
				api_degree: api,
				hydrocarbon: $('#hydrocarbon').val()
			}
		});
		
		$("#btn-add").attr("disabled", true);
		
		ajax.result().then(response => {
			alertify.success('Yacimiento <b>' + $('#reservoir_name').val() + '</b> agregado exitosamente');
			$(`#main-options`).html(this.setHtmlSources(1));
		}).catch(error => {
			if (error.status === 400) {
				this.showInputCorrects(error.errors, this.inputNames);
				this.showInputErrors(error.errors, this.inputNames);
				$("#btn-add").attr("disabled", false);
			} else console.log(error);
		});
		return false;
	}
	
	preAdding(event) {

		this.reloadView();
		event.preventDefault();
		var valueReservoir = $('#reservoir_name').val().trim().replace("\\s+", " ");
		
		const ajax = new Ajax(`${globalConfig.appUrl}/reservoirManagement/preAddReservoir`, {
			headers: {
				accept: 'application/json',
			},
			method: 'POST',
			body: {
				reservoir_name: valueReservoir
			}
		});
		
		$("#btn-continue").attr("disabled", true);
		
		ajax.result().then(response => {
			
			var id = 'api_degree', input = 'hydrocarbon', type = 'type';

			$(`#reservoir`).html(`<h6>Yacimiento</h6>
			<input type="text" id="reservoir_name" name="reservoir_name" style="text-transform:uppercase" class="form-control" value="${response.reservoir}" disabled>
			<div id="wrapper-reservoir_name"></div><br>`);
			
			$(`#set_code`).html(`<h6>Código</h6>
			<input type="text" name="code" id="code" class="form-control" maxlength="10" style="text-transform:uppercase" placeholder="Código">
			<div id="wrapper-code"></div><br>`);
			
			$(`#api`).html(`<h6>ºAPI</h6>
			<input type="text" name="api_degree" id="api_degree" class="form-control" placeholder="ºAPI" maxlength="4" oninput="myReservoir.setTypeApi('${id}', '${input}', '${type}')">
			<div id="wrapper-api_degree"></div>`);
			
			$(`#buttons`).html(`<button class="btn btn-danger" id="btn-cancel">Cancelar</button>
			<button class="btn btn-info float-right" id="btn-add">Agregar</button>`);
			
			$('#api_degree').numeric({ negative: false });

			this.optionPane('btn-add', 'Importante', '¿Seguro que desea agregar este yacimiento?', 1);
			this.optionPane('btn-cancel', 'Aviso', '¿Está seguro que desea cancelar este proceso?', 2);

		}).catch(error => {
			if (error.status === 400) {
				this.showInputCorrects(error.errors, this.reservoirName);
				this.showInputErrors(error.errors, this.reservoirName);
				$("#btn-continue").attr("disabled", false);
			} else {
				console.log(error);
			}
		});
		return false;
	}
	
	updating(table, row) {
		
		this.reloadView();

		var aux = $('#update_api_degree').val(), api = this.setSeparator(aux);
		
		const id = $('#identity').val();
		const index = $('#index').val();

		const ajax = new Ajax(`${globalConfig.appUrl}/reservoirManagement/updateReservoir/${id}`, {
			headers: {
				accept: 'application/json',
			},
			method: 'POST',
			body: {
				update_reservoir_name: $('#update_reservoir_name').val().toUpperCase(),
				update_code:  $('#update_code').val().toUpperCase(),
				update_api_degree: api,
				update_hydrocarbon: $('#update_hydrocarbon').val(),
				identity: id
			}
		});
		
		$("#btn-update").attr("disabled", true);
		
		ajax.result().then(response => {
			
			var dataset = [], mk = CryptoJS.SHA1(this.genToPut).toString(), encrypted;

			dataset.push({
				reservoir_id: response.data.id,
				reservoir_name: response.data.reservoir_name,
				code: response.data.code,
				api: response.data.api_degree,
				hydrocarbon: response.data.hydrocarbon,
				index: index
			});
			encrypted = CryptoJS.AES.encrypt(JSON.stringify(dataset), mk);
			alertify.alert('Enhorabuena', 'El yacimiento fue modificado exitosamente.', function(e){
				if(e) {

					var update = [index, response.data.code,response.data.reservoir_name,myReservoir.toLocale(response.data.api_degree),response.data.hydrocarbon,`<div class="btn-group btn-group-sm"><button class="btn btn-info" id="btn-edit" data-toggle="modal" data-target="#modal-xl" value='${encrypted}' title="Editar"><i class="fa fa-edit"></i></button><button class="btn btn-danger" id="btn-delete" value="${response.data.id}"><i class="fa fa-trash" title="Borrar"></i></button></div>`];
                    
                    table.row(table.row(row).data(update).draw());
				}
			}).set({'movable': false, 'moveBounded': false});
			$(`#modal-xl`).modal('toggle');

		}).catch(error => {
			if (error.status === 400) {
				this.showInputCorrects(error.errors, this.inputEditName);
				this.showInputErrors(error.errors, this.inputEditName);
				$("#btn-update").attr("disabled", false);
			} else console.log(error);
		});
		return false;
	}
	
	returnMain() {
		$(`#main-options`).html(`<div class="card-body"><div class="col-md-6"><h6>¿Qué desea hacer?</h6><div class="input-group mb-3"><select class="form-control" id="options" name="options"><option value>-</option><option value=1>Agregar</option><option value=2>Gestionar</option></select><span class="input-group-append"><button class="btn btn-info btn-block" id="btn-search" onclick="myReservoir.getSelectedOption()">OK</button></span></div></div></div>`);
		this.genToPut = ``;
		$(`#alert`).show();
	}
	
	setTypeApi($id, $input, $type) {

		var api = parseFloat($(`#${$id}`).val()), value = "";
		var element = `<h6>Tipo Hidrocarburo</h6><select id="${$input}" name="${$input}" class="form-control"></select>`;
		
		if (api >= 42 && api <= 70.0) value += `<option value="C">Condensado</option>`;
		if (api >= 30 && api <= 42) value += `<option value="L">Liviano</option>`;
		if (api >= 22 && api <= 30) value += `<option value="M">Mediano</option>`;
		if (api >= 10 && api <= 22) value += `<option value="P">Pesado</option>`;
		if (api > 6 && api < 10) value += `<option value="XP">Extrapesado</option>`;
		if (api >= 3 && api < 10) value += `<option value="B">Bitumen</option>`;
		if (api > 6 && api < 10) value += `<option value="A">Asfalto</option>`;
		
		if (value != "") {
			$(`#${$type}`).html(`${element}`);
			$(`#${$input}`).html(`${value}`);
		} else $(`#${$type}`).html(``);
		
		$(`#${$id}`).removeClass();
		$(`#${$id}`).addClass('form-control');

		this.reloadView();
	}
	
	modify(val, table, row)	{

		var decrypt = CryptoJS.AES.decrypt(val, CryptoJS.SHA1(this.genToPut).toString()).toString(CryptoJS.enc.Utf8), data = JSON.parse(decrypt), type = ``, id = 'update_api_degree', input = 'update_hydrocarbon', $type = 'update_type';

		if (data[0].hydrocarbon == 'C') type = 'Condensado';
		else if (data[0].hydrocarbon == 'L') type = 'Liviano';
		else if (data[0].hydrocarbon == 'P') type = 'Pesado';
		else if (data[0].hydrocarbon == 'XP') type = 'Extrapesado';
		else if (data[0].hydrocarbon == 'A') type = 'Asfalto';
		else if (data[0].hydrocarbon == 'M') type = 'Mediano';
		else type = 'Bitumen';

		$(`#modal-xl`).html(`<div class="modal-dialog modal-xl"><div class="modal-content"><div class="modal-header"><h4 class="modal-title">Editar Yacimiento</h4><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button></div><div class="modal-body"><div class="row"><div class="col-md-3 pt-2"><h6>Código</h6><input type="text" placeholder="Código" class="form-control" value="${data[0].code}" id="update_code" maxlength="10" style="text-transform:uppercase" autocomplete="off"/><div id="wrapper-update_code"></div></div><div class="col-md-3 pt-2"><h6>Nombre</h6><input type="text" placeholder="Yacimiento" class="form-control" value="${data[0].reservoir_name}" style="text-transform: uppercase" id="update_reservoir_name" maxlength="12" autocomplete="off"/><div id="wrapper-update_reservoir_name"></div></div><div class="col-md-3 pt-2"><h6>ºAPI</h6><input type="text" placeholder="ºAPI" class="form-control" value="${myReservoir.toLocale(data[0].api)}" id="update_api_degree" oninput="myReservoir.setTypeApi('${id}', '${input}', '${$type}')" maxlength="4" autocomplete="off"/><div id="wrapper-update_api_degree"></div></div><div class="col-md-3 pt-2" id="update_type"><h6>Tipo Hidrocarburo</h6><select id="update_hydrocarbon" name="update_hydrocarbon" class="form-control"><option value=${data[0].hydrocarbon}>${type}</option></select></div><div class="col-md-12 pt-4"></div></div></div><input type="hidden" id="identity" value="${data[0].reservoir_id}"><input type="hidden" id="element" value="${data[0].id}"><input type="hidden" id="index" value="${data[0].index}"><div class="modal-footer justify-content-between"><button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button><button type="button" class="btn btn-primary" id="btn-update">Guardar cambios</button></div></div></div>`);
		
		$('#update_api_degree').numeric({ negative: false });

		this.optionPane('btn-update', 'Pregunta', '¿Está seguro que desea realizar cambios en este yacimiento?', 3, table, row);
	}

	optionPane(btnName, title, dialog, option, table = null, row = null) {
		$(`#${btnName}`).click(function(event) {
			alertify.confirm(title, dialog, function(e) {
				if (e) {
					if (option == 1) myReservoir.adding(event);
					else if (option == 2) {myReservoir.returnMain();}
					else myReservoir.updating(table, row);
				}
			}, function(){}).set({'movable': false, 'moveBounded': false, 'modal': false, 'labels': {ok: 'Sí', cancel: 'No'}});
		});
	}

	addAndRemoveClass(id, remove, add) {
		$(`#${id}`).removeClass(remove);
		$(`#${id}`).addClass(add);
	}

	setHtmlSources(val) {
		if (val == 1) return `<div class="card-header"><h3 class="card-title"><b>Yacimientos</b> / Agregar nuevo</h3></div><div class="card-body"><div class="row" id="addNewReservoir"><div class="col-md-3" id="reservoir"><h6>Yacimiento</h6><input type="text" id="reservoir_name" name="reservoir_name" style="text-transform:uppercase" class="form-control" maxlength="12" placeholder="Yacimiento" autofocus><div id="wrapper-reservoir_name"></div></div><div class="col-md-3" id="set_code"></div><div class="col-md-3" id="api"></div><div class="col-md-3" id="type"></div><div id="form-adding"></div></div></div><div class="card-footer" id="buttons"><button class="btn btn-secondary" onclick="myReservoir.returnMain()">Regresar</button><button class="btn btn-info float-right" onclick="myReservoir.preAdding(event)" id="btn-continue">Continuar</button></div>`;
		else return `<div class="card-header"><h3 class="card-title"><button onclick="myReservoir.returnMain()" class="btn btn-outline-secondary btn-sm">Volver</button> <strong>Junín 6</strong> / Yacimientos</h3><div class="card-tools"><div class="input-group input-group-sm" style="width:150px;position:relative;top:5px"><input type="text" id="table-search" class="form-control float-right" placeholder="Buscar" maxlength="15" autocomplete="off"></div></div></div><div class="card-body" id="reservoir_table"><table class="display" id="table-list" width="100%"><thead><tr><th>#</th><th>Código</th><th>Nombre</th><th>ºAPI</th><th>Hidrocarburo</th><th>Acción</th></tr></thead><tbody id="datatable"></tbody></table></div><div class="modal fade" id="modal-xl" style="display: none;" aria-hidden="true" data-keyboard="false" data-backdrop="static"></div>`;
	}

	setSeparator(val) {
		let aux, api;
		if (val != "") { aux = val.replace(",", "."); api = parseFloat(aux); }
		else api = val;
		return api;
	}

	toLocale(number) {
		return ($.isNumeric(number) && number < 10) ? number.toLocaleString("de-DE", { minimumFractionDigits: 1,maximumFractionDigits: 1}) : parseInt(number);
	}

	randomString(length, chars) {
		length = length || 16;
		chars = chars || "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ@#!?=";
		
		var text = "", max = chars.length-1;
		
		for (var i=1; i<length; i++)  text += chars[Math.floor(Math.random() * (max+1))];
		return text;
	}
}

window.onload = () => {
  myReservoir = new Reservoir();
}