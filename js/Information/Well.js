class Well {
	constructor() {
		this.adding = this.adding.bind(this);
		this.searching = this.searching.bind(this);
		this.setSelected = this.setSelected.bind(this);
		this.modify = this.modify.bind(this);
		this.updating = this.updating.bind(this);
		this.loadReservoirs = this.loadReservoirs.bind(this);
		this.reloadView = this.reloadView.bind(this);
		this.showInputErrors = this.showInputErrors.bind(this);
		this.showInputCorrects = this.showInputCorrects.bind(this);
		this.inputNames = ['macolla_name', 'location_name', 'well_name', 'reservoir_name', 'type', 'classification', 'condition', 'drill', 'gl', 'rt', 'height', 'ppn'];
		this.inputEditNames = ['update_well_name', 'update_location_name', 'update_reservoir_id', 'update_type', 'update_classification', 'update_condition', 'update_drill', 'update_gl', 'update_rt', 'update_height', 'update_ppn'];
		this.genToPut = ``;
	}
	
	reloadView() {
		document.querySelectorAll('.text-danger').forEach(function(a){
			a.remove()
		});
	}
	
	showInputErrors(errors, inputNames) {
		for(const errorName in errors) {
			inputNames.map(inputName => {
				if (errorName === inputName) {
					document.getElementById(`${inputName}`).className = "form-control is-invalid";
					document.querySelector(`#wrapper-${inputName}`).innerHTML += `<small class="text-danger">${errors[errorName]}</small>`;
				}
			});
		}
	}
	
	showInputCorrects(errors, inputNames) {
		for(const errorName in errors) {
			inputNames.map(inputName => {
				if (errorName !== inputName) {
					$(`#${inputName}`).removeClass();
					$(`#${inputName}`).addClass("form-control is-valid");
				}
			});
		}
	}

	getSelectedOption() {
		var option = $(`#options`).val();
		if (option == 1 || option == 2) {
			if (option == 1) {
				this.addAndRemoveClass('main-options', 'card-secondary', 'card-default');
				$(`#main-options`).html(this.setHtmlSources(1));
			} else {
				this.addAndRemoveClass('main-options', 'card-secondary', 'card-default');
				$(`#main-options`).html(this.setHtmlSources(2));
			}
			$(`#alert`).hide();
		}
		return false;
	}
	
	setSelected() {
		this.reloadView();
		$(`#btn-continue`).attr('disabled', true);
		var json = JSON.parse($('#macolla_name').val()), prepend = ``;
		const ajax = new Ajax(`${globalConfig.appUrl}/wellManagement/setSelected`, {
			headers: {
				accept: 'application/json',
			},
			method: 'POST',
			body: {
				macolla_id: json[0].id,
				macolla_name: json[0].name
			}
		});

		ajax.result().then(response => {
			$(`#default-button`).html(``);
			$(`#form`).css('display', 'flex');
			$(`#macolla`).html(`<label>Macolla</label>
			<input type="text" id="macolla_name" name="macolla_name" class="form-control" value="${response.data.name}" disabled="disabled">`);
			
			$(`#location`).html(``);
    		if (response.data.id != 6) prepend = '<span class="input-group-text">' + response.data.name + '</span>';
    		else prepend = '<input class="input-group-text form-control" id="prepend" type="text" style="width:60px;text-align:left;text-transform:uppercase">';
		
			$(`#location`).html(`<label>Localización</label>
			<div class="input-group">
			<div class="input-group-prepend">${prepend}</div>
			<input type="text" class="form-control" id="location_name" name="location_name" placeholder="00" autocomplete="off" maxlength="4"></div>
			<div id="wrapper-location_name"></div>`);
			$(`#location_name`).numeric({negative: false, decimal: false});

			$(`#reservoir`).html(`
			<h6>Yacimiento</h6>
			<select class="form-control" id="reservoir_name" name="reservoir_name"></select>
			<div id="wrapper-reservoir_name"></div>`);
			$(`#reservoir_name`).html(`<option value="">--</option>`);
		
			var option = ``, size = Object.keys(response.array).length, datares = [], numericInputs = ['gl', 'rt', 'height', 'ppn'];
			for(let i = 0; i < size; i++) {
				datares.push({id: response.array[i].id});
				option += `<option value='${JSON.stringify(datares)}'>${response.array[i].reservoir_name}</option>`;
			}
			$(`#reservoir_name`).html(option);
		
			this.setTextfield('well', 'Nombre', 'Nombre del pozo', 8, 'well_name');
			this.setComboBox('set_type', 'Tipo', 'type', ['', 'H', 'V', 'D'], ['--', 'Horizontal', 'Vertical', 'Desviado']);  		
			this.setComboBox('set_classification', 'Clasificación', 'classification', ['', 'PRODUCTOR', 'OBSERVADOR', 'ESTRATIGRÁFICO'], ['--', 'PRODUCTOR', 'OBSERVADOR', 'ESTRATIGRÁFICO']);
			this.setComboBox('set_condition', 'Condición inicial', 'condition', ['', 'ACTIVO', 'EN PERFORACIÓN', 'ABANDONADO', 'NO COMPLETADO'], ['--', 'ACTIVO', 'EN PERFORACIÓN', 'ABANDONADO', 'NO COMPLETADO']);
			this.setTextfield('set_drill', 'Taladro', 'Taladro', 12, 'drill');
			this.setTextfield('set_gl', 'GL <i class="text-muted">pies</i>', 'GL', 6, 'gl');
			this.setTextfield('set_rt', 'RT <i class="text-muted">pies</i>', 'RT', 6, 'rt');
			this.setTextfield('set_height', 'Altura <i class="text-muted">pies</i>', 'Altura', 6, 'height');
			this.setTextfield('set_ppn', 'PPN <i class="text-muted">pies</i>', 'PPN', 6, 'ppn');

			for (var i = 0; i < numericInputs.length; i++) {
				$(`#${numericInputs[i]}`).numeric({ negative: false });
			}
		
			$(`#group-buttons`).html(`<div class="card-footer" id="buttons"><button type="submit" class="btn btn-danger" id="btn-cancel">Cancelar</button><button class="btn btn-info float-right" id="btn-add">Agregar</button></div>`);

			this.optionPane('btn-add', 'Importante', '¿Está seguro que desea agregar este pozo?', 1);
			this.optionPane('btn-cancel', 'Importante', '¿Está seguro que desea cancelar este proceso?', 2);
	
		}).catch(error => { console.log(error); });
		return false;
	}
	
	adding(event) {
		
		this.reloadView();
		var locationName = $('#location_name').val(), locationN = $('#prepend').val() != null ? $('#prepend').val().toUpperCase().trim() : $('#macolla_name').val().toUpperCase();		

		/** Set value UWI constant */
		const uwi = 49406;
		var wellName = ($('#well_name').val()) ? $('#well_name').val().toUpperCase() : null, reservoir = JSON.parse($('#reservoir_name').val());

		const ajax = new Ajax(`${globalConfig.appUrl}/wellManagement/addWell`, {
			headers: {
				accept: 'application/json',
			},
			method: 'POST',
			body: {
				macolla_name: $('#macolla_name').val(),
				location_name: `${locationN}-${locationName}`,
				uwi: uwi + wellName,
				well_name: wellName,
				reservoir_name: reservoir[0].id,
				type: $('#type').val(),
				classification: $('#classification').val(),
				condition: $('#condition').val(),
				drill: ($('#drill').val()) ? $('#drill').val().toUpperCase() : '',
				gl: $('#gl').val(),
				rt: $('#rt').val(),
				height:$('#height').val(),
				ppn: $('#ppn').val()
			}
		});
		
		$("#btn-add").attr("disabled", true);
		ajax.result().then(response => {
			alertify.success(`Pozo <b>${wellName}</b> agregado exitosamente a la macolla <b>${$('#macolla_name').val()}</b>`);
			$(`#main-options`).html(this.setHtmlSources(1));
		}).catch(error => {
			if (error.status === 400) {
				this.showInputCorrects(error.errors, this.inputNames);
				this.showInputErrors(error.errors, this.inputNames);
				$("#btn-add").attr("disabled", false);
			} else {
				console.log(error.message);
			}
		});
		return false;
	}
	
	updating(table, row) {
		
		this.reloadView();
		const id = $('#identity').val();
		const element = $('#element').val();
		const index = $('#index').val();

		var wellName = $('#update_well_name').val().toUpperCase();
		
		/*Set value UWI in constant*/
		const uwi = 49406;
		const ajax = new Ajax(`${globalConfig.appUrl}/wellManagement/updateWell/${id}`, {
			headers: {
				accept: 'application/json',
			},
			method: 'POST',
			body: {
				update_location_name: $("#update_location_name").val(),
				update_well_name: wellName,
				update_uwi: uwi + wellName,
				update_type: $('#update_type').val(),
				update_classification: $('#update_classification').val(),
				update_reservoir_id: $('#update_reservoir_id').val(),
				update_condition:  $('#update_condition').val(),
				update_drill: $('#update_drill').val().toUpperCase(),
				update_gl: this.convertDecimal($('#update_gl').val()),
				update_rt: this.convertDecimal($('#update_rt').val()),
				update_height: this.convertDecimal($('#update_height').val()),
				update_ppn: this.convertDecimal($('#update_ppn').val()),
				identity: id
			}
		});
		
		$("#btn-update").attr("disabled", true);
		
		ajax.result().then(response => {
			
			var dataset = [], mk = CryptoJS.SHA1(this.genToPut).toString(), encrypted;
			
			dataset.push({
				well_id: response.data.id,
				wellname: response.data.well_name,
				location_name: response.data.location_name,
				reservoir: response.data.reservoir_name,
				reservoir_id: response.data.reservoir_id,
				type: response.data.type,
				classification: response.data.classification,
				gl: response.data.gl,
				rt: response.data.rt,
				ppn: response.data.ppn,
				condition: response.data.condition,
				drill: response.data.drill,
				height: response.data.height,
				index: index,
				id: element
			});
			encrypted = CryptoJS.AES.encrypt(JSON.stringify(dataset), mk);
			alertify.alert('Enhorabuena', 'El pozo fue modificado exitosamente.', function(e){
				if (e) {

					var update = [index, response.data.location_name,response.data.well_name,response.data.uwi,response.data.reservoir_name,response.data.type,response.data.classification,response.data.condition,response.data.drill,response.data.gl,response.data.rt,response.data.height,response.data.ppn, `<div class="btn-group btn-group-sm"><button class="btn btn-info" id="btn-modify" value='${encrypted}' data-toggle="modal" data-target="#modal-xl" title="Modificar"><i class="fas fa-edit"></i></button><button class="btn btn-danger" id="btn-delete" value="${response.data.id}"><i class="fa fa-trash" title="Borrar"></i></button></div>`];
                    
                    table.row(table.row(row).data(update).draw());
				}
			}).set({'movable': false, 'moveBounded': false});
			$(`#modal-xl`).modal('toggle');

		}).catch(error => {
			if (error.status === 400) {
				this.showInputCorrects(error.errors, this.inputEditNames);
				this.showInputErrors(error.errors, this.inputEditNames);
				$("#btn-update").attr("disabled", false);
			} else console.log(error);
		});
		return false;
	}
	
	searching(event) {
		
		this.reloadView();
		var json = JSON.parse($('#macolla_id').val());
		
		const ajax = new Ajax(`${globalConfig.appUrl}/wellManagement/searchWells`, {
			headers: {
				accept: 'application/json',
			},
			method: 'POST',
			body: {
				macolla_id: json[0].id
			}
		});
		
		$("#btn-search").attr("disabled", true);
		
		ajax.result().then(response => {

			this.genToPut = this.randomString(240);
			var size = Object.keys(response.data).length, dataset = [], data = [], mk = CryptoJS.SHA1(this.genToPut).toString(), encrypted;
			
			if (size > 0) {

				$(`#show-wells`).html(`<div class="card-header"><h3 class="card-title"><strong>Pozos</strong> / ${response.macolla.macolla_name}</h3><div class="card-tools"><div class="input-group input-group-sm" style="width:150px;"><input type="text" id="table-search" class="form-control float-right" placeholder="Buscar" maxlength="15" autocomplete="off"></div></div></div><div class="card-body" id="macolla_table"><table class="display" id="table-results" width="100%"><thead><tr><th># </th><th>Localización </th><th>Nombre </th><th>UWI </th><th>Yacimiento&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </th><th>Tipo </th><th>Clasificación </th><th>Condición&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </th><th>Taladro&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </th><th>GL <h6 class="text-muted">pies</h6></th><th>RT <h6 class="text-muted">pies</h6></th><th>Altura <h6 class="text-muted">pies</h6></th><th>PPN <h6 class="text-muted">pies</h6></th><th>Acción </th></tr></thead><tbody id="data"></tbody></table></div>`);
				
				for (var i=0; i < size; i++) {
					dataset.push({
						well_id: response.data[i].id,
						wellname: response.data[i].well_name,
						location_name: response.data[i].location_name,
						reservoir: response.data[i].reservoir_name,
						reservoir_id: response.data[i].reservoir_id,
						type: response.data[i].type,
						classification: response.data[i].classification,
						gl: response.data[i].gl,
						rt: response.data[i].rt,
						ppn: response.data[i].ppn,
						condition: response.data[i].condition,
						drill: response.data[i].drill,
						height: response.data[i].height,
						index: (i+1)
					});

					encrypted = CryptoJS.AES.encrypt(JSON.stringify(dataset), mk);

					data.push([
						(i+1),
						response.data[i].location_name,
						response.data[i].well_name,
						response.data[i].uwi,
						response.data[i].reservoir_name,
						response.data[i].type,
						response.data[i].classification,
						response.data[i].condition,
						(response.data[i].drill == '' ? '-' : response.data[i].drill),
						Math.round(response.data[i].gl),
						Math.round(response.data[i].rt),
						Math.round(response.data[i].height),
						Math.round(response.data[i].ppn),
						`<div class="btn-group btn-group-sm">
							<button class="btn btn-info" id="btn-modify" value='${encrypted}' data-toggle="modal" data-target="#modal-xl" title="Modificar"><i class="fas fa-edit"></i></button>
							<button class="btn btn-danger" id="btn-delete" value="${response.data[i].id}"><i class="fa fa-trash" title="Borrar"></i></button>
						</div>`
					]);

					dataset = [];
				}

				var table = $("#table-results").DataTable({
					dom: 'frptip',
					processing: true,
					draw: data.draw,
					data: data,
					deferRender: true,
					recordsTotal: size,
					recordsFiltered: size,
					searching: true,
					info: true,
					ordering: false,
					scrollY: 400,
					scrollX: true,
					scrollColapse: true,
					paging: false,
					language: {
						info: "Mostrando _START_ a _END_ de _TOTAL_ resultados"
					}
				});
				
				document.getElementById('imhere').focus();

				$('.dataTables_filter').hide();
				$('#table-search').keyup(function(){table.search($(this).val()).draw();});

				table.on('click', '#btn-delete', function() {
					let $tr = $(this).closest('tr'), $button = $(this);
					const id = $(this).val();
					alertify.confirm("<i class='fa fa-exclamation-triangle text-danger'></i> Advertencia", "La eliminación de este pozo causará la pérdida de la información asociada al registro en otros módulos del sistema. ¿Está seguro que desea continuar?", function() {},
					function(e) {
						if (e) {
							$button.attr('disabled', true);
							const ajax = new Ajax(`${globalConfig.appUrl}/wellManagement/deleteWell/${id}`, {
								headers: { accept: 'application/json' },
								method: 'POST'
							});
							ajax.result().then(response => {table.row($tr).remove().draw(false);alertify.warning("El pozo ha sido eliminado exitosamente");
							}).catch(error => { console.log(error); });
						}
					}).set({'movable': false, 'moveBounded': false, 'modal': false, 'labels': {ok: 'No', cancel: 'Sí'}});
				});

				table.on('click', '#btn-modify', function() {
					var $tr = $(this).closest('tr'), $value = $(this).val();
					myWell.modify($value, table, $tr);
				});
	
			} else {
				$(`#show-wells`).html(`<div class="card-header"><h3 class="card-title"><b>Pozos</b> / ${response.macolla.macolla_name}</h3></div><div class="card card-body"><h5>No se encontraron resultados.</h5></div>`);
				document.getElementById('imhere').focus();
			}

			$("#btn-search").attr("disabled", false);
		}).catch(error => { console.log(error) });
		return false;
	}
	
	modify(val, table, row) {
		var decrypt = CryptoJS.AES.decrypt(val, CryptoJS.SHA1(this.genToPut).toString()).toString(CryptoJS.enc.Utf8), data = JSON.parse(decrypt);
		this.loadReservoirs(data[0].reservoir);
		
		$(`#modal-xl`).html(`<div class="modal-dialog modal-xl"><div class="modal-content"><div class="modal-header"><h4 class="modal-title">Editar Pozo</h4><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button></div><div class="modal-body"><div class="row"><div class="col-md-3 pt-2"><h6>Localización</h6><input type="text" placeholder="Localización" class="form-control" value="${data[0].location_name}" style="text-transform: uppercase" id="update_location_name" name="update_location_name" maxlength="8" autocomplete="off"><div id="wrapper-update_location_name"></div></div><div class="col-md-3 pt-2"><h6>Nombre</h6><input type="text" placeholder="Nombre" class="form-control" value="${data[0].wellname}" style="text-transform: uppercase" id="update_well_name" name="update_well_name" maxlength="8" autocomplete="off"><div id="wrapper-update_well_name"></div></div><div class="col-md-3 pt-2"><h6>Yacimiento</h6><select type="text" class="form-control" value="" style="text-transform: uppercase" id="update_reservoir_id" name="update_reservoir_id"><option value="${data[0].reservoir_id}">${data[0].reservoir}</option></select></div><div class="col-md-3 pt-2"><h6>Clasificación</h6><select id="update_classification" name="update_classification" class="form-control"><option value="${data[0].classification}">${data[0].classification}</option></select></div><div class="col-md-3 pt-3"><h6>Tipo</h6><select type="text" class="form-control" id="update_type" name="update_type"><option value="${data[0].type}">${this.isType(data[0].type)}</option></select></div><div class="col-md-3 pt-3"><h6>Condición</h6><select id="update_condition" name="update_condition" class="form-control"><option value="${data[0].condition}">${data[0].condition}</option></select></div><div class="col-md-3 pt-3"><h6>Taladro</h6><input type="text" placeholder="Taladro" class="form-control" value="${data[0].drill}" style="text-transform: uppercase" id="update_drill" name="update_drill" maxlength="12" ><div id="wrapper-update_drill"></div></div><div class="col-md-3 pt-3"><h6>Altura <i class="text-muted">pies</i></h6><input type="text" placeholder="Altura" class="form-control" value="${data[0].height}" style="text-transform: uppercase" id="update_height" name="update_height" maxlength="6" autocomplete="off"><div id="wrapper-update_height"></div></div><div class="col-md-3 pt-3"><h6>GL <i class="text-muted">pies</i></h6><input type="text" placeholder="GL" class="form-control" value="${data[0].gl}" style="text-transform: uppercase" id="update_gl" name="update_gl" maxlength="6" ><div id="wrapper-update_gl"></div></div><div class="col-md-3 pt-3"><h6>RT <i class="text-muted">pies</i></h6><input type="text" placeholder="RT" class="form-control" value="${data[0].rt}" style="text-transform: uppercase" id="update_rt" name="update_rt" maxlength="6" ><div id="wrapper-update_rt"></div></div><div class="col-md-3 pt-3"><h6>PPN <i class="text-muted">pies</i></h6><input type="text" placeholder="PPN" class="form-control" value="${data[0].ppn}" style="text-transform: uppercase" id="update_ppn" name="update_ppn" maxlength="6" ><div id="wrapper-update_ppn"></div></div></div></div><input type="hidden" id="identity" value="${data[0].well_id}"><input type="hidden" id="element" value="${data[0].id}"><input type="hidden" id="index" value="${data[0].index}"><div class="modal-footer justify-content-between"><button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button><button type="button" class="btn btn-primary" id="btn-update">Guardar cambios</button></div></div></div>`);
		
		var numericInputs = ['update_gl', 'update_rt', 'update_height', 'update_ppn']
		
		for (var i = 0; i < numericInputs.length; i++) {
			$(`#${numericInputs[i]}`).numeric({ negative: false, decimal: false });
		}
		
		this.getAttributes(data[0].classification, data[0].type, data[0].condition);
		this.optionPane('btn-update', 'Pregunta', '¿Está seguro que desea guardar los cambios realizados en este pozo?', 3, table, row);
	}
	
	loadReservoirs(value) {
		const ajax = new Ajax(`${globalConfig.appUrl}/reservoirManagement/loadReservoirs`, {
			headers: {
				accept: 'application/json',
			},
			method: 'POST'
		});
		
		ajax.result().then(response => {
			var size = Object.keys(response.data).length;
			for(var i=0; i<size; i++) {
				if(value != response.data[i].reservoir_name) {
					document.querySelector(`#update_reservoir_id`).innerHTML += `<option value="${response.data[i].id}">${response.data[i].reservoir_name}</option>`;
				}
			}
		}).catch(error => { console.log(error) });
		return false;
	}
	
	getAttributes(x, y, z) {
		var collection = ['PRODUCTOR', 'OBSERVADOR', 'ESTRATIGRÁFICO'], types = ['V', 'H', 'D'], wellCondition = ['ACTIVO', 'ABANDONADO', 'NO COMPLETADO', 'EN PERFORACIÓN'];
		this.setValuesInOption(collection, x, '#update_classification');
		this.setValuesInOption(types, y, '#update_type');
		this.setValuesInOption(wellCondition, z, '#update_condition');
	}
	
	setValuesInOption(array, value, id) {
		for (var i = 0; i < array.length; i++) {
			if (value != array[i]) {
				document.querySelector(`${id}`).innerHTML += `<option value="${array[i]}">${this.isType(array[i])}</option>`;
			}
		}
	}

	isType(val) {
		if (val == 'V') return 'Vertical';
		else if (val == 'H') return 'Horizontal';
		else if (val == 'D') return 'Desviado';
		else return val;
	}

	returnMain() {
		$(`#main-options`).html(`<div class="card-body"><div class="col-md-6"><h6>¿Qué desea hacer?</h6><div class="input-group mb-3"><select class="form-control" id="options" name="options"><option value>-</option><option value=1>Agregar</option><option value=2>Gestionar</option></select><span class="input-group-append"><button type="button" class="btn btn-info btn-block" id="btn-search" onclick="myWell.getSelectedOption()">OK</button></span></div></div></div>`);
		$(`#show-wells`).html(``);
		$(`#alert`).show();
		this.genToPut = ``;
	}
	
	convertDecimal(value) {
		return (value == '') ? '' : (value).replace(',', '.');
	}
	
	setComboBox(selector, label, id, arr, values) {
		var container = ``, options = ``;
		for (var i = 0; i < values.length; i++) {
			options += `<option value="${arr[i]}">${values[i]}</option>`;
		}
		container += `<h6>${label}</h6>
		<select id="${id}" name="${id}" class="form-control">${options}</select>
		<div id="wrapper-${id}"></div>`;
		$(`#${selector}`).html(container);
	}
	
	setTextfield(selector, label, placeholder, maxlength, id) {
		$(`#${selector}`).html(`<h6>${label}</h6>
		<input type="text" placeholder="${placeholder}" class="form-control" id="${id}" name="${id}" style="text-transform: uppercase;" maxlength="${maxlength}" autocomplete="off">
		<div id="wrapper-${id}"></div>`);
	}

	addAndRemoveClass(id, remove, add) {
		$(`#${id}`).removeClass(remove);
		$(`#${id}`).addClass(add);
	}

	setHtmlSources(val) {
		
		var html;

		if (val == 1) {
			
			html = `<div class="card-header"><h3 class="card-title"><b>Pozos</b> / Agregar nuevo</h3></div><div class="card-body"><div id="addNewWell"><div class="row"><div class="col-md-6" id="macolla"><label>Macolla</label><div class="form-group mb-3"><select type="text" id="macolla_name" name="macolla_name" class="form-control"><option value="">--</option></select></div></div><div class="col-md-6" id="location"></div></div></div><div class="row" id="form" style="display:none"><div class="col-md-12 pt-2"><hr></div><div class="col-md-3 pt-2" id="well"></div><div class="col-md-3 pt-2" id="reservoir"></div><div class="col-md-3 pt-2" id="set_type"></div><div class="col-md-3 pt-2" id="set_classification"></div><div class="col-md-3 pt-4" id="set_gl"></div><div class="col-md-3 pt-4" id="set_rt"></div><div class="col-md-3 pt-4" id="set_ppn"></div><div class="col-md-4 pt-4" id="set_condition"></div><div class="col-md-4 pt-4" id="set_drill"></div><div class="col-md-4 pt-4" id="set_height"></div></div></div><div id="default-button"><div class="card-footer"><button class="btn btn-secondary" onclick="myWell.returnMain()">Regresar</button><button class="btn btn-info float-right" onclick="myWell.setSelected(macolla_name.value)" id="btn-continue" disabled>Continuar</button></div></div><div id="group-buttons"></div>`;
			
			this.loadMacollasToWell(1);

		} else {
			
			html = `<div class="card-header"><h3 class="card-title"><b><button onclick="myWell.returnMain()" class="btn btn-outline-secondary btn-sm">Volver</button> Junín 6</b> / Pozos</h3></div><div class="card-body"><div id="searchWell"><div class="row"><div class="col-6" id="chose-macolla"><h6>En la macolla</h6><div class="input-group mb-3"><select type="text" id="macolla_id" name="macolla_id" class="form-control"><option value="-">Cargando...</option></select><span class="input-group-append"><button class="btn btn-default btn-block" onclick="myWell.searching(event)" id="btn-search" disabled="disabled"><i class="fa fa-search"></i></button></span></div></div></div></div><div class="modal fade" id="modal-xl" style="display: none;" aria-hidden="true" data-keyboard="false" data-backdrop="static"></div>`;

			$(`#show-wells`).html(``);

			this.loadMacollasToWell(2);
		}

		return html;
	}

	loadMacollasToWell(where) {
		const ajax = new Ajax(`${globalConfig.appUrl}/loadMacollas`, {
            headers: { accept: 'application/json' },
            method: 'POST'
        });
      
        ajax.result().then(response => {	
			
			var options = ``, size = Object.keys(response.data).length, selector = (where == 1) ? `macolla_name` : `macolla_id`, dataset = [];
			
			$(`#btn-continue`).attr('disabled', false);
			
            for (var i=0; i < size; i++) {
				if (response.data[i].id != 6) {
					dataset.push({id:response.data[i].id,name:response.data[i].macolla_name});
					options += `<option value='${JSON.stringify(dataset)}'>${response.data[i].macolla_name}</option>`;
					dataset = [];
				}
			}
			
			dataset.push({id:response.e.id,name:response.e.macolla_name});
			options += `<option value='${JSON.stringify(dataset)}'>S/M</option>`;
			
			$(`#${selector}`).html(options);   	
			$(`#btn-search`).attr('disabled', false);
            
        }).catch(error => { console.log(error) });
        return false;
	}

	optionPane(btnName, title, dialog, option, table = null, row = null) {
		$(`#${btnName}`).click(function(event) {
			alertify.confirm(title, dialog, function(e) {
				if (e) {
					if (option == 1) myWell.adding(event);
					else if (option == 2) myWell.returnMain();
					else myWell.updating(table, row);
				}
			}, function(){}).set({'movable': false, 'moveBounded': false, 'modal': false, 'labels': {ok: 'Sí', cancel: 'No'}});
		});
	}

	randomString(length, chars) {
		length = length || 16;
		chars = chars || "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ@#!?=";
		
		var text = "", max = chars.length-1;
		
		for (var i=1; i<length; i++)  text += chars[Math.floor(Math.random() * (max+1))];
		return text;
	}
}

window.onload = () => {
	myWell = new Well();
}