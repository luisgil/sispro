﻿class Macolla {

	constructor() {
		this.adding = this.adding.bind(this);
		this.modify = this.modify.bind(this);
		this.updating = this.updating.bind(this);
		this.reloadView = this.reloadView.bind(this);
		this.showInputErrors = this.showInputErrors.bind(this);
		this.inputNames = ['macolla_name', 'camp_name'];
		this.inputEditName = ['update_macolla_name'];
		this.genToPut = ``;
	}
	
	reloadView() {
		document.querySelectorAll('.text-danger').forEach(function(a){
			a.remove()
		});
	}
	
	showInputErrors(errors, inputNames) {
		for(const errorName in errors) {
			inputNames.map(inputName => {
				if (errorName === inputName) {
					$(`#${inputName}`).addClass("is-invalid");
					$(`#wrapper-${inputName}`).html(`<small class="text-danger">${errors[errorName]}</small>`);
				}
			});
		}
	}
	
	getSelectedOption() {

		var option = $(`#options`).val();
		
		if (option == 1 || option == 2) {
			if (option == 1) {
				this.addAndRemoveClass('main-options', 'card-secondary', 'card-default');
				$(`#main-options`).html(this.setHtmlSources(1));
			} else {
				this.addAndRemoveClass('main-options', 'card-secondary', 'card-default');
				$(`#main-options`).html(this.setHtmlSources(2));
				
				const ajax = new Ajax(`${globalConfig.appUrl}/loadMacollas`, {
					headers: {accept: 'application/json',},
					method: 'POST'
				});

				ajax.result().then(response => {

					this.genToPut = this.randomString(240);
					var size = Object.keys(response.list).length, datatable = ``, dataset = [], data = [], mk = CryptoJS.SHA1(this.genToPut).toString(), encrypted;

					for(var i = 0; i < size; i++) {
						
						dataset.push({
							macolla_id: response.list[i].macolla_id,
							macolla_name: response.list[i].macolla_name,
							camp_name: response.list[i].camp_name,
							camp_id: response.list[i].camp_id,
							index: (i+1)
						});

						encrypted = CryptoJS.AES.encrypt(JSON.stringify(dataset), mk);

						data.push([
							(i+1),
							response.list[i].macolla_name,
							response.list[i].camp_name,
							`<div class="btn-group btn-group-sm">
								<button class="btn btn-info" id="btn-edit" data-toggle="modal" data-target="#modal-xl" value='${encrypted}' title="Editar"><i class="fa fa-edit"></i></button>
								<button class="btn btn-danger" id="btn-delete" value="${response.list[i].macolla_id}"><i class="fa fa-trash" title="Borrar"></i></button>
							</div>`
						]);

						dataset = [];
					}

					var table = $("#table-list").DataTable({
						dom: 'frptip',
						processing: true,
						draw: data.draw,
						data: data,
						deferRender: true,
						recordsTotal: size,
						recordsFiltered: size,
						searching: true,
						info: true,
						ordering: false,
						scrollY: 400,
						scrollX: true,
						scrollColapse: true,
						paging: false,
						language: {
							info: "Mostrando _START_ a _END_ de _TOTAL_ resultados"
						}
					});
					
					$('.dataTables_filter').hide();
					$('#table-search').keyup(function(){table.search($(this).val()).draw();});

					table.on('click', '#btn-delete', function() {
						let $tr = $(this).closest('tr'), $button = $(this);
						const id = $(this).val();
						alertify.confirm("<i class='fa fa-exclamation-triangle text-danger'></i> Advertencia", "La eliminación de esta macolla causará la pérdida de la información asociada al registro en otros módulos del sistema. ¿Está seguro que desea continuar?", function() {},
						function(e) {
							if (e) {
								$button.attr('disabled', true);
								const ajax = new Ajax(`${globalConfig.appUrl}/macollaManagement/deleteMacolla/${id}`, {
									headers: { accept: 'application/json' },
									method: 'POST'
								});
								ajax.result().then(response => {table.row($tr).remove().draw(false);alertify.warning("La macolla ha sido eliminada");
								}).catch(error => { console.log(error) });
							}
						}).set({'movable': false, 'moveBounded': false, 'modal': false, 'labels': {ok: 'No', cancel: 'Sí'}});
					});

					table.on('click', '#btn-edit', function() {
						var $tr = $(this).closest('tr'), $value = $(this).val();
						myMacolla.modify($value, table, $tr);
					});

				}).catch(error => { console.log(error); });
			}
			$(`#alert`).css('display','none');
		}
		return false;
	}
	
	adding(event) {
		
		this.reloadView();
		event.preventDefault();

		const ajax = new Ajax(`${globalConfig.appUrl}/macollaManagement/addMacolla`, {
			headers: {
				accept: 'application/json',
			},
			method: 'POST',
			body: {
				macolla_name: $('#macolla_name').val(),
				camp_name: $('#camp_name').val()
			}
		});
		
		$("#btn-add").attr("disabled", true);
		
		ajax.result().then(response => {
			alertify.success('Macolla <b>' + $('#macolla_name').val() + '</b> agregada exitosamente');
			$(`#main-options`).html(this.setHtmlSources(1));
		}).catch(error => {
			if (error.status === 400) {
				this.showInputErrors(error.errors, this.inputNames);
				$("#btn-add").attr("disabled", false);
			} else console.log(error);
		});
		return false;
	}
	
	preAdding(event) {
		
		this.reloadView();
		event.preventDefault();

		const ajax = new Ajax(`${globalConfig.appUrl}/macollaManagement/preAddMacolla`, {
			headers: {
				accept: 'application/json',
			},
			method: 'POST',
			body: {
				macolla_name: $('#macolla_name').val(),
			}
		});
		
		$("#btn-continue").attr("disabled", true);
		
		ajax.result().then(response => {
			
			$(`#macolla`).html(`<h6>Macolla</h6>
			<input type="text" id="macolla_name" name="macolla_name" style="text-transform:uppercase" maxlength="5" class="form-control" value="${response.macolla}" disabled>
			<div id="wrapper-macolla_name"></div>`);
			
			$(`#camp`).html(`<h6>Campo</h6>
			<select type="text" id="camp_name" name="camp_name" class="form-control" placeholder=""></select>
			<div id="wrapper-camp_name"></div>`);
			
			$(`#buttons`).html(`<button type="submit" class="btn btn-danger" id="btn-cancel">Cancelar</button>
			<button type="submit" class="btn btn-info float-right" id="btn-add">Agregar</button>`);
			
			var deployCamps = `<option value>-</option>`, campSize = Object.keys(response.camps).length;
			for (var i = 0; i < campSize; i++) {
				deployCamps += `<option value="${response.camps[i].id}">${response.camps[i].camp_name}</option>`;
			}
			$(`#camp_name`).html(deployCamps);
			
			this.optionPane('btn-add', 'Aviso', '¿Está seguro que desea guardar esta macolla?', 1);
			this.optionPane('btn-cancel', 'Aviso', '¿Está seguro que desea cancelar este proceso?', 2);
			
		}).catch(error => {
			if (error.status === 400) {
				this.showInputErrors(error.errors, this.inputNames);
				$("#btn-continue").attr("disabled", false);
			} else console.log(error);
		});
		return false;
	}
	
	updating(table, row) {
		
		this.reloadView();

		const id = $('#identity').val();
		const index = $('#index').val();

		const ajax = new Ajax(`${globalConfig.appUrl}/macollaManagement/updateMacolla/${id}`, {
			headers: {accept: 'application/json',},
			method: 'POST',
			body: {
				update_macolla_name: $('#update_macolla_name').val() != null ? $('#update_macolla_name').val().toUpperCase() : null,
				camp_name: $('#update_camp_name').val(),
				identity: $('#identity').val()
			}
		});
		
		$("#btn-update").attr("disabled", true);
		
		ajax.result().then(response => {
			
			var dataset = [], mk = CryptoJS.SHA1(this.genToPut).toString(), encrypted, update = [];

			dataset.push({
				macolla_id: response.data.macolla_id,
				macolla_name: response.data.macolla_name,
				camp_name: response.data.camp_name,
				camp_id: response.data.camp_id,
				index: index,
			});

			encrypted = CryptoJS.AES.encrypt(JSON.stringify(dataset), mk);

			alertify.alert('Enhorabuena', 'La macolla fue modificada exitosamente.', function(e){
				if(e) {

					update = [index, response.data.macolla_name,response.data.camp_name,`<div class="btn-group btn-group-sm"><button class="btn btn-info" id="btn-edit" data-toggle="modal" data-target="#modal-xl" value='${encrypted}' title="Editar"><i class="fa fa-edit"></i></button><button class="btn btn-danger" id="btn-delete" value="${response.data.macolla_id}"><i class="fa fa-trash" title="Borrar"></i></button>`];
                    
					table.row(table.row(row).invalidate().data(update).draw());
				}
			}).set({'movable': false, 'moveBounded': false});
			$(`#modal-xl`).modal('toggle');

		}).catch(error => {
			if (error.status === 400) {
				this.showInputErrors(error.errors, this.inputEditName);
				$("#btn-update").attr("disabled", false);
			} else console.log(error);
		});
		return false;
	}
	
	returnMain() {
		document.querySelector(`#main-options`).innerHTML = `<div class="card-body"><div class="col-md-6"><h6>¿Qué desea hacer?</h6><div class="input-group mb-3"><select class="form-control" id="options" name="options"><option value>-</option><option value=1>Agregar</option><option value=2>Gestionar</option></select><span class="input-group-append"><button type="button" class="btn btn-info btn-block" id="btn-search" onclick="myMacolla.getSelectedOption()">OK</button></span></div></div></div>`;
		$(`#alert`).show();
		this.genToPut = ``;
	}
	
	modify(val, table, row) {

		var options = ``, camps = ['Iguana Zuata', 'Zuata Principal'], ids = [1, 2], decrypt = CryptoJS.AES.decrypt(val, CryptoJS.SHA1(this.genToPut).toString()).toString(CryptoJS.enc.Utf8), data = JSON.parse(decrypt);

		for (var i = 0; i < camps.length; i++) {
			if (data[0].camp_name != camps[i]) {
				options += `<option value=${ids[i]}>${camps[i]}</option>`;
			}
		}

		$(`#modal-xl`).html(`<div class="modal-dialog modal-xl" id="modal-show"><div class="modal-content"><div class="modal-header"><h4 class="modal-title">Editar Macolla</h4><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button></div><div class="modal-body"><div class="row"><div class="col-md-6 pt-2"><h6>Nombre</h6><input type="text" placeholder="Nombre de la macolla" class="form-control" style="text-transform: uppercase" id="update_macolla_name" maxlength="5" value="${data[0].macolla_name}" autocomplete="off"><div id="wrapper-update_macolla_name"></div></div><div class="col-md-6 pt-2"><h6>Campo</h6><select class="form-control" id="update_camp_name"><option value="${data[0].camp_id}">${data[0].camp_name}</option>${options}</select><div id="wrapper-update_camp_name"></div></div><div class="col-md-12 pt-4"></div></div></div><input type="hidden" id="identity" value="${data[0].macolla_id}"><input type="hidden" id="element" value="${data[0].id}"><input type="hidden" id="index" value="${data[0].index}"><div class="modal-footer justify-content-between"><button class="btn btn-default" data-dismiss="modal">Cerrar</button><button class="btn btn-primary" id="btn-update">Guardar cambios</button></div></div></div>`);

		this.optionPane('btn-update', 'Pregunta', '¿Está seguro que desea realizar cambios en esta macolla?', 3, table, row);
	}

	optionPane(btnName, title, dialog, option, table = null, row = null) {
		$(`#${btnName}`).click(function(event) {
			alertify.confirm(title, dialog, function(e) {
				if (e) {
					if (option == 1) myMacolla.adding(event);
					else if (option == 2) myMacolla.returnMain();
					else myMacolla.updating(table, row);
				}
			}, function(){}).set({'movable': false, 'moveBounded': false, 'modal': false, 'labels': {ok: 'Sí', cancel: 'No'}});
		});
	}

	addAndRemoveClass(id, remove, add) {
		$(`#${id}`).removeClass(remove);
		$(`#${id}`).addClass(add);
	}

	setHtmlSources(val) {
		if (val == 1) return `<div class="card-header"><h3 class="card-title"><b>Macollas</b> / Agregar nueva</h3></div><div class="card-body"><div class="row" id="addNewMacolla"><div class="col-md-4" id="macolla"><h6>Macolla</h6><input type="text" id="macolla_name" name="macolla_name" class="form-control" style="text-transform:uppercase" placeholder="Nombre de la macolla" maxlength="5" autofocus><div id="wrapper-macolla_name"></div></div><div class="col-md-4" id="camp"></div><div id="form-adding"></div></div></div><div class="card-footer" id="buttons"><button class="btn btn-secondary" onclick="myMacolla.returnMain()">Regresar</button><button class="btn btn-info float-right" onclick="myMacolla.preAdding(event)" id="btn-continue">Continuar</button></div>`;
		else return`<div class="card-header"><h3 class="card-title"><strong><button onclick="myMacolla.returnMain()" class="btn btn-outline-secondary btn-sm">Volver</button> Junín 6</strong> / Macollas</h3><div class="card-tools"><div class="input-group input-group-sm" style="width:150px;position:relative;top:5px"><input type="text" id="table-search" class="form-control float-right" placeholder="Buscar" maxlength="15" autocomplete="off"></div></div></div><div class="card-body" id="macolla_table"><table class="display" id="table-list" width="100%"><thead><tr><th>#</th><th>Nombre</th><th>Campo</th><th>Acción</th></tr></thead><tbody id="datatable"></tbody></table></div><div class="modal fade" id="modal-xl" style="display: none;" aria-hidden="true" data-keyboard="false" data-backdrop="static"></div>`;
	}

	randomString(length, chars) {
		length = length || 16;
		chars = chars || "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ@#!?=";
		
		var text = "", max = chars.length-1;
		
		for (var i=1; i<length; i++)  text += chars[Math.floor(Math.random() * (max+1))];
		return text;
	}
}

window.onload = () => {
	myMacolla = new Macolla();
}