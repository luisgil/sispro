class NavigationByWell {
    constructor() {
    }
    
    validateSearch() {
        if ($(`#macolla`).val() != '') this.searchByFilter();
        else alertify.alert('Importante', 'Debe seleccionar primero una macolla.', function(){}).set({'movable': false, 'moveBounded': false});
    }

    searchByFilter() {
        
        const ajax = new Ajax(`${globalConfig.appUrl}/navigation/searchByFilterNavigation`, {
            headers: { accept: 'application/json' },
            method: 'POST',
            body: {
                macolla_id: parseInt($(`#macolla`).val()),
                well_id: parseInt($(`#well`).val())
            }
        });

        m.setLoadingMessageBefore();
        
        ajax.result().then(response => {

            var size = Object.keys(response.chart).length, size2 = Object.keys(response.chart2).length, size3 = Object.keys(response.chart3).length;

            m.setLoadingMessageAfter();
            m.showMessageDialog(size);

            if (size > 0 || size2 > 0 || size3 > 0) {

                document.querySelector(`#title-charts`).innerHTML = 'Navegación en ' + response.location;
                
                $(`#showChart`).css('display','block');

                var chart = AmCharts.makeChart("chart", {
                    //"hideCredits": true,
                    "type": "xy",
                    "theme": "none",
                    "language": "es",
                    "autoMarginOffset": 20,
                    "dataProvider": response.chart,
                    "valueAxes": [{
                        "position": "top",
                        "axisAlpha": 0,
                        "dashLength": 1,
                        "title": "Vertical Section (ft)"
                    }, {
                        "axisAlpha": 0,
                        "dashLength": 1,
                        "position": "left",
                        "title": "TVD (ft)",
                        "reversed": true
                    }],
                    "startDuration": 0,
                    /*"titles": [{
                        //"text": "Navegación " + response.location,
                        //"size": 22
                    }],*/
                    "graphs": [{
                        "balloonText": "<b>Vertical Section: </b>[[x]] ft<br><b>TVD:</b> [[y]] ft",
                        "bullet": "round",
                        "bulletSize": 2,
                        "lineThickness": 2,
                        "lineAlpha": 1,
                        "xField": "ax",
                        "yField": "ay",
                        "lineColor": "red",
                        "fillAlphas": 0,
                        "title": "Real Survey"
                    }, {
                        "balloonText": "<b>Vertical Section: </b>[[x]] ft<br><b>TVD:</b> [[y]] ft",
                        "bullet": "round",
                        "bulletSize": 2,
                        "lineThickness": 2,
                        "lineAlpha": 1,
                        "xField": "bx",
                        "yField": "by",
                        "lineColor": "blue",
                        "fillAlphas": 0,
                        "title": "Plan Survey"
                    }],
                    "guides": [{
                        "fillAlpha": 0.3,
                        "fillColor": "#ff8000",
                        "id": "Guide-1",
                        "toValue": -2,
                        "value": -8,
                        "valueAxis": "ValueAxis-2"
                    }],
                    "legend": {
                        "position": "bottom",
                        "align": "center",
                        "markerType": "circle",
                        "valueText": "[[value]]",
                        "valueWidth": 100,
                        "valueAlign": "left",
                        "equalWidths": false,
                        "useGraphSettings": true
                    },
                    "numberFormatter": {
                        "precission": -1,
                        "decimalSeparator": ",",
                        "thousandsSeparator": ""
                    },
                    "chartCursor": {},
                    "export": {
                        "enabled": true,
                        "position": "bottom-right",
                        "language": "es",
                        "fileName": "vSEC vs TVD " + response.location
                    }
                });

                var chart = AmCharts.makeChart("chart-2", {
                    //"hideCredits": true,
                    "type": "xy",
                    "theme": "none",
                    "language": "es",
                    "autoMarginOffset": 20,
                    "dataProvider": response.chart2,
                    "valueAxes": [{
                        "position": "top",
                        "axisAlpha": 0,
                        "dashLength": 1,
                        "title": "MD (ft)"
                    }, {
                        "axisAlpha": 0,
                        "dashLength": 1,
                        "position": "left",
                        "title": "TVD (ft)",
                        "reversed": true
                    }],
                    "startDuration": 0,
                    "titles": [{
                        "text": "Vista Lateral " + response.location,
                        "size": 18
                    }],
                    "graphs": [{
                        "balloonText": "<b>MD: </b>[[x]] ft<br><b>TVD:</b> [[y]] ft",
                        "bullet": "round",
                        "bulletSize": 2,
                        "lineThickness": 2,
                        "lineAlpha": 1,
                        "xField": "ax",
                        "yField": "ay",
                        "lineColor": "red",
                        "fillAlphas": 0,
                        "title": "Real Survey"
                    }, {
                        "balloonText": "<b>MD: </b>[[x]] ft<br><b>TVD:</b> [[y]] ft",
                        "bullet": "round",
                        "bulletSize": 2,
                        "lineThickness": 2,
                        "lineAlpha": 1,
                        "xField": "bx",
                        "yField": "by",
                        "lineColor": "blue",
                        "fillAlphas": 0,
                        "title": "Plan Survey"
                    }],
                    "guides": [{
                        "fillAlpha": 0.3,
                        "fillColor": "#ff8000",
                        "id": "Guide-1",
                        "toValue": -2,
                        "value": -8,
                        "valueAxis": "ValueAxis-2"
                    }],
                    "legend": {
                        "position": "bottom",
                        "align": "center",
                        "markerType": "circle",
                        "valueText": "[[value]]",
                        "valueWidth": 100,
                        "valueAlign": "left",
                        "equalWidths": false,
                        "useGraphSettings": true
                    },
                    "numberFormatter": {
                        "precission": -1,
                        "decimalSeparator": ",",
                        "thousandsSeparator": ""
                    },
                    "chartCursor": {},
                    "export": {
                        "enabled": true,
                        "position": "bottom-right",
                        "language": "es",
                        "fileName": "MD vs TVD " + response.location
                    }
                });

                var chart = AmCharts.makeChart("chart-3", {
                    //"hideCredits": true,
                    "type": "xy",
                    "theme": "none",
                    "language": "es",
                    "autoMarginOffset": 20,
                    "dataProvider": response.chart3,
                    "valueAxes": [{
                        "position": "bottom",
                        "axisAlpha": 0,
                        "dashLength": 1,
                        "title": "East/West",
                        //"reversed": true
                    }, {
                        "axisAlpha": 0,
                        "dashLength": 1,
                        "position": "right",
                        "title": "North/South",
                        "reversed": true
                    }],
                    //"startDuration": 0,
                    "titles": [{
                        "text": "Vista Planta " + response.location,
                        "size": 18
                    }],
                    "graphs": [{
                        "balloonText": "<b>E/W: </b>[[x]] <br><b>N/S:</b> [[y]]",
                        "bullet": "round",
                        "bulletSize": 2,
                        "lineThickness": 2,
                        "lineAlpha": 1,
                        "xField": "ax",
                        "yField": "ay",
                        "lineColor": "red",
                        "fillAlphas": 0,
                        "title": "Real Survey"
                    }, {
                        "balloonText": "<b>E/W: </b>[[x]] <br><b>N/S:</b> [[y]]",
                        "bullet": "round",
                        "bulletSize": 2,
                        "lineThickness": 2,
                        "lineAlpha": 1,
                        "xField": "bx",
                        "yField": "by",
                        "lineColor": "blue",
                        "fillAlphas": 0,
                        "title": "Plan Survey"
                    }],
                    "guides": [{
                        "fillAlpha": 0.3,
                        "fillColor": "#ff8000",
                        "id": "Guide-1",
                        "toValue": -2,
                        "value": -8,
                        "valueAxis": "ValueAxis-2"
                    }],
                    "legend": {
                        "position": "bottom",
                        "align": "center",
                        "markerType": "circle",
                        "valueText": "[[value]]",
                        "valueWidth": 100,
                        "valueAlign": "left",
                        "equalWidths": false,
                        "useGraphSettings": true
                    },
                    "numberFormatter": {
                        "precission": -1,
                        "decimalSeparator": ",",
                        "thousandsSeparator": ""
                    },
                    "chartCursor": {},
                    "export": {
                        "enabled": true,
                        "position": "bottom-right",
                        "language": "es",
                        "fileName": "MD vs TVD " + response.location
                    }
                });

                //document.getElementById('imhere').focus();

            } else $(`#table-content`).html(``);

    
        }).catch(error => { console.log(error) });
        return false;
    }
}

window.onload = () => {
    m = new Method();
    nbw = new NavigationByWell();
}