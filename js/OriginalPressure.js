class OriginalPressure {
    constructor() {
        this.searchByFilter = this.searchByFilter.bind(this);
        this.setHeader = this.setHeader.bind(this);
    }

    searchByFilter() {
        
        var size = m.getColumnsFromDb().length, values = m.getColumnsFromDb(), preset = ['well_name', 'pressure_date', 'macolla_name', 'location_name', 'reservoir'];

        for (var i = 0; i <= 4; i++) values[size + i] = preset[i];
        
        var range, typeSearch, reservoirId = [], collection = [], whatIDo = document.querySelector(`#dual-list`).style.display == 'none';
        
        /*By wells or by macollas ?*/
        if (!whatIDo) {
            typeSearch = 1;
            var sizeIs = ($(`#location_well`).val()).length, store = $(`#location_well`).val();
            if (sizeIs > 1) {
                for (var i = 0; i < sizeIs; i++) collection[i] = store[i];
            } else collection = store[0];
        } else {
            /*This is for show all macollas and reservoirs!*/
            $('input[name="option[]"]:checked').each(function(i) {
                reservoirId[i] = $(this).val();
            });
            typeSearch = 2;
            var sizeIs = ($(`#macolla_list`).val()).length, store = $(`#macolla_list`).val();
            if (sizeIs > 1) {
                for (var i = 0; i < sizeIs; i++) collection[i] = store[i];
            } else collection = store[0];
        }
        
        const ajax = new Ajax(`${globalConfig.appUrl}/originalPressure/searchByFilterOriginal`, {
            headers: {
                accept: 'application/json',
            },
            method: 'POST',
            body: {
                items: values,
                data: collection,
                macolla_id: parseInt($(`#macolla`).val()),
                reservoir_id: reservoirId != null ? reservoirId : null,
                type: typeSearch
            }
        });

        m.setLoadingMessageBefore();
        
        ajax.result().then(response => {

            var options = ['type_test', 'sensor_model', 'pressure_gradient', 'deepdatum_mc_md', 'deepdatum_mc_tvd', 'press_datum_macolla', 'drill', 'temp_datum_macolla', 'temperature_gradient'];
            var size = Object.keys(response.data).length;
            
            m.setLoadingMessageAfter();
            m.showMessageDialog(size);
            
            if (size > 0) {

                var selected = m.getColumnsToHead(options, Object.keys(response.data[0]));
                var header = m.setColumn(response.type, response.info)[0], column = m.setColumn(response.type, response.info)[1];

                let detail = (response.type == 1) ? header : m.selectedItems(response.total);
                var titleText = detail == 'SIN MACOLLA' ? detail : `Macolla ${detail}`;

                m.setHtmlTable(1, "Presión Original PMVM", `${titleText}`);
                m.setHtmlTable(2);

                let headerSelected = `<th class='text-muted'>#</th>${column}<th class='text-muted'>Localización</th><th class='text-muted'>Pozo</th><th class='text-muted'>Yacimiento</th><th class='text-muted'>Fecha</th>`;

                for (var i=0; i<selected.length; i++) headerSelected += `<th>${this.setHeader(selected[i])}</th>`;

                document.querySelector(`#header`).innerHTML = headerSelected;

                var table = m.newDateTable(response.results, size, `Presión Original PMVM ${titleText}`);

                table.buttons().container().appendTo(`#buttons`);

                $(`.dts_label`).remove();

                document.querySelector('#imhere').focus();

            } else document.querySelector(`#table-content`).innerHTML = ``;
        
        }).catch(error => { console.log(error) });
        return false;
    }

    setHeader(value) {
        if (value === 'type_test') return 'Tipo&nbsp;de&nbsp;prueba';
        else if (value === 'sensor_model') return 'Sensor&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <h6 class="text-muted">modelo</h6>';
        else if (value === 'pressure_gradient') return 'Gradiente <h6 class="text-muted">lppc/pies</h6>';
        else if (value === 'deepdatum_mc_md') return 'Prof.&nbsp;DATUM&nbsp;Macolla&nbsp;MD <h6 class="text-muted">pies</h6>';
        else if (value === 'deepdatum_mc_tvd') return 'Prof.&nbsp;DATUM&nbsp;Macolla&nbsp;TVD <h6 class="text-muted">pies</h6>';
        else if (value === 'press_datum_macolla') return 'Presión&nbsp;Original&nbsp;DATUM&nbsp;Macolla <h6 class="text-muted">pies</h6>';
        else if (value === 'drill') return 'Taladro';
        else if (value === 'temp_datum_macolla') return 'Temp.&nbsp;Original&nbsp;DATUM&nbsp;Macolla <h6 class="text-muted">pies</h6>';
        else if (value === 'temperature_gradient') return 'Gradiente <h6 class="text-muted">ºF/pies</h6>';
    }
}

window.onload = () => {
    m = new Method();
    org = new OriginalPressure();
  }