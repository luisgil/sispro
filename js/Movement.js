class Movement {
    constructor() {
    }

    showResults() {

        var range = $("#filter-date").val() == 1 ?  m.getDatesRange($(`#date-pick`).val()) : m.getDatesRange(`${$(`#from`).val()}|${$(`#until`).val()}`);

        const ajax = new Ajax(`${globalConfig.appUrl}/movements/consult`, {
            headers: {
                accept: 'application/json',
            },
            method: 'POST',
            body: {
                date_from: range[0],
                date_until: range[1],
            }
        });

        m.setLoadingMessageBefore();

        ajax.result().then(response => {

            var size = Object.keys(response.data).length;

            m.setLoadingMessageAfter();
            m.showMessageDialog(size);

            m.setHtmlTable(1, `Movimientos`, ``);
            m.setHtmlTable(2);

            $('.card-tools').html(`<div class="input-group input-group-sm col-m-12"><input type="text" name="table_search" id="search-data" class="form-control float-right" placeholder="Búsqueda" maxlength="15" autocomplete="off"></div>`);
            $(`#header`).html(`<th class='text-muted'>#</th><th class='text-muted'>Fecha&nbsp;y&nbsp;Hora</th><th class='text-muted'>Rol</th><th class='text-muted'>Email</th><th class='text-muted'>Apellido,&nbsp;Nombre</th><th class='text-muted'>Descripción&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>`);

            var table = $("#datatable").DataTable({
                dom: 'frptip',
                processing: true,
                draw: response.data.draw,
                data: response.data,
                deferRender: true,
                recordsTotal: size,
                recordsFiltered: size,
                searching: true,
                info: true,
                ordering: false,
                scrollY: 400,
                scrollX: true,
                scrollColapse: true,
                paging: false,
                buttons: [{
                    extend: 'csvHtml5',
                    charset: "UTF-8",
                    bom: true,
                    text: '<b>CSV</b>',
                    className: 'btn btn-success btn-sm',
                    title: "Registros de movimientos",
                    fieldSeparator: ";",
                    exportOptions: {
                        columns: ':gt(0)',
                    }
                }],
                language: {
                    info: "Mostrando _START_ a _END_ de _TOTAL_ resultados"
                }
            });

            table.buttons().container().appendTo(`.input-group`);
            
            document.getElementById('imhere').focus();

            $('#datatable_filter').hide();
            $('#search-data').keyup(function(){table.search($(this).val()).draw();});

        }).catch(error => {
            if (error.status === 400) {}
            else {console.log(error)}
        });
        return false;
    }

    getSpanishRole(string) {
        if (string == "ADMIN") return "Administrador";
        else if (string == "OPERATOR") return "Operador";
        else if (string == "VALIDATOR") return "Validador";
        else return "Consultor";
    }
}

window.onload = () => {
    m = new Method();
    mvt = new Movement();
}