class Events {
    constructor() {
    }

    searchByFilter() {
        
        var size = m.getColumnsFromDb().length, values = m.getColumnsFromDb(), preset = ['well_name', 'location_name', 'reservoir_name', 'event_date', 'event_time', 'diluent', 'rpm', 'pump_efficiency', 'torque', 'frequency', 'current', 'power', 'mains_voltage', 'output_voltage', 'vfd_temperature', 'head_temperature', 'head_pressure', 'observations'];

        for (var i = 0; i < 18; i++) values[size + i] = preset[i];
        
        var range, typeSearch, reservoirId = [], collection = [];
        
        range = $("#filter-date").val() == 1 ?  m.getDatesRange($(`#date-pick`).val()) : m.getDatesRange(`${$(`#from`).val()}|${$(`#until`).val()}`);

        var whatIDo = document.querySelector(`#dual-list`).style.display == 'none';
        
        var from = moment(range[0]), until = moment(range[1]), isFrom = m.validateDate(from, 'msgFrom'), isUntil = m.validateDate(until, 'msgUntil');

        if (!whatIDo) {
            typeSearch = 1;
            var sizeIs = ($(`#location_well`).val()).length, store = $(`#location_well`).val();
            if (sizeIs > 1) for (var i = 0; i < sizeIs; i++) collection[i] = store[i];
            else collection = store[0];
        }

        if ((from.isValid() && isFrom) && (until.isValid() && isUntil)) {

            $(`#msgFrom, #msgUntil`).html(``);

            const ajax = new Ajax(`${globalConfig.appUrl}/wellEvents/searchByFilterEvents`, {
                headers: { accept: 'application/json' },
                method: 'POST',
                body: {
                    items: values,
                    data: collection,
                    macolla_id: parseInt($(`#macolla`).val()),
                    reservoir_id: reservoirId != null ? reservoirId : null,
                    date_from: range[0],
                    date_to: range[1],
                    type: typeSearch
                }
            });

            m.setLoadingMessageBefore();

            ajax.result().then(response => {

                var size = Object.keys(response.data).length;

                m.setLoadingMessageAfter();
                m.showMessageDialog(size);

                var header = m.setColumn(response.type, response.info)[0], column = m.setColumn(response.type, response.info)[1];

                let detail = (response.type == 1) ? header : m.selectedItems(response.total);
                var titleText = detail == 'SIN MACOLLA' ? detail : `Macolla ${detail}`;

                if (size > 0) {

                    m.setHtmlTable(1, "Eventos de pozo", `${titleText}`);
                    m.setHtmlTable(2);

                    $(`#header`).html(`<th class="text-muted">#</th><th>Localización</th><th>Pozo</th><th>Yacimiento</th><th class="text-muted">Fecha</th><th class="text-muted">Hora</th><th class="text-muted">Diluente</th><th class="text-muted">RPM</th><th class="text-muted">Bomba <h6 class="text-muted">eficiencia</h6></th><th class="text-muted">Torque <h6>Lb/pies</h6></th><th class="text-muted">Frecuencia <h6>Hz</h6></th><th class="text-muted">Corriente <h6>(A)</h6></th><th class="text-muted">Potencia <h6>Kw-H</h6></th><th class="text-muted">Tensión&nbsp;red <h6>(V)</h6></th><th class="text-muted">Tensión&nbsp;salida <h6>(V)</h6></th><th class="text-muted">VFD&nbsp;Temperatura <h6>ºF</h6></th><th class="text-muted">Temperatura&nbsp;cabezal <h6>ºF</h6></th><th class="text-muted">Presión&nbsp;cabezal <h6>lppc</h6></th><th class="text-muted">Observaciones</th>`);

                    var table = m.newDateTable(response.results, size, `Eventos ${titleText}`);

                    table.buttons().container().appendTo(`#buttons`);

                    $(`.dts_label`).remove();

                    document.querySelector('#imhere').focus();
                }
                
            }).catch(error => { console.log(error) });
        }
        return false;
    }
}

window.onload = () => {
    m = new Method();
    eve = new Events();
}