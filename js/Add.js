class Add {
	constructor() {
    }

    addExtendedTest() {

        $("#card-title").html(`<b>Cargar datos</b> / Pruebas Extendidas`);

        $("#main-options").html(`
            <div class="col-md-6">
                <h6>Macolla</h6>
                <select class="form-control" id="macolla" onchange="t.loadLocations()">
                    <option>Cargando...</option>
                </select>
            </div>

            <div class="col-md-6">
                <h6>Localización</h6>
                <select class="form-control" id="well_id">
                    <option value="">--</option>
                </select>
                <div id="wrapper-well_id"></div>
            </div>
            
            <div class="col-md-12 pt-3"><hr></div>

            <div class="col-md-4 pt-4">
                <h6>Prueba</h6>
                <input type="text" class="form-control" placeholder="Ejemplo: AA101M2000" id="test" maxlength="20" style="text-transform:uppercase" autocomplete="off" />
                <small class="text-muted">Formato: Localización, tipo de prueba y año.</small>
                <div id="wrapper-test"></div>
            </div>

            <div class="col-md-12 pt-0"></div>
            
            <div class="col-md-4 pt-4">
                <h6>Fecha</h6>
                <input type="date" class="form-control" id="extended_date" name="extended_date" />
                <div id="wrapper-extended_date"></div>
            </div>

            <div class="col-md-4 col-lg-2 pt-4">
                <h6>Hora inicial</h6>
                <input type="time" class="form-control" placeholder="Hora inicial" id="hour_start" />
                <div id="wrapper-hour_start"></div>
            </div>

            <div class="col-md-4 col-lg-2 pt-4">
                <h6>Hora final</h6>
                <input type="time" class="form-control" placeholder="Hora final" id="hour_end" />
                <div id="wrapper-hour_end"></div>
            </div>

            <div class="col-md-4 pt-4">
                <h6>Compañía</h6>
                <input type="text" class="form-control" placeholder="Compañía" id="company" maxlength="20" style="text-transform:uppercase" autocomplete="off" />
                <div id="wrapper-company"></div>
            </div>

            <div class="col-md-12 pt-4"><h5 class="text-info">Línea de Producción</h5></div>

            <div class="col-md-6 pt-3">
                <h6>Presión Línea Producción <i class="text-muted">lppc</i></h6>
                <input type="text" class="form-control" placeholder="Presión" id="line_pressure" maxlength="6" autocomplete="off" />
                <div id="wrapper-line_pressure"></div>
            </div>

            <div class="col-md-6 pt-3">
                <h6>Temp. Línea Producción <i class="text-muted">ºF</i></h6>
                <input type="text" class="form-control" placeholder="Temperatura" id="line_temperature" maxlength="6" autocomplete="off" />
                <div id="wrapper-line_temperature"></div>
            </div>

            <div class="col-md-12 pt-4"><h5 class="text-info">Tasas de Producción</h5></div>

            <div class="col-md-3 col-lg-3 pt-3">
                <h6>Tasa Prod. de Gas <i class="text-muted">MPCND</i></h6>
                <input type="text" class="form-control" placeholder="Producción de Gas" id="gas_rate_prod" maxlength="6" autocomplete="off" />
                <div id="wrapper-gas_rate_prod"></div>
            </div>

            <div class="col-md-3 col-lg-3 pt-3">
                <h6>Tasa Prod. de Agua <i class="text-muted">bpd</i></h6>
                <input type="text" class="form-control" placeholder="Producción de Agua" id="water_rate_prod" maxlength="6" autocomplete="off" />
                <div id="wrapper-water_rate_prod"></div>
            </div>

            <div class="col-md-3 col-lg-3 pt-3">
                <h6>Tasa Prod. de Crudo <i class="text-muted">bpd</i></h6>
                <input type="text" class="form-control" placeholder="Producción de Crudo" id="neat_rate" maxlength="6" autocomplete="off" />
                <div id="wrapper-neat_rate"></div>
            </div>

            <div class="col-md-3 col-lg-3 pt-3">
                <h6>Tasa Inyec. Diluente <i class="text-muted">bpd</i></h6>
                <input type="text" class="form-control" placeholder="Diluente inyectado" id="inyected_diluent_rate" maxlength="6" autocomplete="off" />
                <div id="wrapper-inyected_diluent_rate"></div>
            </div>

            <div class="col-md-12 pt-4"><hr></div>

            <div class="col-md-6 col-lg-6 pt-4">
                <h6>Barriles Totales <i class="text-muted">bpd</i></h6>
                <input type="text" class="form-control" placeholder="Barriles Totales" id="total_barrels" maxlength="6" autocomplete="off" />
                <div id="wrapper-total_barrels"></div>
            </div>

            <div class="col-md-6 col-lg-6 pt-4">
                <h6>Fracción Volumen Gas <i class="text-muted">porcentaje (%)</i></h6>
                <input type="text" class="form-control" placeholder="FVG" id="gvf" maxlength="6" autocomplete="off" />
                <div id="wrapper-gvf"></div>
            </div>

            <input type="hidden" value="P1" id="form-type" />
        `);

        var input = ['line_pressure', 'line_temperature', 'water_rate_prod', 'gas_rate_prod', 'total_barrels', 'neat_rate', 'gvf', 'inyected_diluent_rate'];
        
        for (let i = 0; i < input.length; i++) $(`#${input[i]}`).numeric({ negative: false });

        this.loadMacollas();
    }

    addAverageTest() {
        $("#card-title").html(`<b>Cargar datos</b> / Pruebas Promedio`);

        $("#main-options").html(`
            <div class="col-md-6">
                <h6>Macolla</h6>
                <select class="form-control" id="macolla" name="macolla" onchange="t.loadLocations()"><option value="">Cargando...</option></select>
            </div>

            <div class="col-md-6">
                <h6>Localización</h6>
                <select class="form-control" id="well_id" name="well_id"><option value="">--</option></select>
                <div id="wrapper-well_id"></div>
            </div>
            
            <div class="col-md-12 pt-3"><hr></div>

            <div class="col-md-4 pt-4">
                <h6>Empresa</h6>
                <input type="text" class="form-control" placeholder="Empresa" id="company" name="company" style="text-transform:uppercase" maxlength="20" autocomplete="off" />
                <div id="wrapper-company"></div>
            </div>

            <div class="col-md-4 pt-4">
                <h6>Inicia Prueba</h6>
                <input type="date" class="form-control" id="avg_start_test" name="avg_start_test" />
                <div id="wrapper-avg_start_test"></div>
            </div>

            <div class="col-md-4 pt-4">
                <h6>Finaliza Prueba</h6>
                <input type="date" class="form-control" id="avg_end_test" name="avg_end_test" />
                <div id="wrapper-avg_end_test"></div>
            </div>

            <div class="col-md-4 pt-4">
                <h6>RPM <i class="text-muted">BCP</i></h6>
                <input type="text" class="form-control" placeholder="RPM" id="rpm" name="rpm" maxlength="4" autocomplete="off" />
                <div id="wrapper-rpm"></div>
            </div>

            <div class="col-md-4 pt-4">
                <h6>Duración <i class="text-muted">Horas</i></h6>
                <input type="text" class="form-control" placeholder="Duración" id="duration" name="duration" maxlength="4" autocomplete="off" />
                <div id="wrapper-duration"></div>
            </div>

            <div class="col-md-12 pt-4"><h5 class="text-info">Líneas de Producción</h5></div>

            <div class="col-md-6 pt-3">
                <h6>Línea de Presión <i class="text-muted">lppc</i></h6>
                <input type="text" class="form-control" placeholder="Presión" id="line_pressure" name="line_pressure" maxlength="6" autocomplete="off" />
                <div id="wrapper-line_pressure"></div>
            </div>

            <div class="col-md-6 pt-3">
                <h6>Línea de Temperatura <i class="text-muted">ºF</i></h6>
                <input type="text" class="form-control" placeholder="Temperatura" id="line_temperature" name="line_temperature" maxlength="6" autocomplete="off" />
                <div id="wrapper-line_temperature"></div>
            </div>

            <div class="col-md-12 pt-4"><h5 class="text-info">Tasas de Producción</h5></div>

            <div class="col-md-3 pt-3">
                <h6>Tasa Prod. de Gas <i class="text-muted">MPCND</i></h6>
                <input type="text" class="form-control" placeholder="Tasa Producción de Gas" id="gas_rate_prod" name="gas_rate_prod" maxlength="6" autocomplete="off" />
                <div id="wrapper-gas_rate_prod"></div>
            </div>

            <div class="col-md-3 pt-3">
                <h6>Tasa Prod. de Agua <i class="text-muted">bpd</i></h6>
                <input type="text" class="form-control" placeholder="Tasa Producción de Agua" id="water_rate_prod" name="water_rate_prod" maxlength="6" autocomplete="off" />
                <div id="wrapper-water_rate_prod"></div>
            </div>

            <div class="col-md-3 pt-3">
                <h6>Tasa Fluidos Totales <i class="text-muted">BFPD</i></h6>
                <input type="text" class="form-control" placeholder="Tasa Fluidos Totales" id="fluid_total_rate" name="fluid_total_rate" maxlength="6" autocomplete="off" />
                <div id="wrapper-fluid_total_rate"></div>
            </div>

            <div class="col-md-3 pt-3">
                <h6>Tasa Prod. Neta <i class="text-muted">BPPD</i></h6>
                <input type="text" class="form-control" placeholder="Tasa Producción Neta" id="neat_rate_prod" name="neat_rate_prod" maxlength="6" autocomplete="off" />
                <div id="wrapper-neat_rate_prod"></div>
            </div>

            <div class="col-md-12 pt-4"><h5 class="text-info">Mezcla</h5></div>

            <div class="col-md-6 pt-3">
                <h6>% AyS de la Mezcla</h6>
                <input type="text" class="form-control" placeholder="% AyS Mezcla" id="percent_mixture" name="percent_mixture" maxlength="6" autocomplete="off" />
                <div id="wrapper-percent_mixture"></div>
            </div>

            <div class="col-md-6 pt-3">
                <h6>ºAPI de la Mezcla</h6>
                <input type="text" class="form-control" placeholder="ºAPI Mezcla" id="api_degree_mixture" name="api_degree_mixture" maxlength="6" autocomplete="off" />
                <div id="wrapper-api_degree_mixture"></div>
            </div>

            <div class="col-md-12 pt-4"><h5 class="text-info">Formación</h5></div>

            <div class="col-md-6 pt-3">
                <h6>% AyS de la Formación</h6>
                <input type="text" class="form-control" placeholder="% AyS Formación" id="percent_formation" name="percent_formation" maxlength="6" autocomplete="off" />
                <div id="wrapper-percent_formation"></div>
            </div>

            <div class="col-md-12 pt-4"><h5 class="text-info">Diluente</h5></div>

            <div class="col-md-6 pt-3">
                <h6>Tasa Inyección Diluente <i class="text-muted">BDPD</i></h6>
                <input type="text" class="form-control" placeholder="Tasa Inyección Diluente" id="inyected_diluent_rate" name="inyected_diluent_rate" maxlength="6" autocomplete="off" />
                <div id="wrapper-inyected_diluent_rate"></div>
            </div>
            
            <div class="col-md-6 pt-3">
                <h6>ºAPI Diluente</h6>
                <input type="text" class="form-control" placeholder="ºAPI Diluente" id="api_degree_diluent" name="api_degree_diluent" maxlength="6" autocomplete="off" />
                <div id="wrapper-api_degree_diluent"></div>
            </div>

            <div class="col-md-12 pt-4"><h5 class="text-info">Prueba</h5></div>

            <div class="col-md-3 pt-3">
                <h6>Tipo de Prueba</h6>
                <input type="text" class="form-control" placeholder="Prueba" id="test" name="test" maxlength="1" style="text-transform:uppercase" autocomplete="off" />
                <div id="wrapper-test"></div>
            </div>

            <div class="col-md-3 pt-3">
                <h6>PIP <i class="text-muted">lppc</i></h6>
                <input type="text" class="form-control" placeholder="PIP" id="pip" name="pip" maxlength="6" autocomplete="off" />
                <div id="wrapper-pip"></div>
            </div>

            <div class="col-md-6 pt-3">
                <h6>Fracción Volumen de Gas <i class="text-muted">porcentaje (%)</i></h6>
                <input type="text" class="form-control" placeholder="FVG" id="gvf" name="gvf" maxlength="6" autocomplete="off" />
                <div id="wrapper-gvf"></div>
            </div>

            <div class="col-md-12 pt-4"><hr></div>

            <div class="col-md-12 pt-3">
                <h6>Observaciones <i class="text-muted">120 caracteres</i></h6>
                <textarea class="form-control" style="max-height:80px" placeholder="Observaciones" id="observations" name="observations" maxlength="120"></textarea>
            </div>

            <input type="hidden" value="P2" id="form-type" />
        `);

        var input = ['rpm', 'duration', 'line_pressure', 'line_temperature', 'gas_rate_prod', 'water_rate_prod', 'fluid_total_rate', 'percent_mixture', 'api_degree_mixture', 'neat_rate_prod', 'percent_formation', 'gvf', 'inyected_diluent_rate', 'api_degree_diluent', 'pip'];
        
        for (let i = 0; i < input.length; i++) $(`#${input[i]}`).numeric({ negative: false });

        this.loadMacollas();
    }

    addSign() {
        $("#card-title").html(`<b>Cargar datos</b> / Muestras`);

        $("#main-options").html(`
            <div class="col-md-6">
                <h6>Macolla</h6>
                <select class="form-control" id="macolla" name="macolla" onchange="t.loadLocations()"><option value="">Cargando...</option></select>
            </div>
            
            <div class="col-md-6">
                <h6>Localización</h6>
                <select class="form-control" id="well_id" name="well_id"><option value="">--</option></select>
                <div id="wrapper-well_id"></div>
            </div>
            
            <div class="col-md-12 pt-3"><hr></div>
            
            <div class="col-md-4 pt-4">
                <h6>Fecha</h6>
                <input type="date" class="form-control" id="sign_date" name="sign_date" />
                <div id="wrapper-sign_date"></div>
            </div>
            
            <div class="col-md-4 pt-4">
                <h6>Hora</h6>
                <input type="time" class="form-control" id="sign_time" name="sign_time" />
                <div id="wrapper-sign_time"></div>
            </div>

            <div class="col-md-4 pt-4">
                <h6>Compañía</h6>
                <input type="text" class="form-control" placeholder="Compañía" id="company" name="company" maxlength="20" style="text-transform:uppercase" autocomplete="off" />
                <div id="wrapper-company"></div>
            </div>
            
            <div class="col-md-4 pt-4">
                <h6>ºAPI</h6>
                <input type="text" class="form-control" placeholder="ºAPI" id="api_degree_diluted" name="api_degree_diluted" maxlength="6" autocomplete="off" />
                <div id="wrapper-api_degree_diluted"></div>
            </div>
            
            <div class="col-md-4 pt-4">
                <h6>% AyS</h6>
                <input type="text" class="form-control" placeholder="% AyS" id="percent_mixture" name="percent_mixture" maxlength="6" autocomplete="off" />
                <div id="wrapper-percent_mixture"></div>
            </div>
            
            <div class="col-md-4 pt-4">
                <h6>% Sedimentos</h6>
                <input type="text" class="form-control" placeholder="% Sedimentos" id="percent_sediment" name="percent_sediment" maxlength="6" autocomplete="off" />
                <div id="wrapper-percent_sediment"></div>
            </div>
            
            <div class="col-md-4 pt-4">
                <h6>PTB</h6>
                <input type="text" class="form-control" placeholder="PTB" id="ptb" name="ptb" maxlength="6" autocomplete="off" />
                <div id="wrapper-ptb"></div>
            </div>

            <input type="hidden" value="P3" id="form-type" />
        `);

        var input = ['percent_mixture', 'percent_sediment', 'api_degree_diluted', 'ptb'];
        
        for (let i = 0; i < input.length; i++) $(`#${input[i]}`).numeric({ negative: false });

        this.loadMacollas();
    }

    addPressureAndTemp() {

        $("#card-title").html(`<b>Cargar datos</b> / Presión y Temperatura Original`);

        $("#main-options").html(`
            <div class="col-md-6">
                <h6>Macolla</h6>
                <select class="form-control" id="macolla" name="macolla" onchange="t.loadLocations()"><option value="">Cargando...</option></select>
            </div>
            
            <div class="col-md-6">
                <h6>Localización</h6>
                <select class="form-control" id="well_id" name="well_id"><option value="">--</option></select>
                <div id="wrapper-well_id"></div>
            </div>
            
            <div class="col-md-12 pt-3"><hr></div>
            
            <div class="col-md-3 pt-4">
                <h6>Fecha</h6>
                <input type="date" class="form-control" id="pt_date" name="pt_date">
                <div id="wrapper-pt_date"></div>
            </div>
            
            <div class="col-md-3 pt-4">
                <h6>Compañía</h6>
                <input type="text" class="form-control" placeholder="Compañía" id="company" name="company" style="text-transform:uppercase" maxlength="20" autocomplete="off" />
                <div id="wrapper-company"></div>
            </div>

            <div class="col-md-3 pt-4">
                <h6>Yacimiento</h6>
                <select type="text" class="form-control" placeholder="Yacimiento" id="reservoir" name="reservoir"><option value="">Cargando...</option></select>
                <div id="wrapper-reservoir"></div>
            </div>

            <div class="col-md-3 pt-4">
                <h6>Arena</h6>
                <input type="text" class="form-control" placeholder="Arena" id="sand" name="sand" style="text-transform:uppercase" maxlength="20" autocomplete="off" />
                <div id="wrapper-sand"></div>
            </div>

            <div class="col-md-3 pt-4">
                <h6>Prueba </h6>
                <input type="text" class="form-control" placeholder="Prueba" id="test" name="test" style="text-transform:uppercase" maxlength="10" autocomplete="off" />
                <div id="wrapper-test"></div>
            </div>

            <div class="col-md-3 pt-4">
                <h6>Temperatura <i class="text-muted">ºF</i></h6>
                <input type="text" class="form-control" placeholder="Temperatura" id="temperature" name="temperature" maxlength="6" autocomplete="off" />
                <div id="wrapper-temperature"></div>
            </div>

            <div class="col-md-3 pt-4">
                <h6>Duración <i class="text-muted">minutos</i></h6>
                <input type="text" class="form-control" placeholder="Duración" id="duration" name="duration" maxlength="4" autocomplete="off" />
                <div id="wrapper-duration"></div>
            </div>

            <div class="col-md-3 pt-4">
                <h6>Movilidad <i class="text-muted">mD/cP</i></h6>
                <input type="text" class="form-control" placeholder="Movilidad" id="mobility" name="mobility" maxlength="6" autocomplete="off" />
                <div id="wrapper-mobility"></div>
            </div>
            
            <div class="col-md-12 pt-4"><h5 class="text-info">Profundidad</h5></div>

            <div class="col-md-3 pt-3">
                <h6>MD <i class="text-muted">pies</i></h6>
                <input type="text" class="form-control" placeholder="Profundidad MD" id="deep_md" name="deep_md" maxlength="6" autocomplete="off" />
                <div id="wrapper-deep_md"></div>
            </div>

            <div class="col-md-3 pt-3">
                <h6>TVD <i class="text-muted">pies</i></h6>
                <input type="text" class="form-control" placeholder="Profundidad TVD" id="deep_tvd" name="deep_tvd" maxlength="6" autocomplete="off" />
                <div id="wrapper-deep_tvd"></div>
            </div>

            <div class="col-md-12 pt-4"><h5 class="text-info">Presión</h5></div>

            <div class="col-md-3 pt-3">
                <h6>Presión Hid. Antes <i class="text-muted">lppc</i></h6>
                <input type="text" class="form-control" placeholder="Presión Hid. Antes" id="pressbefore" name="pressbefore" maxlength="6" autocomplete="off" />
                <div id="wrapper-pressbefore"></div>
            </div>

            <div class="col-md-3 pt-3">
                <h6>Presión Hid. Después <i class="text-muted">lppc</i></h6>
                <input type="text" class="form-control" placeholder="Presión Hid. Después" id="pressafter" name="pressafter" maxlength="6" autocomplete="off" />
                <div id="wrapper-pressafter"></div>
            </div>

            <div class="col-md-3 pt-3">
                <h6>Presión de Formación <i class="text-muted">lppc</i></h6>
                <input type="text" class="form-control" placeholder="Presión de Formación" id="pressformation" name="pressformation" maxlength="6" autocomplete="off" />
                <div id="wrapper-pressformation"></div>
            </div>
            
            <div class="col-md-12 pt-4"><hr></div>

            <div class="col-md-12 pt-4">
            <h6>Observaciones <i class="text-muted">120 caracteres</i></h6>
            <textarea class="form-control" style="max-height:80px" placeholder="Observaciones" id="observations" name="observations" maxlength="120"></textarea>
            </div>

            <input type="hidden" value="C1" id="form-type" />
        `);

        var input = ['deep_md', 'deep_tvd', 'temperature', 'duration', 'pressbefore', 'pressafter', 'pressformation', 'mobility'];

        for (let i = 0; i < input.length; i++) $(`#${input[i]}`).numeric({ negative: false });

        this.loadMacollas();
        this.loadReservoirs();
    }

    addHistorical() {

        $("#card-title").html(`<b>Cargar datos</b> / Histórico Completaciones`);

        $("#main-options").html(`
            <div class="col-md-6">
                <h6>Macolla</h6>
                <select class="form-control" id="macolla" name="macolla" onchange="t.loadLocations()"><option value="">Cargando...</option></select>
            </div>
            
            <div class="col-md-6">
                <h6>Localización</h6>
                <select class="form-control" id="well_id" name="well_id"><option value="">--</option></select>
                <div id="wrapper-well_id"></div>
            </div>
            
            <div class="col-md-12 pt-3"><hr></div>
            
            <div class="col-md-4 pt-4">
                <h6>Fecha y Hora inicial</h6>
                <input type="datetime-local" class="form-control" id="begin_date" name="begin_date" />
                <div id="wrapper-begin_date"></div>
            </div>

            <div class="col-md-4 pt-4">
                <h6>Fecha y Hora final</h6>
                <input type="datetime-local" class="form-control" id="end_date" name="end_date" />
                <div id="wrapper-end_date"></div>
            </div>

            <div class="col-md-12 pt-0"></div>

            <div class="col-md-3 pt-4">
                <h6>Tipo de Trabajo</h6>
                <input type="text" class="form-control" placeholder="Tipo de Trabajo" id="type_work_i" name="type_work_i" style="text-transform:uppercase" maxlength="20" autocomplete="off" />
                <div id="wrapper-type_work_i"></div>
            </div>

            <div class="col-md-3 pt-4">
                <h6>Tipo de Trabajo II</h6>
                <input type="text" class="form-control" placeholder="Tipo de Trabajo II" id="type_work_ii" name="type_work_ii" style="text-transform:uppercase" maxlength="20" autocomplete="off" />
                <div id="wrapper-type_work_ii"></div>
            </div>
            
            <div class="col-md-3 pt-4">
                <h6>Equipo</h6>
                <input type="text" class="form-control" placeholder="Equipo" id="rig" name="rig" style="text-transform:uppercase" maxlength="20" autocomplete="off" />
                <div id="wrapper-rig"></div>
            </div>

            <div class="col-md-3 pt-4">
                <h6>Yacimiento</h6>
                <select type="text" class="form-control" placeholder="Yacimiento" id="reservoir" name="reservoir"><option value="">Cargando...</option></select>
                <div id="wrapper-reservoir"></div>
            </div>

            <div class="col-md-12 pt-4"><h5 class="text-info">Sensor</h5></div>

            <div class="col-md-4 pt-3">
                <h6>Marca de Sensor</h6>
                <input type="text" class="form-control" placeholder="Marca de Sensor" id="sensor_brand" name="sensor_brand" style="text-transform:uppercase" maxlength="20" autocomplete="off" />
                <div id="wrapper-sensor_brand"></div>
            </div>

            <div class="col-md-4 pt-3">
                <h6>Modelo de Sensor</h6>
                <input type="text" class="form-control" placeholder="Modelo de Sensor" id="sensor_model" name="sensor_model" style="text-transform:uppercase" maxlength="20" autocomplete="off" />
                <div id="wrapper-sensor_model"></div>
            </div>

            <div class="col-md-4 pt-3">
                <h6>Compañía de Sensor</h6>
                <input type="text" class="form-control" placeholder="Compañía de Sensor" id="sensor_company" name="sensor_company" style="text-transform:uppercase" maxlength="20" autocomplete="off" />
                <div id="wrapper-sensor_company"></div>
            </div>

            <div class="col-md-4 pt-3">
                <h6>Diámetro de Sensor <i class="text-muted">pulgadas</i></h6>
                <input type="text" class="form-control" placeholder="Diámetro de Sensor" id="sensor_diameter" name="sensor_diameter" maxlength="6" autocomplete="off" />
                <div id="wrapper-sensor_diameter"></div>
            </div>

            <div class="col-md-4 pt-3">
                <h6>Prof. Superior Sensor MD <i class="text-muted">pies</i></h6>
                <input type="text" class="form-control" placeholder="Prof. Superior MD Sensor" id="deeptop_sensor_md" name="deeptop_sensor_md" maxlength="6" autocomplete="off" />
                <div id="wrapper-deeptop_sensor_md"></div>
            </div>

            <div class="col-md-4 pt-3">
                <h6>Prof. Inferior Sensor MD <i class="text-muted">pies</i></h6>
                <input type="text" class="form-control" placeholder="Prof. Inferior MD Sensor" id="deepbottom_sensor_md" name="deepbottom_sensor_md" maxlength="6" autocomplete="off" />
                <div id="wrapper-deepbottom_sensor_md"></div>
            </div>

            <div class="col-md-12 pt-4"><h5 class="text-info">Bomba</h5></div>

            <div class="col-md-4 pt-3">
                <h6>Modelo de Bomba</h6>
                <input type="text" class="form-control" placeholder="Modelo de Bomba" id="pump_model" name="pump_model" style="text-transform:uppercase" maxlength="20" autocomplete="off" />
                <div id="wrapper-pump_model"></div>
            </div>

            <div class="col-md-4 pt-3">
                <h6>Compañía de Bomba</h6>
                <input type="text" class="form-control" placeholder="Compañía de Bomba" id="pump_company" name="pump_company" style="text-transform:uppercase" maxlength="20" autocomplete="off" />
                <div id="wrapper-pump_company"></div>
            </div>

            <div class="col-md-4 pt-3">
                <h6>Diámetro de Bomba <i class="text-muted">pulgadas</i></h6>
                <input type="text" class="form-control" placeholder="Diámetro de Bomba" id="pump_diameter" name="pump_diameter" maxlength="6" autocomplete="off" />
                <div id="wrapper-pump_diameter"></div>
            </div>

            <div class="col-md-6 pt-3">
                <h6>Tipo de Bomba</h6>
                <input type="text" class="form-control" placeholder="Tipo de Bomba" id="pump_type" name="pump_type" style="text-transform:uppercase" maxlength="20" autocomplete="off" />
                <div id="wrapper-pump_type"></div>
            </div>

            <div class="col-md-6 pt-3">
                <h6>Capacidad de Bomba <i class="text-muted">BPD/100 RPM</i></h6>
                <input type="text" class="form-control" placeholder="Capacidad de Bomba" id="pump_capacity" name="pump_capacity" maxlength="6" autocomplete="off" />
                <div id="wrapper-pump_capacity"></div>
            </div>

            <div class="col-md-6 pt-3">
                <h6>Prof. Superior Bomba MD <i class="text-muted">pies</i></h6>
                <input type="text" class="form-control" placeholder="Prof. Superior Bomba MD" id="deeptop_pump_md" name="deeptop_pump_md" maxlength="6" autocomplete="off" />
                <div id="wrapper-deeptop_pump_md"></div>
            </div>

            <div class="col-md-6 pt-3">
                <h6>Prof. Inferior Bomba MD <i class="text-muted">pies</i></h6>
                <input type="text" class="form-control" placeholder="Prof. Inferior Bomba MD" id="deepbottom_pump_md" name="deepbottom_pump_md" maxlength="6" autocomplete="off" />
                <div id="wrapper-deepbottom_pump_md"></div>
            </div>
            
            <div class="col-md-12 pt-4"><hr></div>

            <div class="col-md-6 pt-3">
                <h6>Inyección de Diluente</h6>
                <input type="text" class="form-control" placeholder="Inyección de Diluente" id="inyected_diluent" name="inyected_diluent" style="text-transform:uppercase" maxlength="20" autocomplete="off"/>
                <div id="wrapper-inyected_diluent"></div>
            </div>

            <div class="col-md-6 pt-3">
                <h6>Medidas desde</h6>
                <input type="text" class="form-control" placeholder="Medidas desde" id="measure_from" name="measure_from" style="text-transform:uppercase" maxlength="20" autocomplete="off" />
                <div id="wrapper-measure_from"></div>
            </div>

            <div class="col-md-12 pt-4"><hr></div>
            
            <div class="col-md-8 pt-4">
            <h6>Observaciones <i class="text-muted">120 caracteres</i></h6>
            <textarea class="form-control" style="max-height:80px" placeholder="Observaciones" id="observations" name="observations" maxlength="120"></textarea>
            </div>

            <div class="col-md-4 pt-4">
                <h6>Referencias</h6>
                <input type="text" class="form-control" placeholder="Referencias" id="references" name="references" style="text-transform:uppercase" maxlength="20" autocomplete="off" />
            </div>

            <input type="hidden" value="C2" id="form-type" />
        `);

        var input = ['sensor_diameter', 'deeptop_sensor_md', 'deepbottom_sensor_md', 'pump_diameter', 'pump_capacity', 'deeptop_pump_md', 'deepbottom_pump_md'];

        for (let i = 0; i < input.length; i++) $(`#${input[i]}`).numeric({ negative: false });

        this.loadMacollas();
        this.loadReservoirs();

    }

    addStatic() {

        $("#card-title").html(`<b>Cargar datos</b> / Presión Estática`);

        $("#main-options").html(`
            <div class="col-md-6">
                <h6>Macolla</h6>
                <select class="form-control" id="macolla" name="macolla" onchange="t.loadLocations()"><option value="">Cargando...</option></select>
            </div>
            
            <div class="col-md-6">
                <h6>Localización</h6>
                <select class="form-control" id="well_id" name="well_id"><option value="">--</option></select>
                <div id="wrapper-well_id"></div>
            </div>
            
            <div class="col-md-12 pt-3"><hr></div>
            
            <div class="col-md-4 pt-3">
                <h6>Fecha y Hora</h6>
                <input type="datetime-local" class="form-control" id="static_date" name="static_date" />
                <div id="wrapper-static_date"></div>
            </div>

            <div class="col-md-12 pt-0"></div>

            <div class="col-md-3 pt-4">
                <h6>Calidad</h6>
                <select class="form-control" id="quality" name="quality">
                    <option value="">--</option>
                    <option value="B">B</option>
                    <option value="M">M</option>
                    <option value="R">R</option>
                </select>
                <div id="wrapper-quality"></div>
            </div>

            <div class="col-md-3 pt-4">
                <h6>Pe)EB <i class="text-muted">lppc</i></h6>
                <input type="text" class="form-control" placeholder="PIP" id="pip" name="pip" maxlength="6" autocomplete="off" />
                <div id="wrapper-pip"></div>
            </div>

            <div class="col-md-3 pt-4">
                <h6>PDPe)SB <i class="text-muted">lppc</i></h6>
                <input type="text" class="form-control" placeholder="PDP" id="pdp" name="pdp" maxlength="6" autocomplete="off" />
                <div id="wrapper-pdp"></div>
            </div>

            <div class="col-md-3 pt-4">
                <h6>Te)EB <i class="text-muted">ºF</i></h6>
                <input type="text" class="form-control" placeholder="Temperatura" id="temperature" name="temperature" maxlength="6" autocomplete="off" />
                <div id="wrapper-temperature"></div>
            </div>

            <div class="col-md-12 pt-3"><hr></div>

            <div class="col-md-12 pt-3">
                <h6>Observaciones <i class="text-muted">120 caracteres</i></h6>
                <textarea class="form-control" style="max-height:80px" placeholder="Observaciones" id="observations" name="observations" maxlength="120"></textarea>
            </div>

            <input type="hidden" value="C3" id="form-type" />
        `);

        var input = ['temperature'];
        
        for (let i = 0; i < input.length; i++) $(`#${input[i]}`).numeric({ negative: false });
        
        $(`#pdp, #pip`).numeric({ decimal: false, negative: false });

        this.loadMacollas();
    }

    addDynamic() {

        $("#card-title").html(`<b>Cargar datos</b> / Presión Dinámica`);

        $("#main-options").html(`
            <div class="col-md-6">
                <h6>Macolla</h6>
                <select class="form-control" id="macolla" name="macolla" onchange="t.loadLocations()"><option value="">Cargando...</option></select>
            </div>
            
            <div class="col-md-6">
                <h6>Localización</h6>
                <select class="form-control" id="well_id" name="well_id"><option value="">--</option></select>
                <div id="wrapper-well_id"></div>
            </div>
            
            <div class="col-md-12 pt-3"><hr></div>
            
            <div class="col-md-4 pt-3">
                <h6>Fecha y Hora</h6>
                <input type="datetime-local" class="form-control" id="dynamic_date" name="dynamic_date" />
                <div id="wrapper-dynamic_date"></div>
            </div>

            <div class="col-md-12 pt-0"></div>

            <div class="col-md-3 pt-4">
                <h6>Calidad</h6>
                <select class="form-control" id="quality" name="quality">
                    <option value="">--</option>
                    <option value="B">B</option>
                    <option value="M">M</option>
                    <option value="R">R</option>
                </select>
                <div id="wrapper-quality"></div>
            </div>

            <div class="col-md-3 pt-4">
                <h6>Pwf)EB <i class="text-muted">lppc</i></h6>
                <input type="text" class="form-control" placeholder="PIP" id="pip" name="pip" maxlength="6" autocomplete="off" />
                <div id="wrapper-pip"></div>
            </div>

            <div class="col-md-3 pt-4">
                <h6>PDP)SB <i class="text-muted">lppc</i></h6>
                <input type="text" class="form-control" placeholder="PDP" id="pdp" name="pdp" maxlength="6" autocomplete="off" />
                <div id="wrapper-pdp"></div>
            </div>

            <div class="col-md-3 pt-4">
                <h6>Twf)EB <i class="text-muted">ºF</i></h6>
                <input type="text" class="form-control" placeholder="Temperatura" id="temperature" name="temperature" maxlength="6" autocomplete="off" />
                <div id="wrapper-temperature"></div>
            </div>

            <div class="col-md-12 pt-3"><hr></div>

            <div class="col-md-12 pt-3">
                <h6>Observaciones <i class="text-muted">120 caracteres</i></h6>
                <textarea class="form-control" style="max-height:80px" placeholder="Observaciones" id="observations" name="observations" maxlength="120"></textarea>
            </div>

            <input type="hidden" value="C4" id="form-type" />
        `);

        var input = ['temperature'];
        
        for (let i = 0; i < input.length; i++) $(`#${input[i]}`).numeric({ negative: false });
        
        $(`#pdp, #pip`).numeric({ decimal: false, negative: false });

        this.loadMacollas();
    }

    addOriginal() {

        $("#card-title").html(`<b>Cargar datos</b> / Presión Original PMVM`);

        $("#main-options").html(`
            <div class="col-md-6">
                <h6>Macolla</h6>
                <select class="form-control" id="macolla" name="macolla" onchange="t.loadLocations()"><option value="">Cargando...</option></select>
            </div>
            
            <div class="col-md-6">
                <h6>Localización</h6>
                <select class="form-control" id="well_id" name="well_id"><option value="">--</option></select>
                <div id="wrapper-well_id"></div>
            </div>
            
            <div class="col-md-12 pt-3"><hr></div>
            
            <div class="col-md-4 pt-3">
                <h6>Fecha</h6>
                <input type="date" class="form-control" id="pressure_date" name="pressure_date" />
                <div id="wrapper-pressure_date"></div>
            </div>

            <div class="col-md-12 pt-0"></div>

            <div class="col-md-3 pt-4">
                <h6>Yacimiento</h6>
                <select type="text" class="form-control" placeholder="Yacimiento" id="reservoir" name="reservoir"><option value="">Cargando...</option></select>
                <div id="wrapper-reservoir"></div>
            </div>

            <div class="col-md-4 pt-4">
                <h6>Tipo de Prueba</h6>
                <input type="text" class="form-control" placeholder="Tipo de Prueba" id="type_test" name="type_test" maxlength="5" style="text-transform:uppercase" autocomplete="off" />
                <div id="wrapper-type_test"></div>
            </div>

            <div class="col-md-4 pt-4">
                <h6>Modelo de Sensor</h6>
                <input type="text" class="form-control" placeholder="Modelo de Sensor" id="sensor_model" name="sensor_model"  maxlength="20" style="text-transform:uppercase" autocomplete="off" />
                <div id="wrapper-sensor_model"></div>
            </div>

            <div class="col-md-12 pt-4"><h5 class="text-info">Gradiente</h5></div>

            <div class="col-md-4 pt-3">
                <h6>Presión <i class="text-muted">lppc/pies</i></h6>
                <input type="text" class="form-control" placeholder="Presión" id="pressure_gradient" name="pressure_gradient"  maxlength="6" autocomplete="off" />
                <div id="wrapper-pressure_gradient"></div>
            </div>

            <div class="col-md-4 pt-3">
                <h6>Temperatura <i class="text-muted">ºF/pies</i></h6>
                <input type="text" class="form-control" placeholder="Temperatura" id="temperature_gradient" name="temperature_gradient" maxlength="6" autocomplete="off" />
                <div id="wrapper-temperature_gradient"></div>
            </div>

            <div class="col-md-12 pt-4"><h5 class="text-info">DATUM Macolla</h5></div>

            <div class="col-md-6 pt-3">
                <h6>Profundidad MD <i class="text-muted">pies</i></h6>
                <input type="text" class="form-control" placeholder="Profundidad MD" id="deepdatum_mc_md" name="deepdatum_mc_md" maxlength="6" autocomplete="off" />
                <div id="wrapper-deepdatum_mc_md"></div>
            </div>

            <div class="col-md-6 pt-3">
                <h6>Profundidad TVD <i class="text-muted">pies</i></h6>
                <input type="text" class="form-control" placeholder="Profundidad TVD" id="deepdatum_mc_tvd" name="deepdatum_mc_tvd" maxlength="6" autocomplete="off" />
                <div id="wrapper-deepdatum_mc_tvd"></div>
            </div>

            <div class="col-md-6 pt-3">
                <h6>Presión Original <i class="text-muted">lppc</i></h6>
                <input type="text" class="form-control" placeholder="Presión Original" id="press_datum_macolla" name="press_datum_macolla" maxlength="6" autocomplete="off" />
                <div id="wrapper-press_datum_macolla"></div>
            </div>

            <div class="col-md-6 pt-3">
                <h6>Temperatura Original <i class="text-muted">ºF</i></h6>
                <input type="text" class="form-control" placeholder="Temperatura Original" id="temp_datum_macolla" name="temp_datum_macolla" maxlength="6" autocomplete="off" />
                <div id="wrapper-temp_datum_macolla"></div>
            </div>

            <input type="hidden" value="C5" id="form-type" />
        `);

        var input = ['pressure_gradient', 'deepdatum_mc_md', 'deepdatum_mc_tvd', 'press_datum_macolla', 'temp_datum_macolla', 'temperature_gradient']
        
        for (let i = 0; i < input.length; i++) $(`#${input[i]}`).numeric({ negative: false });

        this.loadMacollas();
        this.loadReservoirs();
    }

    addSurveyReal() {

        $("#card-title").html(`<b>Cargar datos</b> / Survey Real`);

        $("#main-options").html(`
            <div class="col-md-6">
                <h6>Macolla</h6>
                <select class="form-control" id="macolla" name="macolla" onchange="t.loadLocations()"><option value="">Cargando...</option></select>
            </div>
            
            <div class="col-md-6">
                <h6>Localización</h6>
                <select class="form-control" id="well_id" name="well_id"><option value="">--</option></select>
                <div id="wrapper-well_id"></div>
            </div>
            
            <div class="col-md-12 pt-3"><hr></div>
            
            <div class="col-md-4 pt-3">
                <h6>Fecha</h6>
                <input type="date" class="form-control" id="rs_date" name="rs_date" />
                <div id="wrapper-rs_date"></div>
            </div>

            <div class="col-md-12 pt-0"></div>

            <div class="col-md-4 pt-4">
                <h6>Hoyo</h6>
                <input type="text" class="form-control" placeholder="Hoyo" id="hole" name="hole" style="text-transform:uppercase" maxlength="20" autocomplete="off" />
                <div id="wrapper-hole"></div>
            </div>

            <div class="col-md-4 pt-4">
                <h6>Compañía</h6>
                <input type="text" class="form-control" placeholder="Compañía" id="company" name="company" style="text-transform:uppercase" maxlength="20" autocomplete="off" />
                <div id="wrapper-company"></div>
            </div>

            <div class="col-md-4 pt-4"></div>

            <div class="col-md-4 pt-4">
                <h6>MD <i class="text-muted">pies</i></h6>
                <input type="text" class="form-control" placeholder="MD" id="md" name="md" maxlength="6" autocomplete="off" />
                <div id="wrapper-md"></div>
            </div>

            <div class="col-md-4 pt-4">
                <h6>TVD <i class="text-muted">pies</i></h6>
                <input type="text" class="form-control" placeholder="TVD" id="tvd" name="tvd" maxlength="6" autocomplete="off" />
                <div id="wrapper-tvd"></div>
            </div>

            <div class="col-md-4 pt-4">
                <h6>Sección Vertical <i class="text-muted">pies</i></h6>
                <input type="text" class="form-control" placeholder="Sección Vertical" id="vsec" name="vsec" maxlength="6" autocomplete="off" />
                <div id="wrapper-vsec"></div>
            </div>

            <div class="col-md-4 pt-4">
                <h6>Inclinación <i class="text-muted">grados</i></h6>
                <input type="text" class="form-control" placeholder="Inclinación" id="inclination" name="inclination" maxlength="6" autocomplete="off" />
                <div id="wrapper-inclination"></div>
            </div>

            <div class="col-md-4 pt-4">
                <h6>AZIM GRID <i class="text-muted">grados</i></h6>
                <input type="text" class="form-control" placeholder="AZIM GRID" id="azim_grid" name="azim_grid" maxlength="6" autocomplete="off" />
                <div id="wrapper-azim_grid"></div>
            </div>

            <div class="col-md-4 pt-4">
                <h6>DLS <i class="text-muted">(°/100 pies)</i></h6>
                <input type="number" class="form-control" placeholder="Este-Oeste" id="dls" name="dls" min="-1" max="1000" maxlength="6" autocomplete="off" />
                <div id="wrapper-dls"></div>
            </div>

            <div class="col-md-12 pt-4"><h5 class="text-info">Coordenadas</h5></div>
           
            <div class="col-md-6 pt-3">
                <h6>Norte/Sur <i class="text-muted">(N/S pies)</i></h6>
                <input type="text" class="form-control" placeholder="Norte-Sur" id="ns" name="ns" maxlength="15" autocomplete="off" />
                <div id="wrapper-ns"></div>
            </div>

            <div class="col-md-6 pt-3">
                <h6>Este/Oeste <i class="text-muted">(E/O pies)</i></h6>
                <input type="text" class="form-control" placeholder="Este-Oeste" id="ew" name="ew" maxlength="15" autocomplete="off" />
                <div id="wrapper-ew"></div>
            </div>

            <div class="col-md-12 pt-4"><h5 class="text-info">Ubicación</h5></div>

            <div class="col-md-6 pt-3">
                <h6>Latitud <i class="text-muted">(N/S ° ' ")</i></h6>
                <div class="row">
                    <div class="col-md-3 pt-1">
                        <input type="text" class="form-control" id="la_g" name="la_g" maxlength="5" autocomplete="off" />
                    </div>
                    <div class="col-md-3 pt-1">
                        <input type="text" class="form-control" id="la_m" name="la_m" maxlength="5" autocomplete="off" />
                    </div>
                    <div class="col-md-3 pt-1">
                        <input type="text" class="form-control" id="la_s" name="la_s" maxlength="5" autocomplete="off" />
                    </div>
                </div>
            </div>

            <div class="col-md-6 pt-3">
                <h6>Longitud <i class="text-muted">(E/O ° ' ")</i></h6>
                <div class="row">
                    <div class="col-md-3 pt-1">
                        <input type="text" class="form-control" id="lo_g" name="lo_g" maxlength="5" autocomplete="off" />
                    </div>
                    <div class="col-md-3 pt-1">
                        <input type="text" class="form-control" id="lo_m" name="lo_m" maxlength="5" autocomplete="off" />
                    </div>
                    <div class="col-md-3 pt-1">
                        <input type="text" class="form-control" id="lo_s" name="lo_s" maxlength="5" autocomplete="off" />
                    </div>
                </div>
            </div>

            <div class="col-md-12 pt-3"><hr></div>

            <div class="col-md-12 pt-3">
                <h6>Observaciones <i class="text-muted">120 caracteres</i></h6>
                <textarea class="form-control" style="max-height:80px" placeholder="Observaciones" id="observations" name="observations" maxlength="120"></textarea>
            </div>

            <input type="hidden" value="C6" id="form-type" />
        `);
        
        $(`#md, #inclination, #azim_grid, #tvd, #vsec`).numeric({ negative: false });
        $(`#ns, #ew, #la_g, #la_m, #la_s, #lo_g, #lo_m, #lo_s`).numeric({ negative: true});

        this.loadMacollas();
    }

    addSurveyPlan() {

        $("#card-title").html(`<b>Cargar datos</b> / Survey Plan`);

        $("#main-options").html(`
            <div class="col-md-6">
                <h6>Macolla</h6>
                <select class="form-control" id="macolla" name="macolla" onchange="t.loadLocations()"><option value="">Cargando...</option></select>
            </div>
            
            <div class="col-md-6">
                <h6>Localización</h6>
                <select class="form-control" id="well_id" name="well_id"><option value="">--</option></select>
                <div id="wrapper-well_id"></div>
            </div>
            
            <div class="col-md-12 pt-3"><hr></div>
            
            <div class="col-md-4 pt-3">
                <h6>Fecha</h6>
                <input type="date" class="form-control" id="ps_date" name="ps_date" />
                <div id="wrapper-ps_date"></div>
            </div>

            <div class="col-md-12 pt-0"></div>

            <div class="col-md-4 pt-4">
                <h6>MD <i class="text-muted">pies</i></h6>
                <input type="text" class="form-control" placeholder="MD" id="md" name="md" maxlength="6" autocomplete="off" />
                <div id="wrapper-md"></div>
            </div>

            <div class="col-md-4 pt-4">
                <h6>TVD <i class="text-muted">pies</i></h6>
                <input type="text" class="form-control" placeholder="TVD" id="tvd" name="tvd" maxlength="6" autocomplete="off" />
                <div id="wrapper-tvd"></div>
            </div>

            <div class="col-md-4 pt-4">
                <h6>Sección Vertical <i class="text-muted">pies</i></h6>
                <input type="text" class="form-control" placeholder="Sección Vertical" id="vsec" name="vsec" maxlength="6" autocomplete="off" />
                <div id="wrapper-vsec"></div>
            </div>

            <div class="col-md-4 pt-4">
                <h6>Inclinación <i class="text-muted">grados</i></h6>
                <input type="text" class="form-control" placeholder="Inclinación" id="inclination" name="inclination" maxlength="6" autocomplete="off" />
                <div id="wrapper-inclination"></div>
            </div>

            <div class="col-md-4 pt-4">
                <h6>AZIM GRID <i class="text-muted">grados</i></h6>
                <input type="text" class="form-control" placeholder="AZIM GRID" id="azim_grid" name="azim_grid" maxlength="6" autocomplete="off" />
                <div id="wrapper-azim_grid"></div>
            </div>

            <div class="col-md-4 pt-4">
                <h6>DLS <i class="text-muted">(°/100 pies)</i></h6>
                <input type="number" class="form-control" placeholder="Este-Oeste" id="dls" name="dls" min="-1" max="1000" maxlength="6" autocomplete="off" />
                <div id="wrapper-dls"></div>
            </div>

            <div class="col-md-12 pt-4"><h5 class="text-info">Coordenadas</h5></div>
           
            <div class="col-md-6 pt-3">
                <h6>Norte/Sur <i class="text-muted">(N/S pies)</i></h6>
                <input type="text" class="form-control" placeholder="Norte-Sur" id="ns" name="ns" maxlength="15" autocomplete="off" />
                <div id="wrapper-ns"></div>
            </div>

            <div class="col-md-6 pt-3">
                <h6>Este/Oeste <i class="text-muted">(E/O pies)</i></h6>
                <input type="text" class="form-control" placeholder="Este-Oeste" id="ew" name="ew" maxlength="15" autocomplete="off" />
                <div id="wrapper-ew"></div>
            </div>

            <div class="col-md-12 pt-4"><h5 class="text-info">Ubicación</h5></div>

            <div class="col-md-6 pt-3">
                <h6>Latitud <i class="text-muted">(N/S ° ' ")</i></h6>
                <div class="row">
                    <div class="col-md-3 pt-1">
                        <input type="text" class="form-control" id="la_g" name="la_g" maxlength="5" autocomplete="off" />
                    </div>
                    <div class="col-md-3 pt-1">
                        <input type="text" class="form-control" id="la_m" name="la_m" maxlength="5" autocomplete="off" />
                    </div>
                    <div class="col-md-3 pt-1">
                        <input type="text" class="form-control" id="la_s" name="la_s" maxlength="5" autocomplete="off" />
                    </div>
                </div>
            </div>

            <div class="col-md-6 pt-3">
                <h6>Longitud <i class="text-muted">(E/O ° ' ")</i></h6>
                <div class="row">
                    <div class="col-md-3 pt-1">
                        <input type="text" class="form-control" id="lo_g" name="lo_g" maxlength="5" autocomplete="off" />
                    </div>
                    <div class="col-md-3 pt-1">
                        <input type="text" class="form-control" id="lo_m" name="lo_m" maxlength="5" autocomplete="off" />
                    </div>
                    <div class="col-md-3 pt-1">
                        <input type="text" class="form-control" id="lo_s" name="lo_s" maxlength="5" autocomplete="off" />
                    </div>
                </div>
            </div>

            <div class="col-md-12 pt-3"><hr></div>

            <div class="col-md-12 pt-3">
                <h6>Observaciones <i class="text-muted">120 caracteres</i></h6>
                <textarea class="form-control" style="max-height:80px" placeholder="Observaciones" id="observations" name="observations" maxlength="120"></textarea>
            </div>

            <input type="hidden" value="C7" id="form-type" />
        `);

        var input = ['md', 'inclination', 'azim_grid', 'tvd', 'vsec'], negatives = ['ns', 'ew'];
        
        for (let i = 0; i < input.length; i++) $(`#${input[i]}`).numeric({ negative: false });
        for (let i = 0; i < negatives.length; i++) $(`#${negatives[i]}`).numeric({ negative: true });

        this.loadMacollas();

    }

    addFiscalized() {

        $("#card-title").html(`<b>Cargar datos</b> / Producción Fiscalizada`);

        $("#main-options").html(`
            <div class="col-md-6">
                <h6>Macolla</h6>
                <select class="form-control" id="macolla" name="macolla" onchange="t.loadLocations()"><option value="">Cargando...</option></select>
                <div id="wrapper-macolla"></div>
            </div>
            
            <div class="col-md-12 pt-3"><hr></div>
            
            <div class="col-md-4 pt-3"><h6>Fecha</h6>
                <input type="date" class="form-control" id="fiscalized_date" name="fiscalized_date" />
                <div id="wrapper-fiscalized_date"></div>
            </div>

            <div class="col-md-4 pt-3">
                <h6>Producción EHO <i class="text-muted">Barriles totales</i></h6>
                <input type="text" class="form-control" placeholder="Producción EHO" id="production" name="production" maxlength="6" />
                <div id="wrapper-production"></div>
            </div>

            <div class="col-md-12 pt-0"></div>

            <div class="col-md-12 pt-3"><h5 class="text-info">DCO</h5></div>

            <div class="col-md-4 pt-3">
                <h6>% AyS DCO </h6>
                <input type="text" class="form-control" placeholder="% AyS DCO" readonly />
            </div>

            <div class="col-md-4 pt-3">
                <h6>ºAPI DCO </h6>
                <input type="text" class="form-control" placeholder="ºAPI DCO" readonly />
            </div>

            <div class="col-md-12 pt-4"><h5 class="text-info">Diluente</h5></div>

            <div class="col-md-4 pt-3">
                <h6>Inyección de Diluente</h6>
                <input type="text" class="form-control" placeholder="Inyección de Diluente" readonly />
            </div>

            <div class="col-md-4 pt-3">
                <h6>% AyS Diluente</h6>
                <input type="text" class="form-control" placeholder="% AyS Diluente" readonly />
            </div>
            
            <div class="col-md-4 pt-3">
                <h6>ºAPI Diluente</h6>
                <input type="text" class="form-control" placeholder="ºAPI Diluente" readonly />
            </div>
            
            <input type="hidden" value="F1" id="form-type" />
        `);

        var input = ['production'];
        
        for (let i = 0; i < input.length; i++) $(`#${input[i]}`).numeric({ negative: false });

        this.loadMacollas();
    }

    addEvents() {

        $("#card-title").html(`<b>Cargar datos</b> / Eventos de pozo`);

        $("#main-options").html(`
            <div class="col-md-6">
                <h6>Macolla</h6>
                <select class="form-control" id="macolla" name="macolla" onchange="t.loadLocations()"><option value="">Cargando...</option></select>
            </div>
            
            <div class="col-md-6">
                <h6>Localización</h6>
                <select class="form-control" id="well_id" name="well_id"><option value="">--</option></select>
                <div id="wrapper-well_id"></div>
            </div>
            
            <div class="col-md-12 pt-3"><hr></div>
            
            <div class="col-md-4 pt-3"><h6>Fecha</h6>
                <input type="date" class="form-control" id="event_date" name="event_date" />
                <div id="wrapper-event_date"></div>
            </div>

            <div class="col-md-4 pt-3">
                <h6>Hora</h6>
                <input type="time" class="form-control" id="event_time" name="event_time" />
                <div id="wrapper-event_time"></div>
            </div>

            <div class="col-md-12 pt-0"></div>

            <div class="col-md-4 pt-4">
                <h6>Diluente </h6>
                <input type="text" class="form-control" maxlength="6" placeholder="Diluente" id="diluent" name="diluent" required autocomplete="off" />
                <div id="wrapper-diluent"></div>
            </div>

            <div class="col-md-4 pt-4">
                <h6>RPM </h6>
                <input type="text" class="form-control" maxlength="6" placeholder="RPM" id="rpm" name="rpm" required autocomplete="off" />
                <div id="wrapper-rpm"></div>
            </div>

            <div class="col-md-4 pt-4">
                <h6>Efic. de la Bomba</h6>
                <input type="text" class="form-control" maxlength="6" placeholder="Eficiencia de la Bomba" id="pump_efficiency" name="pump_efficiency" required autocomplete="off" />
                <div id="wrapper-pump_efficiency"></div>
            </div>

            <div class="col-md-4 pt-4">
                <h6>Torque <i class="text-muted">Lb/pies</i></h6>
                <input type="text" class="form-control" maxlength="6" placeholder="Torque" id="torque" name="torque" required autocomplete="off" />
                <div id="wrapper-torque"></div>
            </div>

            <div class="col-md-4 pt-4">
                <h6>Frecuencia <i class="text-muted">Hz</i></h6>
                <input type="text" class="form-control" maxlength="6" placeholder="Frecuencia" id="frequency" name="frequency" required autocomplete="off" />
                <div id="wrapper-frequency"></div>
            </div>

            <div class="col-md-4 pt-4">
                <h6>Corriente <i class="text-muted">(A)</i></h6>
                <input type="text" class="form-control" maxlength="6" placeholder="Corriente" id="current" name="current" required autocomplete="off" />
                <div id="wrapper-current"></div>
            </div>

            <div class="col-md-4 pt-4">
                <h6>Potencia <i class="text-muted">Kw-H</i></h6>
                <input type="text" class="form-control" maxlength="6" placeholder="Potencia" id="power" name="power" required autocomplete="off" />
                <div id="wrapper-power"></div>
            </div>

            <div class="col-md-4 pt-4">
                <h6>Tensión de red <i class="text-muted">(V)</i></h6>
                <input type="text" class="form-control" maxlength="6" placeholder="Tensión de red" id="mains_voltage" name="mains_voltage" required autocomplete="off" />
                <div id="wrapper-mains_voltage"></div>
            </div>

            <div class="col-md-4 pt-4">
                <h6>Tensión de salida <i class="text-muted">(V)</i></h6>
                <input type="text" class="form-control" maxlength="6" placeholder="Tensión de salida" id="output_voltage" name="output_voltage" required autocomplete="off" />
                <div id="wrapper-output_voltage"></div>
            </div>

            <div class="col-md-4 pt-4">
                <h6>Temperatura VFD <i class="text-muted">ºF</i></h6>
                <input type="text" class="form-control" maxlength="6" placeholder="Temperatura VFD" id="vfd_temperature" name="vfd_temperature" required autocomplete="off" />
                <div id="wrapper-vfd_temperature"></div>
            </div>

            <div class="col-md-4 pt-4">
                <h6>Temperatura de cabezal <i class="text-muted">ºF</i></h6>
                <input type="text" class="form-control" maxlength="6" placeholder="Temperatura de cabezal" id="head_temperature" name="head_temperature" required autocomplete="off" />
                <div id="wrapper-head_temperature"></div>
            </div>

            <div class="col-md-4 pt-4">
                <h6>Presión de cabezal <i class="text-muted">lppc</i></h6>
                <input type="text" class="form-control" maxlength="6" placeholder="Presión de cabezal" id="head_pressure" name="head_pressure" required autocomplete="off" />
                <div id="wrapper-head_pressure"></div>
            </div>

            <div class="col-md-12 pt-4"><hr></div>

            <div class="col-md-12 pt-3">
                <h6>Observaciones <i class="text-muted">120 caracteres</i></h6>
                <textarea class="form-control" style="max-height:80px" placeholder="Observaciones" id="observations" name="observations" maxlength="120"></textarea>
            </div>
            
            <input type="hidden" value="E1" id="form-type" />
        `);

        var input = ['diluent', 'rpm', 'pump_efficiency', 'torque', 'frequency', 'current', 'power', 'mains_voltage', 'output_voltage', 'vfd_temperature', 'head_temperature', 'head_pressure'];
        
        for (let i = 0; i < input.length; i++) $(`#${input[i]}`).numeric({ negative: false });

        this.loadMacollas();
    }

    loadMacollas() {
        
        const ajax = new Ajax(`${globalConfig.appUrl}/loadMacollas`, {
            headers: { accept: 'application/json' },
            method: 'POST'
        });

        ajax.result().then(response => {	
            
            var options = ``, size = Object.keys(response._data).length;
            
            options += `<option>--</option>`;

            for (var i=0; i < size; i++) options += `<option value="${response._data[i].id}">${response._data[i].macolla_name}</option>`;
            
            options += `<option value="${response.e.id}">S/M</option>`;

            $('#macolla').html(options);   	

        }).catch(error => {});

        return false;
    }

    loadLocations() {
        
        const ajax = new Ajax(`${globalConfig.appUrl}/loadLocations`, {
            headers: { accept: 'application/json',},
            method: 'POST',
            body: { macollaId: parseInt($(`#macolla`).val()) }
        });

        ajax.result().then(response => {	
            
            var options = ``, size = Object.keys(response.data).length;
            
            for (var i = 0; i < size; i++) options += `<option value="${response.data[i].location_id}">${response.data[i].location_name}</option>`;
            $('#well_id').html(options);

        }).catch(error => {});

        return false;
    }

    loadReservoirs() {
		const ajax = new Ajax(`${globalConfig.appUrl}/reservoirManagement/loadReservoirs`, {
			headers: { accept: 'application/json',},
			method: 'POST'
		});
		
		ajax.result().then(response => {
            var size = Object.keys(response.data).length, options = ``;
            options += `<option value="-">--</option>`;
            for (var i=0; i<size; i++) options += `<option value="${response.data[i].id}">${response.data[i].reservoir_name}</option>`;
            $(`#reservoir`).html(options);
		}).catch(error => { console.log(error) });
		return false;
	}

    inputNames(which) {
        if (which == 1)  return ['well_id', 'extended_date', 'company', 'test', 'hour_start', 'hour_end', 'line_pressure', 'line_temperature', 'water_rate_prod', 'gas_rate_prod', 'total_barrels', 'neat_rate', 'gvf', 'inyected_diluent_rate'];
        else if (which == 2) return ['well_id', 'avg_start_test', 'avg_end_test', 'company', 'rpm', 'duration', 'line_pressure', 'line_temperature', 'gas_rate_prod', 'water_rate_prod', 'fluid_total_rate', 'percent_mixture', 'api_degree_mixture', 'neat_rate_prod', 'percent_formation', 'gvf', 'inyected_diluent_rate', 'api_degree_diluent', 'test', 'pip', 'observations'];
        else if (which == 3) return ['well_id', 'sign_date', 'sign_time', 'company' , 'percent_mixture', 'percent_sediment', 'api_degree_diluted', 'ptb'];
        else if (which == 4) return ['well_id', 'reservoir', 'sand', 'company', 'pt_date', 'test', 'deep_md', 'deep_tvd', 'temperature', 'duration', 'pressbefore', 'pressafter', 'pressformation', 'mobility', 'observations'];
        else if (which == 5) return ['well_id', 'reservoir', 'begin_date', 'end_date', 'type_work_i', 'type_work_ii', 'rig', 'sensor_brand', 'sensor_model', 'sensor_diameter', 'sensor_company', 'deeptop_sensor_md', 'deepbottom_sensor_md', 'pump_model', 'pump_diameter', 'pump_type', 'pump_capacity', 'pump_company', 'deeptop_pump_md', 'deepbottom_pump_md', 'inyected_diluent', 'measure_from', 'observations', 'references'];
        else if (which == 6) return ['well_id', 'static_date', 'quality', 'pip', 'pdp', 'temperature', 'observations'];
        else if (which == 7) return ['well_id', 'dynamic_date', 'quality', 'pip', 'pdp', 'temperature', 'observations'];
        else if (which == 8) return ['well_id', 'reservoir', 'pressure_date', 'type_test', 'sensor_model', 'pressure_gradient', 'deepdatum_mc_md', 'deepdatum_mc_tvd', 'press_datum_macolla', 'temp_datum_macolla', 'temperature_gradient'];
        else if (which == 9) return ['well_id', 'rs_date', 'hole', 'company', 'md', 'inclination', 'azim_grid', 'tvd', 'vsec', 'ns', 'ew', 'dls', 'la_g', 'la_m', 'la_s', 'lo_g', 'lo_m', 'lo_s', 'observations'];
        else if (which == 10) return ['well_id', 'ps_date', 'md', 'inclination', 'azim_grid', 'tvd', 'vsec', 'ns', 'ew', 'dls', 'la_g', 'la_m', 'la_s', 'lo_g', 'lo_m', 'lo_s', 'observations'];
        else if (which == 11) return ['macolla', 'fiscalized_date', 'production'];
        else if (which == 12) return ['well_id', 'event_date', 'event_time', 'diluent', 'rpm', 'pump_efficiency', 'torque', 'frequency', 'current', 'power', 'mains_voltage', 'output_voltage', 'vfd_temperature', 'head_temperature', 'head_pressure', 'observations'];
    }
    
    reloadView() {
		document.querySelectorAll('.text-danger').forEach(function(a){
			a.remove()
		});
	}

	showInputErrors(errors, inputNames) {
        let i = 0;
		for(const errorName in errors) {
			inputNames.map(inputName => {
				if (errorName === inputName) {
					document.getElementById(`${inputName}`).className = "form-control is-invalid";
                    document.querySelector(`#wrapper-${inputName}`).innerHTML += `<small class="text-danger">${errors[errorName]}</small>`;
                    document.querySelector(`#wrapper-${inputName}`).focus();
                }
            });
		}
	}
	
	showInputCorrects(errors, inputNames) {
		for(const errorName in errors) {
			inputNames.map(inputName => {
				if (errorName !== inputName) {
                    document.getElementById(`${inputName}`).className = "form-control is-valid";
				}
			});
		}
    }
    
    convertToDecimal(number) {
        return (number != null || number != '') ? number.replace(',', '.') : '';
    }

    resetTextboxes(arr) {
        for (let i = 0; i < arr.length; i++) {
            $(`#${arr[i]}`).removeClass();
            $(`#${arr[i]}`).addClass(`form-control`);
            $(`#${arr[i]}`).val(``);
        }
    }

    trimString(id) {
        return ($(`#${id}`).val() != null || $(`#${id}`).val() != '') ? $(`#${id}`).val().toUpperCase() : '';
    }

    trimNotRequiredString(id) {
        return ($(`#${id}`).val() == null || $(`#${id}`).val() == '' ) ? '-' : $(`#${id}`).val().toUpperCase();
    }

    trimId(id) {
        return parseInt($(`#${id}`).val()) ? parseInt($(`#${id}`).val()) : "";
    }

    saveRegister(val) {

        var obj, route = ``, inputNames = t.inputNames(val);

        if (val == 1) {

            route = `${globalConfig.appUrl}/saveSingleExtended`;

            obj = {
                well_id: t.trimId('well_id'),
                extended_date: $(`#extended_date`).val(),
                company: t.trimString('company'),
                test: t.trimString('test'),
                hour_start: $(`#hour_start`).val(),
                hour_end: $(`#hour_end`).val(),
                line_pressure: t.convertToDecimal(document.querySelector(`#line_pressure`).value),
                line_temperature: t.convertToDecimal(document.querySelector(`#line_temperature`).value),
                water_rate_prod: t.convertToDecimal(document.querySelector(`#water_rate_prod`).value),
                gas_rate_prod: t.convertToDecimal(document.querySelector(`#gas_rate_prod`).value),
                total_barrels: t.convertToDecimal(document.querySelector(`#total_barrels`).value),
                neat_rate: t.convertToDecimal(document.querySelector(`#neat_rate`).value),
                gvf: t.convertToDecimal(document.querySelector(`#gvf`).value),
                inyected_diluent_rate: t.convertToDecimal(document.querySelector(`#inyected_diluent_rate`).value),
                identifier: 'ADDED-MANUALLY'
            };

        } else if (val == 2) {

            route = `${globalConfig.appUrl}/saveSingleAverage`;
            obj = {
                well_id: t.trimId('well_id'),
                avg_start_test: $(`#avg_start_test`).val(),
                avg_end_test: $(`#avg_end_test`).val(),
                company: t.trimString('company'),
                rpm: t.convertToDecimal(document.querySelector(`#rpm`).value),
                duration: t.convertToDecimal(document.querySelector(`#duration`).value),
                line_pressure: t.convertToDecimal(document.querySelector(`#line_pressure`).value),
                line_temperature: t.convertToDecimal(document.querySelector(`#line_temperature`).value),
                water_rate_prod: t.convertToDecimal(document.querySelector(`#water_rate_prod`).value),
                gas_rate_prod: t.convertToDecimal(document.querySelector(`#gas_rate_prod`).value),
                fluid_total_rate: t.convertToDecimal(document.querySelector(`#fluid_total_rate`).value),
                percent_mixture: t.convertToDecimal(document.querySelector(`#percent_mixture`).value),
                api_degree_mixture: t.convertToDecimal(document.querySelector(`#api_degree_mixture`).value),
                neat_rate_prod: t.convertToDecimal(document.querySelector(`#neat_rate_prod`).value),
                percent_formation:t.convertToDecimal(document.querySelector(`#percent_formation`).value),
                gvf: t.convertToDecimal(document.querySelector(`#gvf`).value),
                inyected_diluent_rate: t.convertToDecimal(document.querySelector(`#inyected_diluent_rate`).value),
                api_degree_diluent: t.convertToDecimal(document.querySelector(`#api_degree_diluent`).value),
                test: t.trimString('test'),
                pip: t.convertToDecimal(document.querySelector(`#pip`).value),
                observations: t.trimNotRequiredString('observations'),
                identifier: 'ADDED-MANUALLY'
            };

            console.log(t.trimString('observations'));

        } else if (val == 3) {

            route = `${globalConfig.appUrl}/saveSingleSign`;
            obj = {
                well_id: t.trimId('well_id'),
                sign_date: document.querySelector(`#sign_date`).value,
                sign_time: document.querySelector(`#sign_time`).value,
                company: t.trimString('company'),
                percent_mixture: m.convertToDecimal(document.querySelector(`#percent_mixture`).value),
                percent_sediment: m.convertToDecimal(document.querySelector(`#percent_sediment`).value),
                api_degree_diluted: m.convertToDecimal(document.querySelector(`#api_degree_diluted`).value),
                ptb: m.convertToDecimal(document.querySelector(`#ptb`).value),
                identifier: 'ADDED-MANUALLY'
            };

        } else if(val == 4) {

            route = `${globalConfig.appUrl}/saveSingleCondition`;
            obj = {
                well_id: t.trimId('well_id'),
                reservoir: t.trimId('reservoir'),
                sand: t.trimString('sand'),
                company: t.trimString('company'),
                pt_date: document.querySelector(`#pt_date`).value,
                test: t.trimString('test'),
                deep_md: m.convertToDecimal(document.querySelector(`#deep_md`).value),
                deep_tvd: m.convertToDecimal(document.querySelector(`#deep_tvd`).value),
                temperature: m.convertToDecimal(document.querySelector(`#temperature`).value),
                duration: m.convertToDecimal(document.querySelector(`#duration`).value),
                pressbefore: m.convertToDecimal(document.querySelector(`#pressbefore`).value),
                pressafter: m.convertToDecimal(document.querySelector(`#pressafter`).value),
                pressformation: m.convertToDecimal(document.querySelector(`#pressformation`).value),
                mobility: m.convertToDecimal(document.querySelector(`#mobility`).value),
                observations: t.trimNotRequiredString('observations'),
                identifier: 'ADDED-MANUALLY'
            };
        
        } else if(val == 5) {

            route = `${globalConfig.appUrl}/saveSingleHistorical`;
            obj = {
                well_id: t.trimId('well_id'),
                reservoir: t.trimId('reservoir'),
                begin_date: document.querySelector(`#begin_date`).value,
                end_date: document.querySelector(`#end_date`).value,
                type_work_i: t.trimString('type_work_i'),
                type_work_ii: t.trimString('type_work_ii'),
                rig: t.trimString('rig'),
                sensor_brand: t.trimNotRequiredString('sensor_brand'),
                sensor_model: t.trimNotRequiredString('sensor_model'),
                sensor_diameter: m.convertToDecimal(document.querySelector(`#sensor_diameter`).value),
                sensor_company: t.trimNotRequiredString('sensor_company'),
                deeptop_sensor_md: m.convertToDecimal(document.querySelector(`#deeptop_sensor_md`).value),
                deepbottom_sensor_md: m.convertToDecimal(document.querySelector(`#deepbottom_sensor_md`).value),
                pump_model: t.trimString('pump_model'),
                pump_diameter: m.convertToDecimal(document.querySelector(`#pump_diameter`).value),
                pump_type: t.trimString('pump_type'),
                pump_capacity: m.convertToDecimal(document.querySelector(`#pump_capacity`).value),
                pump_company: t.trimString('pump_company'),
                deeptop_pump_md: m.convertToDecimal(document.querySelector(`#deeptop_pump_md`).value),
                deepbottom_pump_md: m.convertToDecimal(document.querySelector(`#deepbottom_pump_md`).value),
                inyected_diluent: t.trimNotRequiredString('inyected_diluent'),
                measure_from: t.trimNotRequiredString('measure_from'),
                observations: t.trimNotRequiredString('observations'),
                references: t.trimNotRequiredString('references'),
                identifier: 'ADDED-MANUALLY'
            };

        } else if(val == 6) {

            route = `${globalConfig.appUrl}/saveSingleStatic`;
            obj = {
                well_id: t.trimId('well_id'),
                static_date: document.querySelector(`#static_date`).value,
                quality: t.trimString('quality'),
                pip: m.convertToDecimal(document.querySelector(`#pip`).value),
                pdp: m.convertToDecimal(document.querySelector(`#pdp`).value),
                temperature: m.convertToDecimal(document.querySelector(`#temperature`).value),
                observations: t.trimNotRequiredString('observations'),
                identifier: 'ADDED-MANUALLY'
            };

        } else if(val == 7) {

            route = `${globalConfig.appUrl}/saveSingleDynamic`;
            obj = {
                well_id: t.trimId('well_id'),
                dynamic_date: document.querySelector(`#dynamic_date`).value,
                quality: t.trimString('quality'),
                pip: m.convertToDecimal(document.querySelector(`#pip`).value),
                pdp: m.convertToDecimal(document.querySelector(`#pdp`).value),
                temperature: m.convertToDecimal(document.querySelector(`#temperature`).value),
                observations: t.trimNotRequiredString('observations'),
                identifier: 'ADDED-MANUALLY'
            };

        } else if (val == 8) {

            route = `${globalConfig.appUrl}/saveSingleOriginal`;
            obj = {
                well_id: t.trimId('well_id'),
                reservoir: t.trimId('reservoir'),
                pressure_date: document.querySelector(`#pressure_date`).value,
                type_test: t.trimNotRequiredString('type_test'),
                sensor_model: t.trimNotRequiredString('sensor_model'),
                pressure_gradient: m.convertToDecimal(document.querySelector(`#pressure_gradient`).value),
                temperature_gradient: m.convertToDecimal(document.querySelector(`#temperature_gradient`).value),
                deepdatum_mc_md: m.convertToDecimal(document.querySelector(`#deepdatum_mc_md`).value),
                deepdatum_mc_tvd: m.convertToDecimal(document.querySelector(`#deepdatum_mc_tvd`).value),
                press_datum_macolla: m.convertToDecimal(document.querySelector(`#press_datum_macolla`).value),
                temp_datum_macolla: m.convertToDecimal(document.querySelector(`#temp_datum_macolla`).value),
                identifier: 'ADDED-MANUALLY'
            };

        } else if (val == 9) {

            route = `${globalConfig.appUrl}/saveSingleReal`;
            obj = {
                well_id: t.trimId('well_id'),
                rs_date: document.querySelector(`#rs_date`).value,
                hole: t.trimString('hole'),
                company: t.trimString('company'),
                md: m.convertToDecimal(document.querySelector(`#md`).value),
                inclination: m.convertToDecimal(document.querySelector(`#inclination`).value),
                azim_grid: m.convertToDecimal(document.querySelector(`#azim_grid`).value),
                tvd: m.convertToDecimal(document.querySelector(`#tvd`).value),
                vsec: m.convertToDecimal(document.querySelector(`#vsec`).value),
                ns: m.convertToDecimal(document.querySelector(`#ns`).value),
                ew: m.convertToDecimal(document.querySelector(`#ew`).value),
                dls: m.convertToDecimal(document.querySelector(`#dls`).value),
                la_g: m.convertToDecimal(document.querySelector(`#la_g`).value),
                la_m: m.convertToDecimal(document.querySelector(`#la_m`).value),
                la_s: m.convertToDecimal(document.querySelector(`#la_s`).value),
                lo_g: m.convertToDecimal(document.querySelector(`#lo_g`).value),
                lo_m: m.convertToDecimal(document.querySelector(`#lo_m`).value),
                lo_s: m.convertToDecimal(document.querySelector(`#lo_s`).value),
                observations: t.trimNotRequiredString('observations'),
                identifier: 'ADDED-MANUALLY'
            };
        
        } else if (val == 10) {

            route = `${globalConfig.appUrl}/saveSinglePlan`;
            obj = {
                well_id: t.trimId('well_id'),
                ps_date: document.querySelector(`#ps_date`).value,
                md: m.convertToDecimal(document.querySelector(`#md`).value),
                inclination: m.convertToDecimal(document.querySelector(`#inclination`).value),
                azim_grid: m.convertToDecimal(document.querySelector(`#azim_grid`).value),
                tvd: m.convertToDecimal(document.querySelector(`#tvd`).value),
                vsec: m.convertToDecimal(document.querySelector(`#vsec`).value),
                ns: m.convertToDecimal(document.querySelector(`#ns`).value),
                ew: m.convertToDecimal(document.querySelector(`#ew`).value),
                dls: m.convertToDecimal(document.querySelector(`#dls`).value),
                la_g: m.convertToDecimal(document.querySelector(`#la_g`).value),
                la_m: m.convertToDecimal(document.querySelector(`#la_m`).value),
                la_s: m.convertToDecimal(document.querySelector(`#la_s`).value),
                lo_g: m.convertToDecimal(document.querySelector(`#lo_g`).value),
                lo_m: m.convertToDecimal(document.querySelector(`#lo_m`).value),
                lo_s: m.convertToDecimal(document.querySelector(`#lo_s`).value),
                observations: t.trimNotRequiredString('observations'),
                identifier: 'ADDED-MANUALLY'
            };

        } else if (val == 11) {

            route = `${globalConfig.appUrl}/saveSingleFiscalized`;
            obj = {
                macolla: t.trimId('macolla'),
                fiscalized_date: $(`#fiscalized_date`).val(),
                production: m.convertToDecimal(document.querySelector(`#production`).value),
                identifier: 'ADDED-MANUALLY'
            };

        } else if (val == 12) {

            route = `${globalConfig.appUrl}/saveSingleEvent`;
            obj = {
                well_id: t.trimId('well_id'),
                event_date: document.querySelector(`#event_date`).value,
                event_time: document.querySelector(`#event_time`).value,
                diluent: document.querySelector(`#diluent`).value,
                rpm: m.convertToDecimal(document.querySelector(`#rpm`).value),
                pump_efficiency: m.convertToDecimal(document.querySelector(`#pump_efficiency`).value),
                torque: m.convertToDecimal(document.querySelector(`#torque`).value),
                frequency: m.convertToDecimal(document.querySelector(`#frequency`).value),
                current: m.convertToDecimal(document.querySelector(`#current`).value),
                power: m.convertToDecimal(document.querySelector(`#power`).value),
                mains_voltage: m.convertToDecimal(document.querySelector(`#mains_voltage`).value),
                output_voltage: m.convertToDecimal(document.querySelector(`#output_voltage`).value),
                vfd_temperature: m.convertToDecimal(document.querySelector(`#vfd_temperature`).value),
                head_temperature: m.convertToDecimal(document.querySelector(`#head_temperature`).value),
                head_pressure: m.convertToDecimal(document.querySelector(`#head_pressure`).value),
                observations: t.trimNotRequiredString('observations'),
                identifier: 'ADDED-MANUALLY'
            };
        }

        t.reloadView();
        $(`#btn-save`).attr('disabled', true);

        const ajax = new Ajax(route, {
            headers: { accept: 'application/json' },
            method: 'POST',
            body: obj
        });

        ajax.result().then(response => {	
            
            $(`#btn-save`).attr('disabled', false);
            t.resetTextboxes(inputNames);
            alertify.success(response.message);

        }).catch(error => {
            if (error.status === 400) {
                $(`#btn-save`).attr('disabled', false);
				this.showInputCorrects(error.errors, inputNames);
				this.showInputErrors(error.errors, inputNames);
			} else console.log(error);
        });
        return false;
    }

    saveInputs() {

        $(`#btn-save`).click(function () { 
            var form = $(`#form-type`).val(), which;

            if (form == "P1") which = 1;
            else if (form == "P2") which = 2;
            else if (form == "P3") which = 3;
            else if (form == "C1") which = 4;
            else if (form == "C2") which = 5;
            else if (form == "C3") which = 6;
            else if (form == "C4") which = 7;
            else if (form == "C5") which = 8;
            else if (form == "C6") which = 9;
            else if (form == "C7") which = 10;
            else if (form == "F1") which = 11;
            else if (form =="E1") which = 12;

            if (which >= 1 &&  which <= 12) {
                alertify.confirm('Pregunta', '¿Está seguro que desea guardar este registro? <br> Los campos no completados serán considerados como nulos.', function() {},
                function (e) { if (e) { t.saveRegister(which) } }).set({'movable': false, 'modal': false, 'moveBounded': false, 'labels': {ok: 'No', cancel: 'Sí'}, 'closable': false, 'pinnable': false });
            }
        });
    }
}