class UploadProduction {
	constructor() {
	}

	showGeneralForm(route, cardText, form, token, method, type) {
		var urlMethod = `${globalConfig.appUrl}/${route}`;
		this.changeCard(`card card-default`, `<b>Cargar datos</b> / ${cardText}`);
		this.showForm(form[0], form[1], urlMethod, token);
		this.toReturn(first);
		
		$('#btn-continue').click(function(event) {
			alertify.confirm('<i class="fa fa-question-circle"></i> Pregunta',
				'¿Está seguro que desea cargar los datos que se encuentran en el archivo seleccionado?', function(e) {
				upr.showMessageBefore();
				if (e) {
					event.preventDefault();
					var formData = new FormData();
					formData.append(form[1], $(`#${form[1]}`)[0].files[0]);
					upr.ajax(`#${form[0]}`, formData, method, type);
				} else {
					$('#btn-continue').attr('disabled', false);
				}
			}, function(){}).set({'movable':false,'moveBounded':false,'labels':{ok:'Sí',cancel:'No'}});
		});
	}
	
	showUploadExtended(token) {
		this.showGeneralForm(`extendedImport`, `Pruebas Extendidas`, [`upload-extended`, `extendedTests`], token, upr.showExtendedDoc, 1);
	}
	
	showUploadAverage(token) {
		this.showGeneralForm(`averageImport`, `Pruebas Promedio`, [`upload-average`, `averageTests`], token, upr.showAverageDoc, 2);
	}
	
	showUploadSign(token) {
		this.showGeneralForm(`signImport`, `Muestras`, [`upload-sign`, `signs`], token, upr.showSignDoc, 3);
	}
	
	showExtendedDoc(response) {
		
		/*console.log(response.sheet);*/

		if (response.my)  {

			var macollaName = response.macolla, locationName = response.location, dataTable = ``, objectData = [], documentSize = Object.keys(response.sheet).length;
			
			$(`#location-label`).html(`<p><i class="fa fa-file-excel text-success"></i> <b>${macollaName}</b> / Localización ${locationName}</p>`);
			$(`#file-upload`).html(``);
			
			if (documentSize > 0) {
				
				var start, end, diff;

				var headerItems = [
					upr.setSpacing('#'),
					upr.setSpacing('Fecha'),
					upr.setSpacing('Compañía'),
					upr.setSpacing('Prueba'),
					'Hora <h6>inicio</h6>',
					'Hora <h6>finalización</h6>',
					'Duración <h6>horas</h6>',
					'P<sub>línea</sub> <h6>lppc</h6>',
					'T<sub>línea</sub> <h6>ºF</h6>',
					'q<sub>g</sub> <h6>MPCND</h6>',
					'q<sub>w</sub> <h6>BPD</h6>',
					upr.setSpacing('Barriles totales'),
					'q<sub>n</sub> <h6>BPPD</h6>',
					upr.setSpacing('FVG'),
					'q<sub>d</sub> <h6>BDPD</h6>',
				];

				var headerSize = headerItems.length;

				var tbody = m.createTable(headerItems);
				
				for (var i = 0; i < documentSize; i++) {
					
					start = moment(response.sheet[i].hour_start.date).format('HH:mm')
					end = moment(response.sheet[i].hour_end.date).format('HH:mm')
					
					if (start > end) end = '24:00';

					diff = m.diffTime(moment.duration(start, 'hh:mm'), moment.duration(end, 'hh:mm'));

					dataTable += `
						<tr>
							<td class="text-muted">${(i+1)}</td>
							<td>${m.setFormatDate(1, response.sheet[i].extended_date.date)}</td>
							<td>${m.validateEmptyString(response.sheet[i].company)}</td>
							<td>${m.validateEmptyString(response.sheet[i].test)}</td>
							<td>${moment(response.sheet[i].hour_start.date).format('HH:mm')}</td>
							<td>${moment(response.sheet[i].hour_end.date).format('HH:mm')}</td>
							<td>${diff.hours() + ':' + Math.abs(diff.minutes())}</td>
							${m.concatString(m.addFormatData(response.sheet[i].line_pressure, 10, 2))}
							${m.concatString(m.addFormatData(response.sheet[i].line_temperature, 10, 2))}
							${m.concatString(m.addFormatData(response.sheet[i].gas_rate_prod, 10, 1))}
							${m.concatString(m.addFormatData(response.sheet[i].water_rate_prod, 10, 1))}
							${m.concatString(m.addFormatData(response.sheet[i].total_barrels, 10, 1))}
							${m.concatString(m.addFormatData(response.sheet[i].oil_rate_prod, 10, 1))}
							${m.concatString(m.addFormatData((response.sheet[i].gvf/100), 1, 3))}
							${m.concatString(m.addFormatData(response.sheet[i].inyected_diluent_rate, 10, 1))}
						</tr>
					`;
					
					objectData.push({
						well_id: 0,
						extended_date: moment(response.sheet[i].extended_date.date).format('YYYY-MM-DD'),
						company: response.sheet[i].company,
						test: response.sheet[i].test,
						hour_start: moment(response.sheet[i].hour_start.date).format('HH:mm'),
						hour_end: moment(response.sheet[i].hour_end.date).format('HH:mm'),
						line_pressure: response.sheet[i].line_pressure,
						line_temperature: response.sheet[i].line_temperature,
						gas_rate_prod: response.sheet[i].gas_rate_prod,
						water_rate_prod: response.sheet[i].water_rate_prod,
						total_barrels: response.sheet[i].total_barrels,
						neat_rate: response.sheet[i].oil_rate_prod,
						gvf: response.sheet[i].gvf,
						inyected_diluent_rate: response.sheet[i].inyected_diluent_rate,
						identifier: response.sheet[i].identifier
					});
				}

				document.querySelector(`#data`).innerHTML = dataTable;
				
				upr.setHtmlButtons();
				
				$("#btn-save").click(function(event) {
					alertify.confirm('<b class="text-warning"><i class="fa fa-exclamation-triangle"></i></b> Atención', '¿Está seguro que desea guardar la información de este archivo en la base de datos?', function(e) {
						if (e) {
							upr.saveExtendedTests(objectData, locationName);
						}
					}, function(){}).set({'movable': false, 'moveBounded': false, 'labels': {ok: 'Sí', cancel: 'No'}});
				});
				
				upr.toReturn(first);
			
			} else $(`#excel-output`).html(`<h5>No se encontraron datos que mostrar.</h5>`);
		} else upr.setErrorMessageToUpload(response.location, 1);
	}
	
	showAverageDoc(response) {

		/*console.log(response.sheet); */

		if (response.my)  {
			
			var macollaName = response.macolla, dataTable = ``, objectData = [], names = [], col = [], documentSize = Object.keys(response.sheet).length, text;
			
			$(`#location-label`).html(`<p><i class="fa fa-file-excel text-success"></i> <b>${macollaName}</b></p>`);
			$(`#file-upload`).html(``);
			
			if (documentSize > 0) {

				var headerItems = [
					upr.setSpacing('#'),
					upr.setSpacing('Localización'),
					'Inicio <h6>prueba</h6>',
					'Fin <h6>prueba</h6>',
					upr.setSpacing('Compañía'),
					'RPM <h6>BCP</h6>',
					'Duración <h6>horas</h6>',
					'P<sub>línea</sub> <h6>lppc</h6>',
					'T<sub>línea</sub> <h6>ºF</h6>',
					'q<sub>g</sub> <h6>MPCND</h6>',
					'q<sub>w</sub> <h6>BCP</h6>',
					'q<sub>t</sub> <h6>BFPD</h6>',
					upr.setSpacing('%AyS&nbsp;Mezcla'),
					upr.setSpacing('ºAPI&nbsp;Mezcla'),
					'Tasa&nbsp;neta <h6>BPPD</h6>',
					upr.setSpacing('%AyS&nbsp;Formación'),
					upr.setSpacing('FVG'),
					'Tasa&nbsp;inyección&nbsp;diluente <h6>BDPD</h6>',
					upr.setSpacing('ºAPI&nbsp;Diluente'),
					upr.setSpacing('Prueba'),
					'PIP <h6>lppc</h6>',
					upr.setSpacing('Observaciones')
				];

				var headerSize = headerItems.length;

				var tbody = m.createTable(headerItems);

				for (var i = 0; i < documentSize; i++) {
					dataTable += `
						<tr>
							<td class="text-muted">${(i+1)}</td>
							<td>${m.validateEmptyString(response.sheet[i].location)}</td>
							<td>${m.setFormatDate(1, response.sheet[i].avg_start_test.date)}</td>
							<td>${m.setFormatDate(1, response.sheet[i].avg_end_test.date)}</td>
							<td>${m.validateEmptyString(response.sheet[i].company)}</td>
							${m.concatString(m.addFormatData(response.sheet[i].rpm, 0, 0))}
							${m.concatString(m.addFormatData(response.sheet[i].duration, 0, 0))}
							${m.concatString(m.addFormatData(response.sheet[i].line_pressure, 10, 2))}
							${m.concatString(m.addFormatData(response.sheet[i].line_temperature, 10, 2))}
							${m.concatString(m.addFormatData(response.sheet[i].gas_rate_prod, 10, 1))}
							${m.concatString(m.addFormatData(response.sheet[i].water_rate_prod, 10, 1))}
							${m.concatString(m.addFormatData(response.sheet[i].fluid_total_rate, 10, 1))}
							${m.concatString(m.addFormatData(response.sheet[i].percent_mixture, 10, 1))}
							${m.concatString(m.addFormatData(response.sheet[i].api_degree_mixture, 100, 1))}
							${m.concatString(m.addFormatData(response.sheet[i].neat_rate_prod, 10, 1))}
							${m.concatString(m.addFormatData(response.sheet[i].percent_formation, 10, 1))}
							${m.concatString(m.addFormatData((response.sheet[i].gvf/100), 1, 3))}
							${m.concatString(m.addFormatData(response.sheet[i].inyected_diluent_rate, 10, 1))}
							${m.concatString(m.addFormatData(response.sheet[i].api_degree_diluent, 100, 1))}
							<td>${m.validateEmptyString(response.sheet[i].test)}</td>
							${m.concatString(m.addFormatData(response.sheet[i].pip, 10, 1))}
							<td>${m.validateEmptyString(response.sheet[i].observations)}</td>
						</tr>
					`;

					objectData.push({
						well_id: 0,
						avg_start_test: moment(response.sheet[i].avg_start_test.date).format('YYYY-MM-DD'),
						avg_end_test: moment(response.sheet[i].avg_end_test.date).format('YYYY-MM-DD'),
						company: response.sheet[i].company,
						rpm: response.sheet[i].rpm,
						duration: response.sheet[i].duration,
						line_pressure: response.sheet[i].line_pressure,
						line_temperature: response.sheet[i].line_temperature,
						gas_rate_prod: response.sheet[i].gas_rate_prod,
						water_rate_prod: response.sheet[i].water_rate_prod,
						fluid_total_rate: response.sheet[i].fluid_total_rate,
						percent_mixture: response.sheet[i].percent_mixture,
						api_degree_mixture: response.sheet[i].api_degree_mixture,
						neat_rate_prod: response.sheet[i].neat_rate_prod,
						percent_formation: response.sheet[i].percent_formation,
						gvf: response.sheet[i].gvf,
						inyected_diluent_rate: response.sheet[i].inyected_diluent_rate,
						api_degree_diluent: response.sheet[i].api_degree_diluent,
						test: response.sheet[i].test,
						pip: response.sheet[i].pip,
						observations: response.sheet[i].observations,
						identifier: response.sheet[i].identifier
					});
					names.push({location: response.sheet[i].location});
				}

				document.querySelector(`#data`).innerHTML = dataTable;
				
				upr.setHtmlButtons();
				
				$("#btn-save").click(function(event) {
					alertify.confirm('<b class="text-warning"><i class="fa fa-exclamation-triangle"></i></b> Atención', '¿Está seguro que desea guardar la información de este archivo en la base de datos?', function(e) {
						if (e) {
							upr.saveAverageTests(objectData, names, macollaName);
						}
					}, function(){}).set({'movable': false, 'moveBounded': false, 'labels': {ok: 'Sí', cancel: 'No'}});
				});
				
				upr.toReturn(first);
			
			} else $(`#excel-output`).html(`<h5>No se encontraron datos que mostrar.</h5>`);

		} else upr.setErrorMessageToUpload(response.macolla, 2);
	}
	
	showSignDoc(response) {

		/*console.log(response);*/

		if (response.my) {

			var macollaName = response.macolla, locationName = response.location, dataTable = ``, objectData = [], col = [], documentSize = Object.keys(response.sheet).length, text;
			
			$(`#location-label`).html(`<p><i class="fa fa-file-excel text-success"></i> <b>${macollaName}</b> / Localización ${locationName}</p>`);
			$(`#file-upload`).html(``);
			
			if (documentSize > 0) {

				var headerItems = [
					upr.setSpacing('#'),
					'Fecha <h6>muestra</h6>',
					upr.setSpacing('Hora'),
					upr.setSpacing('Compañía'),
					upr.setSpacing('%AyS&nbsp;Mezcla'),
					upr.setSpacing('%Sedimento'),
					upr.setSpacing('ºAPI&nbsp;Diluido'),
					upr.setSpacing('PTB')
				];

				var headerSize = headerItems.length;

				var tbody = m.createTable(headerItems);
				
				for (var i = 0; i < documentSize; i++) {
					dataTable += `
						<tr>
							<td class="text-muted">${(i+1)}</td>
							<td>${m.setFormatDate(1, response.sheet[i].sign_date.date)}</td>
							<td>${moment(response.sheet[i].sign_time.date).format('HH:mm')}</td>
							<td>${m.validateEmptyString(response.sheet[i].company)}</td>
							${m.concatString(m.addFormatData(response.sheet[i].percent_mixture, 10, 1))}
							${m.concatString(m.addFormatData(response.sheet[i].percent_sediment, 10, 1))}
							${m.concatString(m.addFormatData(response.sheet[i].api_degree_diluted, 100, 1))}
							${m.concatString(m.addFormatData(response.sheet[i].ptb, 10, 1))}
						</tr>
					`;

					objectData.push({
						well_id: 0,
						sign_date: moment(response.sheet[i].sign_date.date).format('YYYY-MM-DD'),
						sign_time: moment(response.sheet[i].sign_time.date).format('HH:mm'),
						company: response.sheet[i].company,
						percent_mixture: response.sheet[i].percent_mixture,
						percent_sediment: response.sheet[i].percent_sediment,
						api_degree_diluted: response.sheet[i].api_degree_diluted,
						ptb: response.sheet[i].ptb,
						identifier: response.sheet[i].identifier
					});
				}

				document.querySelector(`#data`).innerHTML = dataTable;
				
				upr.setHtmlButtons();
				
				$("#btn-save").click(function(event) {
					alertify.confirm('<b class="text-warning"><i class="fa fa-exclamation-triangle"></i></b> Atención', '¿Está seguro que desea guardar la información de este archivo en la base de datos?', function(e) {
						if (e) {
							upr.saveSigns(objectData, locationName);
						}
					}, function(){}).set({'movable': false, 'moveBounded': false, 'labels': {ok: 'Sí', cancel: 'No'}});
				});
				
				upr.toReturn(first);
			
			} else $(`#excel-output`).html(`<h5>No se encontraron datos que mostrar.</h5>`);

		} else upr.setErrorMessageToUpload(response.location, 1);
	}
	
	saveExtendedTests(objects, name) {
		const ajax = new Ajax(`${globalConfig.appUrl}/saveExtended`, {
			headers: {
				accept: 'application/json',
			},
			method: 'POST',
			body: {
				elements: JSON.stringify(objects),
				location_name: name
			}
		});

		this.setHtmlMessage();
		
		ajax.result().then(response => {
			if (response.message) alertify.message(response.message).delay(5);
			alertify.success(response.msgStore).delay(5);
			this.unsetHtmlMessage();
		}).catch(error => {
			if (error.status === 400) {
			} else {
				this.unsetHtmlMessage();
				alertify.alert('<b class="text-danger"><i class="fa fa-exclamation-circle"></i> Error</b>', 'Ha ocurrido un error interno en el servidor. ' + this.setErrorMessage(error.message), function(){}).set({'movable': false, 'moveBounded': false});
				console.log("Error: "+ error.message);
			}
		});
		return false;
	}
	
	saveAverageTests(objects, names, mc) {
		const ajax = new Ajax(`${globalConfig.appUrl}/saveAverage`, {
			headers: {
				accept: 'application/json',
			},
			method: 'POST',
			body: {
				elements: JSON.stringify(objects),
				names: JSON.stringify(names),
				macolla_name: mc
			}
		});

		this.setHtmlMessage();
		
		ajax.result().then(response => {
			if (response.message) alertify.message(response.message).delay(5);
			alertify.success(response.msgStore).delay(5);
			this.unsetHtmlMessage();
		}).catch(error => {
			if (error.status === 400) {
			} else {
				this.unsetHtmlMessage();
				alertify.alert('<b class="text-danger"><i class="fa fa-exclamation-circle"></i> Error</b>', 'Ha ocurrido un error interno en el servidor. ' + this.setErrorMessage(error.message), function(){}).set({'movable': false, 'moveBounded': false});
				console.log("Error: "+ error.message);
			}
		});
		return false;
	}
	
	saveSigns(objects, name) {
		const ajax = new Ajax(`${globalConfig.appUrl}/saveSign`, {
			headers: {
				accept: 'application/json',
			},
			method: 'POST',
			body: {
				elements: JSON.stringify(objects),
				location_name: name
			}
		});

		this.setHtmlMessage();
		
		ajax.result().then(response => {
			if (response.message) alertify.message(response.message).delay(5);
			alertify.success(response.msgStore).delay(5);
			this.unsetHtmlMessage();
		}).catch(error => {
			if (error.status === 400) {
			} else {
				this.unsetHtmlMessage();
				alertify.alert('<b class="text-danger"><i class="fa fa-exclamation-circle"></i> Error</b>', 'Ha ocurrido un error interno en el servidor. ' + this.setErrorMessage(error.message), function(){}).set({'movable': false, 'moveBounded': false});
				console.log("Error: "+ error.message);
			}
		});
		return false;
	}
	
	returnMain(inner) {
		document.querySelector(`#main-card`).className = 'card card-default';
		$(`#card-title`).html(`<h3 class="card-title" id="card-title"><b>Cargar datos</b> / Pruebas de Producción</h3>`);
		$(`#show-form, #option-buttons, #excel-output`).each(function() {
			$(this).html(``);
		});
		$(`#main-options`).html(inner);
		this.toBack();

		upr.setClickEvent(1, "btn-extended-tests-2");
		upr.setClickEvent(2, "btn-average-tests-2");
		upr.setClickEvent(3, "btn-signs-2");
	}
	
	verify(objForm) { 
		var arrExtensions = new Array("xls", "xlsx");
		var objInput = objForm.elements["extendedTests"]||objForm.elements["averageTests"]||objForm.elements["signs"];
		var strFilePath = objInput.value; 
		var arrTmp = strFilePath.split("."); 
		var strExtension = arrTmp[arrTmp.length-1].toLowerCase(); 
		var blnExists = false;

		for (var i = 0; i < arrExtensions.length; i++) {
			if (strExtension == arrExtensions[i]) { 
				blnExists = true; 
				break;
			}
		}

		if (!blnExists) {
			alertify.alert('<b class="text-danger"><i class="fa fa-exclamation-circle"></i> Error</b>', 'Asegúrese de cargar un archivo <b>Excel</b>', function(){}).set({'movable': false, 'moveBounded': false});
			$(`#btn-continue`).attr('disabled', true);
		} else $(`#btn-continue`).attr('disabled', false);
		
		return blnExists;
	}
	
	toBack() {
		$('#btn-extended-tests').click(function(event) {upr.showUploadExtended($(`input[name="_token"]`).val());});
		$('#btn-average-tests').click(function(event) {upr.showUploadAverage($(`input[name="_token"]`).val());});
		$('#btn-signs').click(function(event) {upr.showUploadSign($(`input[name="_token"]`).val());});
	}
	
	toReturn(first) {
		$("#btn-back").click(function(event) {
			alertify.confirm('Atención', '¿Está seguro que desea regresar al menú principal?', function(e) {
				if (e) {upr.returnMain(first);}
			}, function(){}).set({'movable': false, 'moveBounded': false, 'labels': {ok: 'Sí', cancel: 'No'}});
		});
	}
	
	changeCard(card, title) {
		document.querySelector(`#main-card`).className = `${card}`;
		$(`#card-title`).html(`<h3 class="card-title" id="card-title">${title}</h3>`);
		$(`#main-options`).html(``);
	}
	
	showForm(_idForm, _idInputs, url, token) {
		$(`#show-form`).html(`<form onchange="return upr.verify(this)" id="${_idForm}" method="post" action="${url}"><div class="form-group"><div id="location-label"></div><div id="file-upload"><p><i class="fa fa-file-excel text-success"></i> Importar archivo Excel (*.xls, *.xlsx)</p><div class="row"><div class="col-md-12"><input name="${_idInputs}" id="${_idInputs}" class="btn btn-default form-control" style="height:45px" type="file" accept=".xlsx, .xls"><input type="hidden" name="_token" value="${token}"></div></div></div></div></div></form>`);
		$(`#add-self`).html(`<div class="row col-md-12"></div>`);
		$(`#option-buttons`).html(`<div class="card-footer"><button class="btn btn-secondary" id="btn-back"><i class="fas fa-angle-left"></i> Regresar</button><button class="btn btn-success float-right" id="btn-continue" disabled>Continuar <i class="fas fa-angle-right"></i></button></div>`);

		$(`#add-manually`).html(``);
	}
	
	showMessageBefore() {
		$('#btn-continue').attr('disabled', true);
		$("#loading").html(`<div class="loading-animated"></div><small>Cargando, por favor espere...</small>`);
	}
	
	showMessageAfter() {
		$('#btn-continue').attr('disabled', false);
		$("#loading").html(``);
	}
	
	setHtmlButtons() {
		$(`#option-buttons`).html(`<div class="card-footer"><button class="btn btn-secondary" id="btn-back"><i class=" fas fa-angle-left"></i> Regresar</button><button class="btn btn-success float-right" id="btn-save"><i class=" fas fa-save"></i> Guardar</button></div>`);
	}
	
	setHtmlMessage() {
		$("#btn-save").attr("disabled", true);
		$("#loading-msg").html(`<br><small>Guardando, espere hasta que se complete el proceso...</small>`);
	}
	
	unsetHtmlMessage() {
		$("#btn-save").attr("disabled", true);
		$("#loading-msg").html(``);
	}

	setErrorMessage(str) {
		return (str != null) || (str != '') ? '<br><br> <b>Error: </b>' + str : '';
	}
	
	setLabelMessage(option) {
		if (option == 1) {
			return 'Pruebas Extendidas';
		} else if (option == 2) {
			return 'Pruebas Promedio';
		} else {
			return 'Muestras';
		}
	}
	
	ajax(formId, formData, formFunction, type) {
		$.ajax({
			url: $(formId).attr('action') + '?' + $(formId).serialize(),
			method: 'post',
			data: formData,
			processData: false,
			contentType: false
		}).done(function (data) {
			
			upr.showMessageAfter();
			
			if (data.size > 0) {
				formFunction(data);
			} else {
				alertify.alert('<b class="text-danger"><i class="fa fa-exclamation-circle"></i> Aviso</b>', 'No hay datos que cargar.<br><br>Estas podrían ser las causas:<br>- El archivo Excel no corresponde a <b>' + upr.setLabelMessage(type) + '</b>.<br>- Sin información en el archivo a importar.<br>- No se encontraron los pozos/macollas especificados.<br><br>Si ese no es el caso, revise su archivo y verifique que los datos coincidan con el formato especificado.', function(){}).set({'movable': false, 'moveBounded': false});
			}
		}).fail(function (error) {
			upr.showMessageAfter();
			alertify.alert('<b class="text-danger"><i class="fa fa-exclamation-circle"></i> Error</b>', 'La estructura de la hoja es inválida, verifíquela e intente de nuevo con el formato especificado.' + upr.setErrorMessage(error.responseJSON.message), function(){}).set({'movable': false, 'moveBounded': false});
			
			console.log(error);
			
		}).always(function () {
			$(formId)[0].reset();
			$('#btn-continue').attr('disabled', true);
		});
	}

	setErrorMessageToUpload(str, type) {
		if (type == 1) {
			if (str != "" && str != null) {
				alertify.alert('<b class="text-danger"><i class="fa fa-exclamation-circle"></i> Importante</b>', `La localización <b>${str}</b> no existe en el sistema.<br>Regístrela primero antes de proceder.`, function(){}).set({'movable': false, 'moveBounded': false});
			} else {
				alertify.alert('<b class="text-danger"><i class="fa fa-exclamation-circle"></i> Importante</b>', `La localización se encuentra vacía o es nula ya que no se ha específicado ningún nombre.`, function(){}).set({'movable': false, 'moveBounded': false});
			}
		} else {
			if (str != "" && str != null) {
				alertify.alert('<b class="text-danger"><i class="fa fa-exclamation-circle"></i> Importante</b>', `La macolla <b>${str}</b> no existe en el sistema.<br>Regístrela primero antes de proceder.`, function(){}).set({'movable': false, 'moveBounded': false});
			} else {
				alertify.alert('<b class="text-danger"><i class="fa fa-exclamation-circle"></i> Importante</b>', `La macolla se encuentra vacía o es nula ya que no se ha específicado ningún nombre.`, function(){}).set({'movable': false, 'moveBounded': false});
			}
		}
	}

	setClickEvent(type, id) {
		$(`#${id}`).click(function (event) {
			
			if (type == 1) {t.addExtendedTest();}
			else if (type == 2) {t.addAverageTest();}
			else {t.addSign();}

			upr.setHtmlButtons();
			t.saveInputs();
			upr.toReturn(first);
		});
	}

	setSpacing(text) {
		return `${text} <h6></h6>`;
	}
}

window.onload = () => {
	m = new Method();
	upr = new UploadProduction();
	t = new Add();

	$(`#main-options`).html(first);
	$(function() {upr.toBack();});

	upr.setClickEvent(1, "btn-extended-tests-2");
	upr.setClickEvent(2, "btn-average-tests-2");
	upr.setClickEvent(3, "btn-signs-2");
}