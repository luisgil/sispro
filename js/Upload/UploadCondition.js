class UploadCondition {
	constructor() {
	}

	showGeneralForm(route, cardText, form, token, method, type) {
		var urlMethod = `${globalConfig.appUrl}/${route}`;
		this.changeCard(`card card-default`, `<b>Cargar datos</b> / ${cardText}`);
		this.showForm(form[0], form[1], urlMethod, token);
		this.toReturn(second);
		
		$('#btn-continue').click(function(event) {
			alertify.confirm('<i class="fa fa-question-circle"></i> Pregunta',
				'¿Está seguro que desea cargar los datos que se encuentran en el archivo seleccionado?', function(e) {
				ucd.showMessageBefore();
				if (e) {
					event.preventDefault();
					var formData = new FormData();
					formData.append(form[1], $(`#${form[1]}`)[0].files[0]);
					ucd.ajax(`#${form[0]}`, formData, method, type);
				} else {
					$('#btn-continue').attr('disabled', false);
				}
			}, function(){}).set({'movable':false,'moveBounded':false,'labels':{ok:'Sí',cancel:'No'}});
		});
	}
	
	showUploadCondition(token) {
		this.showGeneralForm(`conditionImport`, `Presión y Temperatura Original`, [`upload-condition`, `condition`], token, ucd.showConditionDoc, 1);
	}
	
	showUploadHistorical(token) {
		this.showGeneralForm(`historicalImport`, `Histórico Completaciones`, [`upload-historical`, `historical`], token, ucd.showHistoricalDoc, 2);
	}
	
	showUploadStaticPressure(token) {
		this.showGeneralForm(`staticPressureImport`, `Presiones Estáticas`, [`upload-static-pressure`, `staticPressure`], token, ucd.showStaticPressureDoc, 3);
	}
	
	showUploadDynamicPressure(token) {
		this.showGeneralForm(`dynamicPressureImport`, `Presiones Dinámicas`, [`upload-dynamic-pressure`, `dynamicPressure`], token, ucd.showDynamicPressureDoc, 4);
	}
	
	showUploadOriginalPressure(token) {
		this.showGeneralForm(`originalPressureImport`, `Presión Original PMVM`, [`upload-original-pressure`, `originalPressure`], token, ucd.showOriginalPressureDoc, 5);
	}
	
	showUploadRealSurvey(token) {
		this.showGeneralForm(`realSurveyImport`, `Real Surveys`, [`upload-real-survey`, `realSurvey`], token, ucd.showRealSurveyDoc, 6);
	}
	
	showUploadPlanSurvey(token) {
		this.showGeneralForm(`planSurveyImport`, `Plan Surveys`, [`upload-plan-survey`, `planSurvey`], token, ucd.showPlanSurveyDoc, 7);
	}
	
	showConditionDoc(response) {

		/* console.log(response); */

		if (response.my)  {
			
			var macollaName = response.macolla, locationName = response.location, dataTable = ``, objectData = [], documentSize = Object.keys(response.sheet).length;
			
			$(`#location-label`).html(`<p><i class="fa fa-file-excel text-success"></i> <b>${macollaName}</b> / Localización ${locationName}</p>`);
			$(`#file-upload`).html(``);
			
			if (documentSize > 0) {
				
				var container = `
					<div class="card-body table-responsive p-0" style="height: 400px;"">
						<table class="table table-head-fixed table-bordered">
							<thead>
								<tr>
									<th class="bg-secondary"># <h6></h6></th>
									<th class="bg-secondary">Fecha <h6></h6></th>
									<th class="bg-secondary">Yacimiento <h6></h6></th>
									<th class="bg-secondary">Arena <h6></h6></th>
									<th class="bg-secondary">Compañía <h6></h6></th>
									<th class="bg-secondary">Test <h6></h6></th>
									<th class="bg-secondary">Profundidad&nbsp;MD <h6>pies</h6></th>
									<th class="bg-secondary">Profundidad&nbsp;TVD <h6>pies</h6></th>
									<th class="bg-secondary">Temperatura <h6>ºF</h6></th>
									<th class="bg-secondary">Duración <h6>minutos</h6></th>
									<th class="bg-secondary">Presión&nbsp;Hid.&nbsp;Antes <h6>lppc</h6></th>
									<th class="bg-secondary">Presión&nbsp;Hid.&nbsp;Después <h6>lppc</h6></th>
									<th class="bg-secondary">Presión&nbsp;de&nbsp;Formación <h6>lppc</h6></th>
									<th class="bg-secondary">Movilidad <h6>mD/cP</h6></th>
									<th class="bg-secondary">Observaciones  <h6></h6></th>
								</tr>
							</thead>
							<tbody id="data"></tbody>
						</table>
					</div>
				`;
				$(`#excel-output`).html(container);
				
				for (var i = 0; i < documentSize; i++) {
					
					dataTable += `
						<tr>
							<td class="text-muted">${(i+1)}</td>
							<td>${m.setFormatDate(1, response.sheet[i].pt_date.date)}</td>
							${m.validateEmpty(response.sheet[i].reservoir.toString())}
							${m.validateEmpty(response.sheet[i].sand.toString())}
							${m.validateEmpty(response.sheet[i].company.toString())}
							${m.validateEmpty(response.sheet[i].test.toString())}
							${m.concat(response.sheet[i].deep_md, m.verifyValue(response.sheet[i].deep_md) ? '-' : m.setFormat(response.sheet[i].deep_md, 0) )}
							${m.concat(response.sheet[i].deep_tvd, m.verifyValue(response.sheet[i].deep_tvd) ? '-' : m.setFormat(response.sheet[i].deep_tvd, 0) )}
							${m.concat(response.sheet[i].temperature, m.verifyValue(response.sheet[i].temperature) ? '-' : m.setFormat(response.sheet[i].temperature, 0) )}
							${m.concat(response.sheet[i].duration, m.verifyValue(response.sheet[i].duration) ? '-' : m.setFormat(response.sheet[i].duration, 0) )}
							${m.concat(response.sheet[i].pressbefore, m.verifyValue(response.sheet[i].pressbefore) ? '-' : m.setFormat(response.sheet[i].pressbefore, 2) )}
							${m.concat(response.sheet[i].pressafter, m.verifyValue(response.sheet[i].pressafter) ? '-' : m.setFormat(response.sheet[i].pressafter, 2) )}
							${m.concat(response.sheet[i].pressformation, m.verifyValue(response.sheet[i].pressformation) ? '-' : m.setFormat(response.sheet[i].pressformation, 0) )}
							${m.concat(response.sheet[i].mobility, m.verifyValue(response.sheet[i].mobility) ? '-' : m.setFormat(response.sheet[i].mobility, 0) )}
							${m.validateEmpty(response.sheet[i].observations.toString())}
						</tr>
					`;
					
					objectData.push({
						well_id: 0,
						company: response.sheet[i].company,
						reservoir: response.sheet[i].reservoir,
						sand: response.sheet[i].sand,
						pt_date: moment(response.sheet[i].pt_date.date).format('YYYY-MM-DD'),
						test: response.sheet[i].test,
						deep_md: response.sheet[i].deep_md,
						deep_tvd: response.sheet[i].deep_tvd,
						temperature: response.sheet[i].temperature,
						duration: response.sheet[i].duration,
						pressbefore: response.sheet[i].pressbefore,
						pressafter: response.sheet[i].pressafter,
						pressformation: response.sheet[i].pressformation,
						mobility: response.sheet[i].mobility,
						observations: response.sheet[i].observations,
						identifier: response.sheet[i].identifier
					});
				}
				$(`#data`).html(dataTable);
				
				ucd.setHtmlButtons();
				
				$("#btn-save").click(function(event) {
					alertify.confirm('<b class="text-warning"><i class="fa fa-exclamation-triangle"></i></b> Atención', '¿Está seguro que desea guardar la información de este archivo en la base de datos?', function(e) {
						if (e) { ucd.saveCondition(objectData, locationName); }
					}, function(){}).set({'movable': false, 'moveBounded': false, 'labels': {ok: 'Sí', cancel: 'No'}});
				});

				ucd.toReturn(second);

			} else $(`#excel-output`).html(`<h5>No se encontraron datos que mostrar.</h5>`);
		} else ucd.setErrorMessageToUpload(response.location, 1);
	}
	
	showHistoricalDoc(response) {

		/*console.log(response.sheet);*/
		
		if (response.my) {
			
			var macollaName = response.macolla, dataTable = ``, objectData = [], names = [], documentSize = Object.keys(response.sheet).length;
			
			$(`#location-label`).html(`<p><i class="fa fa-file-excel text-success"></i> <b>${macollaName}</b></p>`);
			$(`#file-upload`).html(``);
			
			if (documentSize > 0) {
				
				var container = `
					<div class="card-body table-responsive p-0" style="height: 400px;"">
						<table class="table table-head-fixed table-bordered">
							<thead>
								<tr>
									<th class="bg-secondary"># <h6></h6></th>
									<th class="bg-secondary">Localización <h6></h6></th>
									<th class="bg-secondary">Yacimiento <h6></h6></th>
									<th class="bg-secondary">Fecha&nbsp;inicial&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<h6></h6></th>
									<th class="bg-secondary">Fecha&nbsp;final&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<h6></h6></th>
									<th class="bg-secondary">Tipo&nbsp;de&nbsp;Trabajo <h6></h6></th>
									<th class="bg-secondary">Tipo&nbsp;de&nbsp;Trabajo&nbsp;II <h6></h6></th>
									<th class="bg-secondary">Equipo&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<h6></h6></th>
									<th class="bg-secondary">Sensor&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <h6>modelo</h6></th>
									<th class="bg-secondary">Sensor<h6>plg&nbsp;diámetro</h6></th>
									<th class="bg-secondary">Sensor<h6>compañía</h6></th>
									<th class="bg-secondary">Sensor<h6>marca</h6></th>
									<th class="bg-secondary">Prof.&nbsp;Sup.&nbsp;Sensor&nbsp;MD <h6>pies</h6></th>
									<th class="bg-secondary">Prof.&nbsp;Inf.&nbsp;Sensor&nbsp;MD <h6>pies</h6></th>
									<th class="bg-secondary">Bomba&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<h6>modelo</h6></th>
									<th class="bg-secondary">Bomba <h6>plg&nbsp;diámetro</h6></th>
									<th class="bg-secondary">Método&nbsp;bombeo <h6></h6></th>
									<th class="bg-secondary">Capacidad&nbsp;Bomba <h6>BPD/100 RPM</h6></th>
									<th class="bg-secondary">Bomba <h6>Compañía</h6></th>
									<th class="bg-secondary">Prof.&nbsp;Sup.&nbsp;Bomba&nbsp;MD <h6>pies</h6></th>
									<th class="bg-secondary">Prof.&nbsp;Inf.&nbsp;Bomba&nbsp;MD <h6>pies</h6></th>
									<th class="bg-secondary">Inyección&nbsp;diluente <h6></h6></th>
									<th class="bg-secondary">Medidas&nbsp;desde <h6></h6></th>
									<th class="bg-secondary">Observaciones <h6></h6></th>
									<th class="bg-secondary">Fuentes <h6></h6></th>
								</tr>
							</thead>
							<tbody id="data"></tbody>
						</table>
					</div>
				`;  		
				$(`#excel-output`).html(container);
				
				for (var i = 0; i < documentSize; i++) {
					
					dataTable += `
						<tr>
							<td class="text-muted">${(i+1)}</td>
							${m.validateEmpty(response.sheet[i].location)}
							${m.validateEmpty(response.sheet[i].reservoir)}
							<td>${m.setFormatDate(2, response.sheet[i].begin_date.date)}</td>
							<td>${m.setFormatDate(2, response.sheet[i].end_date.date)}</td>
							${m.validateEmpty(response.sheet[i].type_work_i.toString())}
							${m.validateEmpty(response.sheet[i].type_work_ii.toString())}
							${m.validateEmpty(response.sheet[i].rig.toString())}
							${m.validateEmpty(response.sheet[i].sensor_model.toString())}
							${m.concat(response.sheet[i].sensor_diameter, m.verifyValue(response.sheet[i].sensor_diameter) ? '-' : m.decimalToFraction(response.sheet[i].sensor_diameter))}
							${m.validateEmpty(response.sheet[i].sensor_company.toString())}
							${m.validateEmpty(response.sheet[i].sensor_brand.toString())}
							${m.concat(response.sheet[i].deeptop_sensor_md, m.verifyValue(response.sheet[i].deeptop_sensor_md) ? '-' : m.setFormat(response.sheet[i].deeptop_sensor_md, 0))}
							${m.concat(response.sheet[i].deepbottom_sensor_md, m.verifyValue(response.sheet[i].deepbottom_sensor_md) ? '-' : m.setFormat(response.sheet[i].deepbottom_sensor_md, 0))}
							${m.validateEmpty(response.sheet[i].pump_model.toString())}
							${m.concat(response.sheet[i].pump_diameter, m.verifyValue(response.sheet[i].pump_diameter) ? '-' : m.decimalToFraction(response.sheet[i].pump_diameter))}
							${m.validateEmpty(response.sheet[i].pump_type.toString())}
							${m.concat(response.sheet[i].pump_capacity, m.verifyValue(response.sheet[i].pump_capacity) ? '-' : m.setFormat(response.sheet[i].pump_capacity, 0))}
							${m.validateEmpty(response.sheet[i].pump_company.toString())}
							${m.concat(response.sheet[i].deeptop_pump_md, m.verifyValue(response.sheet[i].deeptop_pump_md) ? '-' : m.setFormat(response.sheet[i].deeptop_pump_md, 0))}
							${m.concat(response.sheet[i].deepbottom_pump_md, m.verifyValue(response.sheet[i].deepbottom_pump_md) ? '-' : m.setFormat(response.sheet[i].deepbottom_pump_md, 0))}
							${m.validateEmpty(response.sheet[i].inyected_diluent.toString())}
							${m.validateEmpty(response.sheet[i].measure_from.toString())}
							${m.validateEmpty(response.sheet[i].observations.toString())}
							${m.validateEmpty(response.sheet[i].references.toString())}
						</tr>
					`;
					
					objectData.push({
						well_id: 0,
						reservoir: response.sheet[i].reservoir,
						begin_date: moment(response.sheet[i].begin_date.date).format("YYYY-MM-DD HH:mm:ss"),
						end_date: moment(response.sheet[i].end_date.date).format("YYYY-MM-DD HH:mm:ss"),
						type_work_i: response.sheet[i].type_work_i,
						type_work_ii: response.sheet[i].type_work_ii,
						rig: response.sheet[i].rig,
						sensor_model: response.sheet[i].sensor_model,
						sensor_diameter: response.sheet[i].sensor_diameter,
						sensor_company: response.sheet[i].sensor_company,
						sensor_brand: response.sheet[i].sensor_brand,
						deeptop_sensor_md: response.sheet[i].deeptop_sensor_md,
						deepbottom_sensor_md: response.sheet[i].deepbottom_sensor_md,
						pump_model: response.sheet[i].pump_model,
						pump_diameter: response.sheet[i].pump_diameter,
						pump_type: response.sheet[i].pump_type,
						pump_capacity: response.sheet[i].pump_capacity,
						pump_company: response.sheet[i].pump_company,
						deeptop_pump_md: response.sheet[i].deeptop_pump_md,
						deepbottom_pump_md: response.sheet[i].deepbottom_pump_md,
						inyected_diluent: response.sheet[i].inyected_diluent,
						measure_from: response.sheet[i].measure_from,
						observations: response.sheet[i].observations,
						references: response.sheet[i].references,
						identifier: response.sheet[i].identifier
					});
					
					names.push({location: response.sheet[i].location});
				}
				$(`#data`).html(dataTable);
				
				ucd.setHtmlButtons();
				
				$("#btn-save").click(function(event) {
					alertify.confirm('<b class="text-warning"><i class="fa fa-exclamation-triangle"></i></b> Atención', '¿Está seguro que desea guardar la información de este archivo en la base de datos?', function(e) {
						if (e) { ucd.saveHistorical(objectData, names, macollaName); }
					}, function(){}).set({'movable': false, 'moveBounded': false, 'labels': {ok: 'Sí', cancel: 'No'}});
				});
				
				ucd.toReturn(second);
			
			} else $(`#excel-output`).html(`<h5>No se encontraron datos que mostrar.</h5>`);
		} else ucd.setErrorMessageToUpload(response.macolla, 3);
	}
	
	showStaticPressureDoc(response) {

		/*console.log(response);*/

		if (response.my) {
			
			var dataTable = ``, objectData = [], names = [], macollaName = response.macolla, documentSize = Object.keys(response.sheet).length;
			
			$(`#location-label`).html(`<p><i class="fa fa-file-excel text-success"></i> <b>${macollaName}</b></p>`);
			$(`#file-upload`).html(``);
			
			if (documentSize > 0) {
				
				var container = `
					<div class="card-body table-responsive p-0" style="height: 400px;"">
						<table class="table table-head-fixed table-bordered">
							<thead>
								<tr>
									<th class="bg-secondary"># <h6></h6></th>
									<th class="bg-secondary">Localización <h6></h6></th>
									<th class="bg-secondary">Fecha&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<h6></h6></th>
									<th class="bg-secondary">Calidad <h6></h6></th>
									<th class="bg-secondary">Pe)EB <h6>lppc</h6></th>
									<th class="bg-secondary">PDPe)SB<h6>lppc</h6></th>
									<th class="bg-secondary">Te)EB <h6>ºF</h6></th>
									<th class="bg-secondary">Observaciones <h6></h6></th>
								</tr>
							</thead>
							<tbody id="data"></tbody>
						</table>
					</div>
				`;  		
				$(`#excel-output`).html(container);
				
				for (var i = 0; i < documentSize; i++) {
					dataTable += `
						<tr>
							<td class="text-muted">${(i+1)}</td>
							${m.validateEmpty(response.sheet[i].location.toString())}
							<td>${m.setFormatDate(2, response.sheet[i].static_date.date)}</td>
							${m.validateEmpty(response.sheet[i].quality.toString())}
							${m.concat(response.sheet[i].pip, m.verifyValue(response.sheet[i].pip) ? '-' : m.setFormat(response.sheet[i].pip, 0))}
							${m.concat(response.sheet[i].pdp, m.verifyValue(response.sheet[i].pdp) ? '-' : m.setFormat(response.sheet[i].pdp, 0))}
							${m.concat(response.sheet[i].temperature, m.verifyValue(response.sheet[i].temperature) ? '-' : m.setFormatLimit(response.sheet[i].temperature, 10, 2))}
							${m.validateEmpty(response.sheet[i].observations)}
						</tr>
					`;
					
					objectData.push({
						well_id: 0,
						static_date: moment(response.sheet[i].static_date.date).format('YYYY-MM-DD HH:mm:ss'),
						quality: response.sheet[i].quality,
						pip: response.sheet[i].pip,
						pdp: response.sheet[i].pdp,
						temperature: response.sheet[i].temperature,
						observations: response.sheet[i].observations,
						identifier: response.sheet[i].identifier
					});
					
					names.push({location: response.sheet[i].location});
				}
				$(`#data`).html(dataTable);
				
				ucd.setHtmlButtons();
				
				$("#btn-save").click(function(event) {
					alertify.confirm('<b class="text-warning"><i class="fa fa-exclamation-triangle"></i></b> Atención', '¿Está seguro que desea guardar la información de este archivo en la base de datos?', function(e) {
						if (e) {
							ucd.saveStaticPressure(objectData, names, macollaName);
						}
					}, function(){}).set({'movable': false, 'moveBounded': false, 'labels': {ok: 'Sí', cancel: 'No'}});
				});
				ucd.toReturn(second);
			} else $(`#excel-output`).html(`<h5>No se encontraron datos que mostrar.</h5>`);
		} else ucd.setErrorMessageToUpload(response.macolla, 2);
	}
	
	showDynamicPressureDoc(response) {

		/*console.log(response);*/

		if (response.my) {
			
			var dataTable = ``, objectData = [], names = [], macollaName = response.macolla, documentSize = Object.keys(response.sheet).length;
			
			$(`#location-label`).html(`<p><i class="fa fa-file-excel text-success"></i> <b>${macollaName}</b></p>`);
			$(`#file-upload`).html(``);
			
			if (documentSize > 0) {
				
				var container = `
					<div class="card-body table-responsive p-0" style="height: 400px;"">
						<table class="table table-head-fixed table-bordered">
							<thead>
								<tr>
									<th class="bg-secondary"># <h6></h6></th>
									<th class="bg-secondary">Localización <h6></h6></th>
									<th class="bg-secondary">Fecha&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<h6></h6></th>
									<th class="bg-secondary">Calidad <h6></h6></th>
									<th class="bg-secondary">Pwf)EB <h6>lppc</h6></th>
									<th class="bg-secondary">PDP)SB <h6>lppc</h6></th>
									<th class="bg-secondary">Twf)EB <h6>ºF</h6></th>
									<th class="bg-secondary">Observaciones <h6></h6></th>
								</tr>
							</thead>
							<tbody id="data"></tbody>
						</table>
					</div>
				`;
				$(`#excel-output`).html(container);
				
				for (var i = 0; i < documentSize; i++) {
					dataTable += `
						<tr>
							<td class="text-muted">${(i+1)}</td>
							${m.validateEmpty(response.sheet[i].location.toString())}
							<td>${m.setFormatDate(2, response.sheet[i].dynamic_date.date)}</td>
							${m.validateEmpty(response.sheet[i].quality.toString())}
							${m.concat(response.sheet[i].pip, m.verifyValue(response.sheet[i].pip) ? '-' : m.setFormat(response.sheet[i].pip, 0))}
							${m.concat(response.sheet[i].pdp, m.verifyValue(response.sheet[i].pdp) ? '-' : m.setFormat(response.sheet[i].pdp, 0))}
							${m.concat(response.sheet[i].temperature, m.verifyValue(response.sheet[i].temperature) ? '-' : m.setFormatLimit(response.sheet[i].temperature, 10, 2))}
							${m.validateEmpty(response.sheet[i].observations)}
						</tr>
					`;
					
					objectData.push({
						well_id: 0,
						dynamic_date: moment(response.sheet[i].dynamic_date.date).format('YYYY-MM-DD HH:mm:ss'),
						quality: response.sheet[i].quality,
						pip: response.sheet[i].pip,
						pdp: response.sheet[i].pdp,
						temperature: response.sheet[i].temperature,
						observations: response.sheet[i].observations,
						identifier: response.sheet[i].identifier
					});
					
					names.push({location: response.sheet[i].location});
				}
				$(`#data`).html(dataTable);
				
				ucd.setHtmlButtons();
				
				$("#btn-save").click(function(event) {
					alertify.confirm('<b class="text-warning"><i class="fa fa-exclamation-triangle"></i></b> Atención', '¿Está seguro que desea guardar la información de este archivo en la base de datos?', function(e) {
						if (e) {
							ucd.saveDynamicPressure(objectData, names, macollaName);
						}
					}, function(){}).set({'movable': false, 'moveBounded': false, 'labels': {ok: 'Sí', cancel: 'No'}});
				});	
				ucd.toReturn(second);
			} else $(`#excel-output`).html(`<h5>No se encontraron datos que mostrar.</h5>`);
		} else ucd.setErrorMessageToUpload(response.macolla, 2);
	}
	
	showOriginalPressureDoc(response) {

		/*console.log(response);*/
		
		if (response.my) {
			
			var dataTable = ``, objectData = [], names = [], documentSize = Object.keys(response.sheet).length;
			
			$(`#location-label`).html(`<p><i class="fa fa-file-excel text-success"></i> Presión Original PMVM</p>`);
			$(`#file-upload`).html(``);
			
			if (documentSize > 0) {
				
				var container = `
					<div class="card-body table-responsive p-0" style="height: 400px;"">
						<table class="table table-head-fixed table-bordered">
							<thead>
								<tr>
									<th class="bg-secondary"># <h6></h6></th>
									<th class="bg-secondary">Localización <h6></h6></th>
									<th class="bg-secondary">Yacimiento <h6></h6></th>
									<th class="bg-secondary">Fecha <h6></h6></th>
									<th class="bg-secondary">Tipo&nbsp;de&nbsp;Prueba <h6></h6></th>
									<th class="bg-secondary">Sensor&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <h6>modelo</h6></th>
									<th class="bg-secondary">Gradiente <h6>lppc/pies</h6></th>
									<th class="bg-secondary">Prof.&nbsp;DATUM&nbsp;Macolla&nbsp;MD <h6>pies</h6></th>
									<th class="bg-secondary">Prof.&nbsp;DATUM&nbsp;Macolla&nbsp;TVD <h6>pies</h6></th>
									<th class="bg-secondary">Presión&nbsp;Original&nbsp;DATUM&nbsp;Macolla <h6>lppc</h6></th>
									<th class="bg-secondary">Temp.&nbsp;Original&nbsp;DATUM&nbsp;Macolla <h6>ºF</h6></th>
									<th class="bg-secondary">Gradiente <h6>ºF/pies</h6></th>
								</tr>
							</thead>
							<tbody id="data"></tbody>
						</table>
					</div>
				`;  		
				$(`#excel-output`).html(container);
				
				for (var i = 0; i < documentSize; i++) {
					dataTable += `
						<tr>
							<td class="text-muted">${(i+1)}</td>
							${m.validateEmpty(response.sheet[i].location.toString())}
							${m.validateEmpty(response.sheet[i].reservoir.toString())}
							<td>${m.setFormatDate(1, response.sheet[i].pressure_date.date)}
							${m.validateEmpty(response.sheet[i].type_test.toString())}
							${m.validateEmpty(response.sheet[i].sensor_model.toString())}
							${m.concat(response.sheet[i].pressure_gradient, m.verifyValue(response.sheet[i].pressure_gradient) ? '-' : m.setFormat(response.sheet[i].pressure_gradient, 3))}
							${m.concat(response.sheet[i].deepdatum_mc_md, m.verifyValue(response.sheet[i].deepdatum_mc_md) ? '-' : m.setFormat(response.sheet[i].deepdatum_mc_md, 0))}
							${m.concat(response.sheet[i].deepdatum_mc_tvd, m.verifyValue(response.sheet[i].deepdatum_mc_tvd) ? '-' : m.setFormat(response.sheet[i].deepdatum_mc_tvd, 0))}
							${m.concat(response.sheet[i].press_datum_macolla, m.verifyValue(response.sheet[i].press_datum_macolla) ? '-' : m.setFormat(response.sheet[i].press_datum_macolla, 0))}
							${m.concat(response.sheet[i].temp_datum_macolla, m.verifyValue(response.sheet[i].temp_datum_macolla) ? '-' : m.setFormatLimit(response.sheet[i].temp_datum_macolla, 10, 2))}
							${m.concat(response.sheet[i].temperature_gradient, m.verifyValue(response.sheet[i].temperature_gradient) ? '-' : m.setFormat(response.sheet[i].temperature_gradient, 3))}
						</tr>
					`;
					
					objectData.push({
						well_id: 0,
						reservoir: response.sheet[i].reservoir,
						pressure_date: moment(response.sheet[i].pressure_date.date).format('YYYY-MM-DD'),
						type_test: response.sheet[i].type_test,
						sensor_model: response.sheet[i].sensor_model,
						pressure_gradient: response.sheet[i].pressure_gradient,
						deepdatum_mc_md: response.sheet[i].deepdatum_mc_md,
						deepdatum_mc_tvd: response.sheet[i].deepdatum_mc_tvd,
						press_datum_macolla: response.sheet[i].press_datum_macolla,
						temp_datum_macolla: response.sheet[i].temp_datum_macolla,
						temperature_gradient: response.sheet[i].temperature_gradient,
						identifier: response.sheet[i].identifier
					});
					
					names.push({location: response.sheet[i].location});
				}
				$(`#data`).html(dataTable);
				
				ucd.setHtmlButtons();
				
				$("#btn-save").click(function(event) {
					alertify.confirm('<b class="text-warning"><i class="fa fa-exclamation-triangle"></i></b> Atención', '¿Está seguro que desea guardar la información de este archivo en la base de datos?', function(e) {
						if (e) { ucd.saveOriginalPressure(objectData, names); }
					}, function(){}).set({'movable': false, 'moveBounded': false, 'labels': {ok: 'Sí', cancel: 'No'}});
				});
				ucd.toReturn(second);
			} else $(`#excel-output`).html(`<h5>No se encontraron datos que mostrar.</h5>`);
		} else ucd.setErrorMessageToUpload('', 3);
	}
	
	showRealSurveyDoc(response) {
		
		/*console.log(response);*/

		if (response.my) {
			
			var dataTable = ``, objectData = [], names = [], documentSize = Object.keys(response.sheet).length;
			
			$(`#location-label`).html(`<p><i class="fa fa-file-excel text-success"></i> Real Surveys</p>`);
			$(`#file-upload`).html(``);
			
			if (documentSize > 0) {
				var container = `
					<div class="card-body table-responsive p-0" style="height: 400px;"">
						<table class="table table-head-fixed table-bordered">
							<thead>
								<tr>
									<th class="bg-secondary"># <h6></h6></th>
									<th class="bg-secondary">Localización <h6></h6></th>
									<th class="bg-secondary">Fecha <h6></h6></th>
									<th class="bg-secondary">Hoyo <h6></h6></th>
									<th class="bg-secondary">Compañía <h6></h6></th>
									<th class="bg-secondary">MD <h6>pies</h6></th>
									<th class="bg-secondary">Inclinación <h6>grados</h6></th>
									<th class="bg-secondary">Azim&nbsp;Grid <h6>grados</h6></th>
									<th class="bg-secondary">TVD <h6>pies</h6></th>
									<th class="bg-secondary">VSEC <h6>pies</h6></th>
									<th class="bg-secondary">NS <h6>(N/S ft)</h6></th>
									<th class="bg-secondary">EW <h6>(E/W ft)</h6></th>
									<th class="bg-secondary">DLS <h6>(°/100ft)</h6></th>
									<th class="bg-secondary">Latitud <h6>(N/S ° ' ")</h6></th>
									<th class="bg-secondary">Longitud <h6>(E/W ° ' ")</h6></th>
									<th class="bg-secondary">Observaciones <h6></h6></th>
								</tr>
							</thead>
							<tbody id="data"></tbody>
						</table>
					</div>
				`; 		
				$(`#excel-output`).html(container);
				
				for (var i = 0; i < documentSize; i++) {
					dataTable += `
						<tr>
							<td class="text-muted">${(i+1)}</td>
							${m.validateEmpty(response.sheet[i].location.toString())}
							<td>${m.setFormatDate(1, response.sheet[i].rs_date.date)}</td>
							${m.validateEmpty(response.sheet[i].hole.toString())}
							${m.validateEmpty(response.sheet[i].company.toString())}
							${m.concat(response.sheet[i].md, m.verifyValue(response.sheet[i].md) ? '-' : m.setFormat(response.sheet[i].md, 2))}
							${m.concat(response.sheet[i].inclination, m.verifyValue(response.sheet[i].inclination) ? '-' : m.setFormat(response.sheet[i].inclination, 2))}
							${m.concat(response.sheet[i].azim_grid, m.verifyValue(response.sheet[i].azim_grid) ? '-' : m.setFormat(response.sheet[i].azim_grid, 2))}
							${m.concat(response.sheet[i].tvd, m.verifyValue(response.sheet[i].tvd) ? '-' : m.setFormat(response.sheet[i].tvd, 2))}
							${m.concat(response.sheet[i].vsec, m.verifyValue(response.sheet[i].vsec) ? '-' : m.setFormat(response.sheet[i].vsec, 2))}
							${m.concat(response.sheet[i].ns, m.verifyValue(response.sheet[i].ns) ? '-' : m.setFormat(response.sheet[i].ns, 2))}
							${m.concat(response.sheet[i].ew, m.verifyValue(response.sheet[i].ew) ? '-' : m.setFormat(response.sheet[i].ew, 2))}
							${m.concat(response.sheet[i].dls, m.verifyValue(response.sheet[i].dls) ? '-' : m.setFormat(response.sheet[i].dls, 2))}
							${m.concatString(m.formatString(m.setGeoData(response.sheet[i].la_g, response.sheet[i].la_m, response.sheet[i].la_s)))}
							${m.concatString(m.formatString(m.setGeoData(response.sheet[i].lo_g, response.sheet[i].lo_m, response.sheet[i].lo_s)))}
							${m.validateEmpty(response.sheet[i].observations.toString())}
						</tr>
					`;
					
					objectData.push({
						well_id: 0,
						rs_date: moment(response.sheet[i].rs_date.date).format('YYYY-MM-DD'),
						hole: response.sheet[i].hole,
						company: response.sheet[i].company,
						md: response.sheet[i].md,
						inclination: response.sheet[i].inclination,
						azim_grid: response.sheet[i].azim_grid,
						tvd: response.sheet[i].tvd,
						vsec: response.sheet[i].vsec,
						ns: response.sheet[i].ns,
						ew: response.sheet[i].ew,
						dls: response.sheet[i].dls,
						la_g: response.sheet[i].la_g,
						la_m: response.sheet[i].la_m,
						la_s: response.sheet[i].la_s,
						lo_g: response.sheet[i].lo_g,
						lo_m: response.sheet[i].lo_m,
						lo_s: response.sheet[i].lo_s,
						observations: response.sheet[i].observations,
						identifier: response.sheet[i].identifier
					});
					
					names.push({location: response.sheet[i].location});
				}
				$(`#data`).html(dataTable);
				
				ucd.setHtmlButtons();
				
				$("#btn-save").click(function(event) {
					alertify.confirm('<b class="text-warning"><i class="fa fa-exclamation-triangle"></i></b> Atención', '¿Está seguro que desea guardar la información de este archivo en la base de datos?', function(e) {
						if (e) ucd.saveRealSurvey(objectData, names);
					}, function(){}).set({'movable': false, 'moveBounded': false, 'labels': {ok: 'Sí', cancel: 'No'}});
				});

				ucd.toReturn(second);

			} else $(`#excel-output`).html(`<h5>No se encontraron datos que mostrar.</h5>`);
		} else ucd.setErrorMessageToUpload('', 3);
	}
	
	showPlanSurveyDoc(response) {
		
		/* console.log(response); */

		if (response.my) {
			
			var dataTable = ``, objectData = [], names = [], documentSize = Object.keys(response.sheet).length;
			
			$(`#location-label`).html(`<p><i class="fa fa-file-excel text-success"></i> Plan Surveys</p>`);
			$(`#file-upload`).html(``);
			
			if (documentSize > 0) {
				var container = `
					<div class="card-body table-responsive p-0" style="height: 400px;"">
						<table class="table table-head-fixed table-bordered">
							<thead>
								<tr>
									<th class="bg-secondary"># <h6></h6></th>
									<th class="bg-secondary">Localización <h6></h6></th>
									<th class="bg-secondary">Fecha <h6></h6></th>
									<th class="bg-secondary">MD <h6>pies</h6></th>
									<th class="bg-secondary">Inclinación <h6>grados</h6></th>
									<th class="bg-secondary">Azim&nbsp;Grid <h6>grados</h6></th>
									<th class="bg-secondary">TVD <h6>pies</h6></th>
									<th class="bg-secondary">VSEC <h6>pies</h6></th>
									<th class="bg-secondary">NS <h6>(N/S&nbsp;ft)</h6></th>
									<th class="bg-secondary">EW <h6>(E/W&nbsp;ft)</h6></th>
									<th class="bg-secondary">DLS <h6>(°/100&nbsp;ft)</h6></th>
									<th class="bg-secondary">Latitud <h6>(N/S ° ' ")</h6></th>
									<th class="bg-secondary">Longitud <h6>(E/W ° ' ")</h6></th>
									<th class="bg-secondary">Observaciones <h6></h6></th>
								</tr>
							</thead>
							<tbody id="data"></tbody>
						</table>
					</div>
				`;
				$(`#excel-output`).html(container);
				
				for (var i = 0; i < documentSize; i++) {
					console.log( i + " -> " + m.concat(response.sheet[i].dls, m.setFormat(response.sheet[i].dls, 2)));
					dataTable += `
						<tr>
							<td class="text-muted">${(i+1)}</td>
							${m.validateEmpty(response.sheet[i].location.toString())}
							<td>${m.setFormatDate(1, response.sheet[i].ps_date.date)}</td>
							${m.concat(response.sheet[i].md, m.setFormat(parseFloat(response.sheet[i].md), 2))}
							${m.concat(response.sheet[i].inclination, m.setFormat(parseFloat(response.sheet[i].inclination), 2))}
							${m.concat(response.sheet[i].azim_grid, m.setFormat(parseFloat(response.sheet[i].azim_grid), 2))}
							${m.concat(response.sheet[i].tvd, m.setFormat(parseFloat(response.sheet[i].tvd), 2))}
							${m.concat(response.sheet[i].vsec, m.setFormat(parseFloat(response.sheet[i].vsec), 2))}
							${m.concat(response.sheet[i].ns, m.setFormat(parseFloat(response.sheet[i].ns), 2))}
							${m.concat(response.sheet[i].ew, m.setFormat(parseFloat(response.sheet[i].ew), 2))}
							${m.concat(response.sheet[i].dls, m.setFormat(response.sheet[i].dls, 2))}
							${m.concatString(m.formatString(m.setGeoData(response.sheet[i].la_g, response.sheet[i].la_m, response.sheet[i].la_s)))}
							${m.concatString(m.formatString(m.setGeoData(response.sheet[i].lo_g, response.sheet[i].lo_m, response.sheet[i].lo_s)))}
							${m.validateEmpty(response.sheet[i].observations.toString())}
						</tr>
					`;
					
					objectData.push({
						well_id: 0,
						ps_date: moment(response.sheet[i].ps_date.date).format("YYYY-MM-DD"),
						md: response.sheet[i].md,
						inclination: response.sheet[i].inclination,
						azim_grid: response.sheet[i].azim_grid,
						tvd: response.sheet[i].tvd,
						vsec: response.sheet[i].vsec,
						ns: response.sheet[i].ns,
						ew: response.sheet[i].ew,
						dls: response.sheet[i].dls,
						la_g: response.sheet[i].la_g,
						la_m: response.sheet[i].la_m,
						la_s: response.sheet[i].la_s,
						lo_g: response.sheet[i].lo_g,
						lo_m: response.sheet[i].lo_m,
						lo_s: response.sheet[i].lo_s,
						observations: response.sheet[i].observations,
						identifier: response.sheet[i].identifier
					});	
					names.push({location: response.sheet[i].location});
				}
				$(`#data`).html(dataTable);

				ucd.setHtmlButtons();
				
				$("#btn-save").click(function(event) {
					alertify.confirm('<b class="text-warning"><i class="fa fa-exclamation-triangle"></i></b> Atención', '¿Está seguro que desea guardar la información de este archivo en la base de datos?', function(e) {
						if (e) {
							ucd.savePlanSurvey(objectData, names);
						}
					}, function(){}).set({'movable': false, 'moveBounded': false, 'labels': {ok: 'Sí', cancel: 'No'}});
				});

				ucd.toReturn(second);

			} else $(`#excel-output`).html(`<h5>No se encontraron datos que mostrar.</h5>`);
		} else ucd.setErrorMessageToUpload('', 3);
	}
	
	saveCondition(objects, name) {
		const ajax = new Ajax(`${globalConfig.appUrl}/saveCondition`, {
			headers: {
				accept: 'application/json',
			},
			method: 'POST',
			body: {
				elements: JSON.stringify(objects),
				location_name: name
			}
		});
		
		this.setHtmlMessage();
		ajax.result().then(response => {
			if (response.message) alertify.message(response.message).delay(5);
			alertify.success(response.msgStore).delay(5);
			this.unsetHtmlMessage();
		}).catch(error => {
			if (error.status === 400) {
			} else {
				this.unsetHtmlMessage();
				alertify.alert('<b class="text-danger"><i class="fa fa-exclamation-circle"></i> Error</b>', 'Ha ocurrido un error interno en el servidor. ' + this.setErrorMessage(error.message), function(){}).set({'movable': false, 'moveBounded': false});
				console.log("Error: "+ error.message);
			}
		});
		return false;
	}
	
	saveHistorical(objects, names, mc) {
		const ajax = new Ajax(`${globalConfig.appUrl}/saveHistorical`, {
			headers: {
				accept: 'application/json',
			},
			method: 'POST',
			body: {
				elements: JSON.stringify(objects),
				names: JSON.stringify(names),
				macolla_name: mc
			}
		});
    
		this.setHtmlMessage();
		ajax.result().then(response => {
			if (response.message) alertify.message(response.message).delay(5);
			alertify.success(response.msgStore).delay(5);
			this.unsetHtmlMessage();
		}).catch(error => {
			if (error.status === 400) {
			} else {
				this.unsetHtmlMessage();
				alertify.alert('<b class="text-danger"><i class="fa fa-exclamation-circle"></i> Error</b>', 'Ha ocurrido un error interno en el servidor', function(){}).set({'movable': false, 'moveBounded': false});
				console.log("Error: "+ error.message);
			}
		});
		return false;
	}
	
	saveStaticPressure(objects, names, mc) {
		const ajax = new Ajax(`${globalConfig.appUrl}/saveStaticPressure`, {
			headers: {
				accept: 'application/json',
			},
			method: 'POST',
			body: {
				elements: JSON.stringify(objects),
				names: JSON.stringify(names),
				macolla_name: mc
			}
		});

		this.setHtmlMessage();
		ajax.result().then(response => {
			if (response.message) alertify.message(response.message).delay(5);
			alertify.success(response.msgStore).delay(5);
			this.unsetHtmlMessage();
		}).catch(error => {
			if (error.status === 400) {
			} else {
				this.unsetHtmlMessage();
				alertify.alert('<b class="text-danger"><i class="fa fa-exclamation-circle"></i> Error</b>', 'Ha ocurrido un error interno en el servidor.', function(){}).set({'movable': false, 'moveBounded': false});
				console.log("Error: "+ error.message);
			}
		});
		return false;
	}
	
	saveDynamicPressure(objects, names, mc) {
		const ajax = new Ajax(`${globalConfig.appUrl}/saveDynamicPressure`, {
			headers: {
				accept: 'application/json',
			},
			method: 'POST',
			body: {
				elements: JSON.stringify(objects),
				names: JSON.stringify(names),
				macolla_name: mc
			}
		});

		this.setHtmlMessage();
		ajax.result().then(response => {
			if (response.message) alertify.message(response.message).delay(5);
			alertify.success(response.msgStore).delay(5);
			this.unsetHtmlMessage();
		}).catch(error => {
			if (error.status === 400) {
			} else {
				this.unsetHtmlMessage();
				alertify.alert('<b class="text-danger"><i class="fa fa-exclamation-circle"></i> Error</b>', 'Ha ocurrido un error interno en el servidor', function(){}).set({'movable': false, 'moveBounded': false});
				console.log("Error: "+ error.message);
			}
		});
		return false;
	}
	
	saveOriginalPressure(objects, names) {
		const ajax = new Ajax(`${globalConfig.appUrl}/saveOriginalPressure`, {
			headers: {
				accept: 'application/json',
			},
			method: 'POST',
			body: {
				elements: JSON.stringify(objects),
				names: JSON.stringify(names)
			}
		});

		this.setHtmlMessage();
		ajax.result().then(response => {
			if (response.message) alertify.message(response.message).delay(5);
			alertify.success(response.msgStore).delay(5);
			this.unsetHtmlMessage();
		}).catch(error => {
			if (error.status === 400) {
			} else {
				this.unsetHtmlMessage();
				alertify.alert('<b class="text-danger"><i class="fa fa-exclamation-circle"></i> Error</b>', 'Ha ocurrido un error interno en el servidor', function(){}).set({'movable': false, 'moveBounded': false});
				console.log("Error: "+ error.message);
			}
		});
		return false;
	}
	
	saveRealSurvey(objects, names) {
		const ajax = new Ajax(`${globalConfig.appUrl}/saveRealSurvey`, {
			headers: {
				accept: 'application/json',
			},
			method: 'POST',
			body: {
				elements: JSON.stringify(objects),
				names: JSON.stringify(names)
			}
		});

		this.setHtmlMessage();
		
		ajax.result().then(response => {
			if (response.message) alertify.message(response.message).delay(5);
			alertify.success(response.msgStore).delay(5);
			this.unsetHtmlMessage();
		}).catch(error => {
			if (error.status === 400) {
			} else {
				this.unsetHtmlMessage();
				alertify.alert('<b class="text-danger"><i class="fa fa-exclamation-circle"></i> Error</b>', 'Ha ocurrido un error interno en el servidor', function(){}).set({'movable': false, 'moveBounded': false});
				console.log("Error: "+ error.message);
			}
		});
		return false;
	}
	
	savePlanSurvey(objects, names) {
		const ajax = new Ajax(`${globalConfig.appUrl}/savePlanSurvey`, {
			headers: {
				accept: 'application/json',
			},
			method: 'POST',
			body: {
				elements: JSON.stringify(objects),
				names: JSON.stringify(names)
			}
		});

		this.setHtmlMessage();
		ajax.result().then(response => {
			if (response.message) alertify.message(response.message).delay(5);
			alertify.success(response.msgStore).delay(5);
			this.unsetHtmlMessage();
		}).catch(error => {
			if (error.status === 400) {
			} else {
				this.unsetHtmlMessage();
				alertify.alert('<b class="text-danger"><i class="fa fa-exclamation-circle"></i> Error</b>', 'Ha ocurrido un error interno en el servidor', function(){}).set({'movable': false, 'moveBounded': false});
				console.log("Error: "+ error.message);
			}
		});
		return false;
	}
	
	returnMain(inner) {
		document.querySelector(`#main-card`).className = 'card card-default';
		$(`#card-title`).html(`<h3 class="card-title" id="card-title"><b>Cargar datos</b> / Condiciones de subsuelo</h3>`);

		$(`#show-form, #option-buttons, #excel-output`).each(function() {
			$(this).html(``);
		});
		$(`#main-options`).html(inner);
		this.toBack();

		ucd.setClickEvent(1, "btn-condition-2");
		ucd.setClickEvent(2, "btn-historical-2");
		ucd.setClickEvent(3, "btn-static-pressure-2");
		ucd.setClickEvent(4, "btn-dynamic-pressure-2");
		ucd.setClickEvent(5, "btn-original-pressure-2");
		ucd.setClickEvent(6, "btn-real-survey-2");
		ucd.setClickEvent(7, "btn-plan-survey-2");
	}
	
	verify(objForm) { 
		var arrExtensions = new Array("xls", "xlsx");
		var objInput = objForm.elements["condition"]||objForm.elements["historical"]||objForm.elements["staticPressure"]||objForm.elements["originalPressure"]||objForm.elements["realSurvey"]||objForm.elements["dynamicPressure"]||objForm.elements["planSurvey"];
		var strFilePath = objInput.value; 
		var arrTmp = strFilePath.split("."); 
		var strExtension = arrTmp[arrTmp.length-1].toLowerCase(); 
		var blnExists = false; 
		
		for (var i = 0; i < arrExtensions.length; i++) {
			if (strExtension == arrExtensions[i]) { 
				blnExists = true; 
				break;
			}
		}
		
		if (!blnExists) {
			alertify.alert('<b class="text-danger"><i class="fa fa-exclamation-circle"></i> Error</b>', 'Asegúrese de cargar un archivo <b>Excel</b>', function(){}).set({'movable': false, 'moveBounded': false});
			$(`#btn-continue`).attr('disabled', true);
		} else $(`#btn-continue`).attr('disabled', false);
		
		return blnExists;
	}
	
	toBack() {
		$('#btn-condition').click(function(event) {
			ucd.showUploadCondition($(`input[name="_token"]`).val());
		});
		$('#btn-historical').click(function(event) {
			ucd.showUploadHistorical($(`input[name="_token"]`).val());
		});
		$('#btn-static-pressure').click(function(event) {
			ucd.showUploadStaticPressure($(`input[name="_token"]`).val());
		});
		$('#btn-dynamic-pressure').click(function(event) {
			ucd.showUploadDynamicPressure($(`input[name="_token"]`).val());
		});
		$('#btn-original-pressure').click(function(event) {
			ucd.showUploadOriginalPressure($(`input[name="_token"]`).val());
		});
		$('#btn-real-survey').click(function(event) {
			ucd.showUploadRealSurvey($(`input[name="_token"]`).val());
		});
		$('#btn-plan-survey').click(function(event) {
			ucd.showUploadPlanSurvey($(`input[name="_token"]`).val());
		});
	}
	
	toReturn(second) {
		$("#btn-back").click(function(event) {
			alertify.confirm('Atención', '¿Está seguro que desea regresar al menú principal?', function(e) {
				if (e) {
					ucd.returnMain(second);
				}
			}, function(){}).set({'movable': false, 'moveBounded': false, 'labels': {ok: 'Sí', cancel: 'No'}});
		});
	}
	
	changeCard(card, title) {
		document.querySelector(`#main-card`).className = `${card}`;
		$(`#card-title`).html(`<h3 class="card-title" id="card-title">${title}</h3>`);
		$(`#main-options`).html(``);
	}
	
	showForm(_idForm, _idInputs, url, token) {
		$(`#show-form`).html(`
			<form onchange="return ucd.verify(this)" id="${_idForm}" method="post" action="${url}">
				<div class="form-group">
    				<div id="location-label"></div>
    					<div id="file-upload">
							<p>
								<i class="fa fa-file-excel text-success"></i> Importar archivo Excel (*.xls, *.xlsx)
							</p>
    						<div class="row">
    							<div class="col-md-12">
    								<input name="${_idInputs}" id="${_idInputs}" class="form-control btn btn-default" type="file" accept=".xlsx, .xls" style="height:45px">
    								<input type="hidden" name="_token" value="${token}">
    							</div>
    						</div>
						</div>
					</div>
				</div>
			</form>    	
		`);
		
		$(`#add-self`).html(`<div class="row col-md-12"></div>`);
		
		$(`#option-buttons`).html(`
			<div class="card-footer">
  				<button class="btn btn-secondary" id="btn-back"><i class="fas fa-angle-left"></i> Regresar</button>
  				<button class="btn btn-info float-right" id="btn-continue" disabled>Continuar <i class="fas fa-angle-right"></i></button>
  			</div>
		  `);
	}
	
	showMessageBefore() {
		$('#btn-continue').attr('disabled', true);
		$("#loading").html(`<div class="loading-animated"></div><small>Cargando, por favor espere...</small>`);
	}
	
	showMessageAfter() {
		$('#btn-continue').attr('disabled', false);
		$("#loading").html(``);
	}
	
	setHtmlButtons(value) {
		$(`#option-buttons`).html(`
			<div class="card-footer">
				<button class="btn btn-secondary" id="btn-back"><i class=" fas fa-angle-left"></i> Regresar</button>
				<button class="btn btn-success float-right" id="btn-save"><i class=" fas fa-save"></i> Guardar</button>
			</div>
		`);
	}
	
	setHtmlMessage() {
		$("#btn-save").attr("disabled", true);
		$("#loading-msg").html(`<br><h6>Guardando, espere hasta que se complete el proceso...</h6>`);
	}
	
	unsetHtmlMessage() {
		$("#btn-save").attr("disabled", true);
		$("#loading-msg").html(``);
	}

	setErrorMessage(str) {
		return (str != null) || (str != '') ? '<br><br> <b>Error: </b>' + str : '';
	}

	setLabelMessage(option) {
		if (option == 1) {
			return 'PyT Original';
		} else if (option == 2) {
			return 'Histórico Completación';
		} else if (option == 3) {
			return 'Presión Estática';
		} else if (option == 4) {
			return 'Presión Dinámica';
		} else if (option == 5) {
			return 'Presión Original';
		} else if (option == 6) {
			return 'Real Survey';
		} else if (option == 7) {
			return 'Plan Survey';
		}
	}
	
	ajax(formId, formData, formFunction, type) {
		$.ajax({
			url: $(formId).attr('action') + '?' + $(formId).serialize(),
			method: 'post',
			data: formData,
			processData: false,
			contentType: false
		}).done(function (data) {
			
			ucd.showMessageAfter();
			
			if (data.size > 0) {
				formFunction(data);
			} else {
				alertify.alert('<b class="text-danger"><i class="fa fa-exclamation-circle"></i> Aviso</b>', 'No hay datos que cargar.<br><br>Estas podrían ser las causas:<br>- El archivo Excel no corresponde a <b>' + ucd.setLabelMessage(type) + '</b>.<br>- Sin información en el archivo a importar.<br>- No se encontraron los pozos/macollas especificados.<br><br>Si ese no es el caso, revise su archivo y verifique que los datos coincidan con el formato especificado.', function(){}).set({'movable': false, 'moveBounded': false});
			}
		}).fail(function (error) {
			ucd.showMessageAfter();
			alertify.alert('<b class="text-danger"><i class="fa fa-exclamation-circle"></i> Error</b>', 'La estructura de la hoja es inválida, verifíquela e intente de nuevo con el formato especificado.' + ucd.setErrorMessage(error.responseJSON.message), function(){}).set({'movable': false, 'moveBounded': false});
			
		}).always(function () {
			$(formId)[0].reset();
			$('#btn-continue').attr('disabled', true);
		});
	}

	setErrorMessageToUpload(str, type) {
		if (type == 1) {
			if (str != "" && str != null) {
				alertify.alert('<b class="text-danger"><i class="fa fa-exclamation-circle"></i> Importante</b>', `La localización <b>${str}</b> no existe en el sistema.<br>Regístrela primero antes de proceder.`, function(){}).set({'movable': false, 'moveBounded': false});
			} else {
				alertify.alert('<b class="text-danger"><i class="fa fa-exclamation-circle"></i> Importante</b>', `La localización se encuentra vacía o es nula ya que no se ha específicado ningún nombre.`, function(){}).set({'movable': false, 'moveBounded': false});
			}
		} else if (type == 2) {
			if (str != "" && str != null) {
				alertify.alert('<b class="text-danger"><i class="fa fa-exclamation-circle"></i> Importante</b>', `La macolla <b>${str}</b> no existe en el sistema.<br>Regístrela primero antes de proceder.`, function(){}).set({'movable': false, 'moveBounded': false});
			} else {
				alertify.alert('<b class="text-danger"><i class="fa fa-exclamation-circle"></i> Importante</b>', `La macolla se encuentra vacía o es nula ya que no se ha específicado ningún nombre.`, function(){}).set({'movable': false, 'moveBounded': false});
			}
		} else {
			alertify.alert('<b class="text-danger"><i class="fa fa-exclamation-circle"></i> Importante</b>', `No se encontraron registros compatibles en su archivo.`, function(){}).set({'movable': false, 'moveBounded': false});
		}
	}

	setClickEvent(type, id) {
		$(`#${id}`).click(function (event) {
			
			if (type == 1) {t.addPressureAndTemp();}
			else if (type == 2) {t.addHistorical();}
			else if (type == 3) {t.addStatic();}
			else if (type == 4) {t.addDynamic();}
			else if (type == 5) {t.addOriginal();}
			else if (type == 6) {t.addSurveyReal();}
			else {t.addSurveyPlan();}

			ucd.setHtmlButtons();
			t.saveInputs();
			/*$(`#btn-save`).attr("disabled", true);*/
			ucd.toReturn(second);
		});
	}
}

window.onload = () => {
	ucd = new UploadCondition();
	m = new Method();
	t =new Add();

	$(`#main-options`).html(second);

	$(function() {ucd.toBack();});

	ucd.setClickEvent(1, "btn-condition-2");
	ucd.setClickEvent(2, "btn-historical-2");
	ucd.setClickEvent(3, "btn-static-pressure-2");
	ucd.setClickEvent(4, "btn-dynamic-pressure-2");
	ucd.setClickEvent(5, "btn-original-pressure-2");
	ucd.setClickEvent(6, "btn-real-survey-2");
	ucd.setClickEvent(7, "btn-plan-survey-2");
}