class UploadEvents {
	constructor() {
	}
	
	showUploadEvent(token) {
		
		var urlMethod = `${globalConfig.appUrl}/eventsImport`;
		this.changeCard('card card-default', '<b>Cargar datos</b> / Eventos de pozo');
		this.showForm('upload-events', 'events', urlMethod, token);
		this.toReturn(fourth);
		
		$('#btn-continue').click(function(event) {
			alertify.confirm('<i class="fa fa-question-circle"></i> Pregunta', '¿Está seguro que desea cargar los datos que se encuentran en el archivo seleccionado?', function(e) {
				uev.showMessageBefore();
				if (e) {
					event.preventDefault();
					var formData = new FormData();
					formData.append('events', $('#events')[0].files[0]);
					uev.ajax('#upload-events', formData, uev.showEventsDoc, 1);
				} else $('#btn-continue').attr('disabled', false);
			}, function(){}).set({'movable':false,'moveBounded':false,'modal': false, 'labels':{ok:'Sí',cancel:'No'}});
		});
	}
	
	showEventsDoc(response) {

		if (response.my) {
			
			var macollaName = response.macolla, wellName = response.well, reservoirName = response.reservoir, dataTable = ``, objectData = [], documentSize = Object.keys(response.sheet).length;
			
			$(`#location-label`).html(`<p><i class="fa fa-file-excel text-success"></i> <b>${macollaName}</b> / ${wellName} / <i>${reservoirName}</i></p>`);
			$(`#file-upload`).html(``);
			
			if (documentSize > 0) {

				$(`#excel-output`).html(`<div class="card-body table-responsive p-0" style="height: 400px;""><table class="table table-head-fixed table-bordered"><thead><tr><th class="bg-secondary"># <h6></h6></th><th class="bg-secondary">Fecha <h6></h6></th><th class="bg-secondary">Hora <h6></h6></th><th class="bg-secondary">Diluente<h6></h6></th><th class="bg-secondary">RPM<h6></h6></th><th class="bg-secondary">Bomba<h6>eficiencia</h6></th><th class="bg-secondary">Torque <h6>Lb/pies</h6></th><th class="bg-secondary">Frecuencia <h6>Hz</h6></th><th class="bg-secondary">Corriente <h6>(A)</h6></th><th class="bg-secondary">Potencia <h6>Kw-H</h6></th><th class="bg-secondary">Tensión&nbsp;red <h6>(V)</h6></th><th class="bg-secondary">Tensión&nbsp;salida <h6>(V)</h6></th><th class="bg-secondary">VFD&nbsp;Temperatura <h6>ºF</h6></th><th class="bg-secondary">Temperatura&nbsp;cabezal <h6>ºF</h6></th><th class="bg-secondary">Presión&nbsp;cabezal <h6>lppc</h6></th><th class="bg-secondary">Observaciones<h6></h6></th></tr></thead><tbody id="data"></tbody></table></div>`);
				
				for (var i = 0; i < documentSize; i++) {
					dataTable += `
						<tr>
							<td class="text-muted">${(i+1)}</td>
							<td>${m.setFormatDate(1, response.sheet[i].event_date.date)}</td>
							<td>${moment(response.sheet[i].event_time.date).format('HH:mm')}</td>
							${m.concat(response.sheet[i].diluent, m.verifyValue(response.sheet[i].diluent) ? '-' : m.setFormat(response.sheet[i].diluent, 0))}
							${m.concat(response.sheet[i].rpm, m.verifyValue(response.sheet[i].rpm) ? '-' : m.setFormat(response.sheet[i].rpm, 0))}
							${m.concat(response.sheet[i].pump_efficiency, m.verifyValue(response.sheet[i].pump_efficiency) ? '-' : m.setFormat(response.sheet[i].pump_efficiency, 2))}
							${m.concat(response.sheet[i].torque, m.verifyValue(response.sheet[i].torque) ? '-' : m.setFormat(response.sheet[i].torque, 2))}
							${m.concat(response.sheet[i].frequency, m.verifyValue(response.sheet[i].frequency) ? '-' : m.setFormat(response.sheet[i].frequency, 2))}
							${m.concat(response.sheet[i].current, m.verifyValue(response.sheet[i].current) ? '-' : m.setFormat(response.sheet[i].current, 2))}
							${m.concat(response.sheet[i].power, m.verifyValue(response.sheet[i].power) ? '-' : m.setFormat(response.sheet[i].power, 2))}
							${m.concat(response.sheet[i].mains_voltage, m.verifyValue(response.sheet[i].mains_voltage) ? '-' : m.setFormat(response.sheet[i].mains_voltage, 2))}
							${m.concat(response.sheet[i].output_voltage, m.verifyValue(response.sheet[i].output_voltage) ? '-' : m.setFormat(response.sheet[i].output_voltage, 2))}
							${m.concat(response.sheet[i].vfd_temperature, m.verifyValue(response.sheet[i].vfd_temperature) ? '-' : m.setFormat(response.sheet[i].vfd_temperature, 2))}
							${m.concat(response.sheet[i].head_temperature, m.verifyValue(response.sheet[i].head_temperature) ? '-' : m.setFormat(response.sheet[i].head_temperature, 2))}
							${m.concat(response.sheet[i].head_pressure, m.verifyValue(response.sheet[i].head_pressure) ? '-' : m.setFormat(response.sheet[i].head_pressure, 2))}
							${m.validateEmpty(response.sheet[i].observations)}
						</tr>
					`;
					
					objectData.push({
						well_id: 0,
						event_date: moment(response.sheet[i].event_date.date).format('YYYY-MM-DD'),
						event_time: moment(response.sheet[i].event_time.date).format('HH:mm'),
						diluent: response.sheet[i].diluent,
						rpm: response.sheet[i].rpm,
						pump_efficiency: response.sheet[i].pump_efficiency,
						torque: response.sheet[i].torque,
						frequency: response.sheet[i].frequency,
						current: response.sheet[i].current,
						power: response.sheet[i].power,
						mains_voltage: response.sheet[i].mains_voltage,
						output_voltage: response.sheet[i].output_voltage,
						vfd_temperature: response.sheet[i].vfd_temperature,
						head_temperature: response.sheet[i].head_temperature,
						head_pressure: response.sheet[i].head_pressure,
						identifier: response.sheet[i].identifier
					});
				}
				$(`#data`).html(dataTable);
				
				uev.setHtmlButtons();
				
				$("#btn-save").click(function(event) {
					alertify.confirm('<b class="text-warning"><i class="fa fa-exclamation-triangle"></i></b> Atención', '¿Está seguro que desea guardar la información de este archivo en la base de datos?', function(e) {
						if (e) {
							uev.saveEvents(objectData, wellName);
						}
					}, function(){}).set({'movable': false, 'moveBounded': false, 'labels': {ok: 'Sí', cancel: 'No'}});
				});
				
				uev.toReturn(fourth);
			
			} else $(`#excel-output`).html(`<h5>No se encontraron datos que mostrar.</h5>`);
		} else uev.setErrorMessageToUpload(response.well, 1);
	}
	
	saveEvents(objects, str) {
		const ajax = new Ajax(`${globalConfig.appUrl}/saveEvents`, {
			headers: { accept: 'application/json' },
			method: 'POST',
			body: {
				elements: JSON.stringify(objects),
				well_name: str
			}
		});

		this.setHtmlMessage();
		
		ajax.result().then(response => {

			if (response.message) alertify.message(response.message).delay(5);
			alertify.success(response.msgStore).delay(5);
			this.unsetHtmlMessage();

		}).catch(error => {
			if (error.status === 400) {
			} else {
				this.unsetHtmlMessage();
				alertify.alert('<b class="text-danger"><i class="fa fa-exclamation-circle"></i> Error</b>', 'Ha ocurrido un error interno en el servidor', function(){}).set({'movable': false, 'moveBounded': false});
				console.log(error);
			}
		});
		return false;
	}
	
	returnMain(inner) {
		document.querySelector(`#main-card`).className = 'card card-default';
		$(`#card-title`).html(`<h3 class="card-title" id="card-title"><b>Cargar datos</b> / Eventos de pozo</h3>`);
		$(`#show-form, #option-buttons, #excel-output`).each(function() {
			$(this).html(``);
		});
		$(`#main-options`).html(inner);
		this.toBack();

		uev.setClickEvent(1, "btn-events-2");
	}
	
	verify(objForm) { 
		var arrExtensions = new Array("xls", "xlsx");
		var objInput = objForm.elements["events"];
		var strFilePath = objInput.value; 
		var arrTmp = strFilePath.split("."); 
		var strExtension = arrTmp[arrTmp.length-1].toLowerCase(); 
		var blnExists = false; 
		for (var i = 0; i < arrExtensions.length; i++) {
			if (strExtension == arrExtensions[i]) { 
				blnExists = true; 
				break;
			}
		}
		
		if (!blnExists) {
			alertify.alert('<b class="text-danger"><i class="fa fa-exclamation-circle"></i> Error</b>', 'Asegúrese de cargar un archivo <b>Excel</b>', function(){}).set({'movable': false, 'moveBounded': false});
			$(`#btn-continue`).attr('disabled', true);
		} else $(`#btn-continue`).attr('disabled', false);
		
		return blnExists;
	}
	
	toBack() {
		$('#btn-events').click(function(event) { uev.showUploadEvent($(`input[name="_token"]`).val()); });
	}
	
	toReturn(fourth) {
		$("#btn-back").click(function(event) {
			alertify.confirm('Atención', '¿Está seguro que desea regresar al menú principal?', function(e) {
				if (e) uev.returnMain(fourth);
			}, function(){}).set({'movable': false, 'moveBounded': false, 'labels': {ok: 'Sí', cancel: 'No'}});
		});
	}
	
	changeCard(card, title) {
		document.querySelector(`#main-card`).className = `${card}`;
		$(`#card-title`).html(`<h3 class="card-title" id="card-title">${title}</h3>`);
		$(`#main-options`).html(``);
	}
	
	showForm(_idForm, _idInputs, url, token) {
		$(`#show-form`).html(`
			<form onchange="return uev.verify(this)" id="${_idForm}" method="post" action="${url}">
				<div class="form-group">
    				<div id="location-label"></div>
    					<div id="file-upload">
							<p>
								<i class="fa fa-file-excel text-success"></i> Importar archivo Excel (*.xls, *.xlsx)
							</p>
    						<div class="row">
    							<div class="col-md-12">
    								<input name="${_idInputs}" id="${_idInputs}" class="form-control btn btn-default" type="file" accept=".xlsx, .xls" style="height:45px">
    								<input type="hidden" name="_token" value="${token}">
    							</div>
    						</div>
    					</div>
					</div>
				</div>
    		</form>    	
		`);
		
		$(`#add-self`).html(`<div class="row col-md-12"></div>`);
		
		$(`#option-buttons`).html(`<div class="card-footer"><button class="btn btn-secondary" id="btn-back"><i class="fas fa-angle-left"></i> Regresar</button><button class="btn btn-success float-right" id="btn-continue" disabled>Continuar <i class="fas fa-angle-right"></i></button></div>`);
	}
	
	showMessageBefore() {
		$('#btn-continue').attr('disabled', true);
		$("#loading").html(`<div class="loading-animated"></div><small>Cargando, por favor espere...</small>`);
	}
	
	showMessageAfter() {
		$('#btn-continue').attr('disabled', false);
		$("#loading").html(``);
	}
	
	setHtmlButtons(value) {
		$(`#option-buttons`).html(`<div class="card-footer"><button class="btn btn-secondary" id="btn-back"><i class=" fas fa-angle-left"></i> Regresar</button><button class="btn btn-success float-right" id="btn-save"><i class=" fas fa-save"></i> Guardar</button></div>`);
	}
	
	setHtmlMessage() {
		$("#btn-save").attr("disabled", true);
		$("#loading-msg").html(`<br><h6>Guardando, espere hasta que se complete el proceso...</h6>`);
	}
	
	unsetHtmlMessage() {
		$("#btn-save").attr("disabled", true);
		$("#loading-msg").html(``);
	}

	setErrorMessage(str) {
		return (str != null) || (str != '') ? '<br><br> <b>Error: </b>' + str : '';
	}
	
	setLabelMessage(option) {
		if (option == 1) return 'Eventos de pozo';
		
	}
	
	ajax(formId, formData, formFunction, type) {
		$.ajax({
			url: $(formId).attr('action') + '?' + $(formId).serialize(),
			method: 'post',
			data: formData,
			processData: false,
			contentType: false
		}).done(function (data) {
			
			uev.showMessageAfter();
			
			if (data.size > 0) formFunction(data);
			else alertify.alert('<b class="text-danger"><i class="fa fa-exclamation-circle"></i> Aviso</b>', 'No hay datos que cargar.<br><br>Estas podrían ser las causas:<br>- El archivo Excel no corresponde a <b>' + uev.setLabelMessage(type) + '</b>.<br>- Sin información en el archivo a importar.<br>- No se encontraron los pozos/macollas especificados.<br><br>Si ese no es el caso, revise su archivo y verifique que los datos coincidan con el formato especificado.', function(){}).set({'movable': false, 'moveBounded': false});

			console.log(data);
			
		}).fail(function (error) {
			uev.showMessageAfter();
			alertify.alert('<b class="text-danger"><i class="fa fa-exclamation-circle"></i> Error</b>', 'La estructura de la hoja es inválida, verifíquela e intente de nuevo con el formato especificado.' + uev.setErrorMessage(error.responseJSON.message), function(){}).set({'movable': false, 'moveBounded': false});
			
			console.log(error);
			
		}).always(function () {
			$(formId)[0].reset();
			$('#btn-continue').attr('disabled', true);
		});
	}

	setErrorMessageToUpload(str, type) {
		if (type == 1) {
			if (str != "" && str != null) alertify.alert('<b class="text-danger"><i class="fa fa-exclamation-circle"></i> Importante</b>', `El pozo <b>${str}</b> no existe en el sistema.<br>Regístrelo primero antes de proceder.`, function(){}).set({'movable': false, 'moveBounded': false});
			else alertify.alert('<b class="text-danger"><i class="fa fa-exclamation-circle"></i> Importante</b>', `El campo de pozo se encuentra vacío o es nulo ya que no se ha específicado ningún nombre.`, function(){}).set({'movable': false, 'moveBounded': false});
		} else {
			if (str != "" && str != null) alertify.alert('<b class="text-danger"><i class="fa fa-exclamation-circle"></i> Importante</b>', `La macolla <b>${str}</b> no existe en el sistema.<br>Regístrela primero antes de proceder.`, function(){}).set({'movable': false, 'moveBounded': false});
			else alertify.alert('<b class="text-danger"><i class="fa fa-exclamation-circle"></i> Importante</b>', `La macolla se encuentra vacía o es nula ya que no se ha específicado ningún nombre.`, function(){}).set({'movable': false, 'moveBounded': false});
		}
	}

	setClickEvent(type, id) {
		$(`#${id}`).click(function (event) {
			
			if (type == 1) t.addEvents();

			uev.setHtmlButtons();
			t.saveInputs();
			/*$(`#btn-save`).attr("disabled", true);*/
			uev.toReturn(fourth);
		});
	}
}

window.onload = () => {
	m = new Method();
	uev = new UploadEvents();
	t = new Add();

	$(`#main-options`).html(fourth);
	$(function() {uev.toBack();});
	
	uev.setClickEvent(1, "btn-events-2");
}