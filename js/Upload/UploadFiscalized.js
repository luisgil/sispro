class Fiscalized {
	constructor() {
	}
	
	showUploadFiscalized(token) {
		
		var urlMethod = `${globalConfig.appUrl}/fiscalizedImport`;
		this.changeCard('card card-default', '<b>Cargar datos</b> / Producción Fiscalizada');
		this.showForm('upload-fiscalized', 'fiscalized', urlMethod, token);
		this.toReturn(third);
		
		$('#btn-continue').click(function(event) {
			alertify.confirm('<i class="fa fa-question-circle"></i> Pregunta', '¿Está seguro que desea cargar los datos que se encuentran en el archivo seleccionado?', function(e) {
				ufc.showMessageBefore();
				if (e) {
					event.preventDefault();
					var formData = new FormData();
					formData.append('fiscalized', $('#fiscalized')[0].files[0]);
					ufc.ajax('#upload-fiscalized', formData, ufc.showFiscalizedDoc, 1);
				} else $('#btn-continue').attr('disabled', false);
			}, function(){}).set({'movable':false,'moveBounded':false,'modal': false, 'labels':{ok:'Sí',cancel:'No'}});
		});
	}
	
	showFiscalizedDoc(response) {

		if (response.my) {
			
			var macollaName = response.macolla, dataTable = ``, objectData = [], documentSize = Object.keys(response.sheet).length;
			
			$(`#location-label`).html(`<p><i class="fa fa-file-excel text-success"></i> <b>${macollaName}</b></p>`);
			$(`#file-upload`).html(``);
			
			if (documentSize > 0) {
				
				var container = `
					<div class="card-body table-responsive p-0" style="height: 400px;"">
						<table class="table table-head-fixed table-bordered">
							<thead>
								<tr>
									<th class="bg-secondary"># <h6></h6></th>
									<th class="bg-secondary">Fecha <h6></h6></th>
									<th class="bg-secondary">Producción&nbsp;EHO&nbsp;Bruto <h6>BDP</h6></th>
									<th class="bg-secondary">%AyS<h6>DCO</h6></th>
									<th class="bg-secondary">ºAPI<h6>DCO</h6></th>
									<th class="bg-secondary">Inyección&nbsp;diluente <h6></h6></th>
									<th class="bg-secondary">%AyS<h6>diluente</h6></th>
									<th class="bg-secondary">ºAPI<h6>diluente</h6></th>
								</tr>
							</thead>
							<tbody id="data"></tbody>
						</table>
					</div>
				`;
				$(`#excel-output`).html(container);
				
				for (var i = 0; i < documentSize; i++) {
					dataTable += `
						<tr>
							<td class="text-muted">${(i+1)}</td>
							<td>${m.setFormatDate(1, response.sheet[i].fiscalized_date.date)}</td>
							${m.concat(response.sheet[i].production, m.setFormat(parseFloat(response.sheet[i].production), 0))}
							<td>-</td>
							<td>-</td>
							<td>-</td>
							<td>-</td>
							<td>-</td>
						</tr>
					`;
					
					objectData.push({
						macolla_id: 0,
						fiscalized_date: moment(response.sheet[i].fiscalized_date.date).format('YYYY-MM-DD'),
						production: response.sheet[i].production,
						identifier: response.sheet[i].identifier
					});
				}
				$(`#data`).html(dataTable);
				
				ufc.setHtmlButtons();
				
				$("#btn-save").click(function(event) {
					alertify.confirm('<b class="text-warning"><i class="fa fa-exclamation-triangle"></i></b> Atención', '¿Está seguro que desea guardar la información de este archivo en la base de datos?', function(e) {
						if (e) {
							ufc.saveFiscalized(objectData, macollaName);
						}
					}, function(){}).set({'movable': false, 'moveBounded': false, 'labels': {ok: 'Sí', cancel: 'No'}});
				});
				
				ufc.toReturn(third);
			
			} else $(`#excel-output`).html(`<h5>No se encontraron datos que mostrar.</h5>`);
		} else ufc.setErrorMessageToUpload(response.macolla, 2);
	}
	
	saveFiscalized(objects, str) {
		const ajax = new Ajax(`${globalConfig.appUrl}/saveFiscalized`, {
			headers: {
				accept: 'application/json',
			},
			method: 'POST',
			body: {
				elements: JSON.stringify(objects),
				macolla_name: str
			}
		});

		this.setHtmlMessage();
		
		ajax.result().then(response => {

			if (response.message) alertify.message(response.message).delay(5);
			alertify.success(response.msgStore).delay(5);
			this.unsetHtmlMessage();

		}).catch(error => {
			if (error.status === 400) {
			} else {
				this.unsetHtmlMessage();
				alertify.alert('<b class="text-danger"><i class="fa fa-exclamation-circle"></i> Error</b>', 'Ha ocurrido un error interno en el servidor', function(){}).set({'movable': false, 'moveBounded': false});
				console.log(error);
			}
		});
		return false;
	}
	
	returnMain(inner) {
		document.querySelector(`#main-card`).className = 'card card-default';
		$(`#card-title`).html(`<h3 class="card-title" id="card-title"><b>Cargar datos</b> / Producción Fiscalizada</h3>`);
		$(`#show-form, #option-buttons, #excel-output`).each(function() {
			$(this).html(``);
		});
		$(`#main-options`).html(inner);
		this.toBack();

		ufc.setClickEvent(1, "btn-fiscalized-2");
	}
	
	verify(objForm) { 
		var arrExtensions = new Array("xls", "xlsx");
		var objInput = objForm.elements["fiscalized"];
		var strFilePath = objInput.value; 
		var arrTmp = strFilePath.split("."); 
		var strExtension = arrTmp[arrTmp.length-1].toLowerCase(); 
		var blnExists = false; 
		for (var i = 0; i < arrExtensions.length; i++) {
			if (strExtension == arrExtensions[i]) { 
				blnExists = true; 
				break;
			}
		}
		
		if (!blnExists) {
			alertify.alert('<b class="text-danger"><i class="fa fa-exclamation-circle"></i> Error</b>', 'Asegúrese de cargar un archivo <b>Excel</b>', function(){}).set({'movable': false, 'moveBounded': false});
			$(`#btn-continue`).attr('disabled', true);
		} else {
			$(`#btn-continue`).attr('disabled', false);
		}
		return blnExists;
	}
	
	toBack() {
		$('#btn-fiscalized').click(function(event) {
			ufc.showUploadFiscalized($(`input[name="_token"]`).val());
		});
	}
	
	toReturn(third) {
		$("#btn-back").click(function(event) {
			alertify.confirm('Atención', '¿Está seguro que desea regresar al menú principal?', function(e) {
				if (e) {
					ufc.returnMain(third);
				}
			}, function(){}).set({'movable': false, 'moveBounded': false, 'labels': {ok: 'Sí', cancel: 'No'}});
		});
	}
	
	changeCard(card, title) {
		document.querySelector(`#main-card`).className = `${card}`;
		$(`#card-title`).html(`<h3 class="card-title" id="card-title">${title}</h3>`);
		$(`#main-options`).html(``);
	}
	
	showForm(_idForm, _idInputs, url, token) {
		$(`#show-form`).html(`
			<form onchange="return ufc.verify(this)" id="${_idForm}" method="post" action="${url}">
				<div class="form-group">
    				<div id="location-label"></div>
    					<div id="file-upload">
							<p>
								<i class="fa fa-file-excel text-success"></i> Importar archivo Excel (*.xls, *.xlsx)
							</p>
    						<div class="row">
    							<div class="col-md-12">
    								<input name="${_idInputs}" id="${_idInputs}" class="form-control btn btn-default" type="file" accept=".xlsx, .xls" style="height:45px">
    								<input type="hidden" name="_token" value="${token}">
    							</div>
    						</div>
    					</div>
					</div>
				</div>
    		</form>    	
		`);
		
		$(`#add-self`).html(`<div class="row col-md-12"></div>`);
		
		$(`#option-buttons`).html(`
			<div class="card-footer">
				<button class="btn btn-secondary" id="btn-back"><i class="fas fa-angle-left"></i> Regresar</button>
				<button class="btn btn-success float-right" id="btn-continue" disabled>Continuar <i class="fas fa-angle-right"></i></button>
			</div>
		`);
	}
	
	showMessageBefore() {
		$('#btn-continue').attr('disabled', true);
		$("#loading").html(`<div class="loading-animated"></div><small>Cargando, por favor espere...</small>`);
	}
	
	showMessageAfter() {
		$('#btn-continue').attr('disabled', false);
		$("#loading").html(``);
	}
	
	setHtmlButtons(value) {
		$(`#option-buttons`).html(`
			<div class="card-footer">
				<button class="btn btn-secondary" id="btn-back"><i class=" fas fa-angle-left"></i> Regresar</button>
				<button class="btn btn-success float-right" id="btn-save"><i class=" fas fa-save"></i> Guardar</button>
			</div>
		`);
	}
	
	setHtmlMessage() {
		$("#btn-save").attr("disabled", true);
		$("#loading-msg").html(`<br><h6>Guardando, espere hasta que se complete el proceso...</h6>`);
	}
	
	unsetHtmlMessage() {
		$("#btn-save").attr("disabled", true);
		$("#loading-msg").html(``);
	}

	setErrorMessage(str) {
		return (str != null) || (str != '') ? '<br><br> <b>Error: </b>' + str : '';
	}
	
	setLabelMessage(option) {
		if (option == 1) {
			return 'Producción Fiscalizada';
		}
	}
	
	ajax(formId, formData, formFunction, type) {
		$.ajax({
			url: $(formId).attr('action') + '?' + $(formId).serialize(),
			method: 'post',
			data: formData,
			processData: false,
			contentType: false
		}).done(function (data) {
			
			ufc.showMessageAfter();
			
			if (data.size > 0) {
				formFunction(data);
			} else {
				alertify.alert('<b class="text-danger"><i class="fa fa-exclamation-circle"></i> Aviso</b>', 'No hay datos que cargar.<br><br>Estas podrían ser las causas:<br>- El archivo Excel no corresponde a <b>' + ufc.setLabelMessage(type) + '</b>.<br>- Sin información en el archivo a importar.<br>- No se encontraron los pozos/macollas especificados.<br><br>Si ese no es el caso, revise su archivo y verifique que los datos coincidan con el formato especificado.', function(){}).set({'movable': false, 'moveBounded': false});
			}
		}).fail(function (error) {
			ufc.showMessageAfter();
			alertify.alert('<b class="text-danger"><i class="fa fa-exclamation-circle"></i> Error</b>', 'La estructura de la hoja es inválida, verifíquela e intente de nuevo con el formato especificado.' + ufc.setErrorMessage(error.responseJSON.message), function(){}).set({'movable': false, 'moveBounded': false});
			
			console.log(error);
			
		}).always(function () {
			$(formId)[0].reset();
			$('#btn-continue').attr('disabled', true);
		});
	}

	setErrorMessageToUpload(str, type) {
		if (type == 1) {
			if (str != "" && str != null) {
				alertify.alert('<b class="text-danger"><i class="fa fa-exclamation-circle"></i> Importante</b>', `La localización <b>${str}</b> no existe en el sistema.<br>Regístrela primero antes de proceder.`, function(){}).set({'movable': false, 'moveBounded': false});
			} else {
				alertify.alert('<b class="text-danger"><i class="fa fa-exclamation-circle"></i> Importante</b>', `La localización se encuentra vacía o es nula ya que no se ha específicado ningún nombre.`, function(){}).set({'movable': false, 'moveBounded': false});
			}
		} else {
			if (str != "" && str != null) {
				alertify.alert('<b class="text-danger"><i class="fa fa-exclamation-circle"></i> Importante</b>', `La macolla <b>${str}</b> no existe en el sistema.<br>Regístrela primero antes de proceder.`, function(){}).set({'movable': false, 'moveBounded': false});
			} else {
				alertify.alert('<b class="text-danger"><i class="fa fa-exclamation-circle"></i> Importante</b>', `La macolla se encuentra vacía o es nula ya que no se ha específicado ningún nombre.`, function(){}).set({'movable': false, 'moveBounded': false});
			}
		}
	}

	setClickEvent(type, id) {
		$(`#${id}`).click(function (event) {
			
			if (type == 1) {t.addFiscalized();}

			ufc.setHtmlButtons();
			t.saveInputs();
			/*$(`#btn-save`).attr("disabled", true);*/
			ufc.toReturn(third);
		});
	}
}

window.onload = () => {
	m = new Method();
	ufc = new Fiscalized();
	t = new Add();

	$(`#main-options`).html(third);
	$(function() {ufc.toBack();});
	
	ufc.setClickEvent(1, "btn-fiscalized-2");
}

