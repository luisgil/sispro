class AverageTest {
    constructor() {
        this.searchByFilter = this.searchByFilter.bind(this);
        this.setHeader = this.setHeader.bind(this);
    }
    
    searchByFilter() {

        var size = m.getColumnsFromDb().length, values = m.getColumnsFromDb(), preset = ['well_name', 'avg_start_test', 'avg_end_test', 'company', 'duration', 'test', 'macolla_name', 'location_name', 'reservoir_name'];

        for (var i = 0; i <= 8; i++) values[size + i] = preset[i];
        
        var range, typeSearch, reservoirId = [], collection = [];
        
        range = $("#filter-date").val() == 1 ?  m.getDatesRange($(`#date-pick`).val()) : m.getDatesRange(`${$(`#from`).val()}|${$(`#until`).val()}`);
        
        var whatIDo = document.querySelector(`#dual-list`).style.display == 'none';

        var from = moment(range[0]), until = moment(range[1]), isFrom = m.validateDate(from, 'msgFrom'), isUntil = m.validateDate(until, 'msgUntil');

        if (!whatIDo) {
            typeSearch = 1;
            var sizeIs = ($(`#location_well`).val()).length, store = $(`#location_well`).val();
            if (sizeIs > 1) for (var i = 0; i < sizeIs; i++) collection[i] = store[i];
            else collection = store[0];
        } else {
            $('input[name="option[]"]:checked').each(function(i) { reservoirId[i] = $(this).val(); });
            typeSearch = 2;
            var sizeIs = ($(`#macolla_list`).val()).length, store = $(`#macolla_list`).val();
            if (sizeIs > 1) for (var i = 0; i < sizeIs; i++) collection[i] = store[i];
            else collection = store[0];
        }

        if ((from.isValid() && isFrom) && (until.isValid() && isUntil)) {

            const ajax = new Ajax(`${globalConfig.appUrl}/averageTests/searchByFilterAverage`, {
                headers: {
                    accept: 'application/json',
                },
                method: 'POST',
                body: {
                    items: values,
                    data: collection,
                    macolla_id: parseInt($(`#macolla`).val()),
                    reservoir_id: reservoirId != null ? reservoirId : null,
                    date_from: range[0],
                    date_to: range[1],
                    type: typeSearch
                }
            });

            m.setLoadingMessageBefore();

            ajax.result().then(response => {

                var options = ['rpm', 'line_pressure', 'line_temperature', 'gas_rate_prod','water_rate_prod', 'fluid_total_rate', 'percent_mixture', 'api_degree_mixture', 'neat_rate_prod', 'percent_formation', 'gvf', 'inyected_diluent_rate', 'api_degree_diluent', 'pip', 'observations'];
                var size = Object.keys(response.data).length;

                m.setLoadingMessageAfter();
                m.showMessageDialog(size);

                if (size > 0) {

                    var selected = m.getColumnsToHead(options, Object.keys(response.data[0]));
                    var header = m.setColumn(response.type, response.info)[0], column = m.setColumn(response.type, response.info)[1];
                    
                    let detail = (response.type == 1) ? header : m.selectedItems(response.total);
                    var titleText = detail == 'SIN MACOLLA' ? detail : `Macolla ${detail}`;
                    
                    m.setHtmlTable(1, "Prueba Promedio", `${titleText}`);
                    m.setHtmlTable(2);

                    let headerSelected = `<th class='text-muted'>#</th>${column}<th class='text-muted'>Localización</th><th class='text-muted'>Pozo </th><th class='text-muted'>Yacimiento </th><th class='text-muted'>Inicio&nbsp;prueba </th><th class='text-muted'>Fin&nbsp;prueba </th><th class='text-muted'>Compañía </th><th class='text-muted'>Duración <h6 class="text-muted">horas</h6></th><th class='text-muted'>Prueba </th>`;

                    for (var i=0; i<selected.length; i++) headerSelected += `<th>${this.setHeader(selected[i])}</th>`;

                    document.querySelector(`#header`).innerHTML = headerSelected;

                    var table = m.newDateTable(response.results, size, `Pruebas Promedio ${titleText}`);

                    table.buttons().container().appendTo(`#buttons`);

                    $(`.dts_label`).remove();

                    document.querySelector('#imhere').focus();

                } else document.querySelector(`#table-content`).innerHTML = ``;
        
            }).catch(error => { console.log(error) });
        }
        return false;
    }
    
    setHeader(value) {
        if (value === 'line_pressure') return 'P<sub>línea</sub> <h6 class="text-muted">lppc</h6>';
        else if (value === 'line_temperature') return'T<sub>línea</sub> <h6 class="text-muted">ºF</h6>';
        else if(value === 'water_rate_prod') return 'q<sub>w</sub> <h6 class="text-muted">BPD</h6>';
        else if(value === 'gas_rate_prod') return 'q<sub>g</sub> <h6 class="text-muted">MPCND</h6>';
        else if(value === 'rpm') return 'RPM <h6 class="text-muted">BCP</h6>';
        else if(value === 'fluid_total_rate') return 'q<sub>t</sub> <h6 class="text-muted">BFPD</h6>';
        else if(value === 'percent_mixture') return '%AyS&nbsp;Mezcla';
        else if(value === 'api_degree_mixture') return 'ºAPI&nbsp;Mezcla';
        else if(value === 'percent_formation') return '%AyS&nbsp;Formación';
        else if(value === 'gvf') return 'FVG';
        else if(value === 'neat_rate_prod') return 'Tasa&nbsp;neta <h6 class="text-muted">BPPD</h6>';
        else if(value === 'inyected_diluent_rate') return 'Tasa&nbsp;inyección&nbsp;diluente <h6 class="text-muted">BDPD</h6>';
        else if (value === 'api_degree_diluent') return 'ºAPI&nbsp;Diluente';
        else if (value === 'pip') return 'PIP <h6 class="text-muted">lppc</h6>';
        else if (value === 'observations') return 'Observaciones';
    }
}

window.onload = () => {
    m = new Method();
    avg = new AverageTest();
}