class User {
    constructor() {
        this.register = this.register.bind(this);
        this.reloadView = this.reloadView.bind(this);
        this.showInputErrors = this.showInputErrors.bind(this);
        this.showInputCorrects = this.showInputCorrects.bind(this);
        this.inputNames = ['firstname', 'lastname', 'email', 'role', 'password', 'password_confirmation'];
        this.editInputNames = ['firstname', 'lastname', 'role'];
        this.genToPut = ``;
    }

    reloadView() {
        $(`#wrapper-firstname, #wrapper-lastname, #wrapper-email, #wrapper-password, #wrapper-role, #wrapper-password_confirmation`).each(function() {
            $(this).html(``);
        });
    }

    resetTextboxes() {
        for (let i = 0; i < this.inputNames.length; i++) {
            $(`#${this.inputNames[i]}`).removeClass();
            $(`#${this.inputNames[i]}`).addClass(`form-control`);
        }
    }

    showInputErrors(errors, inputNames) {
		for(const errorName in errors) {
			inputNames.map(inputName => {
				if (errorName === inputName) {
					document.getElementById(`${inputName}`).className = "form-control is-invalid";
					document.querySelector(`#wrapper-${inputName}`).innerHTML += `${errors[errorName]}`;
				}
			});
		}
	}
	
	showInputCorrects(errors, inputNames) {
		for(const errorName in errors) {
			inputNames.map(inputName => {
				if (errorName !== inputName && inputName !== 'password_confirmation') {
					$(`#${inputName}`).removeClass();
                    $(`#${inputName}`).addClass("form-control is-valid");
                }
			});
        }
    }
    
    register() {
        this.reloadView();
        event.preventDefault();

        $(`#btn-add`).attr('disabled', true);

        const ajax = new Ajax(`${globalConfig.appUrl}/users/create`, {
            headers: {
                accept: 'application/json',
            },
            method: 'POST',
            body: {
                email: $('#email').val().toLowerCase(),
                password: $('#password').val(),
                password_confirmation: $('#password_confirmation').val(),
                firstname: $('#firstname').val() != null || $('#firstname').val() != "" ? $('#firstname').val().toLowerCase() : "",
                lastname: $('#lastname').val() != null || $('#lastname').val() != "" ? $('#lastname').val().toLowerCase() : "",
                role: $('#role').val()
            }
        });
        
        ajax.result().then(response => {
            alertify.success(response.message);
            this.resetTextboxes();
            $(`#firstname, #lastname, #email, #password, #role, #password_confirmation`).each(function() {
                $(this).val(``);
            });
            $(`#btn-add`).attr('disabled', false);
        
        }).catch(error => {
            if (error.status === 400) {
                this.showInputCorrects(error.errors, this.inputNames);
                this.showInputErrors(error.errors, this.inputNames);
                $(`#btn-add`).attr('disabled', false);
            } else console.log(error);
        });
        return false;
    }
    
    edit(val, row, table) {

        var decrypt = CryptoJS.AES.decrypt(val, CryptoJS.SHA1(this.genToPut).toString()).toString(CryptoJS.enc.Utf8), data = JSON.parse(decrypt);

        $(`#modal-xl`).html(`
        <div class="modal-dialog modal-xl">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Editar Usuario</b></h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-4 pt-3">
                            <h6>Nombre</h6>
                            <input type="text" class="form-control onlychars" value="${data[0].firstname}"  id="firstname" name="firstname" maxlength="27" autocomplete="off"/>
                            <small id="wrapper-firstname" class="text-danger"></small>
                        </div>
                        <div class="col-md-4 pt-3">
                            <h6>Apellido</h6>
                            <input type="text" class="form-control onlychars" value="${data[0].lastname}" id="lastname" name="lastname" autocomplete="off" />
                            <small id="wrapper-lastname" class="text-danger"></small>
                        </div>
                        <div class="col-md-4 pt-3">
                            <h6>Rol</h6>
                            <select id="role" name="role" class="form-control">
                                <option value="${data[0].role}">${this.getSpanishRole(data[0].role)}</option>
                            </select>
                            <small id="wrapper-role" class="text-danger"></small>
                        </div>
                    </div>
                </div>
                <input type="hidden" id="identity" value="${data[0].user_id}">
				<input type="hidden" id="index" value="${data[0].index}">
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    <button type="button" class="btn btn-primary" id="btn-update">Guardar cambios</button>
                </div>
            </div>
        </div>`);

        $(`#btn-update`).attr('disabled', false);

        this.getAttributes(data[0].role);
        this.validateFields();

        $('#btn-update').click(function() {
            alertify.confirm('Pregunta', '¿Está seguro que desea guardar los cambios realizados?', function() {},
            function (e) { if (e) { usr.applyChanges(row, table); }
            }).set({'movable': false, 'moveBounded': false, 'modal': false, 'labels': {ok: 'No', cancel: 'Sí'}});
        });
    }

    getAttributes(x) {
		var roles = ['ADMIN', 'OPERATOR', /*'VALIDATOR',*/ 'EXAMINATOR'];
		this.setValuesInOption(roles, x, '#role');
	}
	
	setValuesInOption(array, value, id) {
		for (var i = 0; i < array.length; i++) {
			if (value != array[i]) {
				document.querySelector(`${id}`).innerHTML += `<option value="${array[i]}">${this.getSpanishRole(array[i])}</option>`;
			}
		}
    }
    
    getSpanishRole(string) {
        if (string == "ADMIN") return "Administrador";
        else if (string == "OPERATOR") return "Operador";
        else if (string == "VALIDATOR") return "Validador";
        else if (string == "EXAMINATOR") return "Consultor";
    }

    validateFields() {
        $('.onlychars').keypress(function(key) {
            if ( (key.charCode < 97 || key.charCode > 122) && (key.charCode < 65 || key.charCode > 90) && (key.charCode != 45) && (key.charCode != 241) && (key.charCode != 209) && (key.charCode != 32) && (key.charCode != 225) && (key.charCode != 233) && (key.charCode != 237) && (key.charCode != 243) && (key.charCode != 250) && (key.charCode != 193) && (key.charCode != 201) && (key.charCode != 205) && (key.charCode != 211) && (key.charCode != 218))
            return false;
        });
    }

    applyChanges(row, table) {
        
        this.reloadView();
        
        $(`#btn-update`).attr('disabled', true);

        const user_id = $('#identity').val();
        const index = $('#index').val();
        
        const ajax = new Ajax(`${globalConfig.appUrl}/edit/${user_id}`, {
            headers: {
                accept: 'application/json',
            },
            method: 'POST',
            body: {
                firstname: $('#firstname').val(),
                lastname: $('#lastname').val(),
                role: $('#role').val()
            }
        });
        
        ajax.result().then(response => {
            
            var dataset = [], mk = CryptoJS.SHA1(this.genToPut).toString(), encrypted, update = [];
            
            dataset.push({
                user_id: response.data.user_id,
                firstname: response.data.firstname,
                lastname: response.data.lastname,
                role: response.data.role,
                index: index
            });

            encrypted = CryptoJS.AES.encrypt(JSON.stringify(dataset), mk);

			alertify.alert('Enhorabuena', response.message, function(e){
				if(e) {
                    update = [index, response.data.firstname,response.data.lastname,response.data.email,usr.getSpanishRole(response.data.role),moment(response.data.created_at).format('DD/MM/YYYY'),((response.data.last_login)?moment(response.data.last_login).format('DD/MM/YYYY HH:mm:ss'):'-'),`<div class="btn-group btn-group-sm"><button class="btn btn-info" id="btn-modify" value='${encrypted}' data-toggle="modal" data-target="#modal-xl"><i class="fas fa-pen"></i></button><button class="btn btn-danger" id="btn-delete" value="${response.data.user_id}"><i class="fa fa-trash"></i></button></div>` ];
                    table.row(table.row(row).data(update).draw());
				}
			}).set({'movable': false, 'moveBounded': false});
            $(`#modal-xl`).modal('toggle');
            $(`#modal-xl`).html(``);
            
        }).catch(error => {
            if (error.status === 400) {
                this.showInputCorrects(error.errors, this.editInputNames);
                this.showInputErrors(error.errors, this.editInputNames);
                $(`#btn-update`).attr('disabled', false);
            } else console.log(error);
        });  
        return false;
    }

    loadUsers() {
        const ajax = new Ajax(`${globalConfig.appUrl}/loadUsers`, {
            headers: {accept: 'application/json',},
            method: 'POST'
        });
        
        ajax.result().then(response => {
            $(`#header`).html(``);
            $(`#header`).addClass('card-header');
            $(`#header`).html(`<h3 class="card-title">Usuarios</h3><div class="card-tools"><div class="input-group input-group-sm" style="width:150px;position:relative;top:1px"><input type="text" id="table-search" class="form-control float-right" placeholder="Buscar" maxlength="15" autocomplete="off"></div></div>`);
            $(`#body`).html(`<div id="table-data"><table class="display cell-border" id="datatable" width="100%"><thead><tr><th>#</th><th>Nombre</th><th>Apellido</th><th>Email</th><th>Rol</th><th>Creado&nbsp;el</th><th>Última&nbsp;conexión&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th><th>Acción</th></tr></thead><tbody id="dataset"></tbody></table></div>`);

            var dataset = [], data = [], size = Object.keys(response.data).length, datatable = ``, mk = CryptoJS.SHA1(this.genToPut).toString(), encrypted;

            for (var i = 0; i < size; i++) {
                dataset.push({
                    user_id: response.data[i].user_id,
                    firstname: response.data[i].firstname,
                    lastname: response.data[i].lastname,
                    role: response.data[i].role,
                    index: (i+1),
                });
                
                encrypted = CryptoJS.AES.encrypt(JSON.stringify(dataset), mk);

                data.push([
                    (i+1),
                    response.data[i].firstname,
                    response.data[i].lastname,
                    response.data[i].email,
                    usr.getSpanishRole(response.data[i].role),
                    moment(response.data[i].created_at).format('DD/MM/YYYY'),
                    (response.data[i].last_login) ? moment(response.data[i].last_login).format('DD/MM/YYYY HH:mm:ss') : '-',
                    `<div class="btn-group btn-group-sm">
                        <button class="btn btn-info" id="btn-modify" value='${encrypted}' data-toggle="modal" data-target="#modal-xl"><i class="fas fa-pen"></i></button>
                        <button class="btn btn-danger" id="btn-delete" value="${response.data[i].user_id}"><i class="fa fa-trash"></i></button>
                    </div>`
                ]);
                
                dataset = [];
            }

            $(`#dataset`).html(datatable);
            
            var table = $("#datatable").DataTable({
                dom: 'frptip',
                processing: true,
                draw: data.draw,
                data: data,
                deferRender: true,
                recordsTotal: size,
                recordsFiltered: size,
                searching: true,
                info: true,
                ordering: false,
                scrollY: 400,
                scrollX: true,
                scrollColapse: true,
                paging: false,
                language: {
                    info: "Mostrando _START_ a _END_ de _TOTAL_ resultados"
                }
            });
            
            $('.dataTables_filter').hide();
            $('#table-search').keyup(function(){table.search($(this).val()).draw();});
            
            table.on('click', '#btn-delete', function() {
                let $tr = $(this).closest('tr'), $button = $(this), id = $(this).val();
                alertify.confirm("<i class='fa fa-exclamation-triangle text-danger'></i> Advertencia", "¿Está seguro que desea eliminar este usuario?", function() {},
                function(e) {
                    if (e) {
                        $button.attr('disabled', true);
                        const ajax = new Ajax(`${globalConfig.appUrl}/delete/${id}`, {
                            headers: { accept: 'application/json' },
                            method: 'POST'
                        });
                        ajax.result().then(response => {table.row($tr).remove().draw(false);alertify.warning("El usuario ha sido eliminado");
                        }).catch(error => { console.log(error); });
                    }
                }).set({'movable': false, 'moveBounded': false, 'modal': false, 'labels': {ok: 'No', cancel: 'Sí'}});
            });

            table.on('click', '#btn-modify', function() {
                var $tr = $(this).closest('tr'), $value = $(this).val();
                usr.edit($value, $tr, table);
            });
            
        }).catch(error => { console.log(error) });  
        return false;
    }
}

window.onload = () => {
    usr = new User();
    usr.validateFields();
    if (`${globalConfig.appUrl}/management` === location.href) usr.loadUsers();

    $(function() {
        $('#btn-add').click(function() {
            alertify.confirm('Importante', '¿Está seguro que desea crear este usuario?', function(e) {
                if (e) { usr.register(); }
            }, function(){}).set({'movable': false, 'moveBounded': false, 'labels': {ok: 'Sí', cancel: 'No'}});
        });
        usr.validateFields();
    });
}