class DynamicPressure {
    constructor() {
        this.searchByFilter = this.searchByFilter.bind(this);
        this.setHeader = this.setHeader.bind(this);
    }

    searchByFilter() {

        var size = m.getColumnsFromDb().length, values = m.getColumnsFromDb(), preset = ['well_name', 'dynamic_date', 'macolla_name', 'location_name', 'reservoir_name'];
        
        for (var i = 0; i <= 4; i++) values[size + i] = preset[i];
  	
        var range, typeSearch, reservoirId = [], collection = [];
        
        range = $("#filter-date").val() == 1 ?  m.getDatesRange($(`#date-pick`).val()) : m.getDatesRange(`${$(`#from`).val()}|${$(`#until`).val()}`);
        
        var whatIDo = document.querySelector(`#dual-list`).style.display == 'none';

        var from = moment(range[0]), until = moment(range[1]), isFrom = m.validateDate(from, 'msgFrom'), isUntil = m.validateDate(until, 'msgUntil');

        if (!whatIDo) {
            typeSearch = 1;
            var sizeIs = ($(`#location_well`).val()).length, store = $(`#location_well`).val();
            if (sizeIs > 1) for (var i = 0; i < sizeIs; i++) collection[i] = store[i];
            else collection = store[0];
        } else {
            $('input[name="option[]"]:checked').each(function(i) { reservoirId[i] = $(this).val(); });
            typeSearch = 2;
            var sizeIs = ($(`#macolla_list`).val()).length, store = $(`#macolla_list`).val();
            if (sizeIs > 1) for (var i = 0; i < sizeIs; i++) collection[i] = store[i];
            else collection = store[0];
        }

        if ((from.isValid() && isFrom) && (until.isValid() && isUntil)) {

            const ajax = new Ajax(`${globalConfig.appUrl}/dynamicPressure/searchByFilterDynamic`, {
                headers: {
                    accept: 'application/json',
                },
                method: 'POST',
                body: {
                    items: values,
                    data: collection,
                    macolla_id: parseInt($(`#macolla`).val()),
                    reservoir_id: reservoirId != null ? reservoirId : null,
                    date_from: range[0],
                    date_to: range[1],
                    type: typeSearch
                }
            });
        
            m.setLoadingMessageBefore();
                
            ajax.result().then(response => {
                    
                var options = ['quality', 'pip', 'pdp', 'temperature', 'observations'], size = Object.keys(response.data).length;
        
                m.setLoadingMessageAfter();
                m.showMessageDialog(size);

                if (size > 0) {

                    var selected = m.getColumnsToHead(options, Object.keys(response.data[0]));
                    var header = m.setColumn(response.type, response.info)[0], column = m.setColumn(response.type, response.info)[1];

                    let detail = (response.type == 1) ? header : m.selectedItems(response.total);
                    var titleText = detail == 'SIN MACOLLA' ? detail : `Macolla ${detail}`;

                    m.setHtmlTable(1, "Histórico Presión Dinámica", `${titleText}`);
                    m.setHtmlTable(2);

                    let headerSelected = `<th class='text-muted'>#</th>${column}<th class='text-muted'>Localización</th><th class='text-muted'>Pozo</th><th class='text-muted'>Yacimiento</th><th class='text-muted'>Fecha&nbsp;y&nbsp;hora&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>`;

                    for (var i=0; i<selected.length; i++) headerSelected += `<th>${this.setHeader(selected[i])}</th>`;
                    
                    document.querySelector(`#header`).innerHTML = headerSelected;

                    var table = m.newDateTable(response.results, size, `Presión Dinámica ${titleText}`);

                    table.buttons().container().appendTo(`#buttons`);

                    $(`.dts_label`).remove();

                    document.querySelector('#imhere').focus();

                } else document.querySelector(`#table-content`).innerHTML = ``;
                    
            }).catch(error => { console.log(error) });
        }
        return false;
    }
    
    setHeader(value) {
        if (value === 'quality') return 'Calidad';
        else if (value === 'pip') return 'Pwf)EB <h6 class="text-muted">lppc</h6>';
        else if (value === 'pdp') return 'PDP)SB <h6 class="text-muted">lppc</h6>';
        else if (value === 'temperature') return 'Twf)EB <h6 class="text-muted">ºF</h6>';
        else if (value === 'observations') return 'Observaciones';
    } 
}

window.onload = () => {
    m = new Method();
    dnm = new DynamicPressure();
}