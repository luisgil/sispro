function selectCheckbox(source) {
	checkboxes = document.getElementsByTagName('input');
	for(i=0;i<checkboxes.length;i++) {
		if(checkboxes[i].type == "checkbox" && checkboxes[i].name == "item[]") checkboxes[i].checked = source.checked;
	}
}

function validate(object) {
	var getAll = [];

	document.querySelectorAll(`input[name="item[]"]:checked`).forEach(function (i, j) {
		getAll[j] = true;
	});
	
	var existValues = false;
	var a = findField('macolla', 'Macolla');
	var b = findField('location_well', 'Localización');
	var c = checkCheckboxes('Yacimiento');
	var d = findField('macolla_list', 'Una o varias macollas');
	
	/** Elements to take in */
	if (document.querySelector(`#btn-consult`).value == 'true') {
		var dump = [a, d];
	} else if (document.querySelector(`#reservoirs`).style.display == 'block' && document.querySelector(`#dual-list`).style.display == 'none') {
		var dump = [a, c, d];
	} else {
		var dump = [a, b];	
	}
	
	dump = dump.filter(function(el) {
		return el != null;
	});
	
	var control = 0;
	for (var i = 0; i < dump.length; i++) {
		if (dump[i] == null) {
			control++;
		}
	}
	
	if (control == dump.length) {
		existValues = true;
	}
	
	if (existValues == true) {
		if (getAll.length == 0) {
			alertify.alert('<b>Aviso</b>', 'Debe seleccionar al menos un ítem', function(){}).set({'movable': false, 'moveBounded': false});
			return false; 
		}
		else {
			object.searchByFilter();
		}
	} else {
		alertify.alert('<b>Aviso</b>', 'Para proceder debe seleccionar:' + dump, function(){}).set({'movable': false, 'moveBounded': false});
		return false; 
	}
}

function findField(id, text) {
	var str;
	var element = document.querySelector(`#${id}`);
	if (element) {
		if (element.value) {
			str = ` ${text}`;
		} else {
			str = null;
		}
		return str;
	}
}

function checkCheckboxes(text) {
	var exist = [], str = '';

	document.querySelectorAll(`input[name="option[]"]:checked`).forEach(function (i, j) {
		exist[j] = true;
	});
	
	if (exist.length == 0) { str = ` ${text}`;}
	else { str = null;}
	return str;
}

var expanded = false;
function showCheckboxes() {
	var checkboxes = document.getElementById("checkboxes");
	if (!expanded) {
		checkboxes.style.display = "block";
		expanded = true;
	} else {
		checkboxes.style.display = "none";
		expanded = false;
	}
}
function selectCheckbox(source) {
	checkboxes = document.getElementsByTagName('input');
	for(i=0;i<checkboxes.length;i++) {
		if(checkboxes[i].type == "checkbox" && checkboxes[i].name == "item[]") checkboxes[i].checked = source.checked;
	}
}

function validate(object) {
	var getAll = [];
	$('input[name="item[]"]:checked').each(function(i) {
		getAll[i] = true;
	});
	
	var existValues = false;
	var a = findField('macolla', 'Macolla');
	var b = findField('location_well', 'Localización');
	var c = checkCheckboxes('Yacimiento');
	var d = findField('macolla_list', 'Una o varias macollas');
	
	/** Elements to take in */
	if ($("#btn-consult").val() == 'true') {
		var dump = [a, d];
	} else if (document.querySelector(`#reservoirs`).style.display == 'block' && document.querySelector(`#dual-list`).style.display == 'none') {
		var dump = [a, c, d];
	} else {
		var dump = [a, b];	
	}
	
	dump = dump.filter(function(el) {
		return el != null;
	});
	
	var control = 0;
	for (var i = 0; i < dump.length; i++) {
		if (dump[i] == null) {
			control++;
		}
	}
	
	if (control == dump.length) {
		existValues = true;
	}
	
	if (existValues == true) {
		if (getAll.length == 0) {
			alertify.alert('<b>Aviso</b>', 'Debe seleccionar al menos un ítem', function(){}).set({'movable': false, 'moveBounded': false});
			return false; 
		}
		else {
			object.searchByFilter();
		}
	} else {
		alertify.alert('<b>Aviso</b>', 'Para proceder debe seleccionar:' + dump, function(){}).set({'movable': false, 'moveBounded': false});
		return false; 
	}
}

function findField(id, text) {
	var str = '';
	if ($(`#${id}`).val() == '' || $(`#${id}`).val() == null) {
		str = ' ' + text;
	} else {
		str = null;
	}
	return str;
}

function checkCheckboxes(text) {
	var exist = [], str = '';
	$('input[name="option[]"]:checked').each(function(i) {
		exist[i] = true;
	});
	
	if (exist.length == 0) { str = ` ${text}`;}
	else { str = null;}
	return str;
}

var expanded = false;
function showCheckboxes() {
	var checkboxes = document.getElementById("checkboxes");
	if (!expanded) {
		checkboxes.style.display = "block";
		expanded = true;
	} else {
		checkboxes.style.display = "none";
		expanded = false;
	}
}