class FiscalizedProduction {
    constructor() {
    }
    
    validateSearch() {
        if ($(`#macolla`).val() != '') this.searchByFilter();
        else alertify.alert('Importante', 'Debe seleccionar primero una macolla.', function(){}).set({'movable': false, 'moveBounded': false});
    }

    searchByFilter() {
        
        var range = range = $("#filter-date").val() == 1 ?  m.getDatesRange($(`#date-pick`).val()) : m.getDatesRange(`${$(`#from`).val()}|${$(`#until`).val()}`);
        var from = moment(range[0]), until = moment(range[1]), isFrom = m.validateDate(from, 'msgFrom'), isUntil = m.validateDate(until, 'msgUntil');


        if ((from.isValid() && isFrom) && (until.isValid() && isUntil)) {
            const ajax = new Ajax(`${globalConfig.appUrl}/controlledProduction/searchByFilterProduction`, {
                headers: { accept: 'application/json' },
                method: 'POST',
                body: {
                    id: parseInt($(`#macolla`).val()),
                    date_from: range[0],
                    date_to: range[1],
                }
            });
        
            m.setLoadingMessageBefore();
                
            ajax.result().then(response => {
        
                var size = Object.keys(response.data).length;
        
                m.setLoadingMessageAfter();
                m.showMessageDialog(size);

                if (size) {

                    m.setHtmlTable(1, "Producción EHO Fiscalizada", `Macolla ${response.name}`);
                    m.setHtmlTable(2);

                    let headerSelected = `<th >#</th><th >Fecha</th><th>${response.name} <h6 class="text-muted">Barriles Netos</h6></th>`;

                    headerSelected += `<th >%AyS<h6>DCO</h6></th><th >ºAPI<h6>DCO</h6></th><th >Inyección&nbsp;diluente</th><th >%AyS<h6>diluente</h6></th><th >ºAPI<h6>diluente</h6></th>`;

                    document.querySelector(`#header`).innerHTML = headerSelected;
                        
                    var table = m.newDateTable(response.results, size, `Producción Fiscalizada ${response.name}`);
                    
                    table.buttons().container().appendTo(`#buttons`); 
        
                    $(`.dts_label`).remove();
        
                    document.querySelector('#imhere').focus();
        
                } else document.querySelector(`#table-content`).innerHTML = ``;
            
            }).catch(error => { console.log(error) });
        }
        return false;
    }
}

window.onload = () => {
    m = new Method();
    fpr = new FiscalizedProduction();
}