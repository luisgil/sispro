class Management {
    constructor() {
    }

    optionChooser() {

        let option = document.querySelector(`#select-option`).value;

        if (option == 1) {
            location.href = `${globalConfig.appUrl}/managementExtended`;
        } else if(option == 2) {
            location.href = `${globalConfig.appUrl}/managementAverage`;
        } else if(option == 3) {
            location.href = `${globalConfig.appUrl}/managementSign`;
        } else if(option == 4) {
            location.href = `${globalConfig.appUrl}/managementCondition`;
        } else if(option == 5) {
            location.href = `${globalConfig.appUrl}/managementHistorical`;
        } else if (option == 6) {
            location.href = `${globalConfig.appUrl}/managementStatic`;
        } else if (option == 7) {
            location.href = `${globalConfig.appUrl}/managementDynamic`; 
        } else if (option == 8) {
            location.href = `${globalConfig.appUrl}/managementOriginal`; 
        } else if (option == 9) {
            location.href = `${globalConfig.appUrl}/managementReal`; 
        } else if (option == 10) {
            location.href = `${globalConfig.appUrl}/managementPlan`;
        } else if (option == 11) {
            location.href = `${globalConfig.appUrl}/managementFiscalized`;
        } else if (option == 12) {
            location.href = `${globalConfig.appUrl}/managementWellEvents`;
        } else if (option == '') {
            alertify.alert('Aviso', 'Debe seleccionar una opción para poder continuar.', function(){}).set({'movable': false, 'moveBounded': false});
        }
    }
}

window.onload = () => {
    mnt = new Management();
}