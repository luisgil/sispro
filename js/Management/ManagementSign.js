class ManagementSign {
    constructor() {
        this.searchByFilter = this.searchByFilter.bind(this);
        this.setHeader = this.setHeader.bind(this);
        this.inputNames = ['sign_time', 'company' , 'percent_mixture', 'percent_sediment', 'api_degree_diluted', 'ptb'];
    }
    
    searchByFilter() {

        var size = m.getColumnsFromDb().length, values = m.getColumnsFromDb(), preset = ['sign_date', 'well_name', 'company', 'sign_time', 'macolla_name', 'location_name', 'reservoir_name'];

        for (var i = 0; i <= 9; i++) values[size + i] = preset[i];
        
        var range, typeSearch, reservoirId = [], collection = [];
        
        range = $("#filter-date").val() == 1 ?  m.getDatesRange($(`#date-pick`).val()) : m.getDatesRange(`${$(`#from`).val()}|${$(`#until`).val()}`);
        
        var whatIDo = document.querySelector(`#dual-list`).style.display == 'none';

        if (!whatIDo) {
            typeSearch = 1;
            var sizeIs = ($(`#location_well`).val()).length, store = $(`#location_well`).val();
            if (sizeIs > 1) for (var i = 0; i < sizeIs; i++) collection[i] = store[i];
            else collection = store[0];
        }
        
        var from = moment(range[0]), until = moment(range[1]), isFrom = m.validateDate(from, 'msgFrom'), isUntil = m.validateDate(until, 'msgUntil');

        if ((from.isValid() && isFrom) && (until.isValid() && isUntil)) {

            $(`#msgFrom, #msgUntil`).html(``);

            const ajax = new Ajax(`${globalConfig.appUrl}/searchSign`, {
                headers: {
                    accept: 'application/json',
                },
                method: 'POST',
                body: {
                    items: values,
                    data: collection,
                    macolla_id: parseInt($(`#macolla`).val()),
                    reservoir_id: reservoirId != null ? reservoirId : null,
                    date_from: range[0],
                    date_to: range[1],
                    type: typeSearch
                }
            });

            m.setLoadingMessageBefore();

            ajax.result().then(response => {

                var size = Object.keys(response.data).length;

                m.setLoadingMessageAfter();
                m.showMessageDialog(size);

                var options = ['percent_mixture', 'percent_sediment', 'api_degree_diluted', 'ptb'];

                var selected = m.getColumnsToHead(options, Object.keys(response.data[0]));
                var header = m.setColumn(response.type, response.info)[0];

                let detail = (response.type == 1) ? header : m.selectedItems(response.total);
                var titleText = detail == 'SIN MACOLLA' ? detail : `Macolla ${detail}`;

                m.setHtmlTable(1, "Muestra", `${titleText}`);
                m.setHtmlTable(2);

                let headerSelected = `<th class='text-muted'>#</th><th class='text-muted'>Fecha&nbsp;muestreo</th><th class='text-muted'>Localización</th><th class='text-muted'>Pozo</th><th class='text-muted'>Yacimiento</th><th class='text-muted'>Compañía</th><th class='text-muted'>Hora&nbsp;muestreo</th>`;

                for (var i=0; i<selected.length; i++) headerSelected += `<th>${this.setHeader(selected[i])}</th>`;

                headerSelected += `<th class='text-muted'>Acción</th>`;

                $(`#header`).html(headerSelected);

                var table = m.newDateTable(response.results, size);

                document.getElementById('imhere').focus();

                $(`.dts_label, .dt-buttons, .dataTables_filter`).remove();

                $(`#datatable`).on('click', '#btn-delete', function() {
                    
                    let $tr = $(this).closest('tr'), $button = $(this);
                    const id = $(this).val();

                    alertify.confirm("<i class='fa fa-exclamation-triangle text-danger'></i> Advertencia", "Se eliminará este registro permanentemente. ¿Está seguro que desea continuar?", function() {},
                        function(e) {
                            if (e) {
                                $button.attr('disabled', true);
                                const ajax = new Ajax(`${globalConfig.appUrl}/deleteSign/${id}`, {
                                    headers: { accept: 'application/json' },
                                    method: 'POST'
                                });
                                ajax.result().then(response => {table.row($tr).remove().draw(false);alertify.warning("El registro ha sido eliminado");}).catch(error => { console.log(error); });
                            }
                        }).set({'movable': false, 'modal': false, 'moveBounded': false, 'labels': {ok: 'No', cancel: 'Sí'}});
                });

                $(`#datatable`).on('click', '#btn-edit', function() {
                    var $tr = $(this).closest('tr'), id = $(this).val(), $value = table.row($tr).data();
                    sgn.modify($value, table, $tr, id);
                });
            }).catch(error => { console.log(error) });
        }
        return false;
    }
    
    updating(table, tr) {
        
        m.reloadView();

        const id = $('#identity').val();
        const index = $('#index').val();

        const ajax = new Ajax(`${globalConfig.appUrl}/updateSign/${id}`, {
            headers: {
                accept: 'application/json',
            },
            method: 'POST',
            body: {
                sign_time: document.querySelector(`#sign_time`).value,
                company: m.trimString('company'),
                api_degree_diluted: m.convertToDecimal(document.querySelector(`#api_degree_diluted`).value),
                percent_mixture: m.convertToDecimal(document.querySelector(`#percent_mixture`).value),
                percent_sediment: m.convertToDecimal(document.querySelector(`#percent_sediment`).value),
                ptb: m.convertToDecimal(document.querySelector(`#ptb`).value)
            }
        });

        $("#btn-update").attr("disabled", true);
        
        ajax.result().then(response => {

            var data = [];

            response.data[0][0] = index;

            table.row(table.row(tr).invalidate().data(response.data[0]).draw(false));
            
            alertify.success(response.message);

            $(`#modal-xl`).modal('toggle');

        }).catch(error => {
            if (error.status === 400) {
                m.showInputCorrects(error.errors, this.inputNames);
                m.showInputErrors(error.errors, this.inputNames);
                $("#btn-update").attr("disabled", false);
            } else console.log(error);
        });
        return false;
    }

    modify(array, table, tr, id) {
        
        var data = [];

        data.push({
            sign_time: array[6],
            company: array[5],
            api_degree_diluted: array[9],
            percent_mixture: array[7],
            percent_sediment: array[8],
            ptb: array[10],
            sign_id: id, 
            index: array[0]
        });

        $(`#modal-xl`).html(`<div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title"><i class="fa fa-edit"></i> Muestra</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-4 pt-4">
                        <h6>Hora</h6>
                        <input type="time" class="form-control form-control-sm" id="sign_time" name="sign_time" value="${data[0].sign_time}" />
                        <div id="wrapper-sign_time"></div>
                    </div>
                    <div class="col-md-4 pt-4">
                        <h6>Compañía</h6>
                        <input type="text" class="form-control form-control-sm" placeholder="Compañía" id="company" name="company" value="${data[0].company}" maxlength="20" style="text-transform:uppercase" autocomplete="off" />
                        <div id="wrapper-company"></div>
                    </div>
                    <div class="col-md-4 pt-4">
                        <h6>ºAPI</h6>
                        <input type="text" class="form-control form-control-sm" placeholder="ºAPI" id="api_degree_diluted" name="api_degree_diluted" value="${m.setNullToValue(data[0].api_degree_diluted)}" maxlength="6" autocomplete="off" />
                        <div id="wrapper-api_degree_diluted"></div>
                    </div>
                    <div class="col-md-4 pt-4">
                        <h6>% AyS</h6>
                        <input type="text" class="form-control form-control-sm" placeholder="% AyS" id="percent_mixture" name="percent_mixture" value="${m.setNullToValue(data[0].percent_mixture)}" maxlength="6" autocomplete="off" />
                        <div id="wrapper-percent_mixture"></div>
                    </div>
                    <div class="col-md-4 pt-4">
                        <h6>% Sedimentos</h6>
                        <input type="text" class="form-control form-control-sm" placeholder="% Sedimentos" id="percent_sediment" name="percent_sediment" value="${m.setNullToValue(data[0].percent_sediment)}" maxlength="6" autocomplete="off" />
                        <div id="wrapper-percent_sediment"></div>
                    </div>
                    <div class="col-md-4 pt-4">
                        <h6>PTB</h6>
                        <input type="text" class="form-control form-control-sm" placeholder="PTB" id="ptb" name="ptb" value="${m.setNullToValue(data[0].ptb)}" maxlength="6" autocomplete="off" />
                        <div id="wrapper-ptb"></div>
                    </div>
                </div>
            </div>

            <input type="hidden" id="identity" value="${data[0].sign_id}">
            <input type="hidden" id="index" value="${data[0].index}">

            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-primary" id="btn-update">Guardar cambios</button>
            </div>
        </div>
        </div>`);
    
        var input = ['api_degree_diluted', 'percent_mixture', 'percent_sediment', 'ptb'];
        
        for (let i = 0; i < input.length; i++) $(`#${input[i]}`).numeric({ negative: false });

        $(`#btn-update`).click(function () {
            alertify.confirm('Pregunta', '¿Está seguro que desea guardar los cambios realizados en este registro?', function(e) {
                if (e) sgn.updating(table, tr);
            }, function(){}).set({'movable': false, 'moveBounded': false, 'modal': false, 'labels': {ok: 'Sí', cancel: 'No'}});
        });
    }

    setHeader(value) {
        if (value === 'percent_mixture') return '%AyS&nbsp;Mezcla';
        else if (value === 'percent_sediment') return '%Sedimentos';
        else if (value === 'api_degree_diluted') return'ºAPI&nbsp;crudo&nbsp;diluido';
        else if(value === 'ptb') return 'PTB';
    }
}

window.onload = () => {
    m = new Method();
    sgn = new ManagementSign();
}