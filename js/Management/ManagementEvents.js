class ManagementEvents {
    constructor() {
        this.inputNames = ['event_date', 'event_time', 'diluent', 'rpm', 'pump_efficiency', 'torque', 'frequency', 'current', 'power', 'mains_voltage', 'output_voltage', 'vfd_temperature', 'head_temperature', 'head_pressure', 'observations'];
    }
 
    searchByFilter() {
        
        var range, typeSearch, reservoirId = [], collection = [];
        
        range = $("#filter-date").val() == 1 ?  m.getDatesRange($(`#date-pick`).val()) : m.getDatesRange(`${$(`#from`).val()}|${$(`#until`).val()}`);

        var whatIDo = document.querySelector(`#dual-list`).style.display == 'none';
        
        if (!whatIDo) {
            typeSearch = 1;
            var sizeIs = ($(`#location_well`).val()).length, store = $(`#location_well`).val();
            if (sizeIs > 1) for (var i = 0; i < sizeIs; i++) collection[i] = store[i];
            else collection = store[0];
        }

        var from = moment(range[0]), until = moment(range[1]), isFrom = m.validateDate(from, 'msgFrom'), isUntil = m.validateDate(until, 'msgUntil');

        if ((from.isValid() && isFrom) && (until.isValid() && isUntil)) {

            $(`#msgFrom, #msgUntil`).html(``);

            const ajax = new Ajax(`${globalConfig.appUrl}/searchEvents`, {
                headers: {
                    accept: 'application/json',
                },
                method: 'POST',
                body: {
                    data: collection,
                    macolla_id: parseInt($(`#macolla`).val()),
                    reservoir_id: reservoirId != null ? reservoirId : null,
                    date_from: range[0],
                    date_to: range[1],
                    type: typeSearch
                }
            });

            m.setLoadingMessageBefore();

            ajax.result().then(response => {

                var size = Object.keys(response.data).length;

                m.setLoadingMessageAfter();
                m.showMessageDialog(size);

                var header = m.setColumn(response.type, response.info)[0], column = m.setColumn(response.type, response.info)[1];

                let detail = (response.type == 1) ? header : m.selectedItems(response.total);
                var titleText = detail == 'SIN MACOLLA' ? detail : `Macolla ${detail}`;

                if (size > 0 ) {

                    m.setHtmlTable(1, "Eventos de pozo", `${titleText}`);
                    m.setHtmlTable(2);

                    $(`#header`).html(`<th class="text-muted">#</th><th>Localización</th><th>Pozo</th><th>Yacimiento</th><th class="text-muted">Fecha</th><th class="text-muted">Hora</th><th class="text-muted">Diluente</th><th class="text-muted">RPM</th><th class="text-muted">Bomba <h6 class="text-muted">eficiencia</h6></th><th class="text-muted">Torque <h6>Lb/pies</h6></th><th class="text-muted">Frecuencia <h6>Hz</h6></th><th class="text-muted">Corriente <h6>(A)</h6></th><th class="text-muted">Potencia <h6>Kw-H</h6></th><th class="text-muted">Tensión&nbsp;red <h6>(V)</h6></th><th class="text-muted">Tensión&nbsp;salida <h6>(V)</h6></th><th class="text-muted">VFD&nbsp;Temperatura <h6>ºF</h6></th><th class="text-muted">Temperatura&nbsp;cabezal <h6>ºF</h6></th><th class="text-muted">Presión&nbsp;cabezal <h6>lppc</h6></th><th class="text-muted">Observaciones</th><th class="text-muted">Acción</th>`);

                    var table = m.newDateTable(response.results, size);

                    document.getElementById('imhere').focus();

                    $(`.dts_label, .dt-buttons, .dataTables_filter`).remove();

                    $(`#datatable`).on('click', '#btn-delete', function() {
                        let $tr = $(this).closest('tr'), $button = $(this);
                        const id = $(this).val();
                        alertify.confirm("<i class='fa fa-exclamation-triangle text-danger'></i> Advertencia", "Se eliminará este registro permanentemente. ¿Está seguro que desea continuar?", function() {}, function(e) {
                            if (e) {
                                $button.attr('disabled', true);
                                const ajax = new Ajax(`${globalConfig.appUrl}/deleteEvents/${id}`, {
                                    headers: { accept: 'application/json' },
                                    method: 'POST'
                                });
                                ajax.result().then(response => {
                                    table.row($tr).remove().draw(false);
                                    alertify.warning("El registro ha sido eliminado");
                                }).catch(error => { console.log(error); });
                            }
                        }).set({'movable': false, 'modal': false, 'moveBounded': false, 'labels': {ok: 'No', cancel: 'Sí'}});
                    });

                    $(`#datatable`).on('click', '#btn-edit', function() {
                        var $tr = $(this).closest('tr'), id = $(this).val(), $value = table.row($tr).data();
                        eve.modify($value, table, $tr, id);
                    });
                    
                } else document.querySelector(`#table-content`).innerHTML = ``;

            }).catch(error => { console.log(error) });
        }
        return false;
    }

    updating(table, tr) {
        
        m.reloadView();

        const id = $('#identity').val();
        const index = $('#index').val();
        var data = [];

        const ajax = new Ajax(`${globalConfig.appUrl}/updateEvents/${id}`, {
            headers: { accept: 'application/json' },
            method: 'POST',
            body: {
                event_date: document.querySelector(`#event_date`).value,
                event_time: document.querySelector(`#event_time`).value,
                diluent: document.querySelector(`#diluent`).value,
                rpm: m.convertToDecimal(document.querySelector(`#rpm`).value),
                pump_efficiency: m.convertToDecimal(document.querySelector(`#pump_efficiency`).value),
                torque: m.convertToDecimal(document.querySelector(`#torque`).value),
                frequency: m.convertToDecimal(document.querySelector(`#frequency`).value),
                current: m.convertToDecimal(document.querySelector(`#current`).value),
                power: m.convertToDecimal(document.querySelector(`#power`).value),
                mains_voltage: m.convertToDecimal(document.querySelector(`#mains_voltage`).value),
                output_voltage: m.convertToDecimal(document.querySelector(`#output_voltage`).value),
                vfd_temperature: m.convertToDecimal(document.querySelector(`#vfd_temperature`).value),
                head_temperature: m.convertToDecimal(document.querySelector(`#head_temperature`).value),
                head_pressure: m.convertToDecimal(document.querySelector(`#head_pressure`).value),
                observations: m.trimNotRequiredString('observations')
            }
        });

        $("#btn-update").attr("disabled", true);
        
        ajax.result().then(response => {
            
            var data = [];

            response.data[0][0] = index;

            table.row(table.row(tr).invalidate().data(response.data[0]).draw(false));
            
            alertify.success(response.message);

            $(`#modal-xl`).modal('toggle');
            $(`#modal-xl`).html(``);

        }).catch(error => {
            if (error.status === 400) {
                m.showInputCorrects(error.errors, this.inputNames);
                m.showInputErrors(error.errors, this.inputNames);
                $("#btn-update").attr("disabled", false);
            } else console.log(error);
        });
        return false;
    }

    modify(array, table, tr, id) {
        
        var data = [];
        
        data.push({
            location_name: array[1],
            well_name: array[2],
            reservoir_name: array[3],
            event_date: array[4],
            event_time: array[5],
            diluent: array[6],
            rpm: array[7],
            pump_efficiency: array[8],
            torque: array[9],
            frequency: array[10],
            current: array[11],
            power: array[12],
            mains_voltage: array[13],
            output_voltage: array[14],
            vfd_temperature: array[15],
            head_temperature: array[16],
            head_pressure: array[17],
            observations: array[18],
            event_id: id, 
            index: array[0]
        });
        
        $(`#modal-xl`).html(`
        <div class="modal-dialog modal-xl">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title"><i class="fa fa-edit"></i> Evento de pozo</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-4 pt-3"><h6>Fecha</h6>
                            <input type="date" class="form-control form-control-sm" id="event_date" name="event_date" value="${moment(data[0].event_date, "DD/MM/YYYY").format("YYYY-MM-DD")}" />
                            <div id="wrapper-event_date"></div>
                        </div>
                        <div class="col-md-4 pt-3">
                            <h6>Hora</h6>
                            <input type="time" class="form-control form-control-sm" id="event_time" name="event_time" value="${data[0].event_time}" />
                            <div id="wrapper-event_time"></div>
                        </div>

                        <div class="col-md-12 pt-0"></div>

                        <div class="col-md-4 pt-4">
                            <h6>Diluente </h6>
                            <input type="text" class="form-control form-control-sm" maxlength="6" placeholder="Diluente" id="diluent" name="diluent" required autocomplete="off" value="${m.setNullToValue(data[0].diluent)}" />
                            <div id="wrapper-diluent"></div>
                        </div>
                        <div class="col-md-4 pt-4">
                            <h6>RPM </h6>
                            <input type="text" class="form-control form-control-sm" maxlength="6" placeholder="RPM" id="rpm" name="rpm" required autocomplete="off" value="${m.setNullToValue(data[0].rpm)}" />
                            <div id="wrapper-rpm"></div>
                        </div>
                        <div class="col-md-4 pt-4">
                            <h6>Efic. de la Bomba</h6>
                            <input type="text" class="form-control form-control-sm" maxlength="6" placeholder="Eficiencia de la Bomba" id="pump_efficiency" name="pump_efficiency" required autocomplete="off" value="${m.setNullToValue(data[0].pump_efficiency)}" />
                            <div id="wrapper-pump_efficiency"></div>
                        </div>

                        <div class="col-md-4 pt-4">
                            <h6>Torque <i class="text-muted">Lb/pies</i></h6>
                            <input type="text" class="form-control form-control-sm" maxlength="6" placeholder="Torque" id="torque" name="torque" required autocomplete="off" value="${m.setNullToValue(data[0].torque)}" />
                            <div id="wrapper-torque"></div>
                        </div>

                        <div class="col-md-4 pt-4">
                            <h6>Frecuencia <i class="text-muted">Hz</i></h6>
                            <input type="text" class="form-control form-control-sm" maxlength="6" placeholder="Frecuencia" id="frequency" name="frequency" required autocomplete="off" value="${m.setNullToValue(data[0].frequency)}" />
                            <div id="wrapper-frequency"></div>
                        </div>

                        <div class="col-md-4 pt-4">
                            <h6>Corriente <i class="text-muted">(A)</i></h6>
                            <input type="text" class="form-control form-control-sm" maxlength="6" placeholder="Corriente" id="current" name="current" required autocomplete="off" value="${m.setNullToValue(data[0].current)}" />
                            <div id="wrapper-current"></div>
                        </div>

                        <div class="col-md-4 pt-4">
                            <h6>Potencia <i class="text-muted">Kw-H</i></h6>
                            <input type="text" class="form-control form-control-sm" maxlength="6" placeholder="Potencia" id="power" name="power" required autocomplete="off" value="${m.setNullToValue(data[0].power)}" />
                            <div id="wrapper-power"></div>
                        </div>

                        <div class="col-md-4 pt-4">
                            <h6>Tensión red <i class="text-muted">(V)</i></h6>
                            <input type="text" class="form-control form-control-sm" maxlength="6" placeholder="Tensión de red" id="mains_voltage" name="mains_voltage" required autocomplete="off" value="${m.setNullToValue(data[0].mains_voltage)}" />
                            <div id="wrapper-mains_voltage"></div>
                        </div>

                        <div class="col-md-4 pt-4">
                            <h6>Tensión salida <i class="text-muted">(V)</i></h6>
                            <input type="text" class="form-control form-control-sm" maxlength="6" placeholder="Tensión de salida" id="output_voltage" name="output_voltage" required autocomplete="off" value="${m.setNullToValue(data[0].output_voltage)}" />
                            <div id="wrapper-output_voltage"></div>
                        </div>

                        <div class="col-md-4 pt-4">
                            <h6>Temperatura VFD <i class="text-muted">ºF</i></h6>
                            <input type="text" class="form-control form-control-sm" maxlength="6" placeholder="Temperatura VFD" id="vfd_temperature" name="vfd_temperature" required autocomplete="off" value="${m.setNullToValue(data[0].vfd_temperature)}" />
                            <div id="wrapper-vfd_temperature"></div>
                        </div>

                        <div class="col-md-4 pt-4">
                            <h6>Temperatura de cabezal <i class="text-muted">ºF</i></h6>
                            <input type="text" class="form-control form-control-sm" maxlength="6" placeholder="Temperatura de cabezal" id="head_temperature" name="head_temperature" required autocomplete="off" value="${m.setNullToValue(data[0].head_temperature)}" />
                            <div id="wrapper-head_temperature"></div>
                        </div>

                        <div class="col-md-4 pt-4">
                            <h6>Presión de cabezal <i class="text-muted">lppc</i></h6>
                            <input type="text" class="form-control form-control-sm" maxlength="6" placeholder="Presión de cabezal" id="head_pressure" name="head_pressure" required autocomplete="off" value="${m.setNullToValue(data[0].head_pressure)}" />
                            <div id="wrapper-head_pressure"></div>
                        </div>

                        <div class="col-md-12 pt-4"><hr></div>

                        <div class="col-md-12 pt-3">
                            <h6>Observaciones <i class="text-muted">120 caracteres</i></h6>
                            <textarea class="form-control form-control-sm" style="max-height:80px" placeholder="Observaciones" id="observations" name="observations" maxlength="120">${data[0].observations}</textarea>
                        </div>

                        <input type="hidden" id="identity" value="${data[0].event_id}">
                        <input type="hidden" id="index" value="${data[0].index}">
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    <button type="button" class="btn btn-primary" id="btn-update">Guardar cambios</button>
                </div>
            </div>
        </div>`);
    
        var input = ['diluent', 'rpm', 'pump_efficiency', 'torque', 'frequency', 'current', 'power', 'mains_voltage', 'output_voltage', 'vfd_temperature', 'head_temperature', 'head_pressure'];
        
        for (let i = 0; i < input.length; i++) $(`#${input[i]}`).numeric({ negative: false });

        $(`#btn-update`).click(function () {
            alertify.confirm('Pregunta', '¿Está seguro que desea guardar los cambios realizados en este registro?', function(e) {
                if (e) eve.updating(table, tr);
            }, function(){}).set({'movable': false, 'moveBounded': false, 'modal': false, 'labels': {ok: 'Sí', cancel: 'No'}});
        });
    }
}

window.onload = () => {
    m = new Method();
    eve = new ManagementEvents();
}