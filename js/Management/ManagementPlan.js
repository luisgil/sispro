class ManagementPlan {
    constructor() {
        this.searchByFilter = this.searchByFilter.bind(this);
        this.setHeader = this.setHeader.bind(this);
        this.inputNames = ['md', 'tvd', 'vsec', 'inclination', 'azim_grid', 'dls', 'ns', 'ew', 'la_g', 'la_m', 'la_s', 'lo_g', 'lo_m', 'lo_s', 'observations'];
    }

    searchByFilter() {

        var size = m.getColumnsFromDb().length, values = m.getColumnsFromDb(), preset = ['well_name', 'ps_date', 'macolla_name', 'location_name', 'reservoir_name'];

        for (var i = 0; i < 5; i++) values[size + i] = preset[i];
        
        var typeSearch, reservoirId = [], collection = [], whatIDo = document.querySelector(`#dual-list`).style.display == 'none';
        
        /*By wells or by macollas ?*/
        if (!whatIDo) {
            typeSearch = 1;
            var sizeIs = ($(`#location_well`).val()).length, store = $(`#location_well`).val();
            if (sizeIs > 1) for (var i = 0; i < sizeIs; i++) collection[i] = store[i];
            else collection = store[0];
        } else {
            /*This is for show all macollas and reservoirs!*/
            $('input[name="option[]"]:checked').each(function(i) {
                reservoirId[i] = $(this).val();
            });
            typeSearch = 2;
            var sizeIs = ($(`#macolla_list`).val()).length, store = $(`#macolla_list`).val();
            if (sizeIs > 1) for (var i = 0; i < sizeIs; i++) collection[i] = store[i];
            else collection = store[0];
        }
        
        const ajax = new Ajax(`${globalConfig.appUrl}/searchPlan`, {
            headers: {
                accept: 'application/json',
            },
            method: 'POST',
            body: {
                data: collection,
                macolla_id: parseInt($(`#macolla`).val()),
                type: typeSearch
            }
        });

        m.setLoadingMessageBefore();

        ajax.result().then(response => {
            
            var options = ['md', 'inclination', 'azim_grid', 'tvd', 'dls', 'vsec' , 'ns', 'ew', 'latitude', 'longitude', 'observations'];
            var size = Object.keys(response.data).length;

            m.setLoadingMessageAfter();
            m.showMessageDialog(size);

            
            if (size > 0) {

                var selected = m.getColumnsToHead(options, Object.keys(response.data[0]));
                var header = m.setColumn(response.type, response.info)[0], column = m.setColumn(response.type, response.info)[1];

                let detail = (response.type == 1) ? header : m.selectedItems(response.total);
                var titleText = detail == 'SIN MACOLLA' ? detail : `Macolla ${detail}`;

                m.setHtmlTable(1, "Survey Plan", `${titleText}`);
                m.setHtmlTable(2);

                let headerSelected = `<th class='text-muted'>#</th><th class='text-muted'>Macolla</th><th class='text-muted'>Pozo</th><th class='text-muted'>Localización</th><th class='text-muted'>Yacimiento</th><th class='text-muted'>Fecha</th>`;

                for (var i=0; i<selected.length; i++) headerSelected += `<th>${this.setHeader(selected[i])}</th>`;

                headerSelected += `<th>Acción</th>`;

                document.querySelector(`#header`).innerHTML = headerSelected;

                var table = m.newDateTable(response.results, size);

                document.getElementById('imhere').focus();

                $(`.dts_label, .dt-buttons, .dataTables_filter`).remove();

                $(`#datatable`).on('click', '#btn-delete', function() {
                    let $tr = $(this).closest('tr'), $button = $(this);
                    const id = $(this).val();
                    alertify.confirm("<i class='fa fa-exclamation-triangle text-danger'></i> Advertencia", "Se eliminará este registro permanentemente. ¿Está seguro que desea continuar?", function() {},
                        function(e) {
                            if (e) {
                                $button.attr('disabled', true);
                                const ajax = new Ajax(`${globalConfig.appUrl}/deletePlan/${id}`, {
                                    headers: { accept: 'application/json' },
                                    method: 'POST'
                                });
                                ajax.result().then(response => {table.row($tr).remove().draw(false);alertify.warning("El registro ha sido eliminado");}).catch(error => { console.log(error); });
                            }
                        }).set({'movable': false, 'modal': false, 'moveBounded': false, 'labels': {ok: 'No', cancel: 'Sí'}});
                }); 

                document.querySelector('#imhere').focus();

                $('.dataTables_filter').remove();

                $(`#datatable`).on('click', '#btn-edit', function() {
                    var $tr = $(this).closest('tr'), id = $(this).val(), $value = table.row($tr).data();
                    psv.modify($value, table, $tr, id);
                });


            } else document.querySelector(`#table-content`).innerHTML = ``;
        
        }).catch(error => { console.log(error) });
        return false;
    }

    updating(table, tr) {
        
        m.reloadView();

        const id = document.querySelector(`#identity`).value;
        const index = document.querySelector(`#index`).value;
        var data = [];

        const ajax = new Ajax(`${globalConfig.appUrl}/updatePlan/${id}`, {
            headers: { accept: 'application/json' },
            method: 'POST',
            body: {
                md: m.convertToDecimal(document.querySelector(`#md`).value),
                inclination: m.convertToDecimal(document.querySelector(`#inclination`).value),
                azim_grid: m.convertToDecimal(document.querySelector(`#azim_grid`).value),
                tvd: m.convertToDecimal(document.querySelector(`#tvd`).value),
                vsec: m.convertToDecimal(document.querySelector(`#vsec`).value),
                ns: m.convertToDecimal(document.querySelector(`#ns`).value),
                ew: m.convertToDecimal(document.querySelector(`#ew`).value),
                dls: m.convertToDecimal(document.querySelector(`#dls`).value),
                la_g: m.convertToDecimal(document.querySelector(`#la_g`).value),
                la_m: m.convertToDecimal(document.querySelector(`#la_m`).value),
                la_s: m.convertToDecimal(document.querySelector(`#la_s`).value),
                lo_g: m.convertToDecimal(document.querySelector(`#lo_g`).value),
                lo_m: m.convertToDecimal(document.querySelector(`#lo_m`).value),
                lo_s: m.convertToDecimal(document.querySelector(`#lo_s`).value),
                observations: m.trimNotRequiredString('observations'),
            }
        });

        $("#btn-update").attr("disabled", true);
        
        ajax.result().then(response => {
            
            var data = [];

            response.data[0][0] = index;

            table.row(table.row(tr).invalidate().data(response.data[0]).draw(false));
            
            alertify.success(response.message);

            $(`#modal-xl`).modal('toggle');
            $(`#modal-xl`).html(``);

        }).catch(error => {
            if (error.status === 400) {
                m.showInputCorrects(error.errors, this.inputNames);
                m.showInputErrors(error.errors, this.inputNames);
                $("#btn-update").attr("disabled", false);
            } else console.log(error);
        });
        return false;
    }

    modify(array, table, tr, id) {
        
        var data = [];
        console.log(array);

        data.push({
            md: array[6],
            inclination: array[7],
            azim_grid: array[8],
            tvd: array[9],
            dls: array[10],
            vsec: array[11],
            ns: array[12],
            ew: array[13],
            la_g: array[18],
            la_m: array[19],
            la_s: array[20],
            lo_g: array[21],
            lo_m: array[22],
            lo_s: array[23],
            observations: array[16],
            survey_id: id, 
            index: array[0]
        });

        console.log(data);
        
        $(`#modal-xl`).html(`<div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title"><i class="fa fa-edit"></i> Survey Plan</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-3 pt-4">
                        <h6>MD <i class="text-muted">pies</i></h6>
                        <input type="text" class="form-control form-control-sm" placeholder="MD" id="md" name="md" value="${m.setNullToValue(data[0].md)}" maxlength="6" autocomplete="off" />
                        <div id="wrapper-md"></div>
                    </div>

                    <div class="col-md-3 pt-4">
                        <h6>TVD <i class="text-muted">pies</i></h6>
                        <input type="text" class="form-control form-control-sm" placeholder="TVD" id="tvd" name="tvd" value="${m.setNullToValue(data[0].tvd)}" maxlength="6" autocomplete="off" />
                        <div id="wrapper-tvd"></div>
                    </div>

                    <div class="col-md-3 pt-4">
                        <h6>Sección Vertical <i class="text-muted">pies</i></h6>
                        <input type="text" class="form-control form-control-sm" placeholder="Sección Vertical" id="vsec" name="vsec" value="${m.setNullToValue(data[0].vsec)}" maxlength="6" autocomplete="off" />
                        <div id="wrapper-vsec"></div>
                    </div>

                    <div class="col-md-3 pt-4">
                        <h6>Inclinación <i class="text-muted">grados</i></h6>
                        <input type="text" class="form-control form-control-sm" placeholder="Inclinación" id="inclination" name="inclination" value="${m.setNullToValue(data[0].inclination)}" maxlength="6" autocomplete="off" />
                        <div id="wrapper-inclination"></div>
                    </div>

                    <div class="col-md-3 pt-4">
                        <h6>AZIM GRID <i class="text-muted">grados</i></h6>
                        <input type="text" class="form-control form-control-sm" placeholder="AZIM GRID" id="azim_grid" name="azim_grid" value="${m.setNullToValue(data[0].azim_grid)}" maxlength="6" autocomplete="off" />
                        <div id="wrapper-azim_grid"></div>
                    </div>

                    <div class="col-md-3 pt-4">
                        <h6>DLS <i class="text-muted">(°/100 pies)</i></h6>
                        <input type="text" class="form-control form-control-sm" placeholder="DLS" id="dls" name="dls" value="${m.setNullToValue(data[0].dls)}" maxlength="6" autocomplete="off" />
                        <div id="wrapper-dls"></div>
                    </div>

                    <div class="col-md-12 pt-4"><h5 class="text-info">Coordenadas</h5></div>
                   
                    <div class="col-md-3 pt-3">
                        <h6>Norte/Sur <i class="text-muted">(N/S pies)</i></h6>
                        <input type="text" class="form-control form-control-sm" placeholder="Norte-Sur" id="ns" name="ns" value="${m.setNullToValue(data[0].ns)}" maxlength="15" autocomplete="off" />
                        <div id="wrapper-ns"></div>
                    </div>

                    <div class="col-md-3 pt-3">
                        <h6>Este/Oeste <i class="text-muted">(E/O pies)</i></h6>
                        <input type="text" class="form-control form-control-sm" placeholder="Este-Oeste" id="ew" name="ew" value="${m.setNullToValue(data[0].ew)}" maxlength="15" autocomplete="off" />
                        <div id="wrapper-ew"></div>
                    </div>

                    <div class="col-md-12 pt-4"><h5 class="text-info">Ubicación</h5></div>

                    <div class="col-md-6 pt-3">
                        <h6>Latitud <i class="text-muted">(N/S ° ' ")</i></h6>
                        <div class="row">
                            <div class="col-md-3 pt-1">
                                <input type="text" class="form-control form-control-sm" id="la_g" name="la_g" value="${m.validateUndefined(data[0].la_g)}" maxlength="5" autocomplete="off" />
                            </div>
                            <div class="col-md-3 pt-1">
                                <input type="text" class="form-control form-control-sm" id="la_m" name="la_m" value="${m.validateUndefined(data[0].la_m)}" maxlength="5" autocomplete="off" />
                            </div>
                            <div class="col-md-3 pt-1">
                                <input type="text" class="form-control form-control-sm" id="la_s" name="la_s" value="${m.validateUndefined(data[0].la_s)}" maxlength="5" autocomplete="off" />
                            </div>
                        </div>
                        <input type="hidden" name="la_g" id="wrapper-la_g" />
                        <input type="hidden" name="la_g" id="wrapper-la_m" />
                        <input type="hidden" name="la_g" id="wrapper-la_s" />
                    </div>
                    
                    <div class="col-md-6 pt-3">
                        <h6>Longitud <i class="text-muted">(E/O ° ' ")</i></h6>
                        <div class="row">
                            <div class="col-md-3 pt-1">
                                <input type="text" class="form-control form-control-sm" id="lo_g" name="lo_g" value="${m.validateUndefined(data[0].lo_g)}" maxlength="5" autocomplete="off" />
                            </div>
                            <div class="col-md-3 pt-1">
                                <input type="text" class="form-control form-control-sm" id="lo_m" name="lo_m" value="${m.validateUndefined(data[0].lo_m)}" maxlength="5" autocomplete="off" />
                            </div>
                            <div class="col-md-3 pt-1">
                                <input type="text" class="form-control form-control-sm" id="lo_s" name="lo_s" value="${m.validateUndefined(data[0].lo_s)}" maxlength="5" autocomplete="off" />
                            </div>
                        </div>
                        <input type="hidden" name="lo_g" id="wrapper-lo_g" />
                        <input type="hidden" name="lo_g" id="wrapper-lo_m" />
                        <input type="hidden" name="lo_g" id="wrapper-lo_s" />
                    </div>

                    <div class="col-md-12 pt-3"><hr></div>

                    <div class="col-md-12 pt-3">
                        <h6>Observaciones <i class="text-muted">120 caracteres</i></h6>
                        <textarea class="form-control form-control-sm" style="max-height:80px" placeholder="Observaciones" id="observations" name="observations" maxlength="120">${data[0].observations}</textarea>
                    </div>
                </div>
            </div>

            <input type="hidden" id="identity" value="${data[0].survey_id}">
            <input type="hidden" id="index" value="${data[0].index}">

            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-primary" id="btn-update">Guardar cambios</button>
            </div>
        </div>
        </div>`);

        $(`#md, #tvd, #vsec, #inclination, #azim_grid, #dls, #ns, #ew, #la_g, #la_m, #la_s, #lo_g, #lo_m, #lo_s`).numeric();

        $(`#btn-update`).click(function () {
            alertify.confirm('Pregunta', '¿Está seguro que desea guardar los cambios Planizados en este registro?', function(e) {
                if (e) psv.updating(table, tr);
            }, function(){}).set({'movable': false, 'moveBounded': false, 'modal': false, 'labels': {ok: 'Sí', cancel: 'No'}});
        });
    }

    setHeader(value) {
        if (value === 'md') return 'MD <h6 class="text-muted">pies</h6>';
        else if (value === 'inclination') return 'Inclinación <h6 class="text-muted">grados</h6>';
        else if (value === 'azim_grid') return 'Azim&nbsp;Grid <h6 class="text-muted">grados</h6>';
        else if (value === 'tvd') return 'TVD <h6 class="text-muted">pies</h6>';
        else if (value === 'dls') return 'DLS <h6 class="text-muted">(°/100&nbsp;pies)</h6>';
        else if (value === 'vsec') return 'vSEC <h6 class="text-muted">pies</h6>';
        else if (value === 'ns') return 'Norte/Sur <h6 class="text-muted">(N/S&nbsp;pies)</h6>';
        else if (value === 'ew') return 'Este/Oeste <h6 class="text-muted">(E/O&nbsp;pies)</h6>';
        else if (value === 'latitude') return 'Latitud <h6 class="text-muted">(N/S&nbsp;°&nbsp;\'&nbsp;")</h6>';
        else if (value === 'longitude') return 'Longitud <h6 class="text-muted">(E/O&nbsp;°&nbsp;\'&nbsp;")</h6>';
        else if (value === 'observations') return 'Observaciones';
    }
}

window.onload = () => {
    m = new Method();
    psv = new ManagementPlan();
}