class ManagementFiscalized {
    constructor() {
    }
    
    validateSearch() {
        if ($(`#macolla`).val() != '') this.searchByFilter();
        else alertify.alert('Importante', 'Debe seleccionar primero una macolla.', function(){}).set({'movable': false, 'moveBounded': false});
    }

    searchByFilter() {
        
        var range = range = $("#filter-date").val() == 1 ?  m.getDatesRange($(`#date-pick`).val()) : m.getDatesRange(`${$(`#from`).val()}|${$(`#until`).val()}`);
        var from = moment(range[0]), until = moment(range[1]), isFrom = m.validateDate(from, 'msgFrom'), isUntil = m.validateDate(until, 'msgUntil');


        if ((from.isValid() && isFrom) && (until.isValid() && isUntil)) {
            const ajax = new Ajax(`${globalConfig.appUrl}/searchFiscalized`, {
                headers: { accept: 'application/json' },
                method: 'POST',
                body: {
                    id: parseInt($(`#macolla`).val()),
                    date_from: range[0],
                    date_to: range[1],
                }
            });
        
            m.setLoadingMessageBefore();
                
            ajax.result().then(response => {
        
                var size = Object.keys(response.data).length;
        
                m.setLoadingMessageAfter();
                m.showMessageDialog(size);

                if (size) {

                    m.setHtmlTable(1, "Producción EHO Fiscalizada", `Macolla ${response.name}`);
                    m.setHtmlTable(2);

                    let headerSelected = `<th >#</th><th >Fecha</th><th>${response.name} <h6 class="text-muted">Barriles Netos</h6></th>`;

                    headerSelected += `<th >%AyS<h6>DCO</h6></th><th >ºAPI<h6>DCO</h6></th><th >Inyección&nbsp;diluente</th><th >%AyS<h6>diluente</h6></th><th >ºAPI<h6>diluente</h6></th><th>Acción</th>`;

                    document.querySelector(`#header`).innerHTML = headerSelected;
                        
                    var table = m.newDateTable(response.results, size);
        
                    document.getElementById('imhere').focus();

                    $(`.dts_label, .dt-buttons, .dataTables_filter`).remove();

                    $(`#datatable`).on('click', '#btn-delete', function() {
                        let $tr = $(this).closest('tr'), $button = $(this);
                        const id = $(this).val();
                        alertify.confirm("<i class='fa fa-exclamation-triangle text-danger'></i> Advertencia", "Se eliminará este registro permanentemente. ¿Está seguro que desea continuar?", function() {},
                            function(e) {
                                if (e) {
                                    $button.attr('disabled', true);
                                    const ajax = new Ajax(`${globalConfig.appUrl}/deleteFiscalized/${id}`, {
                                        headers: { accept: 'application/json' },
                                        method: 'POST'
                                    });
                                    ajax.result().then(response => {table.row($tr).remove().draw(false);alertify.warning("El registro ha sido eliminado");}).catch(error => { console.log(error); });
                                }
                            }).set({'movable': false, 'modal': false, 'moveBounded': false, 'labels': {ok: 'No', cancel: 'Sí'}});
                    });
        
                    document.querySelector('#imhere').focus();

                    $(`#datatable`).on('click', '#btn-edit', function() {
                        var $tr = $(this).closest('tr'), id = $(this).val(), $value = table.row($tr).data();
                        fpr.modify($value, table, $tr, id);
                    });
        
                } else document.querySelector(`#table-content`).innerHTML = ``;
            
            }).catch(error => { console.log(error) });
        }
        return false;
    }

    updating(table, tr) {
        
        m.reloadView();

        const id = $('#identity').val();
        const index = $('#index').val();

        const ajax = new Ajax(`${globalConfig.appUrl}/updateFiscalized/${id}`, {
            headers: {
                accept: 'application/json',
            },
            method: 'POST',
            body: {
                production: m.convertToDecimal(document.querySelector(`#production`).value),
            }
        });

        $("#btn-update").attr("disabled", true);
        
        ajax.result().then(response => {

            var data = [];

            response.data[0][0] = index;

            table.row(table.row(tr).invalidate().data(response.data[0]).draw(false));
            
            alertify.success(response.message);

            $(`#modal-xl`).modal('toggle');

        }).catch(error => {
            if (error.status === 400) {
                m.showInputCorrects(error.errors, this.inputNames);
                m.showInputErrors(error.errors, this.inputNames);
                $("#btn-update").attr("disabled", false);
            } else console.log(error);
        });
        return false;
    }


    modify(array, table, tr, id) {
        
        var data = [];
        
        data.push({
            production: array[2],
            fiscalized_id: id, 
            index: array[0]
        });
        
        $(`#modal-xl`).html(`
        <div class="modal-dialog modal-xl">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title"><i class="fa fa-edit"></i> Producción Fiscalizada</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        
                        <div class="col-md-3 pt-3">
                            <h6>Producción EHO <i class="text-muted">Barriles totales</i></h6>
                            <input type="text" class="form-control form-control-sm" placeholder="Producción EHO" id="production" name="production" value="${data[0].production}" maxlength="6" />
                            <div id="wrapper-production"></div>
                        </div>

                        <div class="col-md-12 pt-0"></div>

                        <div class="col-md-12 pt-3"><h5 class="text-info">DCO</h5></div>

                        <div class="col-md-3 pt-3">
                            <h6>% AyS DCO </h6>
                            <input type="text" class="form-control form-control-sm" placeholder="% AyS DCO" readonly />
                        </div>

                        <div class="col-md-3 pt-3">
                            <h6>ºAPI DCO </h6>
                            <input type="text" class="form-control form-control-sm" placeholder="ºAPI DCO" readonly />
                        </div>

                        <div class="col-md-12 pt-4"><h5 class="text-info">Diluente</h5></div>

                        <div class="col-md-3 pt-3">
                            <h6>Inyección de Diluente</h6>
                            <input type="text" class="form-control form-control-sm" placeholder="Inyección de Diluente" readonly />
                        </div>

                        <div class="col-md-3 pt-3">
                            <h6>% AyS Diluente</h6>
                            <input type="text" class="form-control form-control-sm" placeholder="% AyS Diluente" readonly />
                        </div>
                        
                        <div class="col-md-3 pt-3">
                            <h6>ºAPI Diluente</h6>
                            <input type="text" class="form-control form-control-sm" placeholder="ºAPI Diluente" readonly />
                        </div>

                        <input type="hidden" id="identity" value="${data[0].fiscalized_id}">
                        <input type="hidden" id="index" value="${data[0].index}">
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    <button type="button" class="btn btn-primary" id="btn-update">Guardar cambios</button>
                </div>
            </div>
        </div>`);
    
        var input = ['diluent', 'rpm', 'pump_efficiency', 'torque', 'frequency', 'current', 'power', 'mains_voltage', 'output_voltage', 'vfd_temperature', 'head_temperature', 'head_pressure'];
        
        for (let i = 0; i < input.length; i++) $(`#${input[i]}`).numeric({ negative: false });

        $(`#btn-update`).click(function () {
            alertify.confirm('Pregunta', '¿Está seguro que desea guardar los cambios realizados en este registro?', function(e) {
                if (e) fpr.updating(table, tr);
            }, function(){}).set({'movable': false, 'moveBounded': false, 'modal': false, 'labels': {ok: 'Sí', cancel: 'No'}});
        });
    }
}

window.onload = () => {
    m = new Method();
    fpr = new ManagementFiscalized();
}