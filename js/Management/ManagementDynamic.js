class ManagementDynamic {
    constructor() {
        this.searchByFilter = this.searchByFilter.bind(this);
        this.setHeader = this.setHeader.bind(this);
        this.inputNames = ['quality', 'pip', 'pdp', 'temperature', 'observations'];
    }

    searchByFilter() {

        var size = m.getColumnsFromDb().length, values = m.getColumnsFromDb(), preset = ['well_name', 'dynamic_date', 'macolla_name', 'location_name', 'reservoir_name'];

        for (var i = 0; i <= 4; i++) values[size + i] = preset[i];
        
        var range, typeSearch, reservoirId = [], collection = [];
        
        range = $("#filter-date").val() == 1 ?  m.getDatesRange($(`#date-pick`).val()) : m.getDatesRange(`${$(`#from`).val()}|${$(`#until`).val()}`);
        
        var whatIDo = document.querySelector(`#dual-list`).style.display == 'none';

        var from = moment(range[0]), until = moment(range[1]), isFrom = m.validateDate(from, 'msgFrom'), isUntil = m.validateDate(until, 'msgUntil');

        if (!whatIDo) {
            typeSearch = 1;
            var sizeIs = ($(`#location_well`).val()).length, store = $(`#location_well`).val();
            if (sizeIs > 1) for (var i = 0; i < sizeIs; i++) collection[i] = store[i];
            else collection = store[0];
        } else {
            $('input[name="option[]"]:checked').each(function(i) { reservoirId[i] = $(this).val(); });
            typeSearch = 2;
            var sizeIs = ($(`#macolla_list`).val()).length, store = $(`#macolla_list`).val();
            if (sizeIs > 1) for (var i = 0; i < sizeIs; i++) collection[i] = store[i];
            else collection = store[0];
        }

        if ((from.isValid() && isFrom) && (until.isValid() && isUntil)) {

            const ajax = new Ajax(`${globalConfig.appUrl}/searchDynamic`, {
                headers: {
                    accept: 'application/json',
                },
                method: 'POST',
                body: {
                    items: values,
                    data: collection,
                    macolla_id: parseInt($(`#macolla`).val()),
                    reservoir_id: reservoirId != null ? reservoirId : null,
                    date_from: range[0],
                    date_to: range[1],
                    type: typeSearch
                }
            });
        
            m.setLoadingMessageBefore();
                
            ajax.result().then(response => {
                    
                var options = ['quality', 'pip', 'pdp', 'temperature', 'observations'], size = Object.keys(response.data).length;
    
                m.setLoadingMessageAfter();
                m.showMessageDialog(size);

                if (size > 0) {
                    
                    var selected = m.getColumnsToHead(options, Object.keys(response.data[0]));
                    var header = m.setColumn(response.type, response.info)[0], column = m.setColumn(response.type, response.info)[1];

                    let detail = (response.type == 1) ? header : m.selectedItems(response.total);
                    var titleText = detail == 'SIN MACOLLA' ? detail : `Macolla ${detail}`;

                    m.setHtmlTable(1, "Histórico Presión Dinámica", `${titleText}`);
                    m.setHtmlTable(2);

                    let headerSelected = `<th class='text-muted'>#</th>${column}<th class='text-muted'>Localización</th><th class='text-muted'>Pozo</th><th class='text-muted'>Yacimiento</th><th class='text-muted'>Fecha&nbsp;y&nbsp;hora&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>`;

                    for (var i=0; i<selected.length; i++) headerSelected += `<th>${this.setHeader(selected[i])}</th>`;

                    document.querySelector(`#header`).innerHTML = headerSelected + `<th>Acción</th>`;

                    var table = m.newDateTable(response.results, size);

                    document.getElementById('imhere').focus();

                    $(`.dts_label, .dt-buttons, .dataTables_filter`).remove();

                    $(`#datatable`).on('click', '#btn-delete', function() {
                        let $tr = $(this).closest('tr'), $button = $(this);
                        const id = $(this).val();
                        alertify.confirm("<i class='fa fa-exclamation-triangle text-danger'></i> Advertencia", "Se eliminará este registro permanentemente. ¿Está seguro que desea continuar?", function() {},
                            function(e) {
                                if (e) {
                                    $button.attr('disabled', true);
                                    const ajax = new Ajax(`${globalConfig.appUrl}/deleteDynamic/${id}`, {
                                        headers: { accept: 'application/json' },
                                        method: 'POST'
                                    });
                                    ajax.result().then(response => {table.row($tr).remove().draw(false);alertify.warning("El registro ha sido eliminado");}).catch(error => { console.log(error); });
                                }
                            }).set({'movable': false, 'modal': false, 'moveBounded': false, 'labels': {ok: 'No', cancel: 'Sí'}});
                    });

                    $(`#datatable`).on('click', '#btn-edit', function() {
                        var $tr = $(this).closest('tr'), id = $(this).val(), $value = table.row($tr).data();
                        dnm.modify($value, table, $tr, id);
                    });

                } else document.querySelector(`#table-content`).innerHTML = ``;
                
            }).catch(error => {console.log(error)});
        }
        return false;
    }

    updating(table, tr) {
        
        m.reloadView();

        const id = document.querySelector(`#identity`).value;
        const index = document.querySelector(`#index`).value;
        var data = [];

        const ajax = new Ajax(`${globalConfig.appUrl}/updateDynamic/${id}`, {
            headers: { accept: 'application/json' },
            method: 'POST',
            body: {
                quality: m.trimString('quality'),
                pip: m.convertToDecimal(document.querySelector(`#pip`).value),
                pdp: m.convertToDecimal(document.querySelector(`#pdp`).value),
                temperature: m.convertToDecimal(document.querySelector(`#temperature`).value),
                observations: m.trimNotRequiredString('observations')
            }
        });

        $("#btn-update").attr("disabled", true);
        
        ajax.result().then(response => {
            
            var data = [];

            response.data[0][0] = index;

            table.row(table.row(tr).invalidate().data(response.data[0]).draw(false));
            
            alertify.success(response.message);

            $(`#modal-xl`).modal('toggle');
            $(`#modal-xl`).html(``);

        }).catch(error => {
            if (error.status === 400) {
                m.showInputCorrects(error.errors, this.inputNames);
                m.showInputErrors(error.errors, this.inputNames);
                $("#btn-update").attr("disabled", false);
            } else console.log(error);
        });
        return false;
    }


    modify(array, table, tr, id) {
        
        var data = [];

        data.push({
            quality: array[5],
            pip: array[6],
            pdp: array[7],
            temperature: array[8],
            observations: array[9],
            dynamic_id: id,
            index: array[0]
        });

        console.log(data);
        
        $(`#modal-xl`).html(`<div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title"><i class="fa fa-edit"></i> Presión Dinámica</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-3 pt-4">
                        <h6>Calidad</h6>
                        <select class="form-control form-control-sm" id="quality" name="quality">${m.loadQuality(data[0].quality)}</select>
                        <div id="wrapper-quality"></div>
                    </div>
                    <div class="col-md-3 pt-4">
                        <h6>Pwf)EB <i class="text-muted">lppc</i></h6>
                        <input type="text" class="form-control form-control-sm" placeholder="PIP" id="pip" name="pip" maxlength="6" value="${m.setNullToValue(data[0].pip)}" autocomplete="off" />
                        <div id="wrapper-pip"></div>
                    </div>
                    <div class="col-md-3 pt-4">
                        <h6>PDP)SB <i class="text-muted">lppc</i></h6>
                        <input type="text" class="form-control form-control-sm" placeholder="PDP" id="pdp" name="pdp" maxlength="6" value="${m.setNullToValue(data[0].pdp)}" autocomplete="off" />
                        <div id="wrapper-pdp"></div>
                    </div>
                    <div class="col-md-3 pt-4">
                        <h6>Twf)EB <i class="text-muted">ºF</i></h6>
                        <input type="text" class="form-control form-control-sm" placeholder="Temperatura" id="temperature" name="temperature" maxlength="6" value="${m.setNullToValue(data[0].temperature)}" autocomplete="off" />
                        <div id="wrapper-temperature"></div>
                    </div>
                    <div class="col-md-12 pt-3"><hr></div>
                    <div class="col-md-12 pt-3">
                        <h6>Observaciones <i class="text-muted">120 caracteres</i></h6>
                        <textarea class="form-control form-control-sm" style="max-height:80px" placeholder="Observaciones" id="observations" name="observations" maxlength="120">${data[0].observations}</textarea>
                    </div>
                </div>
            </div>

            <input type="hidden" id="identity" value="${data[0].dynamic_id}">
            <input type="hidden" id="index" value="${data[0].index}">

            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-primary" id="btn-update">Guardar cambios</button>
            </div>
        </div>
        </div>`);
    
        var input = ['temperature'];
        
        for (let i = 0; i < input.length; i++) $(`#${input[i]}`).numeric({ negative: false });
        
        $(`#pdp, #pip`).numeric({ decimal: false, negative: false });

        $(`#btn-update`).click(function () {
            alertify.confirm('Pregunta', '¿Está seguro que desea guardar los cambios realizados en este registro?', function(e) {
                if (e) dnm.updating(table, tr);
            }, function(){}).set({'movable': false, 'moveBounded': false, 'modal': false, 'labels': {ok: 'Sí', cancel: 'No'}});
        });
    }

    setHeader(value) {
        if (value === 'quality') return 'Calidad';
        else if (value === 'pip') return 'Pe)EB <h6 class="text-muted">lppc</h6>';
        else if (value === 'pdp') return 'PDPe)SB <h6 class="text-muted">lppc</h6>';
        else if (value === 'temperature') return 'Te)EB <h6 class="text-muted">ºF</h6>';
        else if (value === 'observations') return 'Observaciones';
    }
}

window.onload = () => {
    m = new Method();
    dnm = new ManagementDynamic();
}