class ManagementAverage {
    constructor() {
        this.searchByFilter = this.searchByFilter.bind(this);
        this.setHeader = this.setHeader.bind(this);
        this.inputNames = ['company', 'rpm', 'duration', 'line_pressure', 'line_temperature', 'gas_rate_prod', 'water_rate_prod', 'fluid_total_rate', 'neat_rate_prod', 'percent_mixture', 'api_degree_mixture', 'percent_formation', 'inyected_diluent_rate', 'api_degree_diluent', 'test', 'pip', 'gvf'];
    }
    
    searchByFilter() {

        var size = m.getColumnsFromDb().length, values = m.getColumnsFromDb(), preset = ['average_tests.id as id', 'well_name', 'avg_start_test', 'avg_end_test', 'company', 'duration', 'test', 'macolla_name', 'location_name', 'reservoir_name'];

        for (var i = 0; i <= 9; i++) values[size + i] = preset[i];
        
        var range, typeSearch, reservoirId = [], collection = [];
        
        range = $("#filter-date").val() == 1 ?  m.getDatesRange($(`#date-pick`).val()) : m.getDatesRange(`${$(`#from`).val()}|${$(`#until`).val()}`);
        
        var whatIDo = document.querySelector(`#dual-list`).style.display == 'none';

        if (!whatIDo) {
            typeSearch = 1;
            var sizeIs = ($(`#location_well`).val()).length, store = $(`#location_well`).val();
            if (sizeIs > 1) for (var i = 0; i < sizeIs; i++) collection[i] = store[i];
            else collection = store[0];
        }
        
        var from = moment(range[0]), until = moment(range[1]), isFrom = m.validateDate(from, 'msgFrom'), isUntil = m.validateDate(until, 'msgUntil');

        if ((from.isValid() && isFrom) && (until.isValid() && isUntil)) {

            $(`#msgFrom, #msgUntil`).html(``);

            const ajax = new Ajax(`${globalConfig.appUrl}/searchAverage`, {
                headers: {
                    accept: 'application/json',
                },
                method: 'POST',
                body: {
                    items: values,
                    data: collection,
                    macolla_id: parseInt($(`#macolla`).val()),
                    reservoir_id: reservoirId != null ? reservoirId : null,
                    date_from: range[0],
                    date_to: range[1],
                    type: typeSearch
                }
            });

            m.setLoadingMessageBefore();

            ajax.result().then(response => {

                var size = Object.keys(response.data).length;

                m.setLoadingMessageAfter();
                m.showMessageDialog(size);

                var options = ['rpm', 'line_pressure', 'line_temperature', 'gas_rate_prod','water_rate_prod', 'fluid_total_rate', 'percent_mixture', 'api_degree_mixture', 'neat_rate_prod', 'percent_formation', 'gvf', 'inyected_diluent_rate', 'api_degree_diluent', 'pip', 'observations'];

                var selected = m.getColumnsToHead(options, Object.keys(response.data[0]));
                var header = m.setColumn(response.type, response.info)[0];

                let detail = (response.type == 1) ? header : m.selectedItems(response.total);
                var titleText = detail == 'SIN MACOLLA' ? detail : `Macolla ${detail}`;

                m.setHtmlTable(1, "Prueba Promedio", `${titleText}`);
                m.setHtmlTable(2);

                let headerSelected = `<th class='text-muted'>#</th><th class='text-muted'>Localización</th><th class='text-muted'>Pozo</th><th class='text-muted'>Yacimiento</th><th class='text-muted'>Inicio&nbsp;prueba</th><th class='text-muted'>Fin&nbsp;prueba</th><th class='text-muted'>Compañía</th><th class='text-muted'>Duración <h6 class="text-muted">horas</h6></th><th class='text-muted'>Prueba</th>`;

                for (var i=0; i<selected.length; i++) headerSelected += `<th>${this.setHeader(selected[i])}</th>`;

                headerSelected += `<th class='text-muted'>Acción</th>`;

                $(`#header`).html(headerSelected);

                var table = m.newDateTable(response.results, size);

                document.getElementById('imhere').focus();

                $(`.dts_label, .dt-buttons, .dataTables_filter`).remove();

                $(`#datatable`).on('click', '#btn-delete', function() {

                    let $tr = $(this).closest('tr'), $button = $(this);
                    const id = $(this).val();

                    alertify.confirm("<i class='fa fa-exclamation-triangle text-danger'></i> Advertencia", "Se eliminará este registro permanentemente. ¿Está seguro que desea continuar?", function() {},
                        function(e) {
                            if (e) {
                                $button.attr('disabled', true);
                                const ajax = new Ajax(`${globalConfig.appUrl}/deleteAverage/${id}`, {
                                    headers: { accept: 'application/json' },
                                    method: 'POST'
                                });
                                ajax.result().then(response => {table.row($tr).remove().draw(false);alertify.warning("El registro ha sido eliminado");}).catch(error => { console.log(error); });
                            }
                        }).set({'movable': false, 'modal': false, 'moveBounded': false, 'labels': {ok: 'No', cancel: 'Sí'}});
                });

                $(`#datatable`).on('click', '#btn-edit', function() {
                    var $tr = $(this).closest('tr'), id = $(this).val(), $value = table.row($tr).data();
                    avg.modify($value, table, $tr, id);
                });
            }).catch(error => { console.log(error) });
        }
        return false;
    }
    
    updating(table, tr) {
        
        m.reloadView();

        const id = $('#identity').val();
        const index = $('#index').val();

        const ajax = new Ajax(`${globalConfig.appUrl}/updateAverage/${id}`, {
            headers: {
                accept: 'application/json',
            },
            method: 'POST',
            body: {
                company: m.trimString('company'),
                rpm: document.querySelector(`#rpm`).value,
                duration: document.querySelector(`#duration`).value,
                line_pressure: m.convertToDecimal(document.querySelector(`#line_pressure`).value),
                line_temperature: m.convertToDecimal(document.querySelector(`#line_temperature`).value),
                gas_rate_prod: m.convertToDecimal(document.querySelector(`#gas_rate_prod`).value),
                water_rate_prod: m.convertToDecimal(document.querySelector(`#water_rate_prod`).value),
                fluid_total_rate: m.convertToDecimal(document.querySelector(`#fluid_total_rate`).value),
                neat_rate_prod: m.convertToDecimal(document.querySelector(`#neat_rate_prod`).value),
                percent_mixture: m.convertToDecimal(document.querySelector(`#percent_mixture`).value),
                api_degree_mixture: m.convertToDecimal(document.querySelector(`#api_degree_mixture`).value),
                percent_formation: m.convertToDecimal(document.querySelector(`#percent_formation`).value),
                inyected_diluent_rate: m.convertToDecimal(document.querySelector(`#inyected_diluent_rate`).value),
                api_degree_diluent: m.convertToDecimal(document.querySelector(`#api_degree_diluent`).value),
                pip: m.convertToDecimal(document.querySelector(`#pip`).value),
                gvf: m.convertToDecimal(document.querySelector(`#gvf`).value),
                test: m.trimString('test'),
                observations: m.trimNotRequiredString('observations')
            }
        });

        $("#btn-update").attr("disabled", true);
        
        ajax.result().then(response => {

            var data = [];

            response.data[0][0] = index;

            table.row(table.row(tr).invalidate().data(response.data[0]).draw(false));
            
            alertify.success(response.message);

            $(`#modal-xl`).modal('toggle');

        }).catch(error => {
            if (error.status === 400) {
                m.showInputCorrects(error.errors, this.inputNames);
                m.showInputErrors(error.errors, this.inputNames);
                $("#btn-update").attr("disabled", false);
            } else console.log(error);
        });
        return false;
    }

    modify(array, table, tr, id) {
        
        var data = [];

        data.push({
            company: array[6],
            rpm: array[9],
            duration: array[7],
            line_pressure: array[10],
            line_temperature: array[11],
            gas_rate_prod: array[12],
            water_rate_prod: array[13],
            fluid_total_rate: array[14],
            neat_rate_prod: array[17],
            percent_mixture: array[15],
            api_degree_mixture: array[16],
            percent_formation: array[18],
            inyected_diluent_rate: array[20],
            api_degree_diluent: array[21],
            test: array[8],
            pip: array[22],
            gvf: array[19],
            observations: array[23],
            average_id: id, 
            index: array[0]
        });
        
        $(`#modal-xl`).html(`<div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title"><i class="fa fa-edit"></i> Prueba Promedio</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-4 pt-4">
                        <h6>Empresa</h6>
                        <input type="text" class="form-control form-control-sm" placeholder="Empresa" id="company" name="company" value="${data[0].company}" style="text-transform:uppercase" maxlength="20" autocomplete="off" />
                        <div id="wrapper-company"></div>
                    </div>
                    <div class="col-md-4 pt-4">
                        <h6>RPM <i class="text-muted">BCP</i></h6>
                        <input type="text" class="form-control form-control-sm" placeholder="RPM" id="rpm" name="rpm" value="${data[0].rpm}" maxlength="4" autocomplete="off" />
                        <div id="wrapper-rpm"></div>
                    </div>
                    <div class="col-md-4 pt-4">
                        <h6>Duración <i class="text-muted">Horas</i></h6>
                        <input type="text" class="form-control form-control-sm" placeholder="Duración" id="duration" name="duration" value="${data[0].duration}" maxlength="4" autocomplete="off" />
                        <div id="wrapper-duration"></div>
                    </div>

                    <div class="col-md-12 pt-4"><h5 class="text-info">Líneas de Producción</h5></div>

                    <div class="col-md-6 pt-3">
                        <h6>Línea de Presión <i class="text-muted">lppc</i></h6>
                        <input type="text" class="form-control form-control-sm" placeholder="Presión" id="line_pressure" name="line_pressure" value="${m.setNullToValue(data[0].line_pressure)}" maxlength="6" autocomplete="off" />
                        <div id="wrapper-line_pressure"></div>
                    </div>
                    <div class="col-md-6 pt-3">
                        <h6>Línea de Temperatura <i class="text-muted">ºF</i></h6>
                        <input type="text" class="form-control form-control-sm" placeholder="Temperatura" id="line_temperature" name="line_temperature" value="${m.setNullToValue(data[0].line_temperature)}" maxlength="6" autocomplete="off" />
                        <div id="wrapper-line_temperature"></div>
                    </div>

                    <div class="col-md-12 pt-4"><h5 class="text-info">Tasas de Producción</h5></div>

                    <div class="col-md-3 pt-3">
                        <h6>Tasa Prod. de Gas <i class="text-muted">MPCND</i></h6>
                        <input type="text" class="form-control form-control-sm" placeholder="Tasa Producción de Gas" id="gas_rate_prod" name="gas_rate_prod" value="${m.setNullToValue(data[0].gas_rate_prod)}" maxlength="6" autocomplete="off" />
                        <div id="wrapper-gas_rate_prod"></div>
                    </div>

                    <div class="col-md-3 pt-3">
                        <h6>Tasa Prod. de Agua <i class="text-muted">bpd</i></h6>
                        <input type="text" class="form-control form-control-sm" placeholder="Tasa Producción de Agua" id="water_rate_prod" name="water_rate_prod" value="${m.setNullToValue(data[0].water_rate_prod)}" maxlength="6" autocomplete="off" />
                        <div id="wrapper-water_rate_prod"></div>
                    </div>

                    <div class="col-md-3 pt-3">
                        <h6>Tasa Fluidos Totales <i class="text-muted">BFPD</i></h6>
                        <input type="text" class="form-control form-control-sm" placeholder="Tasa Fluidos Totales" id="fluid_total_rate" name="fluid_total_rate" value="${m.setNullToValue(data[0].fluid_total_rate)}" maxlength="6" autocomplete="off" />
                        <div id="wrapper-fluid_total_rate"></div>
                    </div>

                    <div class="col-md-3 pt-3">
                        <h6>Tasa Prod. Neta <i class="text-muted">BPPD</i></h6>
                        <input type="text" class="form-control form-control-sm" placeholder="Tasa Producción Neta" id="neat_rate_prod" name="neat_rate_prod" value="${m.setNullToValue(data[0].neat_rate_prod)}" maxlength="6" autocomplete="off" />
                        <div id="wrapper-neat_rate_prod"></div>
                    </div>

                    <div class="col-md-12 pt-4"><h5 class="text-info">Mezcla</h5></div>

                    <div class="col-md-6 pt-3">
                        <h6>% AyS de la Mezcla</h6>
                        <input type="text" class="form-control form-control-sm" placeholder="% AyS Mezcla" id="percent_mixture" name="percent_mixture" value="${m.setNullToValue(data[0].percent_mixture)}" maxlength="6" autocomplete="off" />
                        <div id="wrapper-percent_mixture"></div>
                    </div>

                    <div class="col-md-6 pt-3">
                        <h6>ºAPI de la Mezcla</h6>
                        <input type="text" class="form-control form-control-sm" placeholder="ºAPI Mezcla" id="api_degree_mixture" name="api_degree_mixture" value="${m.setNullToValue(data[0].api_degree_mixture)}" maxlength="6" autocomplete="off" />
                        <div id="wrapper-api_degree_mixture"></div>
                    </div>

                    <div class="col-md-12 pt-4"><h5 class="text-info">Formación</h5></div>

                    <div class="col-md-6 pt-3">
                        <h6>% AyS de la Formación</h6>
                        <input type="text" class="form-control form-control-sm" placeholder="% AyS Formación" id="percent_formation" name="percent_formation" value="${m.setNullToValue(data[0].percent_formation)}" maxlength="6" autocomplete="off" />
                        <div id="wrapper-percent_formation"></div>
                    </div>

                    <div class="col-md-12 pt-4"><h5 class="text-info">Diluente</h5></div>

                    <div class="col-md-6 pt-3">
                        <h6>Tasa Inyección Diluente <i class="text-muted">BDPD</i></h6>
                        <input type="text" class="form-control form-control-sm" placeholder="Tasa Inyección Diluente" id="inyected_diluent_rate" name="inyected_diluent_rate" value="${m.setNullToValue(data[0].inyected_diluent_rate)}" maxlength="6" autocomplete="off" />
                        <div id="wrapper-inyected_diluent_rate"></div>
                    </div>

                    <div class="col-md-6 pt-3">
                        <h6>ºAPI Diluente</h6>
                        <input type="text" class="form-control form-control-sm" placeholder="ºAPI Diluente" id="api_degree_diluent" name="api_degree_diluent" value="${m.setNullToValue(data[0].api_degree_diluent)}" maxlength="6" autocomplete="off" />
                        <div id="wrapper-api_degree_diluent"></div>
                    </div>

                    <div class="col-md-12 pt-4"><h5 class="text-info">Prueba</h5></div>

                    <div class="col-md-3 pt-3">
                        <h6>Tipo de Prueba</h6>
                        <input type="text" class="form-control form-control-sm" placeholder="Prueba" id="test" name="test" maxlength="1" style="text-transform:uppercase" value="${data[0].test}" autocomplete="off" />
                        <div id="wrapper-test"></div>
                    </div>

                    <div class="col-md-3 pt-3">
                        <h6>PIP <i class="text-muted">lppc</i></h6>
                        <input type="text" class="form-control form-control-sm" placeholder="PIP" id="pip" name="pip" value="${m.setNullToValue(data[0].pip)}" maxlength="6" autocomplete="off" />
                        <div id="wrapper-pip"></div>
                    </div>

                    <div class="col-md-6 pt-3">
                        <h6>Fracción Volumen de Gas <i class="text-muted">porcentaje (%)</i></h6>
                        <input type="text" class="form-control form-control-sm" placeholder="FVG" id="gvf" name="gvf" value="${m.setNullToValue(m.setFormat((m.convertToNumber(data[0].gvf) * 100), 2))}" maxlength="6" autocomplete="off" />
                        <div id="wrapper-gvf"></div>
                    </div>

                    <div class="col-md-12 pt-4"><hr></div>

                    <div class="col-md-12 pt-3">
                        <h6>Observaciones <i class="text-muted">120 caracteres</i></h6>
                        <textarea class="form-control form-control-sm" style="max-height:80px" placeholder="Observaciones" id="observations" name="observations" maxlength="120">${data[0].observations}</textarea>
                    </div>
                </div>
            </div>

            <input type="hidden" id="identity" value="${data[0].average_id}">
            <input type="hidden" id="index" value="${data[0].index}">

            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-primary" id="btn-update">Guardar cambios</button>
            </div>
        </div>
        </div>`);
    
        var input = ['line_pressure', 'line_temperature', 'rpm', 'duration', 'gas_rate_prod', 'water_rate_prod', 'fluid_total_rate', 'neat_rate_prod', 'percent_mixture', 'api_degree_mixture', 'percent_formation', 'inyected_diluent_rate', 'api_degree_diluent', 'pip', 'gvf'];
        
        for (let i = 0; i < input.length; i++) $(`#${input[i]}`).numeric({ negative: false });

        $(`#btn-update`).click(function () {
            alertify.confirm('Pregunta', '¿Está seguro que desea guardar los cambios realizados en este registro?', function(e) {
                if (e) avg.updating(table, tr);
            }, function(){}).set({'movable': false, 'moveBounded': false, 'modal': false, 'labels': {ok: 'Sí', cancel: 'No'}});
        });
    }

    setHeader(value) {
        if (value === 'line_pressure') return 'P<sub>línea</sub> <h6 class="text-muted">lppc</h6>';
        else if (value === 'line_temperature') return'T<sub>línea</sub> <h6 class="text-muted">ºF</h6>';
        else if(value === 'water_rate_prod') return 'q<sub>w</sub> <h6 class="text-muted">BPD</h6>';
        else if(value === 'gas_rate_prod') return 'q<sub>g</sub> <h6 class="text-muted">MPCND</h6>';
        else if(value === 'rpm') return 'RPM <h6 class="text-muted">BCP</h6>';
        else if (value === 'fluid_total_rate') return 'q<sub>t</sub> <h6 class="text-muted">BFPD</h6>';
        else if (value === 'percent_mixture') return '%AyS&nbsp;Mezcla';
        else if (value === 'api_degree_mixture') return 'ºAPI&nbsp;Mezcla';
        else if (value === 'percent_formation') return '%AyS&nbsp;Formación';
        else if (value === 'gvf') return 'FVG';
        else if (value === 'neat_rate_prod') return 'Tasa&nbsp;neta <h6 class="text-muted">BPPD</h6>';
        else if (value === 'inyected_diluent_rate') return 'Tasa&nbsp;inyección&nbsp;diluente <h6 class="text-muted">BDPD</h6>';
        else if (value === 'api_degree_diluent') return 'ºAPI&nbsp;Diluente';
        else if (value === 'pip') return 'PIP <h6 class="text-muted">lppc</h6>';
        else if (value === 'observations') return 'Observaciones';
        else if (value === 'macolla_name') return 'Macolla';
    }
}

window.onload = () => {
    m = new Method();
    avg = new ManagementAverage();
}