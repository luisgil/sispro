﻿class ManagementHistorical {
    constructor() {
      this.searchByFilter = this.searchByFilter.bind(this);
      this.setHeader = this.setHeader.bind(this);
      this.inputNames = ['type_work_i', 'type_work_ii', 'rig', 'sensor_model', 'sensor_brand', 'sensor_diameter', 'sensor_company', 'deeptop_sensor_md', 'deepbottom_sensor_md', 'pump_model', 'pump_diameter', 'pump_type', 'pump_capacity', 'pump_company', 'deeptop_pump_md', 'deepbottom_pump_md', 'inyected_diluent', 'measure_from', 'observations', 'references'];
    }

    searchByFilter() {

        var size = m.getColumnsFromDb().length, values = m.getColumnsFromDb(), preset = ['well_name', 'begin_date', 'end_date', 'macolla_name', 'location_name', 'reservoir'];

        for (var i = 0; i <= 5; i++) values[size + i] = preset[i];

        var range, typeSearch, reservoirId = [], collection = [];

        range = $("#filter-date").val() == 1 ?  m.getDatesRange($(`#date-pick`).val()) : m.getDatesRange(`${$(`#from`).val()}|${$(`#until`).val()}`);

        var whatIDo = document.querySelector(`#dual-list`).style.display == 'none';
        
        if (!whatIDo) {
            typeSearch = 1;
            var sizeIs = ($(`#location_well`).val()).length, store = $(`#location_well`).val();
            if (sizeIs > 1) for (var i = 0; i < sizeIs; i++) collection[i] = store[i];
            else collection = store[0];
        }

        var from = moment(range[0]), until = moment(range[1]), isFrom = m.validateDate(from, 'msgFrom'), isUntil = m.validateDate(until, 'msgUntil');

        if ((from.isValid() && isFrom) && (until.isValid() && isUntil)) {

            $(`#msgFrom, #msgUntil`).html(``);

            const ajax = new Ajax(`${globalConfig.appUrl}/searchHistorical`, {
                headers: {
                    accept: 'application/json',
                },
                method: 'POST',
                body: {
                    items: values,
                    data: collection,
                    macolla_id: parseInt($(`#macolla`).val()),
                    reservoir_id: reservoirId != null ? reservoirId : null,
                    date_from: range[0],
                    date_to: range[1],
                    type: typeSearch
                }
            });

            m.setLoadingMessageBefore();

            ajax.result().then(response => {

                var size = Object.keys(response.data).length;

                m.setLoadingMessageAfter();
                m.showMessageDialog(size);

                var options = ['type_work_i', 'type_work_ii', 'rig', 'sensor_model', 'sensor_brand', 'sensor_diameter', 'sensor_company', 'deeptop_sensor_md', 'deepbottom_sensor_md', 'pump_model', 'pump_diameter', 'pump_type', 'pump_capacity', 'pump_company', 'deeptop_pump_md', 'deepbottom_pump_md', 'inyected_diluent', 'measure_from', 'observations', 'references'];

                var selected = m.getColumnsToHead(options, Object.keys(response.data[0]));
                var header = m.setColumn(response.type, response.info)[0];

                let detail = (response.type == 1) ? header : m.selectedItems(response.total);
                var titleText = detail == 'SIN MACOLLA' ? detail : `Macolla ${detail}`;

                m.setHtmlTable(1, "Histórico de completación", `${titleText}`);
                m.setHtmlTable(2);

                let headerSelected = `<th class='text-muted'>#</th><th class='text-muted'>Localización</th><th class='text-muted'>Pozo</th><th class='text-muted'>Yacimiento</th><th class='text-muted'>Fecha&nbsp;y&nbsp;hora&nbsp;inicial</th><th class='text-muted'>Fecha&nbsp;y&nbsp;hora&nbsp;final</th>`;

                for (var i=0; i<selected.length; i++) headerSelected += `<th>${this.setHeader(selected[i])}</th>`;

                headerSelected += `<th class='text-muted'>Acción</th>`;

                $(`#header`).html(headerSelected);

                var table = m.newDateTable(response.results, size);

                document.getElementById('imhere').focus();

                $(`.dts_label, .dt-buttons, .dataTables_filter`).remove();

                $(`#datatable`).on('click', '#btn-delete', function() {
                    let $tr = $(this).closest('tr'), $button = $(this);
                    const id = $(this).val();
                    alertify.confirm("<i class='fa fa-exclamation-triangle text-danger'></i> Advertencia", "Se eliminará este registro permanentemente. ¿Está seguro que desea continuar?", function() {},
                        function(e) {
                            if (e) {
                                $button.attr('disabled', true);
                                const ajax = new Ajax(`${globalConfig.appUrl}/deleteHistorical/${id}`, {
                                    headers: { accept: 'application/json' },
                                    method: 'POST'
                                });
                                ajax.result().then(response => {table.row($tr).remove().draw(false);alertify.warning("El registro ha sido eliminado");}).catch(error => { console.log(error); });
                            }
                        }).set({'movable': false, 'modal': false, 'moveBounded': false, 'labels': {ok: 'No', cancel: 'Sí'}});
                });

                $(`#datatable`).on('click', '#btn-edit', function() {
                    var $tr = $(this).closest('tr'), id = $(this).val(), $value = table.row($tr).data();
                    hst.modify($value, table, $tr, id);
                });

            }).catch(error => { console.log(error) });
        }
        
        return false;
    }

    updating(table, tr) {
		
		m.reloadView();

		const id = $('#identity').val();
        const index = $('#index').val();

		const ajax = new Ajax(`${globalConfig.appUrl}/updateHistorical/${id}`, {
			headers: {
				accept: 'application/json',
			},
			method: 'POST',
			body: {
                reservoir: document.querySelector(`#reservoir_name`).value,
                type_work_i: m.trimString('type_work_i'),
                type_work_ii: m.trimString('type_work_ii'),
                rig: m.trimString('rig'),
                sensor_brand: m.trimNotRequiredString('sensor_brand'),
                sensor_model: m.trimNotRequiredString('sensor_model'),
                sensor_diameter: m.convertToDecimal(document.querySelector(`#sensor_diameter`).value),
                sensor_company: m.trimNotRequiredString('sensor_company'),
                deeptop_sensor_md: m.convertToDecimal(document.querySelector(`#deeptop_sensor_md`).value),
                deepbottom_sensor_md: m.convertToDecimal(document.querySelector(`#deepbottom_sensor_md`).value),
                pump_model: m.trimString('pump_model'),
                pump_diameter: m.convertToDecimal(document.querySelector(`#pump_diameter`).value),
                pump_type: m.trimString('pump_type'),
                pump_capacity: m.convertToDecimal(document.querySelector(`#pump_capacity`).value),
                pump_company: m.trimString('pump_company'),
                deeptop_pump_md: m.convertToDecimal(document.querySelector(`#deeptop_pump_md`).value),
                deepbottom_pump_md: m.convertToDecimal(document.querySelector(`#deepbottom_pump_md`).value),
                inyected_diluent: m.trimNotRequiredString('inyected_diluent'),
                measure_from: m.trimNotRequiredString('measure_from'),
                observations: m.trimNotRequiredString('observations'),
                references: m.trimNotRequiredString('references')
			}
		});

		$("#btn-update").attr("disabled", true);
		
		ajax.result().then(response => {

            var data = [];

            response.data[0][0] = index;

            table.row(table.row(tr).invalidate().data(response.data[0]).draw(false));
            
            alertify.success(response.message);

			$(`#modal-xl`).modal('toggle');

		}).catch(error => {
			if (error.status === 400) {
				m.showInputCorrects(error.errors, this.inputNames);
				m.showInputErrors(error.errors, this.inputNames);
				$("#btn-update").attr("disabled", false);
			} else console.log(error);
		});
		return false;
	}

    modify(array, table, tr, id) {
        
        var data = [];
        
        data.push({
            reservoir: array[3],
            type_work_i: array[6],
            type_work_ii: array[7],
            rig: array[8],
            sensor_brand: array[10],
            sensor_model: array[9],
            sensor_diameter: array[11],
            sensor_company: array[12],
            deeptop_sensor_md: array[13],
            deepbottom_sensor_md: array[14],
            pump_model: array[15],
            pump_diameter: array[16],
            pump_type: array[17],
            pump_capacity: array[18],
            pump_company: array[19],
            deeptop_pump_md: array[20],
            deepbottom_pump_md: array[21],
            inyected_diluent: array[22],
            measure_from: array[23],
            observations: array[24],
            references: array[25],
            historical_id: id,
            index: array[0]
        });
        
        $(`#modal-xl`).html(`<div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title"><i class="fa fa-edit"></i> Hist. completación</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-3 pt-4">
                        <h6>Tipo de Trabajo</h6>
                        <input type="text" class="form-control form-control-sm" placeholder="Tipo de Trabajo" id="type_work_i" name="type_work_i" value="${data[0].type_work_i}" style="text-transform:uppercase" maxlength="20" autocomplete="off" />
                        <div id="wrapper-type_work_i"></div>
                    </div>

                    <div class="col-md-3 pt-4">
                        <h6>Tipo de Trabajo II</h6>
                        <input type="text" class="form-control form-control-sm" placeholder="Tipo de Trabajo II" id="type_work_ii" name="type_work_ii" value="${data[0].type_work_ii}" style="text-transform:uppercase" maxlength="20" autocomplete="off" />
                        <div id="wrapper-type_work_ii"></div>
                    </div>

                    <div class="col-md-3 pt-4">
                        <h6>Equipo</h6>
                        <input type="text" class="form-control form-control-sm" placeholder="Equipo" id="rig" name="rig" value="${data[0].rig}" style="text-transform:uppercase" maxlength="20" autocomplete="off" />
                        <div id="wrapper-rig"></div>
                    </div>

                    <div class="col-md-3 pt-4">
                        <h6>Yacimiento</h6>
                        <select type="text" class="form-control form-control-sm" placeholder="Yacimiento" id="reservoir_name" name="reservoir"><option value="">Cargando...</option></select>
                        <div id="wrapper-reservoir"></div>
                    </div>

                    <div class="col-md-12 pt-4"><h5 class="text-info">Sensor</h5></div>

                    <div class="col-md-4 pt-3">
                        <h6>Marca de Sensor</h6>
                        <input type="text" class="form-control form-control-sm" placeholder="Marca de Sensor" id="sensor_brand" name="sensor_brand" value="${data[0].sensor_brand}" style="text-transform:uppercase" maxlength="20" autocomplete="off" />
                        <div id="wrapper-sensor_brand"></div>
                    </div>

                    <div class="col-md-4 pt-3">
                        <h6>Modelo de Sensor</h6>
                        <input type="text" class="form-control form-control-sm" placeholder="Modelo de Sensor" id="sensor_model" name="sensor_model" value="${data[0].sensor_model}" style="text-transform:uppercase" maxlength="20" autocomplete="off" />
                        <div id="wrapper-sensor_model"></div>
                    </div>

                    <div class="col-md-4 pt-3">
                        <h6>Compañía de Sensor</h6>
                        <input type="text" class="form-control form-control-sm" placeholder="Compañía de Sensor" id="sensor_company" name="sensor_company" value="${data[0].sensor_company}" style="text-transform:uppercase" maxlength="20" autocomplete="off" />
                        <div id="wrapper-sensor_company"></div>
                    </div>

                    <div class="col-md-4 pt-3">
                        <h6>Diámetro de Sensor <i class="text-muted">pulgadas</i></h6>
                        <input type="text" class="form-control form-control-sm" placeholder="Diámetro de Sensor" id="sensor_diameter" name="sensor_diameter" value="${m.setNullToValue(m.fractionStrToDecimal(data[0].sensor_diameter))}" maxlength="6" autocomplete="off" />
                        <div id="wrapper-sensor_diameter"></div>
                    </div>

                    <div class="col-md-4 pt-3">
                        <h6>Prof. Superior Sensor MD <i class="text-muted">pies</i></h6>
                        <input type="text" class="form-control form-control-sm" placeholder="Prof. Superior MD Sensor" id="deeptop_sensor_md" name="deeptop_sensor_md" value="${m.setNullToValue(data[0].deeptop_sensor_md)}" maxlength="6" autocomplete="off" />
                        <div id="wrapper-deeptop_sensor_md"></div>
                    </div>

                    <div class="col-md-4 pt-3">
                        <h6>Prof. Inferior Sensor MD <i class="text-muted">pies</i></h6>
                        <input type="text" class="form-control form-control-sm" placeholder="Prof. Inferior MD Sensor" id="deepbottom_sensor_md" name="deepbottom_sensor_md" value="${m.setNullToValue(data[0].deepbottom_sensor_md)}" maxlength="6" autocomplete="off" />
                        <div id="wrapper-deepbottom_sensor_md"></div>
                    </div>

                    <div class="col-md-12 pt-4"><h5 class="text-info">Bomba</h5></div>

                    <div class="col-md-4 pt-3">
                        <h6>Modelo de Bomba</h6>
                        <input type="text" class="form-control form-control-sm" placeholder="Modelo de Bomba" id="pump_model" name="pump_model" style="text-transform:uppercase" value="${data[0].pump_model}" maxlength="20" autocomplete="off" />
                        <div id="wrapper-pump_model"></div>
                    </div>

                    <div class="col-md-4 pt-3">
                        <h6>Compañía de Bomba</h6>
                        <input type="text" class="form-control form-control-sm" placeholder="Compañía de Bomba" id="pump_company" name="pump_company" style="text-transform:uppercase" value="${data[0].pump_company}" maxlength="20" autocomplete="off" />
                        <div id="wrapper-pump_company"></div>
                    </div>

                    <div class="col-md-4 pt-3">
                        <h6>Diámetro de Bomba <i class="text-muted">pulgadas</i></h6>
                        <input type="text" class="form-control form-control-sm" placeholder="Diámetro de Bomba" id="pump_diameter" name="pump_diameter" value="${m.setNullToValue(m.fractionStrToDecimal(data[0].pump_diameter))}" maxlength="6" autocomplete="off" />
                        <div id="wrapper-pump_diameter"></div>
                    </div>

                    <div class="col-md-6 pt-3">
                        <h6>Tipo de Bomba</h6>
                        <input type="text" class="form-control form-control-sm" placeholder="Tipo de Bomba" id="pump_type" name="pump_type" style="text-transform:uppercase" value="${data[0].pump_type}" maxlength="20" autocomplete="off" />
                        <div id="wrapper-pump_type"></div>
                    </div>

                    <div class="col-md-6 pt-3">
                        <h6>Capacidad de Bomba <i class="text-muted">BPD/100 RPM</i></h6>
                        <input type="text" class="form-control form-control-sm" placeholder="Capacidad de Bomba" id="pump_capacity" name="pump_capacity" value="${m.setNullToValue(data[0].pump_capacity)}" maxlength="6" autocomplete="off" />
                        <div id="wrapper-pump_capacity"></div>
                    </div>

                    <div class="col-md-6 pt-3">
                        <h6>Prof. Superior Bomba MD <i class="text-muted">pies</i></h6>
                        <input type="text" class="form-control form-control-sm" placeholder="Prof. Superior Bomba MD" id="deeptop_pump_md" name="deeptop_pump_md" value="${m.setNullToValue(data[0].deeptop_pump_md)}" maxlength="6" autocomplete="off" />
                        <div id="wrapper-deeptop_pump_md"></div>
                    </div>

                    <div class="col-md-6 pt-3">
                        <h6>Prof. Inferior Bomba MD <i class="text-muted">pies</i></h6>
                        <input type="text" class="form-control form-control-sm" placeholder="Prof. Inferior Bomba MD" id="deepbottom_pump_md" name="deepbottom_pump_md" value="${m.setNullToValue(data[0].deepbottom_pump_md)}" maxlength="6" autocomplete="off" />
                        <div id="wrapper-deepbottom_pump_md"></div>
                    </div>

                    <div class="col-md-12 pt-4"><hr></div>

                    <div class="col-md-6 pt-3">
                        <h6>Inyección de Diluente</h6>
                        <input type="text" class="form-control form-control-sm" placeholder="Inyección de Diluente" id="inyected_diluent" name="inyected_diluent" value="${data[0].inyected_diluent}" style="text-transform:uppercase" maxlength="20" />
                        <div id="wrapper-inyected_diluent"></div>
                    </div>

                    <div class="col-md-6 pt-3">
                        <h6>Medidas desde</h6>
                        <input type="text" class="form-control form-control-sm" placeholder="Medidas desde" id="measure_from" name="measure_from" value="${data[0].measure_from}" style="text-transform:uppercase" maxlength="20" />
                        <div id="wrapper-measure_from"></div>
                    </div>

                    <div class="col-md-12 pt-4"><hr></div>

                    <div class="col-md-8 pt-4">
                        <h6>Observaciones <i class="text-muted">120 caracteres</i></h6>
                        <textarea class="form-control form-control-sm" style="max-height:80px" placeholder="Observaciones" id="observations" name="observations" maxlength="120">${data[0].observations}</textarea>
                    </div>

                    <div class="col-md-4 pt-4">
                        <h6>Referencias</h6>
                        <input type="text" class="form-control form-control-sm" placeholder="Referencias" id="references" name="references" value="${data[0].references}" style="text-transform:uppercase" maxlength="20" autocomplete="off" />
                    </div>
                </div>
            </div>
            <input type="hidden" id="identity" value="${data[0].historical_id}">
            <input type="hidden" id="index" value="${data[0].index}">
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-primary" id="btn-update">Guardar cambios</button>
            </div>
        </div>
        </div>`);
    
        var input = ['sensor_diameter', 'deeptop_sensor_md', 'deepbottom_sensor_md', 'pump_diameter', 'pump_capacity', 'deeptop_pump_md', 'deepbottom_pump_md'];
        
        for (let i = 0; i < input.length; i++) $(`#${input[i]}`).numeric({ negative: false });

        $(`#btn-update`).click(function () {
            alertify.confirm('Pregunta', '¿Está seguro que desea guardar los cambios realizados en este registro?', function(e) {
                if (e) hst.updating(table, tr);
            }, function(){}).set({'movable': false, 'moveBounded': false, 'modal': false, 'labels': {ok: 'Sí', cancel: 'No'}});
        });
        this.loadReservoirs(this.cleanName(data[0].reservoir));
    }
    
    loadReservoirs(value) {
        const ajax = new Ajax(`${globalConfig.appUrl}/reservoirManagement/loadReservoirs`, {
            headers: {accept: 'application/json'},
            method: 'POST'
        });
        
        ajax.result().then(response => {
            var size = Object.keys(response.data).length, content = ``, aux = ``;

            for(var i=0; i<size; i++) {
                if(value != response.data[i].reservoir_name) {
                    content += `<option value="${response.data[i].id}">${response.data[i].reservoir_name}</option>`;
                } else {
                    aux += `<option value="${response.data[i].id}">${response.data[i].reservoir_name}</option>`;
                }
            }

            aux += content;

            $('#reservoir_name').html(aux);

            console.log(value);

        }).catch(error => { console.log(error) });
        return false;
    }

    cleanName(value) {
        return value != null || value != '' ? value.replaceAll('&nbsp;', ' ') : value;
    }

    setHeader(value) {
        if (value === 'type_work_i') return 'Tipo&nbsp;de&nbsp;Trabajo';
        else if (value === 'type_work_ii') return 'Tipo&nbsp;de&nbsp;Trabajo&nbsp;II';
        else if (value === 'rig') return 'Equipo&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
        else if (value === 'sensor_model') return 'Sensor&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <h6 class="text-muted">modelo</h6>';
        else if (value === 'sensor_brand') return 'Sensor <h6 class="text-muted">marca</h6>';
        else if (value === 'sensor_diameter') return 'Sensor <h6 class="text-muted">plg&nbsp;diámetro</h6>';
        else if (value === 'sensor_company') return 'Sensor <h6 class="text-muted">compañía</h6>';
        else if (value === 'deeptop_sensor_md') return 'Prof.&nbsp;Sup.&nbsp;Sensor&nbsp;(MD) <h6 class="text-muted">pies</h6>';
        else if (value === 'deepbottom_sensor_md') return 'Prof.&nbsp;Inf.&nbsp;Sensor&nbsp;(MD) <h6 class="text-muted">pies</h6>';
        else if (value === 'pump_model') return 'Bomba&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<h6 class="text-muted">modelo</h6>';
        else if (value === 'pump_diameter') return 'Bomba&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<h6 class="text-muted">plg&nbsp;diámetro</h6>';
        else if (value === 'pump_type') return 'Método&nbsp;bombeo <h6 class="text-muted"></h6>';
        else if (value === 'pump_capacity') return 'Capacidad&nbsp;Bomba <h6 class="text-muted">BPD/100 RPM</h6>';
        else if (value === 'pump_company') return 'Bomba <h6 class="text-muted">compañía</h6>';
        else if (value === 'deeptop_pump_md') return 'Prof.&nbsp;Sup.&nbsp;Bomba&nbsp;(MD) <h6 class="text-muted">pies</h6>';
        else if (value === 'deepbottom_pump_md') return 'Prof.&nbsp;Inf.&nbsp;Bomba&nbsp;(MD) <h6 class="text-muted">pies</h6>';
        else if (value === 'inyected_diluent') return 'Inyección&nbsp;diluente';
        else if (value === 'measure_from') return 'Medidas&nbsp;desde';
        else if (value === 'observations') return 'Observaciones';
        else if (value === 'references') return 'Referencias';
    }
}

window.onload = () => {
    m = new Method();
    hst = new ManagementHistorical();
}