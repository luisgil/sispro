class ManagementCondition {
    constructor() {
        this.searchByFilter = this.searchByFilter.bind(this);
        this.setHeader = this.setHeader.bind(this);
        this.inputNames = ['company', 'reservoir', 'sand', 'test', 'temperature', 'duration', 'mobility', 'deep_md', 'deep_tvd', 'pressbefore', 'pressafter', 'pressformation', 'observations'];
    }
    
    searchByFilter() {

        var size = m.getColumnsFromDb().length, values = m.getColumnsFromDb(), preset = ['conditions.id as id', 'well_name', 'pt_date', 'company', 'macolla_name', 'location_name', 'reservoir'];

        for (var i = 0; i <= 6; i++) values[size + i] = preset[i];

        var typeSearch, reservoirId = [], collection = [];
        
        var whatIDo = document.querySelector(`#dual-list`).style.display == 'none';

        if (!whatIDo) {
            typeSearch = 1;
            var sizeIs = ($(`#location_well`).val()).length, store = $(`#location_well`).val();
            if (sizeIs > 1) for (var i = 0; i < sizeIs; i++) collection[i] = store[i];
            else collection = store[0];
        } else {
            $('input[name="option[]"]:checked').each(function(i) { reservoirId[i] = $(this).val(); });
            typeSearch = 2;
            var sizeIs = ($(`#macolla_list`).val()).length, store = $(`#macolla_list`).val();
            if (sizeIs > 1) for (var i = 0; i < sizeIs; i++) collection[i] = store[i];
            else collection = store[0];
        }

            const ajax = new Ajax(`${globalConfig.appUrl}/searchCondition`, {
                headers: {
                    accept: 'application/json',
                },
                method: 'POST',
                body: {
                    items: values,
                    data: collection,
                    reservoir_id: reservoirId != null ? reservoirId : null,
                    type: typeSearch
                }
            });

            m.setLoadingMessageBefore();

            ajax.result().then(response => {

                var size = Object.keys(response.data).length;

                m.setLoadingMessageAfter();
                m.showMessageDialog(size);

                var options = ['sand', 'test', 'deep_md', 'deep_tvd', 'temperature', 'duration', 'pressbefore', 'pressafter', 'pressformation' , 'mobility', 'observations'];

                var selected = m.getColumnsToHead(options, Object.keys(response.data[0]));
                var header = m.setColumn(response.type, response.info)[0];

                let detail = (response.type == 1) ? header : m.selectedItems(response.total);
                var titleText = detail == 'SIN MACOLLA' ? detail : `Macolla ${detail}`;

                m.setHtmlTable(1, "PyT Original", `${titleText}`);
                m.setHtmlTable(2);

                let headerSelected = `<th class="text-muted">#</th><th class="text-muted">Macolla</th><th class='text-muted'>Pozo</th><th class='text-muted'>Localización</th><th class='text-muted'>Yacimiento</th><th class='text-muted'>Compañía</th><th class='text-muted'>Fecha</th>`;

                for (var i=0; i<selected.length; i++) headerSelected += `<th>${this.setHeader(selected[i])}</th>`;

                headerSelected += `<th class='text-muted'>Acción</th>`;

                $(`#header`).html(headerSelected);

                var table = m.newDateTable(response.results, size);

                document.getElementById('imhere').focus();

                $(`.dts_label, .dt-buttons, .dataTables_filter`).remove();

                $(`#datatable`).on('click', '#btn-delete', function() {
                    
                    let $tr = $(this).closest('tr'), $button = $(this);
                    const id = $(this).val();

                    alertify.confirm("<i class='fa fa-exclamation-triangle text-danger'></i> Advertencia", "Se eliminará este registro permanentemente. ¿Está seguro que desea continuar?", function() {},
                        function(e) {
                            if (e) {
                                console.log(`${globalConfig.appUrl}/deleteCondition/${id}`);
                                $button.attr('disabled', true);
                                const ajax = new Ajax(`${globalConfig.appUrl}/deleteCondition/${id}`, {
                                    headers: { accept: 'application/json' },
                                    method: 'POST'
                                });
                                ajax.result().then(response => {table.row($tr).remove().draw(false);alertify.warning("El registro ha sido eliminado");}).catch(error => { console.log(error); });
                            }
                        }).set({'movable': false, 'modal': false, 'moveBounded': false, 'labels': {ok: 'No', cancel: 'Sí'}});
                });

                $(`#datatable`).on('click', '#btn-edit', function() {
                    var $tr = $(this).closest('tr'), id = $(this).val(), $value = table.row($tr).data();
                    cnd.modify($value, table, $tr, id);
                });
            }).catch(error => { console.log(error) });
        
        return false;
    }
    
    updating(table, tr) {
        
        m.reloadView();

        const id = $('#identity').val();
        const index = $('#index').val();

        const ajax = new Ajax(`${globalConfig.appUrl}/updateCondition/${id}`, {
            headers: {
                accept: 'application/json',
            },
            method: 'POST',
            body: {
                reservoir: m.trimString('reservoir_name'),
                sand: m.trimString('sand'),
                company: m.trimString('company'),
                test: m.trimString('test'),
                deep_md: m.convertToDecimal(document.querySelector(`#deep_md`).value),
                deep_tvd: m.convertToDecimal(document.querySelector(`#deep_tvd`).value),
                temperature: m.convertToDecimal(document.querySelector(`#temperature`).value),
                duration: m.convertToDecimal(document.querySelector(`#duration`).value),
                pressbefore: m.convertToDecimal(document.querySelector(`#pressbefore`).value),
                pressafter: m.convertToDecimal(document.querySelector(`#pressafter`).value),
                pressformation: m.convertToDecimal(document.querySelector(`#pressformation`).value),
                mobility: m.convertToDecimal(document.querySelector(`#mobility`).value),
                observations: m.trimNotRequiredString('observations')
            }
        });

        $("#btn-update").attr("disabled", true);
        
        ajax.result().then(response => {

            var data = [];

            response.data[0][0] = index;

            table.row(table.row(tr).invalidate().data(response.data[0]).draw(false));
            
            alertify.success(response.message);

            $(`#modal-xl`).modal('toggle');

        }).catch(error => {
            if (error.status === 400) {
                m.showInputCorrects(error.errors, this.inputNames);
                m.showInputErrors(error.errors, this.inputNames);
                $("#btn-update").attr("disabled", false);
            } else console.log(error);
        });
        return false;
    }

    modify(array, table, tr, id) {
        
        var data = [];

        data.push({
            reservoir: array[4],
            sand: array[7],
            company: array[5],
            test: array[8],
            deep_md: array[9],
            deep_tvd: array[10],
            temperature: array[11],
            duration: array[12],
            pressbefore: array[13],
            pressafter: array[14],
            pressformation: array[15],
            mobility: array[16],
            observations: array[17],
            condition_id: id, 
            index: array[0]
        });

        $(`#modal-xl`).html(`<div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title"><i class="fa fa-edit"></i> PyT Original</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-3 pt-4">
                        <h6>Compañía</h6>
                        <input type="text" class="form-control form-control-sm" placeholder="Compañía" id="company" name="company" value="${data[0].company}" style="text-transform:uppercase" maxlength="20" autocomplete="off" />
                        <div id="wrapper-company"></div>
                    </div>
                    <div class="col-md-3 pt-4">
                        <h6>Yacimiento</h6>
                        <select class="form-control form-control-sm" id="reservoir_name" name="reservoir"></select>
                        <div id="wrapper-reservoir"></div>
                    </div>
                    <div class="col-md-3 pt-4">
                        <h6>Arena</h6>
                        <input type="text" class="form-control form-control-sm" placeholder="Arena" id="sand" name="sand" value="${data[0].sand}" style="text-transform:uppercase" maxlength="20" autocomplete="off" />
                        <div id="wrapper-sand"></div>
                    </div>
                    <div class="col-md-3 pt-4">
                        <h6>Prueba </h6>
                        <input type="text" class="form-control form-control-sm" placeholder="Prueba" id="test" name="test" value="${data[0].test}" style="text-transform:uppercase" maxlength="10" autocomplete="off" />
                        <div id="wrapper-test"></div>
                    </div>
                    <div class="col-md-3 pt-4">
                        <h6>Temperatura <i class="text-muted">ºF</i></h6>
                        <input type="text" class="form-control form-control-sm" placeholder="Temperatura" id="temperature" name="temperature" value="${m.setNullToValue(data[0].temperature)}" maxlength="6" autocomplete="off" />
                        <div id="wrapper-temperature"></div>
                    </div>
                    <div class="col-md-3 pt-4">
                        <h6>Duración <i class="text-muted">minutos</i></h6>
                        <input type="text" class="form-control form-control-sm" placeholder="Duración" id="duration" name="duration" value="${m.setNullToValue(data[0].duration)}" maxlength="4" autocomplete="off" />
                        <div id="wrapper-duration"></div>
                    </div>
                    <div class="col-md-3 pt-4">
                        <h6>Movilidad <i class="text-muted">mD/cP</i></h6>
                        <input type="text" class="form-control form-control-sm" placeholder="Movilidad" id="mobility" name="mobility" value="${m.setNullToValue(data[0].mobility)}" maxlength="6" autocomplete="off" />
                        <div id="wrapper-mobility"></div>
                    </div>

                    <div class="col-md-12 pt-4"><h5 class="text-info">Profundidad</h5></div>

                    <div class="col-md-3 pt-3">
                        <h6>MD <i class="text-muted">pies</i></h6>
                        <input type="text" class="form-control form-control-sm" placeholder="Profundidad MD" id="deep_md" name="deep_md" value="${m.setNullToValue(data[0].deep_md)}" maxlength="6" autocomplete="off" />
                        <div id="wrapper-deep_md"></div>
                    </div>

                    <div class="col-md-3 pt-3">
                        <h6>TVD <i class="text-muted">pies</i></h6>
                        <input type="text" class="form-control form-control-sm" placeholder="Profundidad TVD" id="deep_tvd" name="deep_tvd" value="${m.setNullToValue(data[0].deep_tvd)}" maxlength="6" autocomplete="off" />
                        <div id="wrapper-deep_tvd"></div>
                    </div>

                    <div class="col-md-12 pt-4"><h5 class="text-info">Presión</h5></div>

                    <div class="col-md-3 pt-3">
                        <h6>Presión Hid. Antes <i class="text-muted">lppc</i></h6>  
                        <input type="text" class="form-control form-control-sm" placeholder="Presión Hid. Antes" id="pressbefore" name="pressbefore" value="${m.setNullToValue(data[0].pressbefore)}" maxlength="6" autocomplete="off" />
                        <div id="wrapper-pressbefore"></div>
                    </div>

                    <div class="col-md-3 pt-3">
                        <h6>Presión Hid. Después <i class="text-muted">lppc</i></h6>
                        <input type="text" class="form-control form-control-sm" placeholder="Presión Hid. Después" id="pressafter" name="pressafter" value="${m.setNullToValue(data[0].pressafter)}" maxlength="6" autocomplete="off" />
                        <div id="wrapper-pressafter"></div>
                    </div>
                    <div class="col-md-3 pt-3">
                        <h6>Presión de Formación <i class="text-muted">lppc</i></h6>
                        <input type="text" class="form-control form-control-sm" placeholder="Presión de Formación" id="pressformation" name="pressformation" value="${m.setNullToValue(data[0].pressformation)}" maxlength="6" autocomplete="off" />
                        <div id="wrapper-pressformation"></div>
                    </div>

                    <div class="col-md-12 pt-4"><hr></div>

                    <div class="col-md-12 pt-4">
                        <h6>Observaciones <i class="text-muted">120 caracteres</i></h6>
                        <textarea class="form-control form-control-sm" style="max-height:80px" placeholder="Observaciones" id="observations" name="observations" maxlength="120">${data[0].observations}</textarea>
                    </div>
                </div>
            </div>

            <input type="hidden" id="identity" value="${data[0].condition_id}">
            <input type="hidden" id="index" value="${data[0].index}">

            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-primary" id="btn-update">Guardar cambios</button>
            </div>
        </div>
        </div>`);

        var input = ['deep_md', 'deep_tvd', 'temperature', 'duration', 'pressafter', 'pressbefore', 'pressformation', 'mobility'];
        
        for (let i = 0; i < input.length; i++) $(`#${input[i]}`).numeric({ negative: false });

        $(`#btn-update`).click(function () {
            alertify.confirm('Pregunta', '¿Está seguro que desea guardar los cambios realizados en este registro?', function(e) {
                if (e) cnd.updating(table, tr);
            }, function(){}).set({'movable': false, 'moveBounded': false, 'modal': false, 'labels': {ok: 'Sí', cancel: 'No'}});
        });

        this.loadReservoirs(this.cleanName(data[0].reservoir));
    }

    loadReservoirs(value) {
        const ajax = new Ajax(`${globalConfig.appUrl}/reservoirManagement/loadReservoirs`, {
            headers: {accept: 'application/json'},
            method: 'POST'
        });
        
        ajax.result().then(response => {
            var size = Object.keys(response.data).length, content = ``, aux = ``;

            for(var i=0; i<size; i++) {
                if(value != response.data[i].reservoir_name) {
                    content += `<option value="${response.data[i].id}">${response.data[i].reservoir_name}</option>`;
                } else {
                    aux += `<option value="${response.data[i].id}">${response.data[i].reservoir_name}</option>`;
                }
            }

            aux += content;

            $('#reservoir_name').html(aux);

            console.log(value);

        }).catch(error => { console.log(error) });
        return false;
    }

    cleanName(value) {
        return value != null || value != '' ? value.replaceAll('&nbsp;', ' ') : value;
    }

    setHeader(value) {
        if (value === 'test') return 'Test';
        else if (value === 'deep_md') return 'Profundidad&nbsp;MD <h6 class="text-muted">pies</h6>';
        else if (value === 'deep_tvd') return 'Profundidad&nbsp;TVD <h6 class="text-muted">pies</h6>';
        else if (value === 'temperature') return 'Temperatura <h6 class="text-muted">ºF</h6>';
        else if (value === 'duration') return 'Tiempo <h6 class="text-muted">minutos</h6>';
        else if (value === 'sand') return 'Arena';
        else if (value === 'pressbefore') return 'Presión&nbsp;Hid.&nbsp;Antes <h6 class="text-muted">lppc</h6>';
        else if (value === 'pressafter') return 'Presión&nbsp;Hid.&nbsp;Después <h6 class="text-muted">lppc</h6>';
        else if (value === 'pressformation') return 'Presión&nbsp;Formación <h6 class="text-muted">lppc</h6>';
        else if (value === 'mobility') return 'Movilidad <h6 class="text-muted">mD/cP</h6>';
        else if (value === 'observations') return 'Observaciones';
    }
}

window.onload = () => {
    m = new Method();
    cnd = new ManagementCondition();
}