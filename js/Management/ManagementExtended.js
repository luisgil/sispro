﻿class ManagementExtended {
    constructor() {
      this.searchByFilter = this.searchByFilter.bind(this);
      this.setHeader = this.setHeader.bind(this);
      this.inputNames = ['company', 'test', 'hour_start', 'hour_end', 'line_pressure', 'line_temperature', 'water_rate_prod', 'gas_rate_prod', 'total_barrels', 'neat_rate', 'gvf', 'inyected_diluent_rate'];
    }

    searchByFilter() {
        
        var range, typeSearch, reservoirId = [], collection = [];
        
        range = $("#filter-date").val() == 1 ?  m.getDatesRange($(`#date-pick`).val()) : m.getDatesRange(`${$(`#from`).val()}|${$(`#until`).val()}`);

        var whatIDo = document.querySelector(`#dual-list`).style.display == 'none';
        
        if (!whatIDo) {
            typeSearch = 1;
            var sizeIs = ($(`#location_well`).val()).length, store = $(`#location_well`).val();
            if (sizeIs > 1) for (var i = 0; i < sizeIs; i++) collection[i] = store[i];
            else collection = store[0];
        }

        var from = moment(range[0]), until = moment(range[1]), isFrom = m.validateDate(from, 'msgFrom'), isUntil = m.validateDate(until, 'msgUntil');

        if ((from.isValid() && isFrom) && (until.isValid() && isUntil)) {

            $(`#msgFrom, #msgUntil`).html(``);

            const ajax = new Ajax(`${globalConfig.appUrl}/searchExtended`, {
                headers: {
                    accept: 'application/json',
                },
                method: 'POST',
                body: {
                    data: collection,
                    macolla_id: parseInt($(`#macolla`).val()),
                    reservoir_id: reservoirId != null ? reservoirId : null,
                    date_from: range[0],
                    date_to: range[1],
                    type: typeSearch
                }
            });

            m.setLoadingMessageBefore();

            ajax.result().then(response => {

                var size = Object.keys(response.data).length;

                m.setLoadingMessageAfter();
                m.showMessageDialog(size);

                var options = ['line_pressure', 'line_temperature', 'water_rate_prod', 'gas_rate_prod', 'total_barrels', 'neat_rate', 'gvf', 'inyected_diluent_rate'];

                var selected = m.getColumnsToHead(options, Object.keys(response.data[0]));
                var header = m.setColumn(response.type, response.info)[0];

                let detail = (response.type == 1) ? header : m.selectedItems(response.total);
                var titleText = detail == 'SIN MACOLLA' ? detail : `Macolla ${detail}`;

                m.setHtmlTable(1, "Prueba Extendida", `${titleText}`);
                m.setHtmlTable(2);

                let headerSelected = `<th class='text-muted'>#</th><th class='text-muted'>Fecha</th><th class="text-muted">Macolla</th><th class='text-muted'>Localización </th><th class='text-muted'>Pozo </th><th class='text-muted'>Yacimiento</th><th class='text-muted'>Compañía</th><th class='text-muted'>Prueba</th><th class='text-muted'>Hora&nbsp;inicio</th><th class='text-muted'>Hora&nbsp;final</th><th class='text-muted'>Duración</th>`;

                for (var i=0; i<selected.length; i++) headerSelected += `<th>${this.setHeader(selected[i])}</th>`;

                headerSelected += `<th class='text-muted'>Acción</th>`;

                $(`#header`).html(headerSelected);

                var table = m.newDateTable(response.results, size);

                document.getElementById('imhere').focus();

                $(`.dts_label, .dt-buttons, .dataTables_filter`).remove();

                $(`#datatable`).on('click', '#btn-delete', function() {
                    let $tr = $(this).closest('tr'), $button = $(this);
                    const id = $(this).val();
                    alertify.confirm("<i class='fa fa-exclamation-triangle text-danger'></i> Advertencia", "Se eliminará este registro permanentemente. ¿Está seguro que desea continuar?", function() {},
                        function(e) {
                            if (e) {
                                $button.attr('disabled', true);
                                const ajax = new Ajax(`${globalConfig.appUrl}/deleteExtended/${id}`, {
                                    headers: { accept: 'application/json' },
                                    method: 'POST'
                                });
                                ajax.result().then(response => {table.row($tr).remove().draw(false);alertify.warning("El registro ha sido eliminado");}).catch(error => { console.log(error); });
                            }
                        }).set({'movable': false, 'modal': false, 'moveBounded': false, 'labels': {ok: 'No', cancel: 'Sí'}});
                });

                $(`#datatable`).on('click', '#btn-edit', function() {
                    var $tr = $(this).closest('tr'), id = $(this).val(), $value = table.row($tr).data();
                    ext.modify($value, table, $tr, id);
                });
            }).catch(error => { console.log(error) });
        }
        return false;
    }

    updating(table, tr) {
		
		m.reloadView();

		const id = $('#identity').val();
        const index = $('#index').val();

		const ajax = new Ajax(`${globalConfig.appUrl}/updateExtended/${id}`, {
			headers: {
				accept: 'application/json',
			},
			method: 'POST',
			body: {
                company: m.trimString('company'),
                test: m.trimString('test'),
                hour_start: $(`#hour_start`).val(),
                hour_end: $(`#hour_end`).val(),
                line_pressure: m.convertToDecimal(document.querySelector(`#line_pressure`).value),
                line_temperature: m.convertToDecimal(document.querySelector(`#line_temperature`).value),
                water_rate_prod: m.convertToDecimal(document.querySelector(`#water_rate_prod`).value),
                gas_rate_prod: m.convertToDecimal(document.querySelector(`#gas_rate_prod`).value),
                total_barrels: m.convertToDecimal(document.querySelector(`#total_barrels`).value),
                neat_rate: m.convertToDecimal(document.querySelector(`#neat_rate`).value),
                gvf: m.convertToDecimal(document.querySelector(`#gvf`).value),
                inyected_diluent_rate: m.convertToDecimal(document.querySelector(`#inyected_diluent_rate`).value)
			}
		});

		$("#btn-update").attr("disabled", true);
		
		ajax.result().then(response => {

            var data = [];

            response.data[0][0] = index;

            table.row(table.row(tr).invalidate().data(response.data[0]).draw(false));
            
            alertify.success(response.message);

			$(`#modal-xl`).modal('toggle');

		}).catch(error => {
			if (error.status === 400) {
				m.showInputCorrects(error.errors, this.inputNames);
				m.showInputErrors(error.errors, this.inputNames);
				$("#btn-update").attr("disabled", false);
			} else console.log(error);
		});
		return false;
	}

    modify(array, table, tr, id) {
        
        var data = [];
        
        data.push({
            test: array[7],
            hour_start: array[8],
            hour_end: array[9],
            company: array[6],
            line_pressure: array[11],
            line_temperature: array[12],
            gas_rate_prod: array[14],
            water_rate_prod: array[13],
            neat_rate: array[16],
            inyected_diluent_rate: array[18],
            total_barrels: array[15],
            gvf: array[17],
            extended_id: id, 
            index: array[0]
        });
        
        $(`#modal-xl`).html(`<div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title"><i class="fa fa-edit"></i> Prueba Extendida</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-4 pt-3">
                        <h6>Prueba</h6>
                        <input type="text" value="${data[0].test}" class="form-control form-control-sm" placeholder="Prueba" id="test" maxlength="20" style="text-transform:uppercase" autocomplete="off" />
                        <div id="wrapper-test"></div>
                    </div>
                    <div class="col-md-4 col-lg-2 pt-3">
                        <h6>Hora inicial</h6>
                        <input type="time" value="${data[0].hour_start}" class="form-control form-control-sm" placeholder="Hora inicial" id="hour_start" />
                        <div id="wrapper-hour_start"></div>
                    </div>
                    <div class="col-md-4 col-lg-2 pt-3">
                        <h6>Hora final</h6>
                        <input type="time" value="${data[0].hour_end}" class="form-control form-control-sm" placeholder="Hora final" id="hour_end" />
                        <div id="wrapper-hour_end"></div>
                    </div>
                    <div class="col-md-4 pt-3">
                        <h6>Compañía</h6>
                        <input type="text" value="${data[0].company}" class="form-control form-control-sm" placeholder="Compañía" id="company" maxlength="20" style="text-transform:uppercase" autocomplete="off" />
                        <div id="wrapper-company"></div>
                    </div>
                    
                    <div class="col-md-12 pt-4"><h5 class="text-info">Línea de Producción</h5></div>
                    
                    <div class="col-md-6 pt-3">
                        <h6>Presión Línea Producción <i class="text-muted">lppc</i></h6>
                        <input type="text" value="${m.setNullToValue(data[0].line_pressure)}" class="form-control form-control-sm" placeholder="Presión" id="line_pressure" maxlength="6" autocomplete="off" />
                        <div id="wrapper-line_pressure"></div>
                    </div>
                    
                    <div class="col-md-6 pt-3">
                        <h6>Temp. Línea Producción <i class="text-muted">ºF</i></h6>
                        <input type="text" value="${m.setNullToValue(data[0].line_temperature)}" class="form-control form-control-sm" placeholder="Temperatura" id="line_temperature" maxlength="6" autocomplete="off" />
                        <div id="wrapper-line_temperature"></div>
                    </div>
                    
                    <div class="col-md-12 pt-4"><h5 class="text-info">Tasas de Producción</h5></div>
                    
                    <div class="col-md-3 col-lg-3 pt-3">
                        <h6>Prod. de Gas <i class="text-muted">MPCND</i></h6>
                        <input type="text" value="${m.setNullToValue(data[0].gas_rate_prod)}" class="form-control form-control-sm" placeholder="Producción de Gas" id="gas_rate_prod" maxlength="6" autocomplete="off" />
                        <div id="wrapper-gas_rate_prod"></div>
                    </div>
                    
                    <div class="col-md-3 col-lg-3 pt-3">
                        <h6>Prod. de Agua <i class="text-muted">bpd</i></h6>
                        <input type="text" value="${m.setNullToValue(data[0].water_rate_prod)}" class="form-control form-control-sm" placeholder="Producción de Agua" id="water_rate_prod" maxlength="6" autocomplete="off" />
                        <div id="wrapper-water_rate_prod"></div>
                    </div>
                    
                    <div class="col-md-3 col-lg-3 pt-3">
                        <h6>Prod. de Crudo <i class="text-muted">bpd</i></h6>
                        <input type="text" value="${m.setNullToValue(data[0].neat_rate)}" class="form-control form-control-sm" placeholder="Producción de Crudo" id="neat_rate" maxlength="6" autocomplete="off" />
                        <div id="wrapper-neat_rate"></div>
                    </div>
                    
                    <div class="col-md-3 col-lg-3 pt-3">
                        <h6>Inyec. Diluente <i class="text-muted">bpd</i></h6>
                        <input type="text" value="${m.setNullToValue(data[0].inyected_diluent_rate)}" class="form-control form-control-sm" placeholder="Diluente inyectado" id="inyected_diluent_rate" maxlength="6" autocomplete="off" />
                        <div id="wrapper-inyected_diluent_rate"></div>
                    </div>
                    
                    <div class="col-md-12 pt-4"><hr></div>
                    
                    <div class="col-md-6 col-lg-6 pt-4">
                        <h6>Barriles Totales <i class="text-muted">bpd</i></h6>
                        <input type="text" value="${m.setNullToValue(data[0].total_barrels)}" class="form-control form-control-sm" placeholder="Barriles Totales" id="total_barrels" maxlength="6" autocomplete="off" />
                        <div id="wrapper-total_barrels"></div>
                    </div>
                    
                    <div class="col-md-6 col-lg-6 pt-4">
                        <h6>Fracción Volumen Gas <i class="text-muted">porcentaje (%)</i></h6>
                        <input type="text" value="${m.setNullToValue(m.setFormat((m.convertToNumber(data[0].gvf) * 100), 2))}" class="form-control form-control-sm" placeholder="FVG" id="gvf" maxlength="6" autocomplete="off" />
                        <div id="wrapper-gvf"></div>
                    </div>
                </div>
            </div>
            <input type="hidden" id="identity" value="${data[0].extended_id}">
            <input type="hidden" id="index" value="${data[0].index}">
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-primary" id="btn-update">Guardar cambios</button>
            </div>
        </div>
        </div>`);
    
        var input = ['line_pressure', 'line_temperature', 'water_rate_prod', 'gas_rate_prod', 'total_barrels', 'neat_rate', 'gvf', 'inyected_diluent_rate'];
        
        for (let i = 0; i < input.length; i++) $(`#${input[i]}`).numeric({ negative: false });

        $(`#btn-update`).click(function () {
            alertify.confirm('Pregunta', '¿Está seguro que desea guardar los cambios realizados en este registro?', function(e) {
                if (e) ext.updating(table, tr);
            }, function(){}).set({'movable': false, 'moveBounded': false, 'modal': false, 'labels': {ok: 'Sí', cancel: 'No'}});
        });
    }
    
    setHeader(value) {
        if (value === 'line_pressure') return 'P<sub>línea</sub> <h6 class="text-muted">lppc</h6>';
        else if (value === 'line_temperature') return'T<sub>línea</sub> <h6 class="text-muted">ºF</h6>';
        else if(value === 'water_rate_prod') return 'q<sub>w</sub> <h6 class="text-muted">BPD</h6>';
        else if(value === 'gas_rate_prod') return 'q<sub>g</sub> <h6 class="text-muted">MPCND</h6>';
        else if (value === 'total_barrels') return 'Barriles&nbsp;totales <h6 class="text-muted">BPPD</h6>';
        else if (value === 'neat_rate') return 'q<sub>n</sub> <h6 class="text-muted">BPPD</h6>';
        else if (value === 'gvf') return 'FVG';
        else if (value === 'inyected_diluent_rate') return 'q<sub>d</sub> <h6 class="text-muted">BDPD</h6>';
        else if (value === 'macolla_name') return 'Macolla';
    }
}

window.onload = () => {
    m = new Method();
    ext = new ManagementExtended();
}