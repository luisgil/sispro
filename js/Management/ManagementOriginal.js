class ManagementOriginal {
    constructor() {
        this.searchByFilter = this.searchByFilter.bind(this);
        this.setHeader = this.setHeader.bind(this);
        this.inputNames = ['reservoir', 'type_test', 'sensor_model', 'pressure_gradient', 'deepdatum_mc_md', 'deepdatum_mc_tvd', 'press_datum_macolla', 'temp_datum_macolla', 'temperature_gradient'];
    }

    searchByFilter() {
        
        var size = m.getColumnsFromDb().length, values = m.getColumnsFromDb(), preset = ['well_name', 'pressure_date', 'macolla_name', 'location_name', 'reservoir'];

        for (var i = 0; i <= 4; i++) values[size + i] = preset[i];
        
        var range, typeSearch, reservoirId = [], collection = [], whatIDo = document.querySelector(`#dual-list`).style.display == 'none';
        
        /*By wells or by macollas ?*/
        if (!whatIDo) {
            typeSearch = 1;
            var sizeIs = ($(`#location_well`).val()).length, store = $(`#location_well`).val();
            if (sizeIs > 1) {
                for (var i = 0; i < sizeIs; i++) collection[i] = store[i];
            } else collection = store[0];
        } else {
            /*This is for show all macollas and reservoirs!*/
            $('input[name="option[]"]:checked').each(function(i) {
                reservoirId[i] = $(this).val();
            });
            typeSearch = 2;
            var sizeIs = ($(`#macolla_list`).val()).length, store = $(`#macolla_list`).val();
            if (sizeIs > 1) {
                for (var i = 0; i < sizeIs; i++) collection[i] = store[i];
            } else collection = store[0];
        }
        
        const ajax = new Ajax(`${globalConfig.appUrl}/searchOriginal`, {
            headers: {
                accept: 'application/json',
            },
            method: 'POST',
            body: {
                items: values,
                data: collection,
                macolla_id: parseInt($(`#macolla`).val()),
                reservoir_id: reservoirId != null ? reservoirId : null,
                type: typeSearch
            }
        });

        m.setLoadingMessageBefore();
        
        ajax.result().then(response => {

            var options = ['type_test', 'sensor_model', 'pressure_gradient', 'deepdatum_mc_md', 'deepdatum_mc_tvd', 'press_datum_macolla', 'drill', 'temp_datum_macolla', 'temperature_gradient'];
            var size = Object.keys(response.data).length;
            
            m.setLoadingMessageAfter();
            m.showMessageDialog(size);
            
            if (size > 0) {

                var selected = m.getColumnsToHead(options, Object.keys(response.data[0]));
                var header = m.setColumn(response.type, response.info)[0], column = m.setColumn(response.type, response.info)[1];

                let detail = (response.type == 1) ? header : m.selectedItems(response.total);
                var titleText = detail == 'SIN MACOLLA' ? detail : `Macolla ${detail}`;

                m.setHtmlTable(1, "Presión Original PMVM", `${titleText}`);
                m.setHtmlTable(2);

                let headerSelected = `<th class='text-muted'>#</th>${column}<th class='text-muted'>Localización</th><th class='text-muted'>Pozo</th><th class='text-muted'>Yacimiento</th><th class='text-muted'>Fecha</th>`;

                for (var i=0; i<selected.length; i++) headerSelected += `<th>${this.setHeader(selected[i])}</th>`;

                headerSelected += `<th>Acción</th>`;

                document.querySelector(`#header`).innerHTML = headerSelected;

                var table = m.newDateTable(response.results, size);

                document.getElementById('imhere').focus();

                $(`.dts_label, .dt-buttons, .dataTables_filter`).remove();

                $(`#datatable`).on('click', '#btn-delete', function() {
                    let $tr = $(this).closest('tr'), $button = $(this);
                    const id = $(this).val();
                    alertify.confirm("<i class='fa fa-exclamation-triangle text-danger'></i> Advertencia", "Se eliminará este registro permanentemente. ¿Está seguro que desea continuar?", function() {},
                        function(e) {
                            if (e) {
                                $button.attr('disabled', true);
                                const ajax = new Ajax(`${globalConfig.appUrl}/deleteOriginal/${id}`, {
                                    headers: { accept: 'application/json' },
                                    method: 'POST'
                                });
                                ajax.result().then(response => {table.row($tr).remove().draw(false);alertify.warning("El registro ha sido eliminado");}).catch(error => { console.log(error); });
                            }
                        }).set({'movable': false, 'modal': false, 'moveBounded': false, 'labels': {ok: 'No', cancel: 'Sí'}});
                });        

                document.querySelector('#imhere').focus();

                $('.dataTables_filter').remove();

                $(`#datatable`).on('click', '#btn-edit', function() {
                    var $tr = $(this).closest('tr'), id = $(this).val(), $value = table.row($tr).data();
                    org.modify($value, table, $tr, id);
                });

            } else document.querySelector(`#table-content`).innerHTML = ``;
        
        }).catch(error => { console.log(error) });
        return false;
    }

    updating(table, tr) {
        
        m.reloadView();

        const id = document.querySelector(`#identity`).value;
        const index = document.querySelector(`#index`).value;
        var data = [];

        const ajax = new Ajax(`${globalConfig.appUrl}/updateOriginal/${id}`, {
            headers: { accept: 'application/json' },
            method: 'POST',
            body: {
                reservoir: m.trimString('reservoir_name'),
                type_test: m.trimNotRequiredString('type_test'),
                sensor_model: m.trimNotRequiredString('sensor_model'),
                pressure_gradient: m.convertToDecimal(document.querySelector(`#pressure_gradient`).value),
                temperature_gradient: m.convertToDecimal(document.querySelector(`#temperature_gradient`).value),
                deepdatum_mc_md: m.convertToDecimal(document.querySelector(`#deepdatum_mc_md`).value),
                deepdatum_mc_tvd: m.convertToDecimal(document.querySelector(`#deepdatum_mc_tvd`).value),
                press_datum_macolla: m.convertToDecimal(document.querySelector(`#press_datum_macolla`).value),
                temp_datum_macolla: m.convertToDecimal(document.querySelector(`#temp_datum_macolla`).value)
            }
        });

        $("#btn-update").attr("disabled", true);
        
        ajax.result().then(response => {
            
            var data = [];

            response.data[0][0] = index;

            table.row(table.row(tr).invalidate().data(response.data[0]).draw(false));
            
            alertify.success(response.message);

            $(`#modal-xl`).modal('toggle');
            $(`#modal-xl`).html(``);

        }).catch(error => {
            if (error.status === 400) {
                m.showInputCorrects(error.errors, this.inputNames);
                m.showInputErrors(error.errors, this.inputNames);
                $("#btn-update").attr("disabled", false);
            } else console.log(error);
        });
        return false;
    }


    modify(array, table, tr, id) {
        
        var data = [];

        data.push({
            reservoir: array[4],
            type_test: array[6],
            sensor_model: array[7],
            pressure_gradient: array[8],
            deepdatum_mc_md: array[9],
            deepdatum_mc_tvd: array[10],
            press_datum_macolla: array[11],
            temp_datum_macolla: array[12],
            temperature_gradient: array[13],
            original_id: id, 
            index: array[0]
        });
        
        $(`#modal-xl`).html(`<div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title"><i class="fa fa-edit"></i> Presión Original PMVM</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-3 pt-4">
                        <h6>Yacimiento</h6>
                        <select type="text" class="form-control form-control-sm" placeholder="Yacimiento" id="reservoir_name" name="reservoir"></select>
                        <div id="wrapper-reservoir"></div>
                    </div>

                    <div class="col-md-3 pt-4">
                        <h6>Tipo de Prueba</h6>
                        <input type="text" class="form-control form-control-sm" placeholder="Tipo de Prueba" id="type_test" name="type_test" maxlength="5" style="text-transform:uppercase" autocomplete="off" value="${data[0].type_test}" />
                        <div id="wrapper-type_test"></div>
                    </div>

                    <div class="col-md-3 pt-4">
                        <h6>Modelo de Sensor</h6>
                        <input type="text" class="form-control form-control-sm" placeholder="Modelo de Sensor" id="sensor_model" name="sensor_model"  maxlength="20" style="text-transform:uppercase" autocomplete="off" value="${data[0].sensor_model}" />
                        <div id="wrapper-sensor_model"></div>
                    </div>

                    <div class="col-md-12 pt-4"><h5 class="text-info">Gradiente</h5></div>

                    <div class="col-md-3 pt-3">
                        <h6>Presión <i class="text-muted">lppc/pies</i></h6>
                        <input type="text" class="form-control form-control-sm" placeholder="Presión" id="pressure_gradient" name="pressure_gradient"  maxlength="6" autocomplete="off" value="${m.setNullToValue(data[0].pressure_gradient)}" />
                        <div id="wrapper-pressure_gradient"></div>
                    </div>

                    <div class="col-md-3 pt-3">
                        <h6>Temperatura <i class="text-muted">ºF/pies</i></h6>
                        <input type="text" class="form-control form-control-sm" placeholder="Temperatura" id="temperature_gradient" name="temperature_gradient" maxlength="6" autocomplete="off" value="${m.setNullToValue(data[0].temperature_gradient)}" />
                        <div id="wrapper-temperature_gradient"></div>
                    </div>

                    <div class="col-md-12 pt-4"><h5 class="text-info">DATUM Macolla</h5></div>

                    <div class="col-md-3 pt-3">
                        <h6>Profundidad MD <i class="text-muted">pies</i></h6>
                        <input type="text" class="form-control form-control-sm" placeholder="Profundidad MD" id="deepdatum_mc_md" name="deepdatum_mc_md" maxlength="6" autocomplete="off" value="${m.setNullToValue(data[0].deepdatum_mc_md)}" />
                        <div id="wrapper-deepdatum_mc_md"></div>
                    </div>

                    <div class="col-md-3 pt-3">
                        <h6>Profundidad TVD <i class="text-muted">pies</i></h6>
                        <input type="text" class="form-control form-control-sm" placeholder="Profundidad TVD" id="deepdatum_mc_tvd" name="deepdatum_mc_tvd" maxlength="6" autocomplete="off" value="${m.setNullToValue(data[0].deepdatum_mc_tvd)}" />
                        <div id="wrapper-deepdatum_mc_tvd"></div>
                    </div>

                    <div class="col-md-3 pt-3">
                        <h6>Presión Original <i class="text-muted">lppc</i></h6>
                        <input type="text" class="form-control form-control-sm" placeholder="Presión Original" id="press_datum_macolla" name="press_datum_macolla" maxlength="6" autocomplete="off" value="${m.setNullToValue(data[0].press_datum_macolla)}"/>
                        <div id="wrapper-press_datum_macolla"></div>
                    </div>

                    <div class="col-md-3 pt-3">
                        <h6>Temperatura Original <i class="text-muted">ºF</i></h6>
                        <input type="text" class="form-control form-control-sm" placeholder="Temperatura Original" id="temp_datum_macolla" name="temp_datum_macolla" maxlength="6" autocomplete="off" value="${m.setNullToValue(data[0].temp_datum_macolla)}" />
                        <div id="wrapper-temp_datum_macolla"></div>
                    </div>
                </div>
            </div>

            <input type="hidden" id="identity" value="${data[0].original_id}">
            <input type="hidden" id="index" value="${data[0].index}">

            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-primary" id="btn-update">Guardar cambios</button>
            </div>
        </div>
        </div>`);
    
        var input = ['pressure_gradient', 'deepdatum_mc_md', 'deepdatum_mc_tvd', 'press_datum_macolla', 'temp_datum_macolla', 'temperature_gradient']
        
        for (let i = 0; i < input.length; i++) $(`#${input[i]}`).numeric({ negative: false });

        $(`#btn-update`).click(function () {
            alertify.confirm('Pregunta', '¿Está seguro que desea guardar los cambios realizados en este registro?', function(e) {
                if (e) org.updating(table, tr);
            }, function(){}).set({'movable': false, 'moveBounded': false, 'modal': false, 'labels': {ok: 'Sí', cancel: 'No'}});
        });

        this.loadReservoirs(this.cleanName(data[0].reservoir));

        console.log(this.loadReservoirs(this.cleanName(data[0].reservoir)));
    }

    loadReservoirs(value) {
        const ajax = new Ajax(`${globalConfig.appUrl}/reservoirManagement/loadReservoirs`, {
            headers: {accept: 'application/json'},
            method: 'POST'
        });
        
        ajax.result().then(response => {
            var size = Object.keys(response.list).length, content = ``, aux = ``;

            for(var i=0; i<size; i++) {
                if(value != response.list[i].reservoir_name) {
                    content += `<option value="${response.list[i].reservoir_name}">${response.list[i].reservoir_name}</option>`;
                } else {
                    aux += `<option value="${response.list[i].reservoir_name}">${response.list[i].reservoir_name}</option>`;
                }
            }

            aux += content;

            $('#reservoir_name').html(aux);

        }).catch(error => { console.log(error) });
        return false;
    }

    cleanName(value) {
        return value != null || value != '' ? value.replaceAll('&nbsp;', ' ') : value;
    }

    setHeader(value) {
        if (value === 'type_test') return 'Tipo&nbsp;de&nbsp;prueba';
        else if (value === 'sensor_model') return 'Sensor&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <h6 class="text-muted">modelo</h6>';
        else if (value === 'pressure_gradient') return 'Gradiente <h6 class="text-muted">lppc/pies</h6>';
        else if (value === 'deepdatum_mc_md') return 'Prof.&nbsp;DATUM&nbsp;Macolla&nbsp;MD <h6 class="text-muted">pies</h6>';
        else if (value === 'deepdatum_mc_tvd') return 'Prof.&nbsp;DATUM&nbsp;Macolla&nbsp;TVD <h6 class="text-muted">pies</h6>';
        else if (value === 'press_datum_macolla') return 'Presión&nbsp;Original&nbsp;DATUM&nbsp;Macolla <h6 class="text-muted">pies</h6>';
        else if (value === 'drill') return 'Taladro';
        else if (value === 'temp_datum_macolla') return 'Temp.&nbsp;Original&nbsp;DATUM&nbsp;Macolla <h6 class="text-muted">pies</h6>';
        else if (value === 'temperature_gradient') return 'Gradiente <h6 class="text-muted">ºF/pies</h6>';
    }
}

window.onload = () => {
    m = new Method();
    org = new ManagementOriginal();
}