var first = `
	<div class="col-lg-4 col-md-12">
		<div class="small-box bg-light">
			<div class="inner">
				<h5>Pruebas Extendidas</h5>
				</div>
				<div class="icon">
				<i class="ion ion-bag"></i>
			</div>
			<a href="javascript:void(0);" id="btn-extended-tests" class="small-box-footer" style="width: 50%;display: inline-grid;padding-top: 3%">
				<i class="fa fa-file-excel"></i>Excel
			</a>
			<a href="javascript:void(0);" id="btn-extended-tests-2" class="small-box-footer" style="width: 50%;display: inline-grid;padding-top: 3%;position:absolute;">
				<i class="fa fa-edit"></i>Manual
			</a>
		</div>
	</div>
	<div class="col-lg-4 col-md-12">
		<div class="small-box bg-light">
			<div class="inner">
				<h5>Pruebas Promedio</h5>
			</div>
			<div class="icon">
				<i class="ion ion-stats-bars"></i>
			</div>
			<a href="javascript:void(0);" id="btn-average-tests" class="small-box-footer" style="width: 50%;display: inline-grid;padding-top: 3%">
				<i class="fa fa-file-excel"></i>Excel
			</a>
			<a href="javascript:void(0);" id="btn-average-tests-2" class="small-box-footer" style="width: 50%;display: inline-grid;padding-top: 3%;position:absolute;">
				<i class="fa fa-edit"></i>Manual
			</a>
		</div>
	</div>
	<div class="col-lg-4 col-md-12">
		<div class="small-box bg-light">
			<div class="inner">
				<h5>Muestras</h5>
			</div>
			<div class="icon">
				<i class="ion ion-person-add"></i>
			</div>
			<a href="javascript:void(0);" id="btn-signs" class="small-box-footer" style="width: 50%;display: inline-grid;padding-top: 3%">
				<i class="fa fa-file-excel"></i>Excel
			</a>
			<a href="javascript:void(0);" id="btn-signs-2" class="small-box-footer" style="width: 50%;display: inline-grid;padding-top: 3%;position:absolute;">
				<i class="fa fa-edit"></i>Manual
			</a>
		</div>
	</div>
`;

var second = `
	<div class="col-lg-4 col-md-12">
		<div class="small-box bg-light">
			<div class="inner">
				<h5>PyT Original</h5>
			</div>
			<div class="icon">
				<i class="ion ion-bag"></i>
			</div>
			<a href="javascript:void(0);" id="btn-condition" class="small-box-footer" style="width: 50%;display: inline-grid;padding-top: 3%">
				<i class="fa fa-file-excel"></i>Excel
			</a>
			<a href="javascript:void(0);" id="btn-condition-2" class="small-box-footer" style="width: 50%;display: inline-grid;padding-top: 3%;position:absolute;">
				<i class="fa fa-edit"></i>Manual
			</a>
		</div>
	</div>
	<div class="col-lg-4 col-md-12">
		<div class="small-box bg-light">
			<div class="inner">
				<h5>Histórico Completaciones</h5>
			</div>
			<div class="icon">
				<i class="ion ion-stats-bars"></i>
			</div>
			<a href="javascript:void(0);" id="btn-historical" class="small-box-footer" style="width: 50%;display: inline-grid;padding-top: 3%">
				<i class="fa fa-file-excel"></i>Excel
			</a>
			<a href="javascript:void(0);" id="btn-historical-2" class="small-box-footer" style="width: 50%;display: inline-grid;padding-top: 3%;position:absolute;">
				<i class="fa fa-edit"></i>Manual
			</a>
		</div>
	</div>
	<div class="col-lg-4 col-md-12">
		<div class="small-box bg-light">
			<div class="inner">
				<h5>Presiones Estáticas</h5>
			</div>
			<div class="icon">
				<i class="ion ion-person-add"></i>
			</div>
			<a href="javascript:void(0);" id="btn-static-pressure" class="small-box-footer" style="width: 50%;display: inline-grid;padding-top: 3%">
				<i class="fa fa-file-excel"></i>Excel
			</a>
			<a href="javascript:void(0);" id="btn-static-pressure-2" class="small-box-footer" style="width: 50%;display: inline-grid;padding-top: 3%;position:absolute;">
				<i class="fa fa-edit"></i>Manual
			</a>
		</div>
	</div>
	<div class="col-lg-4 col-md-12">
		<div class="small-box bg-light">
			<div class="inner">
				<h5>Presiones Dinámicas</h5>
			</div>
			<div class="icon">
				<i class="ion ion-person-add"></i>
			</div>
			<a href="javascript:void(0);" id="btn-dynamic-pressure" class="small-box-footer" style="width: 50%;display: inline-grid;padding-top: 3%">
				<i class="fa fa-file-excel"></i>Excel
			</a>
			<a href="javascript:void(0);" id="btn-dynamic-pressure-2" class="small-box-footer" style="width: 50%;display: inline-grid;padding-top: 3%;position:absolute;">
				<i class="fa fa-edit"></i>Manual
			</a>
		</div>
	</div>
	<div class="col-lg-4 col-md-12">
		<div class="small-box bg-light">
			<div class="inner">
				<h5>Presión Original PMVM</h5>
			</div>
			<div class="icon">
				<i class="ion ion-bag"></i>
			</div>
			<a href="javascript:void(0);" id="btn-original-pressure" class="small-box-footer" style="width: 50%;display: inline-grid;padding-top: 3%">
				<i class="fa fa-file-excel"></i>Excel
			</a>
			<a href="javascript:void(0);" id="btn-original-pressure-2" class="small-box-footer" style="width: 50%;display: inline-grid;padding-top: 3%;position:absolute;">
				<i class="fa fa-edit"></i>Manual
			</a>
		</div>
	</div>
	<div class="col-lg-4 col-md-12">
		<div class="small-box bg-light">
			<div class="inner">
				<h5>Survey Real</h5>
			</div>
			<div class="icon">
				<i class="ion ion-stats-bars"></i>
			</div>
			<a href="javascript:void(0);" id="btn-real-survey" class="small-box-footer" style="width: 50%;display: inline-grid;padding-top: 3%">
				<i class="fa fa-file-excel"></i>Excel
			</a>
			<a href="javascript:void(0);" id="btn-real-survey-2" class="small-box-footer" style="width: 50%;display: inline-grid;padding-top: 3%;position:absolute;">
				<i class="fa fa-edit"></i>Manual
			</a>
		</div>
	</div>
	<div class="col-lg-4 col-md-12">
		<div class="small-box bg-light">
			<div class="inner">
				<h5>Survey Plan</h5>
			</div>
			<div class="icon">
				<i class="ion ion-stats-bars"></i>
			</div>
			<a href="javascript:void(0);" id="btn-plan-survey" class="small-box-footer" style="width: 50%;display: inline-grid;padding-top: 3%">
				<i class="fa fa-file-excel"></i>Excel
			</a>
			<a href="javascript:void(0);" id="btn-plan-survey-2" class="small-box-footer" style="width: 50%;display: inline-grid;padding-top: 3%;position:absolute;">
				<i class="fa fa-edit"></i>Manual
			</a>
		</div>
	</div>
`;

var third = `
	<div class="col-lg-4 col-md-12">
		<div class="small-box bg-light">
			<div class="inner">
				<h5>Producción Fiscalizada</h5>
			</div>
			<div class="icon">
				<i class="ion ion-bag"></i>
			</div>
			<a href="javascript:void(0);" id="btn-fiscalized" class="small-box-footer" style="width: 50%;display: inline-grid;padding-top: 3%">
				<i class="fa fa-file-excel"></i>Excel
			</a>
			<a href="javascript:void(0);" id="btn-fiscalized-2" class="small-box-footer" style="width: 50%;display: inline-grid;padding-top: 3%;position:absolute;">
				<i class="fa fa-edit"></i>Manual
			</a>
		</div>
	</div>
`;

var fourth = `
	<div class="col-lg-4 col-md-12">
		<div class="small-box bg-light">
			<div class="inner">
				<h5>Eventos de pozo</h5>
			</div>
			<div class="icon">
				<i class="ion ion-bag"></i>
			</div>
			<a href="javascript:void(0);" id="btn-events" class="small-box-footer" style="width: 50%;display: inline-grid;padding-top: 3%">
				<i class="fa fa-file-excel"></i>Excel
			</a>
			<a href="javascript:void(0);" id="btn-events-2" class="small-box-footer" style="width: 50%;display: inline-grid;padding-top: 3%;position:absolute;">
				<i class="fa fa-edit"></i>Manual
			</a>
		</div>
	</div>
`;