class Historical {
    constructor() {
        this.searchByFilter = this.searchByFilter.bind(this);
        this.setHeader = this.setHeader.bind(this);
    }

    searchByFilter() {
        
        var size = m.getColumnsFromDb().length, values = m.getColumnsFromDb(), preset = ['well_name', 'begin_date', 'end_date', 'macolla_name', 'location_name', 'reservoir'];

        for (var i = 0; i <= 5; i++) values[size + i] = preset[i];

        var range, typeSearch, reservoirId = [], collection = [];
        
        range = $("#filter-date").val() == 1 ?  m.getDatesRange($(`#date-pick`).val()) : m.getDatesRange(`${$(`#from`).val()}|${$(`#until`).val()}`);
        
        var whatIDo = document.querySelector(`#dual-list`).style.display == 'none';

        var from = moment(range[0]), until = moment(range[1]), isFrom = m.validateDate(from, 'msgFrom'), isUntil = m.validateDate(until, 'msgUntil');
        
        if (!whatIDo) {
            typeSearch = 1;
            var sizeIs = ($(`#location_well`).val()).length, store = $(`#location_well`).val();
            if (sizeIs > 1) for (var i = 0; i < sizeIs; i++) collection[i] = store[i];
            else collection = store[0];
        } else {
            $('input[name="option[]"]:checked').each(function(i) { reservoirId[i] = $(this).val(); });
            typeSearch = 2;
            var sizeIs = ($(`#macolla_list`).val()).length, store = $(`#macolla_list`).val();
            if (sizeIs > 1) for (var i = 0; i < sizeIs; i++) collection[i] = store[i];
            else collection = store[0];
        }
        
        if ((from.isValid() && isFrom) && (until.isValid() && isUntil)) {
            const ajax = new Ajax(`${globalConfig.appUrl}/historical/searchByFilterHistorical`, {
                headers: {
                    accept: 'application/json',
                },
                method: 'POST',
                body: {
                    items: values,
                    data: collection,
                    macolla_id: parseInt($(`#macolla`).val()),
                    reservoir_id: reservoirId != null ? reservoirId : null,
                    date_from: range[0],
                    date_to: range[1],
                    type: typeSearch
                }
            });
        
            m.setLoadingMessageBefore();
                
            ajax.result().then(response => {
        
                var options = ['type_work_i', 'type_work_ii', 'rig', 'sensor_model', 'sensor_brand', 'sensor_diameter', 'sensor_company', 'deeptop_sensor_md', 'deepbottom_sensor_md', 'pump_model', 'pump_diameter', 'pump_type', 'pump_capacity', 'pump_company', 'deeptop_pump_md', 'deepbottom_pump_md', 'inyected_diluent', 'measure_from', 'observations', 'references'];
                var size = Object.keys(response.data).length;
        
                m.setLoadingMessageAfter();
                m.showMessageDialog(size);

                if (size > 0) {

                    var selected = m.getColumnsToHead(options, Object.keys(response.data[0]));
                    var header = m.setColumn(response.type, response.info)[0], column = m.setColumn(response.type, response.info)[1];

                    let detail = (response.type == 1) ? header : m.selectedItems(response.total);
                    var titleText = detail == 'SIN MACOLLA' ? detail : `Macolla ${detail}`;

                    m.setHtmlTable(1, "Histórico de completación", `${titleText}`);
                    m.setHtmlTable(2);

                    let headerSelected = `<th class='text-muted'>#</th>${column}<th class='text-muted'>Localización</th><th class='text-muted'>Pozo</th><th class='text-muted'>Yacimiento</th><th class='text-muted'>Fecha&nbsp;y&nbsp;hora&nbsp;inicial</th><th class='text-muted'>Fecha&nbsp;y&nbsp;hora&nbsp;final</th>`;

                    for(var i=0; i<selected.length; i++) headerSelected += `<th>${this.setHeader(selected[i])}</th>`;

                    document.querySelector(`#header`).innerHTML = headerSelected;

                    var table = m.newDateTable(response.results, size, `Histórico de completación ${titleText}`);

                    table.buttons().container().appendTo(`#buttons`);

                    $(`.dts_label`).remove();

                    document.querySelector('#imhere').focus();

                } else document.querySelector(`#table-content`).innerHTML = ``;
            
            }).catch(error => { console.log(error) });
        }
        return false;
    }

    setHeader(value) {

        if (value === 'type_work_i') return 'Tipo&nbsp;de&nbsp;Trabajo';
        else if (value === 'type_work_ii') return 'Tipo&nbsp;de&nbsp;Trabajo&nbsp;II';
        else if (value === 'rig') return 'Equipo&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
        else if (value === 'sensor_model') return 'Sensor&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <h6 class="text-muted">modelo</h6>';
        else if (value === 'sensor_brand') return 'Sensor <h6 class="text-muted">marca</h6>';
        else if (value === 'sensor_diameter') return 'Sensor <h6 class="text-muted">plg&nbsp;diámetro</h6>';
        else if (value === 'sensor_company') return 'Sensor <h6 class="text-muted">compañía</h6>';
        else if (value === 'deeptop_sensor_md') return 'Prof.&nbsp;Sup.&nbsp;Sensor&nbsp;(MD) <h6 class="text-muted">pies</h6>';
        else if (value === 'deepbottom_sensor_md') return 'Prof.&nbsp;Inf.&nbsp;Sensor&nbsp;(MD) <h6 class="text-muted">pies</h6>';
        else if (value === 'pump_model') return 'Bomba&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<h6 class="text-muted">modelo</h6>';
        else if (value === 'pump_diameter') return 'Bomba&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<h6 class="text-muted">plg&nbsp;diámetro</h6>';
        else if (value === 'pump_type') return 'Método&nbsp;bombeo <h6 class="text-muted"></h6>';
        else if (value === 'pump_capacity') return 'Capacidad&nbsp;Bomba <h6 class="text-muted">BPD/100 RPM</h6>';
        else if (value === 'pump_company') return 'Bomba <h6 class="text-muted">compañía</h6>';
        else if (value === 'deeptop_pump_md') return 'Prof.&nbsp;Sup.&nbsp;Bomba&nbsp;(MD) <h6 class="text-muted">pies</h6>';
        else if (value === 'deepbottom_pump_md') return 'Prof.&nbsp;Inf.&nbsp;Bomba&nbsp;(MD) <h6 class="text-muted">pies</h6>';
        else if (value === 'inyected_diluent') return 'Inyección&nbsp;diluente';
        else if (value === 'measure_from') return 'Medidas&nbsp;desde';
        else if (value === 'observations') return 'Observaciones';
        else if (value === 'references') return 'Referencias';
    }
}

window.onload = () => {
    m = new Method();
    hst = new Historical();
}