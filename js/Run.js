class Run {
    constructor() {
    }
    
    validateSearch() {
        if ($(`#macolla`).val() != '') this.searchByFilter();
        else alertify.alert('Importante', 'Debe seleccionar primero una macolla.', function(){}).set({'movable': false, 'moveBounded': false});
    }

    searchRunData() {
        
        var range = $("#filter-date").val() == 1 ?  m.getDatesRange($(`#date-pick`).val()) : m.getDatesRange(`${$(`#from`).val()}|${$(`#until`).val()}`);
        
        const ajax = new Ajax(`${globalConfig.appUrl}/getRunData`, {
            headers: { accept: 'application/json' },
            method: 'POST',
            body: {
                macolla_id: parseInt($(`#macolla`).val()),
                well_id: parseInt($(`#well`).val()),
                date_from: range[0],
                date_to: range[1]
            }
        });

        m.setLoadingMessageBefore();
        
        ajax.result().then(response => {

            var size = Object.keys(response.data).length;

            m.setLoadingMessageAfter();
            m.showMessageDialog(size);

            if (size > 0) {

                m.setHtmlTable(1, "Bomba", `Localización ${response.nameOfLocation}`);
                m.setHtmlTable(2);

                let headerSelected = `<th>Bomba</th><th>Fecha <h6>inicio</h6></th><th>Fecha <h6>fin</h6></th><th>Run&nbsp;Revolution</th><th>Run&nbsp;Day</th>`;

                document.querySelector(`#header`).innerHTML = headerSelected;
                
                var table = m.newDateTable(response.data, size, `Bomba ${response.nameOfLocation}`);

                table.buttons().container().appendTo(`#buttons`); 

                $(`.dts_label`).remove();

                document.getElementById('imhere').focus();

            } else document.querySelector(`#table-content`).innerHTML = ``;
    
        }).catch(error => { console.log(error) });
        return false;
    }
}

window.onload = () => {
    m = new Method();
    r = new Run();
}