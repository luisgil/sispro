let baseUrl = '';
// let baseUrl = 'undefined';
let token = '';
// request error timeout
let timeout = (1000 * 60) * 1.5;
// global headers
let globalHeaders = {}; // 'Accept': 'application/json',

class Ajax {
  // base url
  static get baseUrl() {
    return baseUrl;
  }
  static set baseUrl(url) {
    if ( typeof url === 'string' ) {
      baseUrl = url;
    } else {
      throw new Error('Ajax: "baseUrl" must be a string');
    }
  }
  // bearer token
  static get token() {
    return token;
  }
  static set token(bearer) {
    if ( typeof bearer === 'string' ) {
      token = bearer;
    } else {
      throw new Error('Ajax: "token" must be a string');
    }
  }
  // error timeout
  static get timeout() {
    return timeout;
  }
  static set timeout(milliseconds) {
    timeout = milliseconds;
  }
  // global headers
  static set globalHeaders(headers) {
    if ( typeof headers === 'object' ) {
      globalHeaders = headers;
    } else {
      throw new Error('Ajax: "globalHeaders" must be a object');
    }
  }
  static get globalHeaders() {
    return globalHeaders;
  }

  static make(endpoint = '', options = {}) {
    return new Ajax(endpoint, options);
  }

  // native XMLHttpRequest properties
  get status() {
    return this.xhr.status;
  }
  get statusText() {
    return this.xhr.statusText;
  }
  get responseText() {
    return this.xhr.responseText;
  }
  get readyState() {
    return this.xhr.readyState;
  }

  constructor(endpoint = '', options = {}) {
    this.xhr = new XMLHttpRequest();
    this.success = false;
    this.error = false;
    this.options = Ajax.__setRequestOptions(options);
    this.url = (this.options.useBaseUrl) ? `${baseUrl}${endpoint}` : endpoint;
    this.__progressListeners();
    this.__resultListeners();
    this.xhr.timeout = timeout;
    if (this.options.autoSend) {
      this.send();
    }
    this.abort = this.abort.bind(this);
    this.send = this.send.bind(this);
  }

  static __setBody(body, method, headers = {}) {
    if (method.toUpperCase() === 'PUT' || 'Content-Type' in headers || 'content-type' in headers) {
      return JSON.stringify(body);
    } else {
      const formData = new FormData();
      for(let arg of Object.entries(body)){
        formData.append(arg[0], arg[1])
      }
      return formData;
    }
  }

  send() {
    this.xhr.open(this.options.method, this.url, true);
    this.xhr.responseType = this.options.responseType;
    this.__setHeaders();
    if (this.options.body) {
      this.xhr.send(this.options.body);
    } else {
      this.xhr.send();
    }
  }

  __setHeaders() {
    for(let arg of Object.entries(this.options.headers)) {
      if (this.options.method.toUpperCase() !== 'PUT') {
        if (arg[0] !== 'Content-Type' || arg[0] !== 'content-type') {
          this.xhr.setRequestHeader(arg[0],arg[1]);
        }
      } else {
        this.xhr.setRequestHeader(arg[0],arg[1]);
      }
    }
    if(typeof (Ajax.token) === 'string' && Ajax.token.length > 0) {
      // console.log("token: ", Ajax.token);
      this.xhr.setRequestHeader('X-CSRF-TOKEN', Ajax.token);
    }
  }

  abort() {
    this.xhr.abort();
  }

  __progressListeners() {
    this.xhr.addEventListener('progress', (e) => {
      this.downloadProgress = (e.loaded / e.total) * 100;
      this.options.onDownloadProgress(e, this.downloadProgress)
    });
    this.xhr.upload.addEventListener('progress', (e) => {
      this.uploadProgress = (e.loaded / e.total) * 100;
      this.options.onUploadProgress(e, this.uploadProgress)
    });
    this.xhr.onreadystatechange = this.options.onReadyStateChange;
  }

  __getResponse() {
    try {
      console.log(JSON.parse(this.xhr.response));
      return JSON.parse(this.xhr.response);
    } catch (error) {
      return this.xhr.response;
    }
  }

  __resultListeners() {
    this.result = () => new Promise( (resolve, reject) => {
      this.xhr.onload = () => {
        // si no hay errores en la peticion
        if (this.status === 200) {
          this.error = false;
          this.success = this.__getResponse();
          resolve(this.success);
        } else {
          this.error = true;
          reject(this.__getResponse());
        }
      };
      this.xhr.onabort = () => {
        this.success = false;
        this.error = {
          fail: true,
          type: 'abort',
          status: this.status,
          message: 'Aborted Request',
          response: this.__getResponse(),
        };
        reject(this.error);
      };
      this.xhr.onerror = () => {
        this.success = false;
        this.error = {
          fail: true,
          type: 'error',
          status: this.status,
          message: 'Network Connection Error',
          response: this.__getResponse(),
        };
        reject(this.error);
      };
    });
  }

  static __setRequestOptions(options){
    const {
      method = 'GET',
      headers = {},
      body = false,
      onUploadProgress = () => null,
      onDownloadProgress = () => null,
      onReadyStateChange = () => null,
      useBaseUrl = true,
      autoSend = true,
      responseType = 'text',
    } = options;
    const newOptions = {
      method,
      headers: Object.assign({}, globalHeaders, headers),
      body: (body) ? Ajax.__setBody(body, method, headers) : false,
      onUploadProgress,
      onDownloadProgress,
      useBaseUrl,
      onReadyStateChange,
      autoSend,
      responseType,
    };
    return newOptions;
  }
};

if (typeof module !== 'undefined' && module.exports) {
  module.exports = Ajax;
} else {
  window.Ajax = Ajax;
}
