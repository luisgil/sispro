function setChartSign(data, filename) {
	var chart = AmCharts.makeChart('chart', {
		//"hideCredits": true,
		"type": "serial",
		"language": "es",
		"dataProvider": data,
		"valueAxes": [
			{
				"minimum": 0,
				"maximum": 100,
				"axisColor": "#0000ff",
				"axisThickness": 2,
				"title": "%AyS",
				"id": "v1"
			},
			{
				"position": "right",
				"axisColor": "#008000",
				"axisThickness": 2,
				"title": "ºAPI Diluido",
				"id": "v2"
			}
		],
		"graphs": [
			{
				"lineThickness": 2,
				"valueAxis": "v1",
				"valueField": "percent_mixture",
				"type": "line",
				"lineColor": "#0000ff",
				"title": "% AyS"
			},
			{
				"lineThickness": 2,
				"valueAxis": "v2",
				"valueField": "api_degree_diluted",
				"type": "line",
				"lineColor": "#008000",
				"title": "ºAPI Crudo diluido"
			}
		],
		"chartCursor": {},
		"categoryAxis": {
			"parseDates": true,
			"minPeriod": "DD",
			"minorGridEnabled": true
		},
		"categoryField": "date",
		"legend": {
			"position": "top",
			"align": "center",
			"markerType": "circle",
			"valueText": "[[value]]",
			"valueWidth": 100,
			"valueAlign": "left",
			"equalWidths": false,
			"useGraphSettings": true
		},
		"numberFormatter": {
			"precission": -1,
			"decimalSeparator": ",",
			"thousandsSeparator": "."
		},
		"export": {
			"enabled": true,
			"language": "es",
			"position": "top-right",
			"fileName": filename
		}
	});
}

function order(dataChart) {
	
	var aux = 0, length = Object.keys(dataChart).length;
	
	for (var i = 0; i < length; i++) {
		for (var j = i+1; j < length; j++) {
			if (dataChart[i].date > dataChart[j].date) {
				aux = dataChart[i];
				dataChart[i] = dataChart[j];
				dataChart[j] = aux;
			}
		}
	}
	
	var c = 0, aux = dataChart[0].date, dates = Array(length).fill(0), api = Array(length).fill(0), percent = Array(length).fill(0), result = [];
	
	for	(var i = 0; i < length; i++) {		
		if (aux == dataChart[i].date) {
			dates[c] = dataChart[i].date;
			api[c] = Number((api[c] + dataChart[i].api_degree_diluted).toFixed(2));
			percent[c] = Number((percent[c] + dataChart[i].percent_mixture).toFixed(2));
		} else {
			aux = dataChart[i].date;
			i--;
			c++;
		}
	}
	
	dates = dates.filter(function(elm){return elm != 0});
	api = truncate(api, dates.length);
	percent = truncate(percent, dates.length);
	
	/* AVERAGES AND ZERO DOTS */
	
	var repeated = 0, average = [];
	
	for (var i = 0; i < dates.length; i++) {
		for (var j = 0; j < length; j++) {
			if (dates[i] == dataChart[j].date) {
				repeated++;
			}
		}
		average[i] = repeated;
		repeated = 0;
	}
	
	for (var i = 0; i < dates.length; i++) {
		api[i] = (api[i]/average[i]).toFixed(2);
		percent[i] = (percent[i]/average[i]).toFixed(2);
	}
	
	var from = moment(dataChart[0].date, "YYYY-MM-DD");
	var to = moment(dataChart[length - 1].date, "YYYY-MM-DD");
	
	var extendedDates = daysBetweenDates(from, to), _api = [], _percent = [];
	var i = 0, j = 0, z = 0;
	
	for ( ; i < extendedDates.length; ) {
		for ( ; j < dates.length; ) {
			if (extendedDates[i] == moment(dates[j]).format('YYYY-MM-DD')) {
				_api[z] = Number(api[j]).toFixed(2);
				_percent[z] = Number(percent[j]).toFixed(2);
				z++;
				i++;
				j++;
			} else {	
				_api[z] = 0;
				_percent[z] = 0;
				z++;
				i++;
			}
		}
	}
	
	/* end */
	
	for (var i = 0; i < extendedDates.length; i++) {
		result.push({
			date: addDay(new Date(moment(extendedDates[i]).format("YYYY-MM-DD")), 1),
			api_degree_diluted: toTurnZero(_api[i]),
			percent_mixture: toTurnZero(_percent[i])
		});
	}
	
	return result;
}

function truncate(array, length) {
	var arr = [];
	for (var i = 0, x = 0; i < length; i++) {
		if (i < length) {
			arr[x] = array[i];
			x++;
		}
	}
	return arr;
}

function addDay(date, days) {
	return date.setDate(date.getDate() + days);
}

function daysBetweenDates(from, to) {
	var actual = from, hasta = to, myDates = [];
	
	while (actual.isSameOrBefore(to)) {
		myDates.push(actual.format('YYYY-MM-DD'));
		actual.add(1, 'days');
	}
	return myDates;
}

function toTurnZero(number) {
	return number == -1 ? 0 : number;
}