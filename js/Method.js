class Method {
    constructor() {
        this.setFilterDateImproved(1);
    }

    reloadView() {
		document.querySelectorAll('.text-danger').forEach(function(a){
			a.remove()
		});
    }

    showInputErrors(errors, inputNames) {
		for(const errorName in errors) {
			inputNames.map(inputName => {
				if (errorName === inputName) {
					document.getElementById(`${inputName}`).className = "form-control form-control-sm is-invalid";
					document.querySelector(`#wrapper-${inputName}`).innerHTML += `<small class="text-danger">${errors[errorName]}</small>`;
					document.querySelector(`#${inputName}`).focus();
				}
			});
		}
	}
	
	showInputCorrects(errors, inputNames) {
		for(const errorName in errors) {
			inputNames.map(inputName => {
				if (errorName !== inputName) {
					document.getElementById(`${inputName}`).className = "form-control form-control-sm is-valid";      		
				}
			});
		}
    }
    
    randomString(length, chars) {
		length = length || 16;
		chars = chars || "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ@#!?=";
		
		var text = "", max = chars.length-1;
		
		for (var i=1; i<length; i++)  text += chars[Math.floor(Math.random() * (max+1))];
		return text;
    }
    
    trimNotRequiredString(id) {
        return ($(`#${id}`).val() == null || $(`#${id}`).val() == '' ) ? '-' : $(`#${id}`).val().toUpperCase();
    }

    trimString(id) {
        return ($(`#${id}`).val() != null || $(`#${id}`).val() != '') ? $(`#${id}`).val().toUpperCase() : '';
    }

    convertToDecimal(number) {
        return (number != null || number != '') ? number.replace(',', '.') : '';
    }

    convertToNumber(number) {
        return (number != null || number != '') ? parseFloat(number.replace(',', '.')) : '';
    }

    valueIsEmpty(id) {
        return ( $(`#${id}`).val() == "" ? -1 : m.convertToDecimal($(`#${id}`).val()));
    }

    valueIsEmptyToSurvey(id) {
        return ( $(`#${id}`).val() == "" ? 0 : m.convertToDecimal($(`#${id}`).val()));
    }

    restriction(value, strict, min, max) {
        return (value >= strict && value < min) || (value > max) ? 9999 : value;
    }

    verifyValue(value) {
        return value === -1 ? true : null;
    }

    setNullToValue(value) {
        return (value === '-') ? '' : value;
    }

    valueTrimNotRequired(value) {
        return (value == null || value == '' ) ? '-' : value;
    }

    getOptionSelected(value) {
        if (value != 'all' && value != '') {
            this.loadLocations(value);
            $(`#reservoirs, #dual-list-2`).each(function() {
                $(this).css('display', 'none');
            });
            $(`#dual-list`).css('display', 'block');
        } else {
            this.loadMacollas();
            $(`#reservoirs, #dual-list-2`).each(function() {
                $(this).css('display', 'block');
            });
            $(`#dual-list`).css('display', 'none');
        }

        if (value == '') $(`#reservoirs, #dual-list, #dual-list-2`).each(function() { $(this).css('display', 'none'); });
        console.clear();
    }

    getColumnsFromDb() {
        var values = [];
        $('input[name="item[]"]:checked').each(function(i) {
            values[i] = $(this).val();
        });
        return values;
    }

    setLoadingMessageBefore() {
        $("#btn-consult").attr("disabled", true); /*Prevent multiples clicks*/
        $("#loading").html(`<div class="loading-animated"></div><small>Cargando, por favor espere...</small>`);
    }

    setLoadingMessageAfter() {
        $("#loading").html(``);
        $("#btn-consult").attr("disabled", false); /*Enabled button again after receive request*/
    }

    showMessageDialog(size){
        if(size == 0) {
            alertify.alert('<b>Lo sentimos</b>', 'No se ha encontrado ningún resultado', function(){}).set({'movable': false, 'moveBounded': false});
            $(`#table-content`).html(``);
            $('#showChart').css('display', 'none');
        }
    }

    getColumnsToHead(array, object) {
        let j = 0, filter = [];
        for(const index in array) {
            object.map(key => {
                if (key == array[index]) {
                    filter[j] = key;
                    j++
                }
            });
        }
        return filter;
    }

    setGeoData (grades, min, seg) {

        if (min == null || seg == null || grades == null) {
            return "-";
        } else {
            return grades + " " + min + " " + seg;
        }
    }

    setColumn(value, string) {
        let text1 = '', text2 = '';
        if (value == 1) {
            text1 = `${string}`;
        } else {
            text1 = string;
            text2 = `<th class='text-muted'>Macolla</th>`;
        }
        return [text1, text2];
    }

    setDatatable(text, string, columns) {
        var table = $("#datatable").DataTable({
            dom: 'Bfrtip',
            searching: false,
            paging: false,
            info: false,
            ordering: false,
            buttons: [{
                extend: 'excelHtml5',
                text: '<i class=" fas fa-file-excel"></i> Excel',
                className: 'btn btn-success btn-sm',
                title: `${text} ` + string,
                exportOptions: {
                    columns: columns,
                    format: {
                        /*body: function(data, row, column, node) {
                            data = $('<p>' + data + '</p>').text();
                            return $.isNumeric(data.replace(',', '.')) ? data.replace(',', '.') : data;
                        }*/
                    }
                }
            }]
        });
        
        table.buttons().container().appendTo($("#buttons"));        
    }

    newDateTable(data, size, filename) {
        var table = $(`#datatable`).DataTable({
            dom: 'Blfrptip',
            processing: true,
            draw: data.draw,
            data: data,
            deferRender: true,
            pageLength: 150,
            lengthMenu: [150, 200, 250, 300, 500],
            language: {
                lengthMenu: "Mostrar _MENU_",
                info: "Mostrando _START_ a _END_ de _TOTAL_ resultados"
            },
            recordsTotal: size,
            recordsFiltered: size,
            searching: false,
            info: true,
            ordering: false,
            scrollY: 400,
            scrollX: true,
            scrollColapse: true,
            scroller: false,
            paging: true,
            pagingType: 'listbox',
            buttons: [{
                extend: 'csvHtml5',
                charset: "UTF-8",
                bom: true,
                text: '<b>CSV</b>',
                className: 'btn btn-success btn-sm',
                title: (filename) ? filename : "CSV File" ,
                fieldSeparator: ";",
                exportOptions: {
                    columns: ':gt(0)',
                }
            }]
        });

        return table;
    }

    setFilterDate(value) {
        if (value == 1) {
            $(`#filter-customized`).css('display','none');
            $(`#filter-predetermined`).css('display','block');
        } else {
            $(`#filter-customized`).css('display','block');
            $(`#filter-predetermined`).css('display','none');
        }
    }

    setFilterDateImproved(value) {

        /* days transcurred until now in this month*/
        let toThisMonth = moment().format("YYYY-MM") + "-01";

        /* last month*/
        let from = moment().subtract(1, 'months').format("YYYY-MM") + "-01";
        let until = moment().subtract(1, 'months').format("YYYY-MM") + "-" + moment(moment(from), 'YYYY-MM').daysInMonth();

        if (value == 1) {
            
            $(`#filter-customized`).html(``);
            $(`#filter-customized`).hide();


            $(`#filter-predetermined`).html(
                `<label>Predeterminada</label>
                <select class="form-control" name="date-pick" id="date-pick">
                    <option value="${moment().format('YYYY-MM-DD')}|${moment().format('YYYY-MM-DD')}">Hoy</option>
                    <option value="${moment().subtract(1, 'd').format('YYYY-MM-DD')}|${moment().subtract(1, 'd').format('YYYY-MM-DD')}">Ayer</option>
                    <option value="${moment().subtract(7, 'd').format('YYYY-MM-DD')}|${moment().format('YYYY-MM-DD')}">Últimos 7 días</option>
                    <option value="${moment().subtract(15, 'd').format('YYYY-MM-DD')}|${moment().format('YYYY-MM-DD')}">Últimos 15 días</option>
                    <option value="${toThisMonth}|${moment().format('YYYY-MM-DD')}">Este mes</option>
                    <option value="${from}|${until}">Mes pasado</option>
                    <option value="${moment().subtract(6, 'months').format("YYYY-MM-DD")}|${moment().format("YYYY-MM-DD")}">Últimos 6 meses</option>
                    <option value="${moment().subtract(12, 'months').format("YYYY-MM-DD")}|${moment().format("YYYY-MM-DD")}">Último año</option>
                </select>`
            );
            $(`#filter-predetermined`).show();

        } else {

            $(`#filter-predetermined`).html(``);
            $(`#filter-predetermined`).hide();


            $(`#filter-customized`).html(`<div class="row"><div class="col-md-6"><label>Desde</label><input type="date" class="form-control" id="from" value="${moment().format("YYYY-MM-DD")}" /><small id="msgFrom" class="text-danger"></small></div><div class="col-md-6"><label>Hasta</label><input type="date" class="form-control" id="until" value="${moment().format("YYYY-MM-DD")}" /><small id="msgUntil" class="text-danger"></small></div></div>`);
            $(`#filter-customized`).show();
        }
    }

    validateDate(date, id) {
        var state = false;
        if (!date.isSameOrAfter('2012-01-01') || !date.isSameOrBefore(moment())) {
            $(`#${id}`).html(`La fecha no está admitida.`);
        } else {
            state = true;
            $(`#${id}`).html(``);
        }
        return state;
    }

    getDatesRange(date){
        return date.split('|');
    }

    setupDates(value, id) {
        var d = $(`#${id}`).val().split(' - ');
        var dateFrom = moment(d[0], 'DD/MM/YYYY').format('YYYY-MM-DD');
        var dateTo = moment(this.typeFor(value, d[1], d[0]), 'DD/MM/YYYY').format('YYYY-MM-DD');
        return [dateFrom, dateTo];
    }

    setFormatLimit(value, limit, decimals) {
        if ($.isNumeric(value)) {
            if (value != 0) {
                if (value < limit) 
                    return value.toLocaleString("de-DE", {minimumFractionDigits: decimals, maximumFractionDigits: decimals});
                else return Math.round(value);
            } else return 0;
        } else return null;
    }

    setFormat(data, decimals) {
        if (data != 0) {
            return ($.isNumeric(data)) ? data.toLocaleString("de-DE", { minimumFractionDigits: decimals, maximumFractionDigits: decimals}).replace('.', '') : '';
        } else return 0;
        
    }

    concat(data, value) {
        return (data != null || $.isNumeric(data)) ? `<td>${value}</td>` : `<td>-</td>`;
    }

    concatString(value) {
        return (value == null) ? `<td>-</td>` : `<td>${value}</td>`;
    }
    
    validateEmpty(value) {
        if (value != null) {
            return (value == `-`) ? `<td> - </td>` : `<td>${value.replace(/ /g, '&nbsp;')}</td>`;
        } else {
            return ``;
        }
    }

    formatString(text) {
        if (text != null) {
            return `${text.replace(/ /g, '&nbsp;')}`;
        } else {
            return ``;
        }
    }

    diffTime(init, end) {
        var diff = end.subtract(init);
        return diff;

    }

    validateEmptyString(value) {
        if (value != null) {
            return (value == `-`) ? `-` : `${value.replace(/ /g, "\xA0")}`;
        } else {
            return ``;
        }
    }

    replaceIfThereString(value) {
        return (value == null) ? '-' : value.replace(/ /g, '&nbsp;');
    }

    addExtraColumn(type, text) {
        return (type == 2) ? this.replaceIfThereString(text) : ``;
    }

    setFormatDate(type, date) {
        return (type == 1) ? moment(date).format('DD/MM/YYYY') : moment(date).format('DD/MM/YYYY HH:mm');
    }

    limitCero(value, compare) {
        return value == compare ? '-' : value;
    }

    selectedItems(array) {
        let string = ``;
        for(let i = 0; i < array.length; i++) {
            if(i > 0) string += ', ' + array[i];
            else string += array[i];
        }

        return string;
    }

    addFormatData(array, limit, decimals) {
        var content = this.verifyValue(array) ? '-' : this.setFormatLimit(array, limit, decimals);
        return content;

    }

    getArrayData(value) {
        return value.split('|');
    }

    setHtmlTable(param, text, string, only = null) {
        if(param == 1) {
            $(`#table-content`).html(`
            <div class="card">
                <div class="card-header">
                    <div class="card-title"><b>${text} / ${string}</b></div>
                    <div class="card-tools" id="buttons"></div>
                </div>
                <div class="card-body" id="table-data"></div>
            </div>`);
        } else {
            string = ``;
            $(`#table-data`).html(`<table class="display cell-border" id="datatable" width="100%">
            <thead><tr id="header"></tr></thead><tbody id="data"></tbody></table>`);
        }
    }

    skipFirstColumnToExport() {
        var nColumns = $('#datatable tr:last td').length, skip = [];
        for (var i = 1, j = 0; i < nColumns; i++, j++) skip[j] = i;;
    }

    padLeft(nr, n, str) {
        return Array(n-String(nr).length+1).join(str||0)+nr;
    }

    createTable(headerItems) {
        var div = document.createElement('div');
        div.className = "card-body table-responsive p-0";
        div.style.height = "400px";

        var table = document.createElement(`table`);
        table.className = "table table-head-fixed table-bordered";

        var thead = document.createElement(`thead`);
        var tbody = document.createElement(`tbody`);
        var tr = document.createElement(`tr`);

        var headerSize = headerItems.length;

        for (var i=0; i < headerSize; i++) {
            var th = document.createElement(`th`);
            th.className = "bg-info";
            th.innerHTML = headerItems[i];
            tr.appendChild(th);
        }

        document.querySelector(`#excel-output`).appendChild(div);
        document.querySelector(`.table-responsive`).appendChild(table);

        table.appendChild(thead);
        thead.appendChild(tr);
        table.appendChild(tbody);

        document.querySelector(`tbody`).id = `data`;

        return tbody;
    }

    decimalToFraction(value, donly) {
        if ($.isNumeric(value)) {
            var tolerance = 1.0E-6, h1 = 1, h2 = 0, k1 = 0, k2 = 1, negative = false;
            var donly = true, i;
            
            if (parseInt(value) == value) {
                return value;
            } else if (value < 0) {
                negative = true;
                value = -value;
            }
            
            if (donly) {
                i = parseInt(value);
                value -= i;
            }
            
            var b = value, a, aux;
            
            do {
                a = Math.floor(b);	
                aux = h1;
                h1 = a * h1 + h2;
                h2 = aux;
                aux = k1;
                k1 = a * k1 + k2;
                k2 = aux;
                b = 1 / (b - a);
            } while(Math.abs(value - h1 / k1) > value * tolerance);
        
            return (negative ? "-" : '') + ((donly & (i != 0)) ? i + ' ' : '') + (h1 == 0 ? '' : h1 + "/" + k1);
        }
    }

    fractionStrToDecimal(value) {
        var array = value.split(' '), decimal = 0;
        if (array.length > 1) decimal = array[1].split('/').reduce( (p, c) => p / c ) + parseFloat(array[0]);
        else decimal = array[0].split('/').reduce( (p, c) => p / c );
        return this.setFormat(decimal);
    }
 
    validateUndefined(value) {
        return (value == 'undefined') || (value == null) ? '' : value.toLocaleString("es-ES");
    }

    loadLocations(value) {
        if (value.trim() != '') {
            var str = value.split('|');
            var macollaId = parseInt(str[0]), macollaName = str[1];
            const ajax = new Ajax(`${globalConfig.appUrl}/loadLocations`, {
                headers: {
                    accept: 'application/json',
                },
                method: 'POST',
                body: {
                    macollaId: macollaId,
                    macollaName: macollaName
                }
            });
            ajax.result().then(response => {	
                $('#dual-list').html(`<div class="row"><div class="col-12"><div class="form-group"><label>Seleccionar localización</label><select class="duallistbox" multiple="multiple" id="location_well"></select></div></div></div>`);
                
                var options = ``, size = Object.keys(response.data).length;

                for (var i=0; i< size; i++) {
                    options += `<option value="${response.data[i].location_id}">${response.data[i].location_name}</option>`;
                }
                
                $('#location_well').html(options);
                $('.duallistbox').bootstrapDualListbox();
            }).catch(error => {});
            return false;
        }
    }

    loadLocationsToSelect(value) {
        if (value.trim() != '') {
            var str = value.split('|'), id = parseInt(str[0]), macollaName = str[1];

            const ajax = new Ajax(`${globalConfig.appUrl}/loadLocations`, {
                headers: { accept: 'application/json'},
                method: 'POST',
                body: {
                    macollaId: id,
                    macollaName: macollaName
                }
            });
            ajax.result().then(response => {    
                
                var options = ``, size = Object.keys(response.data).length;

                for (let i=0; i< size; i++) {
                    options += `<option value="${response.data[i].location_id}">${response.data[i].location_name}</option>`;
                }
                
                $('#well').html(options);

                console.log(this.loadHoles(20));
            }).catch(error => {});
            return false;
        } else {
            $('#well').html(`<option value="">--</option>`);
        }
    }

    loadMacollas() {
        const ajax = new Ajax(`${globalConfig.appUrl}/loadMacollas`, {
            headers: {
                accept: 'application/json',
            },
            method: 'POST'
        });
      
        ajax.result().then(response => {	
            $('#dual-list-2').html(`<div class="row"><div class="col-12"><div class="form-group"><label>Seleccionar macollas</label><select class="duallistbox" multiple="multiple" id="macolla_list"></select></div></div></div>`);
            
            var options = ``, size = Object.keys(response._data).length;
            
            for(var i=0; i < size; i++) {
                options += `<option value="${response._data[i].id}">${response._data[i].macolla_name}</option>`;
            }
            options += `<option value="${response.e.id}">S/M</option>`;
            $('#macolla_list').html(options);
            $('.duallistbox').bootstrapDualListbox();
        }).catch(error => { console.log(error)});
        return false;
    }

    loadHoles(id) {
        const ajax = new Ajax(`${globalConfig.appUrl}/loadHoles/${id}`, {
            headers: { accept: 'application/json',},
            method: 'POST'
        });

        ajax.result().then(response => {
            var options = ``, size = Object.keys(response.data).length;
            for (let i=0; i< size; i++) {
                options += `<option value="${response.data[i].hole}">${response.data[i].hole}</option>`;
            }
            $('#well').html(options);
        }).catch(error => {});
    }

    loadQuality(value) {
        var qualities = ["B", "M", "R"], container = `<option value="${value}">${value}</option>`;
        for (var i = 0; i < 3; i++) {
            if (value != qualities[i]) {
                container += `<option value="${qualities[i]}">${qualities[i]}</option>`
            }
        }
        return container;
    }
}