class RealSurvey {
    constructor() {
        this.searchByFilter = this.searchByFilter.bind(this);
        this.setHeader = this.setHeader.bind(this);
    }

    searchByFilter() {

        var size = m.getColumnsFromDb().length, values = m.getColumnsFromDb(), preset = ['well_name', 'rs_date', 'company', 'macolla_name', 'location_name', 'reservoir_name'];

        for (var i = 0; i <= 5; i++) values[size + i] = preset[i];
        
        var typeSearch, reservoirId = [], collection = [], whatIDo = document.querySelector(`#dual-list`).style.display == 'none';
        
        /*By wells or by macollas ?*/
        if (!whatIDo) {
            typeSearch = 1;
            var sizeIs = ($(`#location_well`).val()).length, store = $(`#location_well`).val();
            if (sizeIs > 1) {
                for (var i = 0; i < sizeIs; i++) collection[i] = store[i];
            } else collection = store[0];
        } else {
            /*This is for show all macollas and reservoirs!*/
            $('input[name="option[]"]:checked').each(function(i) {
                reservoirId[i] = $(this).val();
            });
            typeSearch = 2;
            var sizeIs = ($(`#macolla_list`).val()).length, store = $(`#macolla_list`).val();
            if (sizeIs > 1) {
                for (var i = 0; i < sizeIs; i++) collection[i] = store[i];
            } else collection = store[0];
        }
        
        const ajax = new Ajax(`${globalConfig.appUrl}/realSurvey/searchByFilterSurvey`, {
            headers: {
                accept: 'application/json',
            },
            method: 'POST',
            body: {
                items: values,
                data: collection,
                macolla_id: parseInt($(`#macolla`).val()),
                type: typeSearch
            }
        });

        m.setLoadingMessageBefore();

        ajax.result().then(response => {
            
            var options = ['hole', 'gl', 'rt', 'md', 'inclination', 'azim_grid', 'tvd', 'dls', 'vsec' , 'ns', 'ew', 'latitude', 'longitude', 'observations'];
            var size = Object.keys(response.data).length;

            m.setLoadingMessageAfter();
            m.showMessageDialog(size);
            
            if (size > 0) {

                var selected = m.getColumnsToHead(options, Object.keys(response.data[0]));
                var header = m.setColumn(response.type, response.info)[0], column = m.setColumn(response.type, response.info)[1];

                let detail = (response.type == 1) ? header : m.selectedItems(response.total);
                var titleText = detail == 'SIN MACOLLA' ? detail : `Macolla ${detail}`;

                m.setHtmlTable(1, "Survey Real", `${titleText}`);
                m.setHtmlTable(2);

                let headerSelected = `<th class='text-muted'>#</th>${column}<th class='text-muted'>Pozo</th><th class='text-muted'>Localización</th><th class='text-muted'>Yacimiento</th><th class='text-muted'>Fecha</th><th class='text-muted'>Compañía</th>`;

                for (var i=0; i<selected.length; i++) headerSelected += `<th>${this.setHeader(selected[i])}</th>`;

                document.querySelector(`#header`).innerHTML = headerSelected;

                var table = m.newDateTable(response.results, size, `Survey Real ${titleText}`);

                table.buttons().container().appendTo(`#buttons`);

                $(`.dts_label`).remove();

                document.querySelector('#imhere').focus();

            } else document.querySelector(`#table-content`).innerHTML = ``;
        
        }).catch(error => { console.log(error) });
        return false;
    }

    setHeader(value) {
        if (value === 'hole') return 'Hoyo';
        else if (value === 'gl') return 'GL <h6 class="text-muted">pies</h6>';
        else if (value === 'rt') return 'RT <h6 class="text-muted">pies</h6>';
        else if (value === 'md') return 'MD <h6 class="text-muted">pies</h6>';
        else if (value === 'inclination') return 'Inclinación <h6 class="text-muted">grados</h6>';
        else if (value === 'azim_grid') return 'Azim&nbsp;Grid <h6 class="text-muted">grados</h6>';
        else if (value === 'tvd') return 'TVD <h6 class="text-muted">pies</h6>';
        else if (value === 'dls') return 'DLS <h6 class="text-muted">(°/100&nbsp;pies)</h6>';
        else if (value === 'vsec') return 'vSEC <h6 class="text-muted">pies</h6>';
        else if (value === 'ns') return 'Norte/Sur <h6 class="text-muted">(N/S&nbsp;pies)</h6>';
        else if (value === 'ew') return 'Este/Oeste <h6 class="text-muted">(E/O&nbsp;pies)</h6>';
        else if (value === 'latitude') return 'Latitud <h6 class="text-muted">(N/S&nbsp;°&nbsp;\'&nbsp;")</h6>';
        else if (value === 'longitude') return 'Longitud <h6 class="text-muted">(E/O&nbsp;°&nbsp;\'&nbsp;")</h6>';
        else if (value === 'observations') return 'Observaciones';
    }
}

window.onload = () => {
    m = new Method();
    rsv = new RealSurvey();
}