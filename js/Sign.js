class Sign {
    constructor() {
      this.searchByFilter = this.searchByFilter.bind(this);
      this.setHeader = this.setHeader.bind(this);
    }

    searchByFilter() {
        
        var size = m.getColumnsFromDb().length, values = m.getColumnsFromDb(), preset = ['sign_date', 'well_name', 'company', 'sign_time', 'macolla_name', 'location_name', 'reservoir_name'];
        
        for (var i = 0; i <= 6; i++) values[size + i] = preset[i];
        
        var range, typeSearch, reservoirId = [], collection = [];
        
        range = $("#filter-date").val() == 1 ?  m.getDatesRange($(`#date-pick`).val()) : m.getDatesRange(`${$(`#from`).val()}|${$(`#until`).val()}`);
        
        var whatIDo = document.querySelector(`#dual-list`).style.display == 'none';

        var from = moment(range[0]), until = moment(range[1]), isFrom = m.validateDate(from, 'msgFrom'), isUntil = m.validateDate(until, 'msgUntil');
        
        if (!whatIDo) {
            typeSearch = 1;
            var sizeIs = ($(`#location_well`).val()).length, store = $(`#location_well`).val();
            if (sizeIs > 1) for (var i = 0; i < sizeIs; i++) collection[i] = store[i];
            else collection = store[0];
        } else {
            $('input[name="option[]"]:checked').each(function(i) { reservoirId[i] = $(this).val(); });
            typeSearch = 2;
            var sizeIs = ($(`#macolla_list`).val()).length, store = $(`#macolla_list`).val();
            if (sizeIs > 1) for (var i = 0; i < sizeIs; i++) collection[i] = store[i];
            else collection = store[0];
        }

        if ((from.isValid() && isFrom) && (until.isValid() && isUntil)) {

            const ajax = new Ajax(`${globalConfig.appUrl}/signs/searchByFilterSign`, {
                headers: {
                    accept: 'application/json',
                },
                method: 'POST',
                body: {
                    items: values,
                    data: collection,
                    macolla_id: parseInt($(`#macolla`).val()),
                    reservoir_id: reservoirId != null ? reservoirId : null,
                    date_from: range[0],
                    date_to: range[1],
                    type: typeSearch
                }
            });
        
            m.setLoadingMessageBefore();
                
            ajax.result().then(response => {
    
                var options = ['percent_mixture', 'percent_sediment', 'api_degree_diluted', 'ptb'];
                var size = Object.keys(response.data).length, sizeChart = Object.keys(response.chart).length, dataChart = [];
        
                m.setLoadingMessageAfter();
                m.showMessageDialog(size);
                
                if (size > 0) {

                    var selected = m.getColumnsToHead(options, Object.keys(response.data[0]));

                    console.log(selected);

                    var header = m.setColumn(response.type, response.info)[0], column = m.setColumn(response.type, response.info)[1];

                    let detail = (response.type == 1) ? header : m.selectedItems(response.total);
                    var titleText = detail == 'SIN MACOLLA' ? detail : `Macolla ${detail}`;

                    m.setHtmlTable(1, "Muestras", `${titleText}`);
                    m.setHtmlTable(2);

                    let headerSelected = `<th class='text-muted'>#</th><th class='text-muted'>Fecha&nbsp;muestreo</th>${column}<th class='text-muted'>Localización</th><th class='text-muted'>Pozo</th><th class='text-muted'>Yacimiento</th><th class='text-muted'>Compañía</th><th class='text-muted'>Hora&nbsp;muestreo</th>`;

                    for (var i=0; i<selected.length; i++) headerSelected += `<th>${this.setHeader(selected[i])}</th>`;

                    document.querySelector(`#header`).innerHTML = headerSelected;

                    var table = m.newDateTable(response.results, size, `Muestras ${titleText}`);

                    table.buttons().container().appendTo(`#buttons`);

                    $(`.dts_label`).remove();

                    document.querySelector('#imhere').focus();
                    
                    document.querySelector(`#showChart`).style.display = "block";

                    setChartSign(order(response.chart), `Gráfica de Muestras Macolla ${detail}`);

                } else document.querySelector(`#table-content`).innerHTML = ``;
                
            }).catch(error => { console.log(error) });
        }
        return false;
    }

    setHeader(value) {
        if (value === 'percent_mixture') return '%AyS&nbsp;Mezcla';
        else if (value === 'percent_sediment') return '%Sedimentos';
        else if (value === 'api_degree_diluted') return'ºAPI&nbsp;crudo&nbsp;diluido';
        else if(value === 'ptb') return 'PTB';
    }
}

window.onload = () => {
    m = new Method();
    sgn = new Sign();
}