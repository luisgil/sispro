class SubsoilCondition {
    constructor() {
        this.searchByFilter = this.searchByFilter.bind(this);
        this.setHeader = this.setHeader.bind(this);
    }

    searchByFilter() {
        
        var size = m.getColumnsFromDb().length, values = m.getColumnsFromDb(), preset = ['well_name', 'pt_date', 'company', 'macolla_name', 'location_name', 'reservoir'];

        for (var i = 0; i <= 5; i++) values[size + i] = preset[i];
        
        var typeSearch, reservoirId = [], collection = [], whatIDo = document.querySelector(`#dual-list`).style.display == 'none';
        
        /*By wells or by macollas ?*/
        if (!whatIDo) {
            typeSearch = 1;
            var sizeIs = ($(`#location_well`).val()).length, store = $(`#location_well`).val();
            if (sizeIs > 1) {
                for (var i = 0; i < sizeIs; i++) collection[i] = store[i];
            } else collection = store[0];
        } else {
            /*This is for show all macollas and reservoirs!*/
            $('input[name="option[]"]:checked').each(function(i) {
                reservoirId[i] = $(this).val();
            });
            typeSearch = 2;
            var sizeIs = ($(`#macolla_list`).val()).length, store = $(`#macolla_list`).val();
            if (sizeIs > 1) {
                for (var i = 0; i < sizeIs; i++) collection[i] = store[i];
            } else collection = store[0];
        }

        const ajax = new Ajax(`${globalConfig.appUrl}/pressureAndTemperature/searchByFilterPressAndTemp`, {
            headers: {
                accept: 'application/json',
            },
            method: 'POST',
            body: {
                items: values,
                data: collection,
                macolla_id: parseInt($(`#macolla`).val()),
                reservoir_id: reservoirId != null ? reservoirId : null,
                type: typeSearch
            }
        });

        m.setLoadingMessageBefore();
        
        ajax.result().then(response => {

            var options = ['sand', 'test', 'deep_md', 'deep_tvd', 'temperature', 'duration', 'pressbefore', 'pressafter', 'pressformation' , 'mobility', 'observations'];
            var size = Object.keys(response.data).length;

            m.setLoadingMessageAfter();
            m.showMessageDialog(size);
            
            if (size > 0) {

                var selected = m.getColumnsToHead(options, Object.keys(response.data[0]));
                var header = m.setColumn(response.type, response.info)[0], column = m.setColumn(response.type, response.info)[1];

                let detail = (response.type == 1) ? header : m.selectedItems(response.total);
                var titleText = detail == 'SIN MACOLLA' ? detail : `Macolla ${detail}`;

                m.setHtmlTable(1, "Presión y Temperatura Original", `${titleText}`);
                m.setHtmlTable(2);

                let headerSelected = `<th class="text-muted">#</th>${column}<th class='text-muted'>Pozo</th><th class='text-muted'>Localización</th><th class='text-muted'>Yacimiento</th><th class='text-muted'>Compañía</th><th class='text-muted'>Fecha</th>`;

                for (var i=0; i<selected.length; i++) headerSelected += `<th>${this.setHeader(selected[i])}</th>`;

                document.querySelector(`#header`).innerHTML = headerSelected;

                var table = m.newDateTable(response.results, size, `PyT Original ${titleText}`);

                table.buttons().container().appendTo(`#buttons`);

                $(`.dts_label`).remove();

                document.querySelector('#imhere').focus();

            } else document.querySelector(`#table-content`).innerHTML = ``;
        
      }).catch(error => { console.log(error) });
      return false;
    }

    setHeader(value) {
        if (value === 'test') return 'Test';
        else if (value === 'deep_md') return 'Profundidad&nbsp;MD <h6 class="text-muted">pies</h6>';
        else if (value === 'deep_tvd') return 'Profundidad&nbsp;TVD <h6 class="text-muted">pies</h6>';
        else if (value === 'temperature') return 'Temperatura <h6 class="text-muted">ºF</h6>';
        else if (value === 'duration') return 'Tiempo <h6 class="text-muted">minutos</h6>';
        else if (value === 'sand') return 'Arena';
        else if (value === 'pressbefore') return 'Presión&nbsp;Hid.&nbsp;Antes <h6 class="text-muted">lppc</h6>';
        else if (value === 'pressafter') return 'Presión&nbsp;Hid.&nbsp;Después <h6 class="text-muted">lppc</h6>';
        else if (value === 'pressformation') return 'Presión&nbsp;Formación <h6 class="text-muted">lppc</h6>';
        else if (value === 'mobility') return 'Movilidad <h6 class="text-muted">mD/cP</h6>';
        else if (value === 'observations') return 'Observaciones';
    }
}

window.onload = () => {
    m = new Method();
    scn = new SubsoilCondition();
}