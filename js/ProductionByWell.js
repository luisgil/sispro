class ProductionByWell {
    constructor() {
    }
    
    validateSearch() {
        if ($(`#macolla`).val() != '') this.searchByFilter();
        else alertify.alert('Importante', 'Debe seleccionar primero una macolla.', function(){}).set({'movable': false, 'moveBounded': false});
    }

    searchByFilter() {
        
        var range;
        
        range = $("#filter-date").val() == 1 ?  m.getDatesRange($(`#date-pick`).val()) : m.getDatesRange(`${$(`#from`).val()}|${$(`#until`).val()}`);
        
        const ajax = new Ajax(`${globalConfig.appUrl}/controlledByWell/searchByFilterControlledByWell`, {
            headers: { accept: 'application/json' },
            method: 'POST',
            body: {
                macolla_id: parseInt($(`#macolla`).val()),
                well_id: parseInt($(`#well`).val()),
                date_from: range[0],
                date_to: range[1],
            }
        });

        m.setLoadingMessageBefore();
        
        ajax.result().then(response => {

            var size = Object.keys(response.data).length;

            m.setLoadingMessageAfter();
            m.showMessageDialog(size);

            if (size > 0) {

                m.setHtmlTable(1, "Producción EHO Fiscalizada", `Localización ${response.nameOfLocation}`);
                m.setHtmlTable(2);

                let headerSelected = `<th>Fecha</th><th>Tasa ${response.nameOfLocation}&nbsp;<h6 class="text-muted">Barriles Netos</h6></th><th>${response.nameOfMacolla}&nbsp;<h6 class="text-muted">Producción Macolla</h6></th><th>Porcentaje</th>`;

                document.querySelector(`#header`).innerHTML = headerSelected;
                
                var table = m.newDateTable(response.data, size, `Producción en ${response.nameOfLocation}`);

                table.buttons().container().appendTo(`#buttons`); 

                $(`.dts_label`).remove();

                document.getElementById('imhere').focus();

            } else document.querySelector(`#table-content`).innerHTML = ``;
    
        }).catch(error => { console.log(error) });
        return false;
    }
}

window.onload = () => {
    m = new Method();
    pbw = new ProductionByWell();
}